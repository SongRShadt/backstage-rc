package com.shadt.core.dao;

import java.util.List;

import com.shadt.core.entity.User;

public interface UserDao extends BaseDao<User>{

	
	List<User> getByLevel(String sql);

	/**
	 * 根据sql获取所有用户
	 * @param sql
	 * @return
	 */
	List<User> getBySql(String sql);
	
	/**
	 * 根据sql添加用户
	 * @param sql
	 * @return
	 */
	String add(String sql);

	/**
	 * 修改用户信息
	 * @param sql
	 * @return
	 */
	String update(String sql);

	/**
	 * 删除用户信息
	 * @param sql
	 * @return
	 */
	String delete(String sql);
}
