package com.shadt.core.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.UserDao;
import com.shadt.core.entity.User;

@Repository
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao{

	@Override
	@SuppressWarnings("rawtypes")
	public List<User> getByLevel(String sql) {
		List list = this.getJdbcTemplate().queryForList(sql);
		List<User> uList = new ArrayList<User>();
		Iterator it = list.iterator();
		if(list.size()>0){
			while(it.hasNext()){
				Map m =(Map)it.next();
				User user=new User();
				user.setId(m.get("id")!=null?m.get("id").toString():"");
				user.setLevel(Integer.parseInt(m.get("level").toString())==1?Integer.parseInt(m.get("level").toString()):0);
				user.setName(m.get("name")!=null?m.get("name").toString():"");
				user.setPassword(m.get("password")!=null?m.get("password").toString():"");
				user.setPortrait(m.get("portrait")!=null?m.get("portrait").toString():"");
				user.setSex(m.get("sex")!=null?m.get("sex").toString():"");
				user.setUserName(m.get("userName")!=null?m.get("userName").toString():"");
				uList.add(user);
			}
		}
		return uList;
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	public List<User> getBySql(String sql){
		List list = this.getJdbcTemplate().queryForList(sql);
		List<User> uList = new ArrayList<User>();
		Iterator it = list.iterator();
		if(list.size()>0){
			while(it.hasNext()){
				Map m =(Map)it.next();
				User u = new User();
				u.setId(m.get("id")!=null?m.get("id").toString():"");
				u.setLevel(Integer.parseInt(m.get("level").toString())!=0?1:0);
				u.setName(m.get("name")!=null?m.get("name").toString():"");
				u.setPassword(m.get("password")!=null?m.get("password").toString():"");
				u.setPortrait(m.get("portrait")!=null?m.get("portrait").toString():"");
				u.setSex(m.get("sex")!=null?m.get("sex").toString():"");
				u.setUserName(m.get("userName")!=null?m.get("userName").toString():"");
				uList.add(u);
			}
		}
		return uList;
	}
	
	
	/**
	 * 实现修改用户信息
	 */
	public String add(String sql) {
		return this.getJdbcTemplate().update(sql)+"";
	}

	/**
	 * 实现修改用户信息
	 */
	@SuppressWarnings("deprecation")
	public String update(String sql) {
		return this.getJdbcTemplate().queryForInt(sql)+"";
	}

	/**
	 * 实现删除用户信息
	 */
	public String delete(String sql) {
		return this.getJdbcTemplate().update(sql)+"";
	}
}
