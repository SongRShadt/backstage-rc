package com.shadt.core.web;

import java.io.Serializable;

public class HttpResult implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4672591581796063224L;
	private Integer returnCode;
	private String returnMsg;
	private Object data;

	public Integer getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(Integer returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMsg() {
		return returnMsg;
	}

	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

 

}
