package com.shadt.core.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UrlUtil {

	/**
	 * 
	 * @param url
	 * @return
	 */
	public static String getIp(String url) {
		Pattern p = Pattern.compile("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}");
		Matcher m = p.matcher(url);
		if (m.find()) {
			return m.group();
		}
		return "";
	}
}
