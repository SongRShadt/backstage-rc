package com.shadt.core.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SpringUtil implements ApplicationContextAware {
	// 上下文
		public static ApplicationContext appContext;

		public void setApplicationContext(ApplicationContext context) throws BeansException {
			appContext = context;
		}


		public static Object getBean(String beanName) {
			return appContext.getBean(beanName);
		}
}
