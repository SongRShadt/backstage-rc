package com.shadt.core.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

public class TestIpUtil {
	public static boolean isAddressAvailable(String ip) {
		try {
			InetAddress address = InetAddress.getByName(ip);// ping this IP
			if (address.isReachable(5000)) {
				return true;
			} else {
				Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();
				while (netInterfaces.hasMoreElements()) {
					NetworkInterface ni = netInterfaces.nextElement();
					if (address.isReachable(ni, 0, 5000)) {
						return true;
					} else {
						return false;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
