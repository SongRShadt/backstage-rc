package com.shadt.core.service;

import java.util.List;

import com.shadt.core.entity.User;

public interface UserService {

	void addUser(User u);
	
	List<User> getByLevel(int level);

	User login(User u);
	
	/**
	 * 获取所有用户信息
	 * @return
	 */
	List<User> getAll();
	
	
	
	/**
	 * 添加用户信息
	 * @param camera
	 * @return
	 */
	String add(User user);

	/**
	 * 修改用户信息
	 * @param user
	 * @return
	 */
	String update(User user);

	/**
	 * 删除用户信息
	 * @param id
	 * @return
	 */
	String delete(String id);

	/**
	 * 按条件分页获取用户信息
	 * @param sSearch
	 * @param iDisplayStart
	 * @param iDisplayLength
	 * @return
	 */
	List<User> getAllBySearch(int iDisplayStart, int iDisplayLength);
	
	/**
	 * 按条件获取用户记录数
	 * @param sSearch
	 * @return
	 */
	String getCount();

}
