package com.shadt.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.backstage.dao.User_townD;
import com.shadt.core.dao.UserDao;
import com.shadt.core.entity.User;
import com.shadt.core.service.UserService;

@Service("userserviceimpl")
public class UserServiceImpl implements UserService {

	@Autowired
	UserDao dao;
	
	@Autowired
	User_townD udaoD;
	
	
	public void addUser(User u) {
		dao.saveOrUpdate(u);
	}
	
	public List<User> getByLevel(int level) {
		return dao.find("from User where level="+level+"");
	}

	public User login(User u) {
		return dao.get("from User where userName='"+u.getUserName()+"' and password='"+u.getPassword()+"'");
	}

	/**
	 * 根据sql获取所有用户信息
	 */
	public List<User> getAll() {
		List<User> uList =dao.getBySql("select * from user");	
		return uList;
	}
	
	
	
	public String add(User user){
		return null;
	}
	
	
	public String update(User user){
		return null;
	}
	
	
	
	public String delete(String id){
		return null;
	}
	
	
	/**
	 * 分页获取用户信息
	 */
	public List<User> getAllBySearch(int iDisplayStart, int iDisplayLength){
		String sql="select * from user where 1=1";
		List<User> uList = new ArrayList<User>();
		sql+= " limit "+iDisplayStart +","+iDisplayLength;
		try{
			uList = dao.getBySql(sql);
		}catch(Exception e){
			e.printStackTrace();
		}
		return uList;
	}
	
	/**
	 * 获取记录数
	 */
	public String getCount(){
		String sql="select count(*) from user where 1=1";
		String count="";
		try{
			count=dao.update(sql);
		}catch (Exception e) {
			System.err.println(new Exception("获取用户信息总记录出错！"));
			e.printStackTrace();
		}
		return count;
	}
}
