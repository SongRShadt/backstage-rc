package com.shadt.core.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.core.entity.User;
import com.shadt.core.model.Json;
import com.shadt.core.service.UserService;
import com.shadt.core.util.DateUtil;

@Controller
@RequestMapping("/user")
public class UserController extends BaseController {

	@Autowired
	UserService service;

	@ResponseBody
	@RequestMapping("/login")
	public Json login(HttpServletRequest request, HttpServletResponse response,
			HttpSession session,User u) {
		Json j = new Json();
		u = service.login(u);
		if(u!=null){
			j.setMsg("登陆成功！");
			j.setSuccess(true);
			j.setObj(u);
			session.setAttribute("user", u);
		}else{
			j.setMsg("登陆失败！");
			j.setSuccess(false);
		}
		return j;
	}
	
	@ResponseBody
	@RequestMapping("/logout")
	public Json logout(HttpSession session){
		Json j = new Json();
		session.removeAttribute("user");
		System.out.println("注销成功！");
		j.setMsg("注销成功！");
		j.setSuccess(true);
		return j;
	}
	
	@RequestMapping("/addUser")
	public Json add(User user){
		Json j = new Json();
		try{
			this.service.addUser(user);
			j.setMsg("成功添加！");
			j.setSuccess(true);
		}catch (Exception e){
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss") + ":保存用户信息出错！在\"UserControllerC.add()\"中"));
			j.setMsg("保存用户信息失败！");
			e.printStackTrace();
		}
		j.setObj(null);
		return j;
	}
}
