package com.shadt.core.listener;

import java.util.UUID;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.shadt.core.entity.User;
import com.shadt.core.service.UserService;
import com.shadt.core.util.SpringUtil;

public class UserListener implements ServletContextListener{

	public void contextInitialized(ServletContextEvent sce) {
		System.out.println("*************初始化管理员信息*************");
		User u = new User();
		u.setId(UUID.randomUUID().toString());
		u.setLevel(0);
		u.setName("超级管理员");
		u.setSex("男");
		u.setUserName("admin");
		u.setPassword("admin");
		UserService service = (UserService) SpringUtil.getBean("userserviceimpl");
		if(service.getByLevel(0).size()<=0){
			service.addUser(u);
			System.out.println("超级管理员初始化成功！");
			System.out.println("用户名：admin");
			System.out.println("密    码：admin");
		}else{
			System.out.println("已存在超级管理员");
		}
		System.out.println("**************************************");
	}

	public void contextDestroyed(ServletContextEvent sce) {
		System.out.println("destroyed");
	}

}
