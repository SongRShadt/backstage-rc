package com.shadt.backstage.vo;

import java.io.Serializable;


public class CameraImg implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6468745946084840477L;
	private String id;
	private String cid;
	private String creattime;
	private String url;
	private String sort;
	private String cameraVoName;
	
	
	
	public String getCameraVoName() {
		return cameraVoName;
	}
	public void setCameraVoName(String cameraVoName) {
		this.cameraVoName = cameraVoName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getCreattime() {
		return creattime;
	}
	public void setCreattime(String creattime) {
		this.creattime = creattime;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	
}
