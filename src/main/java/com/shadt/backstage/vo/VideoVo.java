package com.shadt.backstage.vo;

import java.io.Serializable;

/**
 * 
 *视频信息模型 
 * 
 *@author hebin
 *
 */

public class VideoVo implements Serializable {

	private static final long serialVersionUID = 4897173223664187311L;
		
	private String id;//视频编号
	private String startTime;//视频开始时间
	private String endTime;//视频结束时间
	private String cameraNo;//摄像头编号
	private String taskId;//任务编号
	private String downloadUrl;//视频下载地址
	private String playUrl;//视频播放地址
	private String duration;//视频时长
	
	private CameraVo cameraVo;//对应的摄像头信息

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public String getCameraNo() {
		return cameraNo;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public void setCameraNo(String cameraNo) {
		this.cameraNo = cameraNo;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getPlayUrl() {
		return playUrl;
	}

	public void setPlayUrl(String playUrl) {
		this.playUrl = playUrl;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public CameraVo getCameraVo() {
		return cameraVo;
	}

	public void setCameraVo(CameraVo cameraVo) {
		this.cameraVo = cameraVo;
	}
	
	
}
