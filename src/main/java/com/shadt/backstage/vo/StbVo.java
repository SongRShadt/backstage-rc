package com.shadt.backstage.vo;

import java.io.Serializable;

/**
 * 机顶盒信息模型
 * @author liusongren
 *
 */
@SuppressWarnings("serial")
public class StbVo implements Serializable{
	private String stbNo;//机顶盒编号
	private String remark;//机顶盒描述
	private String villageId;//村编号
	private VillageVo village;//机顶盒对应的村信息
	public String getStbNo() {
		return stbNo;
	}
	public void setStbNo(String stbNo) {
		this.stbNo = stbNo;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getVillageId() {
		return villageId;
	}
	public void setVillageId(String villageId) {
		this.villageId = villageId;
	}
	public VillageVo getVillage() {
		return village;
	}
	public void setVillage(VillageVo village) {
		this.village = village;
	}
	
}
