package com.shadt.backstage.vo;

import java.io.Serializable;
import java.util.List;

/**
 * 村信息模型
 * 
 * @author liusongren
 *
 */
@SuppressWarnings("serial")
public class VillageVo implements Serializable {
	private String villageId;// 村编号
	private String villageName;// 村名称
	private String villageKeyValue;// 村key
	private String orgCode;// 村组织编号
	private String townId;// 对应镇编号
	private String stbNo;//对应机顶盒编号

	private TownVo town;// 对应镇
	private StbVo stb;//对应的机顶盒
	private List<CameraVo> cameras;//对应的摄像头列表

	private String bindInfo;//绑定情况
	private Long villageSort;
	private Integer villageLevel;
	
	public String getVillageId() {
		return villageId;
	}

	public void setVillageId(String villageId) {
		this.villageId = villageId;
	}

	public String getVillageName() {
		return villageName;
	}

	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}

	public String getVillageKeyValue() {
		return villageKeyValue;
	}

	public void setVillageKeyValue(String villageKeyValue) {
		this.villageKeyValue = villageKeyValue;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getTownId() {
		return townId;
	}

	public void setTownId(String townId) {
		this.townId = townId;
	}

	public TownVo getTown() {
		return town;
	}

	public void setTown(TownVo town) {
		this.town = town;
	}

	public List<CameraVo> getCameras() {
		return cameras;
	}

	public void setCameras(List<CameraVo> cameras) {
		this.cameras = cameras;
	}

	public StbVo getStb() {
		return stb;
	}

	public void setStb(StbVo stb) {
		this.stb = stb;
	}

	public String getStbNo() {
		return stbNo;
	}

	public void setStbNo(String stbNo) {
		this.stbNo = stbNo;
	}

	public String getBindInfo() {
		return bindInfo;
	}

	public void setBindInfo(String bindInfo) {
		this.bindInfo = bindInfo;
	}

	public Long getVillageSort() {
		return villageSort;
	}

	public void setVillageSort(Long villageSort) {
		this.villageSort = villageSort;
	}

	public Integer getVillageLevel() {
		return villageLevel;
	}

	public void setVillageLevel(Integer villageLevel) {
		this.villageLevel = villageLevel;
	}
}
