package com.shadt.backstage.vo;

import java.io.Serializable;
import java.util.List;

/**
 * 机顶盒摄像头关系模型
 * @author liusongren
 *
 */
@SuppressWarnings("serial")
public class StbCameraVo implements Serializable{
	private String stbNo;//机顶盒编号
	private String cameraNo;//摄像头编号
	private String rltType;//摄像头类型
	private CameraVo camera;//摄像头
	private StbVo stb;//机顶盒
	private List<StbVo> stbs;//对应的机顶盒列表
	private List<CameraVo> cameras;//对应的摄像头列表
	
	public StbCameraVo(){
	}
	public StbCameraVo(String stbNo,String cameraNo,String rltType){
		this.cameraNo=cameraNo;
		this.stbNo=stbNo;
		this.rltType=rltType;
	}
	public String getStbNo() {
		return stbNo;
	}
	public void setStbNo(String stbNo) {
		this.stbNo = stbNo;
	}
	public String getCameraNo() {
		return cameraNo;
	}
	public void setCameraNo(String cameraNo) {
		this.cameraNo = cameraNo;
	}
	public String getRltType() {
		return rltType;
	}
	public void setRltType(String rltType) {
		this.rltType = rltType;
	}
	public List<StbVo> getStbs() {
		return stbs;
	}
	public void setStbs(List<StbVo> stbs) {
		this.stbs = stbs;
	}
	public List<CameraVo> getCameras() {
		return cameras;
	}
	public void setCameras(List<CameraVo> cameras) {  
		this.cameras = cameras;
	}
	public CameraVo getCamera() {
		return camera;
	}
	public void setCamera(CameraVo camera) {
		this.camera = camera;
	}
	public StbVo getStb() {
		return stb;
	}
	public void setStb(StbVo stb) {
		this.stb = stb;
	}
	
	
}
