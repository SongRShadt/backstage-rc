package com.shadt.backstage.vo;

/**
 * 视频会议vo
 * @author SongR
 *
 */
public class MettingVideoVo {
	private String id;//视频会议编号
	private String name;//视频会议名称
	private String sort;//排序
	private String villageId;//村编号
	private String villageName;//村名称
	private String rtsp;
	private String videourl;
	private String status;//状态（-1、删除 0、可用 1、暂停 ）
	private String sewiseid;
	private String sourceid;
	

	public String getSourceid() {
		return sourceid;
	}

	public void setSourceid(String sourceid) {
		this.sourceid = sourceid;
	}

	public String getStatus() {
		return status;
	}

	public String getSewiseid() {
		return sewiseid;
	}

	public void setSewiseid(String sewiseid) {
		this.sewiseid = sewiseid;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRtsp() {
		return rtsp;
	}

	public void setRtsp(String rtsp) {
		this.rtsp = rtsp;
	}

	public String getVideourl() {
		return videourl;
	}

	public void setVideourl(String videourl) {
		this.videourl = videourl;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getVillageId() {
		return villageId;
	}

	public void setVillageId(String villageId) {
		this.villageId = villageId;
	}

	public String getVillageName() {
		return villageName;
	}

	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}
}
