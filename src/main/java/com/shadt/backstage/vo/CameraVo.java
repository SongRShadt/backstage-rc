package com.shadt.backstage.vo;

import java.io.Serializable;

/**
 * 摄像头信息模型
 * @author liusongren
 *
 */
@SuppressWarnings("serial")
public class CameraVo implements Serializable{
	private String cameraNo;//摄像头编号
	private String cameraName;//摄像头名称
	private String cameraUrl;//摄像头路径
	private String cameraType;//摄像头类型
	private String imgUrl;//图片路径
	private String streamId;//流编号
	private String villageId;//对应村编号
	
	private String s_ipurl;//视频录制url
	
	private VillageVo village;//对应的村信息
	
	
	private String ipId;//服务器ip的ID
	
	

	public String getIpId() {
		return ipId;
	}

	public void setIpId(String ipId) {
		this.ipId = ipId;
	}

	public String getCameraNo() {
		return cameraNo;
	}

	public void setCameraNo(String cameraNo) {
		this.cameraNo = cameraNo;
	}

	public String getCameraName() {
		return cameraName;
	}

	public void setCameraName(String cameraName) {
		this.cameraName = cameraName;
	}

	public String getCameraUrl() {
		return cameraUrl;
	}

	public void setCameraUrl(String cameraUrl) {
		this.cameraUrl = cameraUrl;
	}

	public String getCameraType() {
		return cameraType;
	}

	public void setCameraType(String cameraType) {
		this.cameraType = cameraType;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getStreamId() {
		return streamId;
	}

	public void setStreamId(String streamId) {
		this.streamId = streamId;
	}

	public String getVillageId() {
		return villageId;
	}

	public void setVillageId(String villageId) {
		this.villageId = villageId;
	}

	public VillageVo getVillage() {
		return village;
	}

	public void setVillage(VillageVo village) {
		this.village = village;
	}

	public String getS_ipurl() {
		return s_ipurl;
	}

	public void setS_ipurl(String s_ipurl) {
		this.s_ipurl = s_ipurl;
	}
	
}
