package com.shadt.backstage.vo;

public class VideoMettingManageVo {

	private Long id;
	private String videoMettingName;
	private String url;
	private String areaName;
	private String sort;
	private String ipId;
	private String villageId;
	private String villageName;
	
	
	
	public String getVillageName() {
		return villageName;
	}
	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}
	public String getVillageId() {
		return villageId;
	}
	public void setVillageId(String villageId) {
		this.villageId = villageId;
	}
	public String getIpId() {
		return ipId;
	}
	public void setIpId(String ipId) {
		this.ipId = ipId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getVideoMettingName() {
		return videoMettingName;
	}
	public void setVideoMettingName(String videoMettingName) {
		this.videoMettingName = videoMettingName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	
	
}
