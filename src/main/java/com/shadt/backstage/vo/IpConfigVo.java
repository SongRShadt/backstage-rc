package com.shadt.backstage.vo;

import java.io.Serializable;

public class IpConfigVo implements Serializable{

	
	private static final long serialVersionUID = 3497460904630825712L;	
	
	private int id;
	private String ipUrl;
	private String name;
	private String accessid;
	private String cameraUrl;
	public String getCameraUrl() {
		return cameraUrl;
	}
	public void setCameraUrl(String cameraUrl) {
		this.cameraUrl = cameraUrl;
	}
	public String getAccessid() {
		return accessid;
	}
	public void setAccessid(String accessid) {
		this.accessid = accessid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIpUrl() {
		return ipUrl;
	}
	public void setIpUrl(String ipUrl) {
		this.ipUrl = ipUrl;
	}
}
