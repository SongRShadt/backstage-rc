package com.shadt.backstage.vo;

import java.io.Serializable;
import java.util.List;

/**
 * 镇信息模型
 * 
 * @author liusongren
 *
 */
@SuppressWarnings("serial")
public class TownVo implements Serializable {
	private String townId;// 镇编号
	private String townName;// 镇名称
	private String townKeyValue;// 镇key
	private Long townSort;//排序
	
	private List<VillageVo> villages;// 对应的村列表

	public String getTownId() {
		return townId;
	}

	public void setTownId(String townId) {
		this.townId = townId;
	}

	public String getTownName() {
		return townName;
	}

	public void setTownName(String townName) {
		this.townName = townName;
	}

	public String getTownKeyValue() {
		return townKeyValue;
	}

	public void setTownKeyValue(String townKeyValue) {
		this.townKeyValue = townKeyValue;
	}

	public List<VillageVo> getVillages() {
		return villages;
	}

	public void setVillages(List<VillageVo> villages) {
		this.villages = villages;
	}

	public Long getTownSort() {
		return townSort;
	}

	public void setTownSort(Long townSort) {
		this.townSort = townSort;
	}

}
