package com.shadt.backstage.vo;

import java.io.Serializable;

/**
 * 用户所属镇中间vo
 * @author lingbin
 *
 */
@SuppressWarnings("serial")
public class User_townVo implements Serializable{
	
	private String id;

	private String userId;
	
	private String townId;
	
	





	
	public String getId() {
		return id;
	}


	public void setId(String Id) {
		this.id = Id;
	}

	public String getUserID() {
		return userId;
	}
	
	public void setUserID(String userID) {
		this.userId = userID;
	}

	public String getTownID() {
		return townId;
	}

	public void setTownID(String townID) {
		this.townId = townID;
	}


	@Override
	public String toString() {
		return "User_townVo [userID=" + userId + ", townID="
				+ townId + "]";
	}
	
}
