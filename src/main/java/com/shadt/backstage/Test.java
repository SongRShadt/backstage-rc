package com.shadt.backstage;

import org.apache.commons.lang3.CharSet;

public class Test {

	public static void main(String[] args) {
		System.out.println("**CharSetDemo**");
		CharSet charSet = CharSet.getInstance("aeiou");
		System.out.println(charSet);
		String demoStr = "The quick brown fox jumps over the lazy dog.";
		int count = 0;
		for (int i = 0, len = demoStr.length(); i < len; i++) {
			if (charSet.contains(demoStr.charAt(i))) {
				System.out.println(demoStr.charAt(i));
				count++;
			}
		}
		System.out.println("count: " + count);
	}

}
