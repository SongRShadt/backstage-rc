package com.shadt.backstage.dao;

import java.util.List;

import com.shadt.backstage.vo.TownVo;
import com.shadt.core.dao.BaseDao;

/**
 * 镇信息数据访问层接口
 * @author liusongren
 *
 */
public interface TownD extends BaseDao<TownVo>{

	/**
	 * 根据sql获取镇信息
	 * @param sql
	 * @return
	 */
	List<TownVo> getBySql(String sql);

	/**
	 * 修改镇信息
	 * @param sql
	 * @return
	 */
	int update(String sql);

	/**
	 * 验证值是否存在
	 * 
	 * @param value
	 * @return
	 */
	int checkValue(String sql);

	/**
	 * 按sql获取记录数
	 * @param sql
	 * @return
	 */
	String getCount(String sql);

}
