package com.shadt.backstage.dao;

import java.util.List;

import com.shadt.backstage.vo.IpConfigVo;
import com.shadt.core.dao.BaseDao;

/**
 * ip地址信息数据访问层接口
 * 
 * @author hebin
 *
 */

public interface ipConfigD extends BaseDao<IpConfigVo> {
	 List<IpConfigVo> getBySql(String sql);
}
