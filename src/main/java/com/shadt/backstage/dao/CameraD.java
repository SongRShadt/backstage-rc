package com.shadt.backstage.dao;

import java.util.List;

import com.shadt.backstage.vo.CameraVo;
import com.shadt.core.dao.BaseDao;

/**
 * 摄像头信息数据访问层接口
 * 
 * @author liusongren
 *
 */
public interface CameraD extends BaseDao<CameraVo> {

	/**
	 * 根据sql获取摄像头信息
	 * @param sql
	 * @return
	 */
	List<CameraVo> getBySql(String sql);

	/**
	 * 添加摄像头信息
	 * @param sql
	 * @return
	 */
	String add(String sql);

	/**
	 * 修改摄像头信息
	 * @param sql
	 * @return
	 */
	String update(String sql);

	/**
	 * 删除摄像头信息
	 * @param sql
	 * @return
	 */
	String delete(String sql);


}
