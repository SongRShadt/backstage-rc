package com.shadt.backstage.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.backstage.dao.User_townD;
import com.shadt.backstage.vo.User_townVo;
import com.shadt.core.dao.impl.BaseDaoImpl;

@Repository
public class User_townDI extends BaseDaoImpl<User_townVo> implements User_townD {

	@SuppressWarnings("rawtypes")
	public User_townVo get(String sql){
		List  list = this.getJdbcTemplate().queryForList(sql);
		List<User_townVo> uList = new ArrayList<User_townVo>();
		Iterator it = list.iterator();
		if(list.size()>0){
			while(it.hasNext()){
				Map m=(Map)it.next();
				User_townVo uTownVo=new User_townVo();
				uTownVo.setId(m.get("id")!=null?m.get("id").toString():"");
				uTownVo.setUserID(m.get("userid")!=null?m.get("userid").toString():"");
				uTownVo.setTownID(m.get("townid")!=null?m.get("townid").toString():"");
				uList.add(uTownVo);
			}
			return (User_townVo)uList.get(0);
		}
		return null;
	}	
	
	public void add(User_townVo uTownVo){
		this.saveOrUpdate(uTownVo);
	}
	
	/**
	 * 
	 */
	public String update(String sql){
		return this.getJdbcTemplate().update(sql)+"";
	}
	
}
