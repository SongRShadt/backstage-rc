package com.shadt.backstage.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.backstage.dao.CameraImgD;
import com.shadt.backstage.vo.CameraImg;
import com.shadt.core.dao.impl.BaseDaoImpl;
@Repository
public class CameraImgDI extends BaseDaoImpl<CameraImg> implements CameraImgD {

	@Override
	public List<CameraImg> getBySql(String sql) {
		List list = this.getJdbcTemplate().queryForList(sql);
		List<CameraImg> cList = new ArrayList<CameraImg>();
		Iterator it = list.iterator();
		if(list.size()>0){
			while (it.hasNext()) {
				Map m = (Map) it.next();
				CameraImg c = new CameraImg();
				c.setId(m.get("id")!=null?m.get("id").toString():"");
				c.setCid(m.get("camera_id")!=null?m.get("camera_id").toString():"");
				c.setCreattime(m.get("creattime")!=null?m.get("creattime").toString():"");
				c.setUrl(m.get("image_url")!=null?m.get("image_url").toString():"");
				c.setSort(m.get("ci_sort")!=null?m.get("ci_sort").toString():"");
				
				cList.add(c);
			}
		}
		return cList;
	}

	@Override
	public String add(String sql) {
		return null;
	}

	

	@Override
	public String delete(String sql) {
		return this.getJdbcTemplate().update(sql)+"";
	}

	@Override
	public String update(String sql) {
		return this.getJdbcTemplate().queryForInt(sql)+"";
	}

	

}
