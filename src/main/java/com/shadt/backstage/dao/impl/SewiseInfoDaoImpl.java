package com.shadt.backstage.dao.impl;

import org.springframework.stereotype.Repository;

import com.shadt.backstage.dao.SewiseInfoDao;
import com.shadt.backstage.entity.SewiseInfo;
import com.shadt.core.dao.impl.BaseDaoImpl;

@Repository
public class SewiseInfoDaoImpl extends BaseDaoImpl<SewiseInfo> implements SewiseInfoDao{

}
