package com.shadt.backstage.dao.impl;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.backstage.dao.ipConfigD;
import com.shadt.backstage.vo.IpConfigVo;
import com.shadt.core.dao.impl.BaseDaoImpl;

/**
 * ip地址数据访问层实现类
 * @author hebin
 *
 */
@Repository
public class IpConfigDI extends BaseDaoImpl<IpConfigVo> implements ipConfigD {
	
	@SuppressWarnings("rawtypes")
	public List<IpConfigVo> getBySql(String sql){
		List list = this.getJdbcTemplate().queryForList(sql);
		List<IpConfigVo> cList = new ArrayList<IpConfigVo>();
		Iterator it = list.iterator();
		if(list.size()>0){
			while (it.hasNext()) {
				Map m = (Map) it.next();
				IpConfigVo ip = new IpConfigVo();
				ip.setId((m.get("id")!=null?Integer.parseInt(m.get("id").toString()):0));
				ip.setIpUrl(m.get("ipUrl")!=null?m.get("ipUrl").toString():"");
				ip.setName(m.get("name")!=null?m.get("name").toString():"");
				ip.setAccessid(m.get("accessid")!=null?m.get("accessid").toString():"");
				ip.setCameraUrl(m.get("cameraUrl")!=null?m.get("cameraUrl").toString():"");
				cList.add(ip);
			}
		}
		return cList;
	}
}
