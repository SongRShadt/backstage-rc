package com.shadt.backstage.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.backstage.dao.StbCameraD;
import com.shadt.backstage.vo.StbCameraVo;
import com.shadt.core.dao.impl.BaseDaoImpl;

/**
 * 摄像头与机顶盒数据访问层实现类
 * @author liusongren
 *
 */
@Repository
public class StbCameraDI extends BaseDaoImpl<StbCameraVo> implements StbCameraD{

	@SuppressWarnings("rawtypes")
	@Override
	public List<StbCameraVo> getBySql(String sql) {
		List list = this.getJdbcTemplate().queryForList(sql);
		List<StbCameraVo> cList = new ArrayList<StbCameraVo>();
		Iterator it = list.iterator();
		if(list.size()>0){
			while (it.hasNext()) {
				Map m = (Map) it.next();
				StbCameraVo c = new StbCameraVo();
				c.setCameraNo(m.get("camerano")!=null?m.get("camerano").toString():"");
				c.setStbNo(m.get("stbno")!=null?m.get("stbno").toString():"");
				c.setRltType(m.get("rlt_type")!=null?m.get("rlt_type").toString():"");
				cList.add(c);
			}
		}
		return cList;
	}

	@Override
	public String add(String sql) {
		return this.getJdbcTemplate().update(sql)+"";
	}

	@Override
	public String delete(String sql) {
		return this.getJdbcTemplate().update(sql)+"";
	}

}
