package com.shadt.backstage.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.backstage.dao.StbD;
import com.shadt.backstage.vo.StbVo;
import com.shadt.core.dao.impl.BaseDaoImpl;

/**
 * 机顶盒信息数据访问层实现类
 * @author liusongren
 *
 */
@Repository
public class StbDI extends BaseDaoImpl<StbVo> implements StbD{

	/**
	 * 根据sql获取机顶盒信息
	 */
	@SuppressWarnings("rawtypes")
	public List<StbVo> getBySql(String sql) {
		List list = this.getJdbcTemplate().queryForList(sql);
		List<StbVo> sList = new ArrayList<StbVo>();
		Iterator it = list.iterator();
		if(list.size()>0){
			while (it.hasNext()) {
				Map m = (Map) it.next();
				StbVo s = new StbVo();
				s.setStbNo(m.get("stbno")!=null?m.get("stbno").toString():"");
				s.setVillageId(m.get("villageid")!=null?m.get("villageid").toString():"");
				s.setRemark(m.get("remark")!=null?m.get("remark").toString():"");
				sList.add(s);
			}
		}
		return sList;
	}

	/**
	 * 添加机顶盒信息
	 */
	@SuppressWarnings("deprecation")
	public String add(String sql) {
		return this.getJdbcTemplate().queryForInt(sql)+"";
	}

	/**
	 * 删除机顶盒信息
	 */
	@SuppressWarnings("deprecation")
	public String delete(String sql) {
		return this.getJdbcTemplate().queryForInt(sql)+"";
	}

	/**
	 * 修改机顶盒信息
	 */
	public String update(String sql) {
		return this.getJdbcTemplate().update(sql)+"";
	}
	
	/**
	 * 实现按sql查询记录数
	 */
	@SuppressWarnings("deprecation")
	public String getCount(String sql) {
		return this.getJdbcTemplate().queryForInt(sql)+"";
	}

}
