package com.shadt.backstage.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.backstage.dao.VillageD;
import com.shadt.backstage.vo.VillageVo;
import com.shadt.core.dao.impl.BaseDaoImpl;

/**
 * 村信息数据访问村接口
 * @author liusongren
 *
 */
@Repository
public class VillageDI extends BaseDaoImpl<VillageVo> implements VillageD{

	
	/**
	 * 根据sql获取村信息
	 */
	@SuppressWarnings("rawtypes")
	public List<VillageVo> getBySql(String sql) {
		List list = this.getJdbcTemplate().queryForList(sql);
		List<VillageVo> cList = new ArrayList<VillageVo>();
		Iterator it = list.iterator();
		if(list.size()>0){
			while (it.hasNext()) {
				Map m = (Map) it.next();
				VillageVo c = new VillageVo();
				c.setVillageId(m.get("villageid")!=null?m.get("villageid").toString():"");
				c.setVillageName(m.get("villagename")!=null?m.get("villagename").toString():"");
				c.setOrgCode(m.get("orgcode")!=null?m.get("orgcode").toString():"");
				c.setVillageKeyValue(m.get("villageKeyValue")!=null?m.get("villageKeyValue").toString():"");
				c.setTownId(m.get("townid")!=null?m.get("townid").toString():"");
				c.setVillageSort(m.get("VillageSort")!=null?new Long(m.get("VillageSort").toString()):null);
				c.setVillageLevel(m.get("villageLevel")!=null?Integer.parseInt(m.get("villageLevel").toString()):null);
				cList.add(c);
			}
		}
		return cList;
	}

	/**
	 * 实现按sql查询记录数
	 */
	@SuppressWarnings("deprecation")
	public String getCount(String sql) {
		return this.getJdbcTemplate().queryForInt(sql)+"";
	}
	/**
	 * 实现按sql修改
	 */
	public String update(String sql) {
		return this.getJdbcTemplate().update(sql)+"";
	}

}
