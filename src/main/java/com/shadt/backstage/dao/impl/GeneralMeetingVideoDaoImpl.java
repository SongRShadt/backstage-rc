package com.shadt.backstage.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.backstage.dao.GeneralMeetingVideoDao;
import com.shadt.backstage.vo.GeneralMeetingVideoVO;
import com.shadt.core.dao.impl.BaseDaoImpl;

@Repository
public class GeneralMeetingVideoDaoImpl extends BaseDaoImpl<GeneralMeetingVideoVO> implements GeneralMeetingVideoDao {

	@Override
	public List<GeneralMeetingVideoVO> findAllBySql(String sql) {
		List<Map<String, Object>> list = this.getJdbcTemplate().queryForList(sql);
		List<GeneralMeetingVideoVO> voList = new ArrayList<GeneralMeetingVideoVO>();
		if (list.size() > 0) {
			for (Map<String, Object> result : list) {
				GeneralMeetingVideoVO vo = new GeneralMeetingVideoVO();
				vo.setMeetingId(result.get("meeting_id") != null ? result.get("meeting_id").toString() : "");
				vo.setMeetingName(result.get("meeting_name") != null ? result.get("meeting_name").toString() : "");
				vo.setMeetingCameraType(result.get("camera_type") != null ? result.get("camera_type").toString() : "");
				vo.setOrgcode(result.get("orgcode") != null ? result.get("orgcode").toString() : "");
				vo.setMeetingDate(result.get("starttime").toString().replace(".0", ""));
				vo.setMeetingEndDate(result.get("endtime").toString().replace(".0", ""));
				vo.setMeetingTime(result.get("duration").toString());
				vo.setMeetingPlayUrl(result.get("play_url") != null ? result.get("play_url").toString() : "");
				vo.setMeetingDownloadUrl(result.get("download_url") != null ? result.get("download_url").toString() : "");
				voList.add(vo);
			}
		}
		return voList;
	}

	@Override
	public String rowCount(String sql) {
		try {
			Integer count = (Integer) this.getJdbcTemplate().queryForObject(sql, java.lang.Integer.class);
			return count + "";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public String delete(String sql) {
		int status = 0;
		try {
			status = this.getJdbcTemplate().update(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status + "";
	}

}
