package com.shadt.backstage.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.backstage.dao.MettingVideoDao;
import com.shadt.backstage.vo.MettingVideoVo;
import com.shadt.core.dao.impl.BaseDaoImpl;

/**
 * 视频会议数据访问层实现类
 * @author SongR
 *
 */
@Repository
public class MettingVideoDaoImpl extends BaseDaoImpl<MettingVideoVo> implements MettingVideoDao{

	/**
	 * 按sql查询
	 */
	@SuppressWarnings("rawtypes")
	public List<MettingVideoVo> getBySql(String sql) throws Exception {
		List list = this.getJdbcTemplate().queryForList(sql);
		List<MettingVideoVo> mList = new ArrayList<MettingVideoVo>();
		Iterator it = list.iterator();
		if(list.size()>0){
			while (it.hasNext()) {
				Map m = (Map) it.next();
				MettingVideoVo mv = new MettingVideoVo();
				mv.setId(m.get("id")!=null?m.get("id").toString():"");
				mv.setName(m.get("video_name")!=null?m.get("video_name").toString():"");
				mv.setSort(m.get("video_sort")!=null?m.get("video_sort").toString():"");
				mv.setVillageId(m.get("village_id")!=null?m.get("village_id").toString():"");
				mv.setVillageName(m.get("villagename")!=null?m.get("villagename").toString():"");
				mv.setRtsp(m.get("rtsp")!=null?m.get("rtsp").toString():"");
				mv.setVideourl(m.get("video_url")!=null?m.get("video_url").toString():"");
				mv.setStatus(m.get("video_status")!=null?m.get("video_status").toString():"");
				mv.setSewiseid(m.get("sewiseid")!=null?m.get("sewiseid").toString():"");
				mv.setSourceid(m.get("sourceid")!=null?m.get("sourceid").toString():"");
				mList.add(mv);
			}
		}
		return mList;
	}

	/**
	 * 实现获取总数
	 */
	@SuppressWarnings("deprecation")
	public int countSql(String sql) throws Exception {
		return this.getJdbcTemplate().queryForInt(sql);
	}

	/**
	 * 修改或删除
	 */
	public int update(String sql) throws Exception {
		return this.getJdbcTemplate().update(sql);
	}
}
