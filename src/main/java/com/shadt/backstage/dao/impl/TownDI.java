package com.shadt.backstage.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.backstage.dao.TownD;
import com.shadt.backstage.vo.TownVo;
import com.shadt.core.dao.impl.BaseDaoImpl;

/**
 * 镇信息数据访问层实现类
 * 
 * @author liusongren
 *
 */
@Repository
public class TownDI extends BaseDaoImpl<TownVo> implements TownD {
	/**
	 * 实现根据sql获取镇信息
	 */
	@SuppressWarnings("rawtypes")
	public List<TownVo> getBySql(String sql) {
		List list = this.getJdbcTemplate().queryForList(sql);
		List<TownVo> tList = new ArrayList<TownVo>();
		Iterator it = list.iterator();
		if(list.size()>0){
			while (it.hasNext()) {
				Map m = (Map) it.next();
				TownVo t = new TownVo();
				t.setTownId(m.get("townid").toString());
				t.setTownName(m.get("townname").toString());
				t.setTownKeyValue(m.get("townkeyvalue")!=null?m.get("townkeyvalue").toString():"");
				t.setTownSort(m.get("townsort")!=null?new Long(m.get("townsort").toString()):0);
				tList.add(t);
			}
		}
		return tList;
	}

	/**
	 * 实现修改镇信息
	 */
	public int update(String sql) {
		return this.getJdbcTemplate().update(sql);
	}

	/**
	 * 实现验证值是否存在
	 */
	@SuppressWarnings("deprecation")
	public int checkValue(String sql) {
		return this.getJdbcTemplate().queryForInt(sql);
	}

	/**
	 * 实现按sql获取记录数
	 */
	@SuppressWarnings("deprecation")
	public String getCount(String sql) {
		return this.getJdbcTemplate().queryForInt(sql)+"";
	}
}
