package com.shadt.backstage.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import org.springframework.stereotype.Repository;

import com.shadt.backstage.dao.VideoD;
import com.shadt.backstage.vo.VideoVo;
import com.shadt.core.dao.impl.BaseDaoImpl;

/**
 * 
 * 视频信息借口实现类
 * 
 * @author hebin
 *
 */
@Repository
public class VideoDI extends BaseDaoImpl<VideoVo> implements VideoD{

	@SuppressWarnings("rawtypes")
	public List<VideoVo> findAllBySql(String sql) {
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-M-dd HH:mm:ss"); 
		List list = this.getJdbcTemplate().queryForList(sql);
		List<VideoVo> tList = new ArrayList<VideoVo>();
		Iterator it = list.iterator();
		try {
			if(list.size()>0){
				while (it.hasNext()) {
					Map m = (Map) it.next();
					VideoVo v = new VideoVo();
					v.setId(m.get("id")!=null?m.get("id").toString():"");
					v.setCameraNo(m.get("cameraNo")!=null?m.get("cameraNo").toString():"");
					v.setStartTime(fmt.format(m.get("starttime")));
					v.setEndTime(fmt.format(m.get("endtime")));
					v.setDownloadUrl(m.get("download_url")!=null?m.get("download_url").toString():"");
					v.setPlayUrl(m.get("play_url")!=null?m.get("play_url").toString():"");
					v.setTaskId(m.get("taskId")!=null?m.get("taskId").toString():"");
					v.setDuration(m.get("duration")!=null?m.get("duration").toString():"");
					tList.add(v);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tList;
	}

	@SuppressWarnings("deprecation")
	public String rowCount(String sql) {
		String count = "";
		try{
		count = this.getJdbcTemplate().queryForInt(sql)+"";
		}catch(Exception e){
			e.printStackTrace();
		}
		return count;
	}
	
	public int delete(String sql){
		int status=0;
		try{
			status=this.getJdbcTemplate().update(sql);
		}catch(Exception e){
			e.printStackTrace();
		}
		return status;
	}
	
	
}
