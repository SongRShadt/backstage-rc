package com.shadt.backstage.dao.impl;

import java.util.List;
import org.springframework.stereotype.Repository;

import com.shadt.backstage.dao.VideoMettingManageDao;
import com.shadt.backstage.vo.VideoMettingManageVo;
import com.shadt.core.dao.impl.BaseDaoImpl;

@Repository
public class VideoMettingManageDaoImpl extends BaseDaoImpl<VideoMettingManageVo> implements VideoMettingManageDao {

	@Override
	public List getBySql(String sql) {
		List list=this.getJdbcTemplate().queryForList(sql);
		return list;
	}

	@Override
	public String getCount(String sql) {
		return this.getJdbcTemplate().queryForInt(sql)+"";
	}

	public String add(String sql) {
		return this.getJdbcTemplate().update(sql)+"";
	}

	@Override
	public String delete(String sql) {
			return this.getJdbcTemplate().update(sql)+"";
		}
}
