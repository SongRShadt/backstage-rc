package com.shadt.backstage.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.backstage.dao.UserD;
import com.shadt.backstage.vo.UserVo;
import com.shadt.core.dao.impl.BaseDaoImpl;

/**
 * 用户信息数据访问村接口实现
 * @author Lingbin
 *
 */

@Repository
public class UserDI extends BaseDaoImpl<UserVo> implements UserD{

	@Override
	@SuppressWarnings("rawtypes")
	public List<UserVo> getBySql(String sql) {
		List list = this.getJdbcTemplate().queryForList(sql);
		List<UserVo> uList = new ArrayList<UserVo>();
		Iterator it =list.iterator();
		if(list.size()>0){
			while(it.hasNext()){
				Map m =(Map)it.next();
				UserVo u = new UserVo();
				u.setId(m.get("id")!=null?m.get("id").toString():"");
				u.setLevel(m.get("level")!=null?(int)m.get("level"):0);
				u.setName(m.get("name")!=null?m.get("name").toString():"");
				u.setPassword(m.get("password").toString());
				u.setPortrait("");
				u.setSex(m.get("sex")!=null?m.get("sex").toString():"");
				u.setUserName(m.get("userName")!=null?m.get("userName").toString():"");
				u.setTownName("");
				uList.add(u);
			}
		}
		return uList;
	}

	@SuppressWarnings("deprecation")
	@Override
	public String getCount(String sql) {
		
		return this.getJdbcTemplate().queryForInt(sql)+"";
	}

	@Override
	public String update(String sql) {
		return this.getJdbcTemplate().update(sql)+"";
	}

}
