package com.shadt.backstage.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.backstage.dao.CameraD;
import com.shadt.backstage.vo.CameraVo;
import com.shadt.core.dao.impl.BaseDaoImpl;
/**
 * 摄像头信息数据访问层实现类
 * @author liusongren
 *
 */
@Repository
public class CameraDI extends BaseDaoImpl<CameraVo> implements CameraD{

	/**
	 * 实现根据sql获取摄像头信息
	 */
	@SuppressWarnings("rawtypes")
	public List<CameraVo> getBySql(String sql) {
		List list = this.getJdbcTemplate().queryForList(sql);
		List<CameraVo> cList = new ArrayList<CameraVo>();
		Iterator it = list.iterator();
		if(list.size()>0){
			while (it.hasNext()) {
				Map m = (Map) it.next();
				CameraVo c = new CameraVo();
				c.setCameraNo(m.get("camerano")!=null?m.get("camerano").toString():"");
				c.setCameraName(m.get("cameraname")!=null?m.get("cameraname").toString():"");
				c.setCameraUrl(m.get("cameraurl")!=null?m.get("cameraurl").toString():"");
				c.setCameraType(m.get("cameratype")!=null?m.get("cameratype").toString():"");
				c.setImgUrl(m.get("imgurl")!=null?m.get("imgurl").toString():"");
				c.setStreamId(m.get("streamid")!=null?m.get("streamid").toString():"");
				c.setVillageId(m.get("villageid")!=null?m.get("villageid").toString():"");
				cList.add(c);
			}
		}
		return cList;
	}

	/**
	 * 实现添加摄像头信息
	 */
	public String add(String sql) {
		return this.getJdbcTemplate().update(sql)+"";
	}

	/**
	 * 实现修改摄像头信息
	 */
	@SuppressWarnings("deprecation")
	public String update(String sql) {
		return this.getJdbcTemplate().queryForInt(sql)+"";
	}

	/**
	 * 实现删除摄像头信息
	 */
	public String delete(String sql) {
		return this.getJdbcTemplate().update(sql)+"";
	}

}
