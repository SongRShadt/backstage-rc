package com.shadt.backstage.dao;

import com.shadt.backstage.entity.SewiseInfo;
import com.shadt.core.dao.BaseDao;

public interface SewiseInfoDao extends BaseDao<SewiseInfo>{

}
