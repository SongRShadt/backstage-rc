package com.shadt.backstage.dao;

import java.util.List;

import com.shadt.backstage.vo.GeneralMeetingVideoVO;
import com.shadt.core.dao.BaseDao;

public interface GeneralMeetingVideoDao extends BaseDao<GeneralMeetingVideoVO>{

	/**
	 * 根据sql获取信息
	 * @return
	 */
	List<GeneralMeetingVideoVO> findAllBySql(String sql);

	String rowCount(String sql);

	String delete(String sql);

}
