package com.shadt.backstage.dao;

import java.util.List;

import com.shadt.backstage.vo.StbVo;
import com.shadt.core.dao.BaseDao;

/**
 * 机顶盒数据访问层接口
 * @author liusongren
 *
 */
public interface StbD extends BaseDao<StbVo>{

	List<StbVo> getBySql(String sql);

	String add(String sql);

	String delete(String sql);

	String update(String sql);
	/**
	 * 实现按sql查询记录数
	 */
	String getCount(String sql);
}
