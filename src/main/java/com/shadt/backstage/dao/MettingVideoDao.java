package com.shadt.backstage.dao;

import java.util.List;

import com.shadt.backstage.vo.MettingVideoVo;
import com.shadt.core.dao.BaseDao;

/**
 * 视频会议数据访问层接口
 * @author SongR
 *
 */
public interface MettingVideoDao extends BaseDao<MettingVideoVo>{

	/**
	 * 根据sql查询
	 */
	List<MettingVideoVo> getBySql(String sql) throws Exception;

	/**
	 * 获取总数
	 */
	int countSql(String sql) throws Exception;
	
	/**
	 * 修改或删除
	 */
	int update(String sql) throws Exception;
}
