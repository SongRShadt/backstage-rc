package com.shadt.backstage.dao;

import java.util.List;

import com.shadt.backstage.vo.VideoVo;
import com.shadt.core.dao.BaseDao;

/**
 * 视频信息数据访问层接口
 * @author hb
 *
 */
public interface VideoD extends BaseDao<VideoVo>{
	/**
	 * 
	 * 根据sql获取信息
	 * 
	 * @return
	 */
	List<VideoVo> findAllBySql(String Sql);
	
	String rowCount(String sql);
	
	int delete(String sql);
}
