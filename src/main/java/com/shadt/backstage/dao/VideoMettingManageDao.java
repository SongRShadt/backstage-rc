package com.shadt.backstage.dao;

import java.util.List;

import com.shadt.backstage.vo.VideoMettingManageVo;
import com.shadt.core.dao.BaseDao;

public interface VideoMettingManageDao extends BaseDao<VideoMettingManageVo> {

	List getBySql(String sql);
	String getCount(String sql);
	
	public String add(String sql);
	
	public String delete(String sql);
}
