package com.shadt.backstage.dao;

import java.util.List;

import com.shadt.backstage.vo.UserVo;
import com.shadt.core.dao.BaseDao;

/**
 * 用户信息数据访问层
 * @author Lingbin
 *
 */
public interface UserD extends BaseDao<UserVo>{

	/**
	 * 按sql查询用户信息
	 * @param sql
	 * @return
	 */
	List<UserVo> getBySql(String sql);

	/**
	 * 按sql查询记录数
	 * @param sql
	 * @return
	 */
	String getCount(String sql);

	/**
	 * 根据sql修改用户信息
	 * @param vSql
	 * @return
	 */
	String update(String sql);
	
}
