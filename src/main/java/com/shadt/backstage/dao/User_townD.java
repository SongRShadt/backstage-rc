package com.shadt.backstage.dao;

import com.shadt.backstage.vo.User_townVo;
import com.shadt.core.dao.BaseDao;

/**
 * 用户-镇中间表数据访问层
 * @author lingbin
 */
public interface User_townD extends BaseDao<User_townVo>{

	User_townVo get(String sql);
	
	void add(User_townVo uTownVo);
	
	String update(String sql);

}
