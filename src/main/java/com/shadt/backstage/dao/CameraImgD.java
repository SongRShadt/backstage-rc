package com.shadt.backstage.dao;

import java.util.List;

import com.shadt.backstage.vo.CameraImg;
import com.shadt.core.dao.BaseDao;

public interface CameraImgD extends BaseDao<CameraImg> {

	/**
	 * 根据sql获取摄像头信息
	 * @param sql
	 * @return
	 */
	List<CameraImg> getBySql(String sql);

	/**
	 * 添加摄像头信息
	 * @param sql
	 * @return
	 */
	String add(String sql);



	/**
	 * 删除摄像头信息
	 * @param sql
	 * @return
	 */
	String delete(String sql);

	String update(String sql);
}
