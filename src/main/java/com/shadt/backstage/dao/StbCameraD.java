package com.shadt.backstage.dao;

import java.util.List;

import com.shadt.backstage.vo.StbCameraVo;
import com.shadt.core.dao.BaseDao;

/**
 * 摄像头与机顶盒关系数据访问层接口
 * @author liusongren
 *
 */
public interface StbCameraD extends BaseDao<StbCameraVo>{

	List<StbCameraVo> getBySql(String sql);

	String add(String sql);

	String delete(String sql);

}
