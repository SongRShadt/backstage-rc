package com.shadt.backstage.dao;

import java.util.List;

import com.shadt.backstage.vo.VillageVo;
import com.shadt.core.dao.BaseDao;

/**
 * 村信息数据访问层
 * @author liusongren
 *
 */
public interface VillageD extends BaseDao<VillageVo> {

	/**
	 * 按sql查询村信息
	 * @param sql
	 * @return
	 */
	List<VillageVo> getBySql(String sql);

	/**
	 * 按sql查询记录数
	 * @param sql
	 * @return
	 */
	String getCount(String sql);

	/**
	 * 根据sql修改村信息
	 * @param vSql
	 * @return
	 */
	String update(String sql);
	

}
