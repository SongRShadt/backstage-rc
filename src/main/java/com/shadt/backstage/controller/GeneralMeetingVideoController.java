package com.shadt.backstage.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shadt.backstage.service.GeneralMeetingVideoService;
import com.shadt.backstage.vo.GeneralMeetingVideoVO;
import com.shadt.core.entity.User;
import com.shadt.core.model.Json;
import com.shadt.core.util.DateUtil;

/**
 * 换届会议 
 */
@Controller
@RequestMapping("/generalMeetingVideoC")
public class GeneralMeetingVideoController {

	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	GeneralMeetingVideoService service;

	@ResponseBody
	@RequestMapping(value = "/getAll")
	public String getAll(String aoData,String time,HttpSession session) {
		User user=(User)session.getAttribute("user");
		if(user.getUserName().equals("admin")){
			List<GeneralMeetingVideoVO> lst = null;
			JSONObject obj = new JSONObject();
			try{
				aoData = aoData.replaceAll("&quot;", "\"");
				String sEcho = null, term = "";
				int iDisplayStart = 0;
				int iDisplayLength = 0;
				JSONArray jarray = JSONArray.parseArray(aoData);
				for (int i = 0; i < jarray.size(); i++) {
					JSONObject o = jarray.getJSONObject(i);
					if (o.get("name").equals("sEcho")) {
						sEcho = o.getString("value");
					}
					if (o.get("name").equals("sSearch")) {
						term = o.getString("value");
					}
					if (o.get("name").equals("iDisplayStart")) {
						iDisplayStart = o.getInteger("value");
					}
					if (o.get("name").equals("iDisplayLength")) {
						iDisplayLength = o.getInteger("value");
					}
				}
				lst = service.findAllBySql(time,term, iDisplayStart,iDisplayLength);
				obj.put("sEcho", sEcho);  //搜索的关键字
				obj.put("iTotalRecords", lst.size());// 实际的行数
				obj.put("iTotalDisplayRecords", service.rowCount(term,time));// 显示的行数,这个要和上面写的一样
				obj.put("aaData", lst);// 要以JSON格式返回
			}catch(Exception e){
				log.info(DateUtil.dataToStr(new Date(),"yyyy-MM-dd HH:mm:ss")+ ":获取视频信息列表出错！在\"generalMettingVideoC.getAll()\"中"+e.getMessage());
			}
			return JSONArray.toJSONString(obj);
		}else{
			return null;
		}
		
	}

	@ResponseBody
	@RequestMapping(value = "/deleteRow",method=RequestMethod.POST)
	public Json delete(Integer id) {
		Json j = new Json();
		String count = "";
		try {
			count = service.delete(id);
			j.setMsg("成功删除" + count + "条视频信息！");
			j.setSuccess(true);
		} catch (Exception e) {
			log.info(DateUtil.dataToStr(new Date(),"yyyy-MM-dd HH:mm:ss") + ":删除视频信息出错！在\"generalMettingVideoC.delete()\"中"+e.getMessage());
			j.setMsg("删除视频信息失败！");
			e.printStackTrace();
		}
		j.setObj(null);
		return j;
	}
}
