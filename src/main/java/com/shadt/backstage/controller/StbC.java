package com.shadt.backstage.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.backstage.service.StbS;
import com.shadt.backstage.vo.StbVo;
import com.shadt.core.controller.BaseController;
import com.shadt.core.model.Json;
import com.shadt.core.util.DateUtil;

/**
 * 机顶盒控制器
 * @author liusongren
 *
 */
@Controller
@RequestMapping("/stbC")
public class StbC extends BaseController{
	
	@Autowired
	StbS service;
	
	/**
	 * 获取所有机顶盒信息
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getAll")
	public Json getAll(){
		Json j = new Json();
		List<StbVo> stbList = new ArrayList<StbVo>();
		try {
			stbList = service.getAll();
			j.setMsg("获取所有机顶盒信息成功");
			j.setObj(stbList);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss")
					+ ":获取所有机顶盒信息失败！在\"StbC.getAll()\"中"));
			j.setMsg("获取所有机顶盒信息失败！");
			j.setObj(null);
			e.printStackTrace();
		}
		return j;
	}
	
	/**
	 * 根据机顶盒编号获取机顶盒信息
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getById")
	public Json getById(String id){
		Json j = new Json();
		StbVo stb = new StbVo();
		try {
			stb = service.getById(id);
			j.setMsg("根据机顶盒编号获取机顶盒信息成功！");
			j.setObj(stb);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss")
					+ ":根据机顶盒编号获取机顶盒信息失败！在\"StbC.getById()\"中"));
			j.setMsg("根据机顶盒编号获取机顶盒信息失败！");
			j.setObj(null);
		}
		return j;
	}
	
	/**
	 * 根据村编号获取机顶盒信息
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getByVillageId")
	public Json getByVillageId(String id){
		Json j = new Json();
		StbVo stb = new StbVo();
		try {
			stb = service.getByVillageId(id);
			j.setMsg("根据村编号获取机顶盒信息成功！");
			j.setObj(stb);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss")
					+ ":根据村编号获取机顶盒信息失败！在\"StbC.getByVillageId()\"中"));
			j.setMsg("根据村编号获取机顶盒信息失败！");
			j.setObj(null);
		}
		return j;
	}
	
	/**
	 * 添加机顶盒信息
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/add")
	public Json add(StbVo stb){
		Json j = new Json();
		String count = "";
		try {
			count = service.add(stb);
			j.setMsg("成功添加"+count+"条机顶盒！");
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss")
					+ ":添加机顶盒信息失败！在\"StbC.add()\"中"));
			j.setMsg("添加机顶盒信息失败！");
			j.setObj(null);
		}
		return j;
	}
	
	/**
	 * 删除机顶盒信息
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public Json delete(String id){
		Json j = new Json();
		String count = "";
		try {
			count = service.delete(id);
			j.setMsg("成功删除"+count+"条机顶盒信息！");
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss")
					+ ":删除机顶盒信息失败！在\"StbC.add()\"中"));
			j.setMsg("删除机顶盒信息失败！");
			j.setObj(null);
		}
		return j;
	}
	
	/**
	 * 批量删除机顶盒信息
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/batchDelete")
	public Json batchDelete(String []ids){
		Json j = new Json();
		for (String s : ids) {
			j=this.delete(s);
		}
		return j;
	}
	
	/**
	 * 修改机顶盒信息
	 * @param stb
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/update")
	public Json update(StbVo stb){
		Json j = new Json();
		String count = "";
		try {
			count = service.update(stb);
			j.setMsg("成功修改"+count+"条机顶盒信息！");
			j.setObj(stb);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss")
					+ ":修改机顶盒信息失败！在\"StbC.add()\"中"));
			j.setMsg("修改机顶盒信息失败！");
			j.setObj(null);
		}
		return j;
	}
	
	/**
	 * 验证值是否存在
	 * @param value
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/checkValue")
	public boolean checkValue(String value){
		boolean t = false;
		try {
			t = service.checkValue(value.trim());
			System.out.println(t);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return t;
	}
}
