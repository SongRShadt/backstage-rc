package com.shadt.backstage.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shadt.backstage.service.CameraS;
import com.shadt.backstage.service.User_townS;
import com.shadt.backstage.vo.CameraVo;
import com.shadt.core.controller.BaseController;
import com.shadt.core.entity.User;
import com.shadt.core.model.Json;
import com.shadt.core.util.DateUtil;

/**
 * 摄像头控制器
 * 
 * @author liusongren
 *
 */
@Controller
@RequestMapping("/cameraC")
public class CameraC extends BaseController {
	Logger log = Logger.getLogger(this.getClass());

	@Autowired 
	CameraS service;
	
	@Autowired
	
	User_townS uservice;
	
	
	/**
	 * 分页获取摄像头信息显示在页面jquery.datatables插件中
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getByPaging")
	public String getByPaging(HttpServletRequest req, String aoData,HttpSession session) {
		User user=(User)session.getAttribute("user");
		aoData = aoData.replaceAll("&quot;", "\"");
		String sEcho=null,sSearch = "";
		int iDisplayStart = 0;// 起始索引
		int iDisplayLength = 0;// 每页显示行数
		JSONArray jarray = JSONArray.parseArray(aoData);
		for (int i = 0; i < jarray.size(); i++) {
			JSONObject o = jarray.getJSONObject(i);
			if (o.get("name").equals("sEcho")) {
				sEcho = o.getString("value");
			}
			if(o.get("name").equals("sSearch")){
				sSearch = o.getString("value");
			}
			if (o.get("name").equals("iDisplayStart")) {
				iDisplayStart = o.getInteger("value");
			}
			if (o.get("name").equals("iDisplayLength")) {
				iDisplayLength = o.getInteger("value");
			}
		}
		List<CameraVo> lst = service.getAllBySearch(sSearch, iDisplayStart, iDisplayLength,user);
		JSONObject obj = new JSONObject();
		obj.put("sEcho", sEcho);
		obj.put("iTotalRecords",lst.size());// 实际的行数
		obj.put("iTotalDisplayRecords",service.getCount(sSearch,user));// 显示的行数,这个要和上面写的一样
		obj.put("aaData", lst);// 要以JSON格式返回
		return JSONArray.toJSONString(obj);
	}
	

	/**
	 * 获取所有摄像头信息
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getAll")
	public Json getAll() {
		Json j = new Json();
		List<CameraVo> cameraList = new ArrayList<CameraVo>();
//		User user=(User)session.getAttribute("user");
		try {
//			if(user.getLevel()==0){
//				cameraList = service.getAll();
//			}else{
//				cameraList = service.getAll(user);
//			}
			cameraList = service.getAll();
			j.setMsg("成功获取摄像头信息！");
			j.setObj(cameraList);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(
					DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss") + ":获取摄像头信息列表出错！在\"CameraC.getAll()\"中"));
			j.setMsg("获取摄像头信息失败！");
			j.setObj(null);
			e.printStackTrace();
		}
		return j;
	}

	/**
	 * 根据摄像头编号获取摄像头信息
	 * 
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getById")
	public Json getById(String id) {
		Json j = new Json();
		CameraVo camera = new CameraVo();
		try {
			camera = service.getById(id);
			j.setMsg("成功获取摄像头信息！");
			j.setObj(camera);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(
					DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss") + ":获取摄像信息列表出错！在\"cameraC.getById()\"中"));
			j.setMsg("获取摄像头信息失败！");
			j.setObj(null);
			e.printStackTrace();
		}
		return j;
	}

	/**
	 * 根据村编号获取摄像头列表
	 * 
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getByVillageId")
	public Json getByVillageId(String id) {
		Json j = new Json();
		CameraVo camera = new CameraVo();
		try {
			camera = service.getByVillageId(id);
			j.setMsg("根据村编号获取摄像头信息成功！");
			j.setObj(camera);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss")
					+ ":获取摄像头信息列表出错！在\"cameraC.getByVillageId()\"中"));
			j.setMsg("根据村编号获取摄像头失败！");
			j.setObj(null);
			e.printStackTrace();
		}
		return j;
	}

	/**
	 * 添加摄像头信息
	 * 
	 * @param village
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/add")
	public Json add(CameraVo camera) {
		Json j = new Json();
		String count = "";
		try {
			count = service.add(camera);
			j.setMsg("成功保存" + count + "条摄像头信息！");
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss") + ":保存摄像头信息出错！在\"cameraC.add()\"中"));
			j.setMsg("保存摄像头信息失败！");
			e.printStackTrace();
		}
		j.setObj(null);
		return j;
	}

	/**
	 * 修改摄像头信息
	 * 
	 * @param camera
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/update")
	public Json update(CameraVo camera) {
		Json j = new Json();
		String count = "";
		try {
			count = service.update(camera);
			j.setMsg("成功修改"+count+"条摄像头信息！");
			j.setObj(camera);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(
					DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss") + ":修改摄像头信息出错！在\"cameraC.update()\"中"));
			j.setMsg("修改摄像头信息失败！");
			j.setObj(null);
			e.printStackTrace();
		}
		return j;
	}

	/**
	 * 删除摄像头信息
	 * 
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public Json delete(String id) {
		Json j = new Json();
		String count = "";
		try {
			count = service.delete(id);
			j.setMsg("成功删除"+count+"条摄像头信息！");
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(
					DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss") + ":删除摄像头信息出错！在\"cameraC.delete()\"中"));
			j.setMsg("删除摄像头信息失败！");
			e.printStackTrace();
		}
		j.setObj(null);
		return j;
	}

	/**
	 * 批量删除摄像头信息
	 * 
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/batchDelete")
	public Json batchDelete(String[] ids) {
		Json j = new Json();
		for (String s : ids) {
			j = this.delete(s);
		}
		return j;
	}

}
