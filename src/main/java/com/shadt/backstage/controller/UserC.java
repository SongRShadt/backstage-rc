package com.shadt.backstage.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shadt.backstage.service.UserS;
import com.shadt.backstage.vo.UserVo;
import com.shadt.core.entity.User;
import com.shadt.core.model.Json;
import com.shadt.core.util.DateUtil;


/**
 * 用户信息控制器
 * 
 * 
 */
@Controller
@RequestMapping("/userC")
public class UserC {

	@Autowired
	UserS service;
	
	/**
	 * 获取所有用户信息
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getAll")
	public String getAll(String aoData){
		JSONObject obj = new JSONObject();
		try {
			aoData = aoData.replaceAll("&quot;", "\"");
			String sEcho = null, sSearch = "";
			int iDisplayStart = 0;// 起始索引
			int iDisplayLength = 0;// 每页显示行数
			JSONArray jarray = JSONArray.parseArray(aoData);
			for (int i = 0; i < jarray.size(); i++) {
				JSONObject o = jarray.getJSONObject(i);
				if (o.get("name").equals("sEcho")) {
					sEcho = o.getString("value");
				}
				if (o.get("name").equals("sSearch")) {
					sSearch = o.getString("value");
				}
				if (o.get("name").equals("iDisplayStart")) {
					iDisplayStart = o.getInteger("value");
				}
				if (o.get("name").equals("iDisplayLength")) {
					iDisplayLength = o.getInteger("value");
				}
			}
			List<UserVo> lst = service.getAll(sSearch, iDisplayStart, iDisplayLength);
			obj.put("sEcho", sEcho);
			obj.put("iTotalRecords", lst.size());// 实际的行数
			obj.put("iTotalDisplayRecords", service.getCount(sSearch));// 显示的行数,这个要和上面写的一样
			obj.put("aaData", lst);// 要以JSON格式返回
		}catch(Exception e){
			System.err.println(new Exception(DateUtil.dataToStr(new Date(),"yyyy-MM-dd HH:mm:ss")+ ":获取用户信息列表出错！在\"userC.getAll()\"中"));
			e.printStackTrace();
		}
		return JSONArray.toJSONString(obj);
	}
	
	
	/**
	 * 添加用户
	 */
	@ResponseBody
	@RequestMapping("/add")
	public Json add(UserVo userVo,String townId){
		Json j = new Json();
		String count = "";
		try {
			count = service.add(userVo,townId);
			j.setMsg("成功保存" + count + "条用户信息！");
			j.setObj(userVo);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(),
					"yyyy-MM-dd HH:mm:ss") + ":保存用户信息出错！在\"userC.add()\"中"));
			e.printStackTrace();
			j.setObj(null);
			j.setMsg("保存用户信息失败！");
		}
		return j;
	}
	
	@ResponseBody
	@RequestMapping("/delete")
	public Json delete(String id){
		Json j = new Json();
		String count = "";
		try {
			count = service.delete(id);
			j.setMsg("成功删除" + count + "条用户信息！");
			j.setSuccess(true);
		} catch (Exception e) {
			System.err
					.println(new Exception(DateUtil.dataToStr(new Date(),
							"yyyy-MM-dd HH:mm:ss")
							+ ":删除用户信息出错！在\"userC.delete()\"中"));
			j.setMsg("删除用户信息出错！");
		}
		j.setObj(null);
		return j;
	}
	
	/**
	 * 修改用户信息
	 * @param village
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/update")
	public Json update(UserVo userVo,String townId) {
		Json j = new Json();
		String count = "";
		try {
			count = service.update(userVo,townId);
			j.setMsg("修改用户信息成功！");
			j.setObj(count);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(),
							"yyyy-MM-dd HH:mm:ss")
							+ ":修改用户信息出错！在\"userC.update()\"中"));
			e.printStackTrace();
			j.setMsg("修改用户信息出错！");
			j.setObj(null);
		}
		return j;
	}
	
	@ResponseBody
	@RequestMapping("/updatePwd")
	public Json updPwd(HttpSession session,String newPwd){
		Json j = new Json();
		try {
			User user=(User)session.getAttribute("user");
			String count=service.updPwd(user, newPwd);
			if(Integer.parseInt(count)>0){
				j.setMsg("修改成功！");
				j.setSuccess(true);
			}
			else{
				j.setMsg("修改失败！");
				j.setSuccess(false);
			}
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(),
					"yyyy-MM-dd HH:mm:ss") + ":修改密码出错！在\"userC.updPwd()\"中"));
			e.printStackTrace();
			j.setMsg("修改密码异常！");
			j.setSuccess(false);
		}
		return j;
	}

	/**
	 * 验证值是否存在
	 * @param value
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/checkValue")
	public boolean checkValue(String value){
		boolean t = false;
		try {
			t = service.checkValue(value.trim());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return t;
	}
}
