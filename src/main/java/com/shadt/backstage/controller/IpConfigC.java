package com.shadt.backstage.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.backstage.service.IpConfigS;
import com.shadt.backstage.vo.IpConfigVo;
import com.shadt.core.model.Json;
import com.shadt.core.util.DateUtil;

/**
 * ip地址信息控制器
 * 
 * @author hebin
 *
 */
@Controller
@RequestMapping("/ipConfigC")
public class IpConfigC {
	
	@Resource
	IpConfigS service;
	
	@ResponseBody
	@RequestMapping(value="/getAll")
	public Json getAll(){
		Json j = new Json();
		List<IpConfigVo> ipList = new ArrayList<IpConfigVo>();
		try {
			ipList = service.getBySql("select * from ipconfig");
			j.setMsg("成功获取ip地址信息！");
			j.setObj(ipList);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss") + ":获取摄像头信息列表出错！在\"IpConfigC.getAll()\"中"));
			j.setMsg("获取ip地址失败！");
			j.setObj(null);
			e.printStackTrace();
		}
		return j;
	}
	
	
}
