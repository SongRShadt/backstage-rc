package com.shadt.backstage.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shadt.backstage.service.MettingVideoService;
import com.shadt.backstage.vo.MettingVideoVo;
import com.shadt.core.controller.BaseController;
import com.shadt.core.model.Json;

/**
 * 视频会议控制器
 * @author SongR
 *
 */
@Controller
@RequestMapping(value="/mettingvideo")
public class MettingVideoController extends BaseController{
	
	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	MettingVideoService service;
	/**
	 * 获取已绑定的视频会议列表
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/get/bind/{villageId}",method={RequestMethod.POST})
	public String getBind(HttpServletRequest request,HttpSession session,String aoData,@PathVariable String villageId){
		String result = "";
		if(!aoData.equals("")){
			aoData = aoData.replaceAll("&quot;", "\"");
			String sEcho=null,sSearch = "";
			int iDisplayStart = 0;// 起始索引
			int iDisplayLength = 0;// 每页显示行数
			JSONArray jarray = JSONArray.parseArray(aoData);
			for (int i = 0; i < jarray.size(); i++) {
				JSONObject o = jarray.getJSONObject(i);
				if (o.get("name").equals("draw")) {
					sEcho = o.getString("value");
				}
				if (o.get("name").equals("start")) {
					iDisplayStart = o.getInteger("value");
				}
				if (o.get("name").equals("length")) {
					iDisplayLength = o.getInteger("value");
				}
			}
			try {
				List<MettingVideoVo> vos = service.getByVillage(sSearch, iDisplayStart, iDisplayLength,villageId);
				JSONObject obj = new JSONObject();
				obj.put("sEcho", sEcho);
				obj.put("iTotalRecords",vos.size());// 实际的行数
				obj.put("iTotalDisplayRecords",service.countByVillage(sSearch,villageId));// 显示的行数,这个要和上面写的一样
				obj.put("aaData", vos);// 要以JSON格式返回
				result = JSONArray.toJSONString(obj);
			} catch (Exception e) {
				log.error("接口异常！",e);
//				e.printStackTrace();
			}
		}
		return result;
	}
	
	/**
	 * 删除视频会议绑定
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/delete/{videoId}/{villageId}",method={RequestMethod.POST})
	public Json delete(@PathVariable String videoId,@PathVariable String villageId){
		Json j = new Json();
		try {
			int s = service.delete(videoId,villageId);
			if(s>0){
				j.setMsg("成功删除！");
				j.setSuccess(true);
			}else{
				j.setMsg("删除失败！");
			}
		} catch (Exception e) {
			j.setMsg("接口异常！");
			log.error("接口异常！",e);
//			e.printStackTrace();
		}
		return j;
	}
	
	
	
	/**
	 * 获取所有未绑定的视频会议
	 */
	@ResponseBody
	@RequestMapping(value="/all/{villageId}",method=RequestMethod.POST)
	public Json getAll(HttpServletRequest request,HttpSession session,@PathVariable String villageId){
		Json j = new Json();
		try {
			List<MettingVideoVo> vos = service.getAll(villageId);
			j.setSuccess(true);
			if(vos.size()>0){
				j.setMsg("成功获取未绑定视频会议！");
				j.setObj(vos);
			}else{
				j.setMsg("已绑定所有视频会议！");
			}
		} catch (Exception e) {
			j.setMsg("接口异常！");
			log.error("接口异常！",e);
//			e.printStackTrace();
		}
		return j;
	}
	
	
	/**
	 * 添加视频会议绑定
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/add/{villageId}/{videoId}")
	public Json add(@PathVariable String villageId,@PathVariable String videoId){
		Json j = new Json();
		try {
			int count = service.add(villageId,videoId);
			if(count>0){
				j.setMsg("添加成功！");
				j.setSuccess(true);
			}else{
				j.setMsg("添加失败！");
			}
		} catch (Exception e) {
			j.setMsg("接口异常！");
			log.error("接口异常！",e);
		}
		return j;
	}
	

}
