package com.shadt.backstage.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shadt.backstage.service.TownS;
import com.shadt.backstage.vo.TownVo;
import com.shadt.core.controller.BaseController;
import com.shadt.core.entity.User;
import com.shadt.core.model.Json;
import com.shadt.core.util.DateUtil;

/**
 * 镇信息控制器
 * @author liusongren
 *
 */
@Controller
@RequestMapping("/townC")
public class TownC extends BaseController{

	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	TownS service;
	
	/**
	 * 获取数据到DataTable
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getToDataTable")
	public String getToDataTable(String aoData,HttpSession session){
		User user=(User)session.getAttribute("user");
		aoData = aoData.replaceAll("&quot;", "\"");
		String sEcho=null,sSearch = "";
		int iDisplayStart = 0;// 起始索引
		int iDisplayLength = 0;// 每页显示行数
		JSONArray jarray = JSONArray.parseArray(aoData);
		for (int i = 0; i < jarray.size(); i++) {
			JSONObject o = jarray.getJSONObject(i);
			if (o.get("name").equals("sEcho")) {
				sEcho = o.getString("value");
			}
			if(o.get("name").equals("sSearch")){
				sSearch = o.getString("value");
			}
			if (o.get("name").equals("iDisplayStart")) {
				iDisplayStart = o.getInteger("value");
			}
			if (o.get("name").equals("iDisplayLength")) {
				iDisplayLength = o.getInteger("value");
			}
		}
		List<TownVo> lst = new ArrayList<TownVo>();
		JSONObject obj = new JSONObject();
		try {
			lst = service.getAllBySearch(sSearch, iDisplayStart, iDisplayLength,user);
			obj.put("sEcho", sEcho);
			obj.put("iTotalRecords", lst.size());//实际的行数
			obj.put("iTotalDisplayRecords",service.getCount(sSearch,user));// 显示的行数,这个要和上面写的一样
			obj.put("aaData", lst);// 要以JSON格式返回
		} catch (Exception e) {
			e.printStackTrace();
		}
		return JSONArray.toJSONString(obj);
	}
	
	/**
	 * 获取所有镇信息
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getAll")
	public Json getAll(HttpSession session){
		User user=(User)session.getAttribute("user");
		Json j = new Json();
		List<TownVo> townList = new ArrayList<TownVo>();
		try {
			townList = service.getAll(user);
			j.setMsg("成功获取所有镇信息！");
			j.setSuccess(true);
			j.setObj(townList);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss")+":获取镇信息列表出错！在\"TownC.getAll()\"中"));
			j.setMsg("获取所有镇信息出错！");
			j.setObj(null);
			e.printStackTrace();
		}
		return j;
	}
	/**
	 * 根据镇编号获取镇信息
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getById")
	public Json getById(String id){
		Json j = new Json();
		TownVo town = new TownVo();
		try {
			town = service.getById(id);
			j.setMsg("成功获取镇信息！");
			j.setSuccess(true);
			j.setObj(town);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss")+":获取镇信息出错！在\"TownC.getById()\"中"));
			j.setMsg("获取镇信息出错！");
			j.setObj(null);
			e.printStackTrace();
		}
		return j;
	}
	
	/**
	 * 添加镇信息
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/add")
	public Json add(TownVo town){
		Json j = new Json();
		String count = "";
		try {
			count = service.add(town);
			j.setMsg("成功保存"+count+"条镇信息！");
			j.setSuccess(true);
			j.setObj(town);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss")+":保存镇信息出错！在\"TownC.add()\"中"));
			j.setMsg("保存镇信息出错！");
			j.setObj(null);
			e.printStackTrace();
		}
		return j;
	}
	
	/**
	 * 删除镇信息
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public Json delete(String id){
		Json j = new Json();
		String count = "";
		try {
			count = service.delete(id);
			j.setMsg("成功删除"+count+"条镇信息！");
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss")+":删除镇信息出错！在\"TownC.delete()\"中"));
			j.setMsg("删除镇信息出错！");
			e.printStackTrace();
		}
		j.setObj(null);
		return j;
	}
	
	/**
	 * 批量删除镇信息
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/batchDelete")
	public Json batchDelete(String[] ids){
		Json j = new Json();
		for (String s : ids) {
			j = this.delete(s);
		}
		return j;
	}
	
	/**
	 * 修改镇信息
	 * @param town
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/update")
	public Json update(TownVo town){
		Json j = new Json();
		int t = 0;
		try {
			t = service.updata(town);
			j.setMsg("修改镇信息成功！");
			j.setSuccess(true);
			j.setObj(t);
		} catch (Exception e) {
			log.error(e.toString(),e);
			j.setMsg("修改镇信息失败！");
			j.setObj(null);
//			e.printStackTrace();
		}
		return j;
	}
	
	/**
	 * 验证值是否存在
	 * @param value
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/checkValue")
	public boolean checkValue(String value){
		boolean t = false;
		try {
			t = service.checkValue(value.trim());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return t;
	}
	
}
