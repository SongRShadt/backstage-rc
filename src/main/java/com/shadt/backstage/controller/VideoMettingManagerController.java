package com.shadt.backstage.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shadt.backstage.service.TownS;
import com.shadt.backstage.service.VideoMettingManageService;
import com.shadt.backstage.vo.TownVideoVo;
import com.shadt.backstage.vo.VideoMettingManageVo;
import com.shadt.backstage.vo.VideoMettingVo;
import com.shadt.backstage.vo.VillageVo;
import com.shadt.core.controller.BaseController;
import com.shadt.core.entity.User;
import com.shadt.core.model.Json;

/**
 * 说明：视频会议管理
 * @author  岳云清
 * @version 1.0
 * @param
 * @date 2017年6月27日
 */
@Controller
@RequestMapping("/videoMettingManager")
public class VideoMettingManagerController extends BaseController {

	@Autowired
	private VideoMettingManageService videoMettingManageService;
	
	@Autowired
	TownS townService;
	
	/**
	 * 说明：分页
	 * @param req
	 * @param aoData
	 * @param session
	 * @return
	 * @throws ParseException
	 * @author  岳云清
	 * @time：2017年6月27日 上午11:42:09
	 */
	@ResponseBody
	@RequestMapping("/getpage")
	public String getByPaging(HttpServletRequest req, String aoData,HttpSession session) throws ParseException{
		User user=(User)session.getAttribute("user");
		aoData = aoData.replaceAll("&quot;", "\"");
		String sEcho=null,sSearch = "";
		int iDisplayStart = 0;// 起始索引
		int iDisplayLength = 0;// 每页显示行数
		JSONArray jarray = JSONArray.parseArray(aoData);
		for (int i = 0; i < jarray.size(); i++) {
			JSONObject o = jarray.getJSONObject(i);
			if (o.get("name").equals("sEcho")) {
				sEcho = o.getString("value");
			}
			if(o.get("name").equals("sSearch")){
				sSearch = o.getString("value");
			}
			if (o.get("name").equals("iDisplayStart")) {
				iDisplayStart = o.getInteger("value");
			}
			if (o.get("name").equals("iDisplayLength")) {
				iDisplayLength = o.getInteger("value");
			}
		}
		List<VideoMettingManageVo> lst = videoMettingManageService.getAllBySearch(sSearch, iDisplayStart, iDisplayLength,user);
		JSONObject obj = new JSONObject();
		obj.put("sEcho", sEcho);
		obj.put("iTotalRecords",lst.size());// 实际的行数
		obj.put("iTotalDisplayRecords",videoMettingManageService.getCount(sSearch,user));// 显示的行数,这个要和上面写的一样
		obj.put("aaData", lst);// 要以JSON格式返回
		return JSONArray.toJSONString(obj);
	}
	
	/**
	 * 说明：添加
	 * @param videoMettingManageVo
	 * @return
	 * @author  岳云清
	 * @time：2017年6月27日 上午11:42:20
	 */
	@ResponseBody
	@RequestMapping("/add")
	public Json add(VideoMettingManageVo videoMettingManageVo) {
		Json j = new Json();
		String count = "";
		try {
			count = videoMettingManageService.add(videoMettingManageVo);
			j.setMsg("成功保存" + count + "信息！");
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("保存信息失败！");
			e.printStackTrace();
		}
		j.setObj(null);
		return j;
	}
	
	
	/**
	 * 说明：获取镇信息
	 * @return
	 * @author  岳云清
	 * @time：2017年6月27日 上午10:00:42
	 */
	@ResponseBody
	@RequestMapping("/getByAllTown")
	public Json getByAllTown(){
		Json j = new Json();
		List<TownVideoVo> lst = new ArrayList<TownVideoVo>();
		try {
			lst=townService.getByAllTown();
			j.setMsg("成功获取信息！");
			j.setObj(lst);
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("获取信息失败！");
			j.setObj(null);
			e.printStackTrace();
		}
		return j;
	}
	
	/**
	 * 说明：删除
	 * @param id
	 * @return
	 * @author  岳云清
	 * @time：2017年6月27日 下午3:35:35
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public Json delete(String id) {
		Json j = new Json();
		String count = "";
		try {
			count = videoMettingManageService.delete(id);
			j.setMsg("成功删除"+count+"信息！");
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("删除信息失败！");
			e.printStackTrace();
		}
		j.setObj(null);
		return j;
	}
	
	/**
	 * 说明：获取查看页面
	 * @param req
	 * @param aoData
	 * @param session
	 * @param id
	 * @return
	 * @throws ParseException
	 * @author  岳云清
	 * @time：2017年6月27日 下午3:35:43
	 */
	@ResponseBody
	@RequestMapping("/getVillage")
	public String getVillage(HttpServletRequest req, String aoData,HttpSession session,String id) throws ParseException{
		User user=(User)session.getAttribute("user");
		aoData = aoData.replaceAll("&quot;", "\"");
		String sEcho=null,sSearch = "";
		int iDisplayStart = 0;// 起始索引
		int iDisplayLength = 0;// 每页显示行数
		JSONArray jarray = JSONArray.parseArray(aoData);
		for (int i = 0; i < jarray.size(); i++) {
			JSONObject o = jarray.getJSONObject(i);
			if (o.get("name").equals("sEcho")) {
				sEcho = o.getString("value");
			}
			if(o.get("name").equals("sSearch")){
				sSearch = o.getString("value");
			}
			if (o.get("name").equals("iDisplayStart")) {
				iDisplayStart = o.getInteger("value");
			}
			if (o.get("name").equals("iDisplayLength")) {
				iDisplayLength = o.getInteger("value");
			}
		}
		List<VideoMettingManageVo> lst = videoMettingManageService.getVillage(sSearch, iDisplayStart, iDisplayLength,user,id);
		JSONObject obj = new JSONObject();
		obj.put("sEcho", sEcho);
		obj.put("iTotalRecords",lst.size());// 实际的行数
		obj.put("iTotalDisplayRecords",videoMettingManageService.getCountVillage(sSearch,user,id));// 显示的行数,这个要和上面写的一样
		obj.put("aaData", lst);// 要以JSON格式返回
		return JSONArray.toJSONString(obj);
	}
	
	/**
	 * 说明：删除查看按钮里的村
	 * @param id
	 * @return
	 * @author  岳云清
	 * @time：2017年6月27日 下午5:33:05
	 */
	@ResponseBody
	@RequestMapping("/deleteVillage")
	public Json deleteVillage(String id) {
		Json j = new Json();
		String count = "";
		try {
			count = videoMettingManageService.deleteVillage(id);
			if(count!=null){
				j.setMsg("成功删除！");
				j.setSuccess(true);
			}else{
				j.setMsg("删除失败！");
			}
		} catch (Exception e) {
			j.setMsg("删除信息失败！");
			e.printStackTrace();
		}
		j.setObj(null);
		return j;
	}
	
	
	
	/**
	 * 说明：保存查看按钮中的村信息
	 * @param villeageId   村Id
	 * @param id    视频会议Id
	 * @author  岳云清
	 * @time：2017年6月29日 上午10:22:32
	 */
	@ResponseBody
	@RequestMapping("/addVillageShow")
	public Json addVillageShow(String villeageId,String id) {
		Json j = new Json();
		try {
			videoMettingManageService.addVillageShow(villeageId,id);
			j.setMsg("成功保存信息！");
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("保存信息失败！");
			e.printStackTrace();
		}
		j.setObj(null);
		return j;
	}
	
	
	/**
	 * 说明：获取所有未绑定的村
	 * @param id
	 * @return
	 * @author  岳云清
	 * @time：2017年6月29日 下午1:16:19
	 */
	@ResponseBody
	@RequestMapping("/unbounded")
	public Json unbounded(String id) {
		Json j = new Json();
		try {
			List<VillageVo> list=videoMettingManageService.unbounded(id);
			j.setMsg("成功保存信息！");
			j.setObj(list);
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("保存信息失败！");
			e.printStackTrace();
		}
		return j;
	}
	
	
	
	
	
	/**
	 * 说明：获取视频会议下拉框
	 * @return
	 * @author  岳云清
	 * @time：2017年6月30日 上午9:46:49
	 */
	@ResponseBody
	@RequestMapping("/getByMettingAndVideo")
	public Json getByMettingAndVideo(){
		Json j = new Json();
		List<VideoMettingVo> lst = new ArrayList<VideoMettingVo>();
		try {
			lst=videoMettingManageService.getByMettingAndVideo();
			j.setMsg("成功获取信息！");
			j.setObj(lst);
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("获取信息失败！");
			j.setObj(null);
			e.printStackTrace();
		}
		return j;
	}
	
	
	
	@ResponseBody
	@RequestMapping("/addMettingAndVideoform")
	public Json addMettingAndVideoform(String id){
		Json j = new Json();
		try {
			videoMettingManageService.addMettingAndVideoform(id);
			j.setMsg("成功保存信息！");
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("保存信息失败！");
			j.setObj(null);
			e.printStackTrace();
		}
		return j;
	}
}
