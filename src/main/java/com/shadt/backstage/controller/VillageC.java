package com.shadt.backstage.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shadt.backstage.service.VillageS;
import com.shadt.backstage.vo.VillageVo;
import com.shadt.core.controller.BaseController;
import com.shadt.core.entity.User;
import com.shadt.core.model.Json;
import com.shadt.core.util.DateUtil;

/**
 * 村信息控制器
 * 
 * @author liusongren
 * 
 */
@Controller
@RequestMapping("/villageC")
public class VillageC extends BaseController {
	
	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	VillageS service;

	/**
	 * 获取所有村信息
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getAll")
	public String getAll(String aoData,HttpSession session) {
		User user=(User)session.getAttribute("user");
		JSONObject obj = new JSONObject();
		try {
			aoData = aoData.replaceAll("&quot;", "\"");
			String sEcho = null, sSearch = "";
			int iDisplayStart = 0;// 起始索引
			int iDisplayLength = 0;// 每页显示行数
			JSONArray jarray = JSONArray.parseArray(aoData);
			for (int i = 0; i < jarray.size(); i++) {
				JSONObject o = jarray.getJSONObject(i);
				if (o.get("name").equals("sEcho")) {
					sEcho = o.getString("value");
				}
				if (o.get("name").equals("sSearch")) {
					sSearch = o.getString("value");
				}
				if (o.get("name").equals("iDisplayStart")) {
					iDisplayStart = o.getInteger("value");
				}
				if (o.get("name").equals("iDisplayLength")) {
					iDisplayLength = o.getInteger("value");
				}
			}
			if(null!=user){
				List<VillageVo> lst = service.getAll(sSearch, iDisplayStart,iDisplayLength,user);
				if(null!=lst){
					obj.put("sEcho", sEcho);
					obj.put("iTotalRecords", lst.size());// 实际的行数
					obj.put("iTotalDisplayRecords", service.getCount(sSearch,user));// 显示的行数,这个要和上面写的一样
					obj.put("aaData", lst);// 要以JSON格式返回
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.toString(),e);
		}
		return JSONArray.toJSONString(obj);
	}

	/**
	 * 获取所有
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getByAll")
	public Json getByAll(String townId){
		Json j = new Json();
		List<VillageVo> lst = new ArrayList<VillageVo>();
		try {
			lst=service.getAll(townId==null?"":townId);
			j.setMsg("成功获取村信息！");
			j.setObj(lst);
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("获取村信息失败！");
			j.setObj(null);
			e.printStackTrace();
		}
		return j;
	}
	
	/**
	 * 获取视频会议未绑定的村
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/byvideo",method={RequestMethod.GET,RequestMethod.POST})
	public Json getVillageByVideo(String townid){
		System.out.println(townid);
		Json j = new Json();
		try {
			List<VillageVo> vos = service.getVillageByVideo(townid);
			j.setObj(vos);
			j.setMsg("成功获取村信息！");
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("获取村信息失败！");
			j.setObj(null);
		}
		return j;
	}
	
	
	
	/**
	 * 根据村编号获取村信息
	 * 
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getById")
	public Json getById(String id) {
		Json j = new Json();
		VillageVo village = new VillageVo();
		try {
			village = service.getById(id);
			j.setMsg("成功获取村信息！");
			j.setObj(village);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(),
					"yyyy-MM-dd HH:mm:ss")
					+ ":获取村信息出错！在\"VillageC.getById()\"中"));
			j.setMsg("获取村信息失败！");
			j.setObj(null);
		}
		return j;
	}

	/**
	 * 根据镇编号获取对应的村列表
	 * 
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getByTownId")
	public Json getByTownId(String id) {
		Json j = new Json();
		List<VillageVo> villageList = new ArrayList<VillageVo>();
		try {
			villageList = service.getByTownId(id);
			j.setMsg("按镇获取村信息成功！");
			j.setObj(villageList);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(),
					"yyyy-MM-dd HH:mm:ss")
					+ ":按镇获取村信息出错！在\"VillageC.getByTownId()\"中"));
			j.setMsg("按镇获取村信息失败！");
			j.setObj(null);
		}
		return j;
	}

	/**
	 * 添加村信息
	 * 
	 * @param village
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/add")
	public Json add(VillageVo village) {
		Json j = new Json();
		String count = "";
		try {
			count = service.add(village);
			j.setMsg("成功保存" + count + "条村信息！");
			j.setObj(village);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(),
					"yyyy-MM-dd HH:mm:ss") + ":保存村信息出错！在\"VillageC.add()\"中"));
			e.printStackTrace();
			j.setObj(null);
			j.setMsg("保存村信息失败！");
		}
		return j;
	}

	/**
	 * 删除村信息
	 * 
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public Json delete(String id,String stbNo) {
		Json j = new Json();
		String count = "";
		try {
			count = service.delete(id,stbNo);
			j.setMsg("成功删除" + count + "条村信息！");
			j.setSuccess(true);
		} catch (Exception e) {
			System.err
					.println(new Exception(DateUtil.dataToStr(new Date(),
							"yyyy-MM-dd HH:mm:ss")
							+ ":删除村信息出错！在\"VillageC.delete()\"中"));
			j.setMsg("删除村信息出错！");
		}
		j.setObj(null);
		return j;
	}

	/**
	 * 批量删除村信息
	 * 
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/batchDelete")
	public Json batchDelete(String[] ids,String [] stbNos) {
		Json j = new Json();
		for (String s : ids) {
			j = this.delete(s,s);
		}
		return j;
	}

	/**
	 * 修改村信息
	 * 
	 * @param village
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/updata")
	public Json update(VillageVo village) {
		Json j = new Json();
		String count = "";
		try {
			count = service.update(village);
			j.setMsg("修改村信息成功！");
			j.setObj(count);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err
					.println(new Exception(DateUtil.dataToStr(new Date(),
							"yyyy-MM-dd HH:mm:ss")
							+ ":修改村信息出错！在\"VillageC.delete()\"中"));
			e.printStackTrace();
			j.setMsg("修改村信息出错！");
			j.setObj(null);
		}
		return j;
	}
	
	/**
	 * 验证值是否存在
	 * @param value
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/checkValue")
	public boolean checkValue(String value){
		boolean t = false;
		try {
			t = service.checkValue(value.trim());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return t;
	}
	
}
