package com.shadt.backstage.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.backstage.service.StbCameraS;
import com.shadt.backstage.service.StbS;
import com.shadt.backstage.service.TownS;
import com.shadt.backstage.vo.CameraVo;
import com.shadt.backstage.vo.StbCameraVo;
import com.shadt.backstage.vo.StbVo;
import com.shadt.backstage.vo.TownVo;
import com.shadt.backstage.vo.VillageVo;
import com.shadt.core.controller.BaseController;
import com.shadt.core.entity.User;
import com.shadt.core.model.Json;
import com.shadt.core.util.DateUtil;

/**
 * 机顶盒与摄像头关系控制器
 * 
 * @author liusongren
 *
 */
@Controller
@RequestMapping("/stbCameraC")
public class StbCameraC extends BaseController {

	@Autowired
	StbCameraS service;
	@Autowired
	TownS tservice;
	@Autowired
	StbS sservice;

	/**
	 * 绑定摄像头
	 * 
	 * @param type
	 * @param villageId
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/bindCamera")
	public Json bindCamera(String type, String villageId) {
		Json j = new Json();
		try {
			StbVo stb = sservice.getByVillageId(villageId);
			if (null == stb) {
				j.setMsg("此村暂无机顶盒信息！请绑定机顶盒到本村！");
				j.setSuccess(true);
				j.setObj(0);
			}else{
				String count = service.bindCamera(type, villageId);
				j.setMsg("绑定成功！类型为：" + type);
				j.setSuccess(true);
				j.setObj(count);
			}
		} catch (Exception e) {
			j.setMsg("绑定失败！类型为：" + type);
			j.setObj(null);
			e.printStackTrace();
		}
		return j;
	}

	/**
	 * 获取所有机顶盒与摄像头关系
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getAll")
	public Json getAll() {
		Json j = new Json();
		List<StbCameraVo> stbCameraList = new ArrayList<StbCameraVo>();
		try {
			stbCameraList = service.getAll();
			j.setMsg("获取机顶盒与摄像头关系列表成功！");
			j.setObj(stbCameraList);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss")
					+ ":获取机顶盒与摄像头关系失败！在\"StbCameraC.getAll()\"中"));
			j.setMsg("获取机顶盒与摄像头关系列表失败！");
			j.setObj(null);
		}
		return j;
	}

	/**
	 * 根据机顶盒编号获取机顶盒与摄像头关系列表
	 * 
	 * @param stbNo
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getByStbNo")
	public Json getByStbNo(String stbNo) {
		Json j = new Json();
		List<StbCameraVo> stbCameraList = new ArrayList<StbCameraVo>();
		try {
			stbCameraList = service.getByStbNo(stbNo);
			j.setMsg("根据机顶盒编号获取机顶盒与摄像头关系成功！");
			j.setObj(stbCameraList);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss")
					+ ":获取机顶盒与摄像头关系失败！在\"StbCameraC.getByStbNo()\"中"));
			j.setMsg("根据机顶盒编号获取机顶盒与是相同关系失败！");
			j.setObj(null);
			e.printStackTrace();
		}
		return j;
	}

	/**
	 * 根据摄像头编号获取机顶盒与摄像头关系
	 * 
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getByCameraNo")
	public Json getByCameraNo(String cameraNo, String stbNo) {
		Json j = new Json();
		List<StbCameraVo> stbCameraList = new ArrayList<StbCameraVo>();
		try {
			stbCameraList = service.getByCameraNo(cameraNo, stbNo);
			j.setMsg("根据摄像头编号获取机顶盒与摄像头关系成功！");
			j.setObj(stbCameraList);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss")
					+ ":获取机顶盒与摄像头关系失败！在\"StbCameraC.getByCameraNo()\"中"));
			j.setMsg("根据摄像头编号获取机顶盒与摄像头关系失败！");
			j.setObj(null);
		}
		return j;
	}

	/**
	 * 添加机顶盒与摄像头关系
	 * 
	 * @param stbCamera
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/add")
	public Json add(StbCameraVo stbCamera) {
		Json j = new Json();
		String count = "";
		try {
			if (stbCamera.getCameraNo().equals("90001") || stbCamera.getCameraNo().equals("90002")
					|| stbCamera.getCameraNo().equals("90003") || stbCamera.getCameraNo().equals("99998")) {
				stbCamera.setRltType("0");
			} else {
				stbCamera.setRltType("1");
			}
			count = service.add(stbCamera);
			j.setMsg("成功添加" + count + "条机顶盒与摄像头关系！");
			j.setObj(stbCamera);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(
					DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss") + ":添加机顶盒与摄像头关系失败！在\"StbCameraC.add()\"中"));
			j.setMsg("添加机顶盒与摄像头关系失败！");
			j.setObj(null);
			e.printStackTrace();
		}
		return j;
	}

	/**
	 * 批量添加机顶盒与摄像头关系
	 * 
	 * @param stbNo
	 * @param cameraNo
	 * @param rlt
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/batchAdd")
	public Json batchAdd(String stbNo, String[] cameraNo, String rlt) {
		Json j = new Json();
		List<StbCameraVo> stbCameraList = service.getByStbNo(stbNo);
		for (StbCameraVo sc : stbCameraList) {
			service.delete(sc);
		}
		if (cameraNo != null) {
			for (String s : cameraNo) {
				List<StbCameraVo> scList = service.getByCameraNo(s, stbNo);
				if (scList.size() <= 0) {
					j = this.add(new StbCameraVo(stbNo, s, rlt));
				}
			}
		}
		return j;
	}

	/**
	 * 删除机顶盒与摄像头关系
	 * 
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public Json delete(StbCameraVo stbCamera) {
		Json j = new Json();
		String count = "";
		try {
			count = service.delete(stbCamera);
			j.setMsg("成功删除" + count + "条机顶盒与摄像头关系！");
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss")
					+ ":删除机顶盒与摄像头关系失败！在\"StbCameraC.delete()\"中"));
			j.setMsg("删除机顶盒与摄像头关系失败！");
			j.setObj(null);
		}
		return j;
	}

	/**
	 * 批量删除机顶盒与摄像头关系
	 * 
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/batchDelete")
	public Json batchDelete(String[] ids) {
		Json j = new Json();
		// for (String s : ids) {
		// j=this.delete(s);
		// }
		return j;
	}

	/**
	 * 修改机顶盒与摄像头关系
	 * 
	 * @param stbCamera
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/update")
	public Json update(StbCameraVo stbCamera) {
		Json j = new Json();
		String count = "";
		try {
			count = service.update(stbCamera);
			j.setMsg("成功修改" + count + "条机顶盒与摄像头关系！");
			j.setObj(stbCamera);
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(), "yyyy-MM-dd HH:mm:ss")
					+ ":修改机顶盒与摄像头关系失败！在\"StbCameraC.update()\"中"));
			j.setMsg("修改机顶盒与摄像头关系失败！");
			j.setObj(null);
		}
		return j;
	}

	/**
	 * 根据stb获取jsTree
	 * 
	 * @param stbNo
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/handleJsTree")
	public String handleJsTree(String stbNo,HttpSession session) {
		User user =(User)session.getAttribute("user");
		Json j = new Json();
		List<TownVo> tList = tservice.getAll(user);
		List<StbCameraVo> scList = service.getByStbNo(stbNo);
		System.out.println(scList.size());

		j.setObj(tList);
		String jsTree = "[{'text':'全部','children':[";
		for (TownVo t : tList) {
			jsTree += "{'text':'" + t.getTownName() + "','id':'town_" + t.getTownId() + "','children':[";
			List<VillageVo> vList = t.getVillages();
			for (VillageVo v : vList) {
				jsTree += "{'text':'" + v.getVillageName() + "','id':'village_" + v.getVillageId() + "','children':[";
				List<CameraVo> cList = v.getCameras();
				for (CameraVo c : cList) {
					boolean isTrue = false;
					for (StbCameraVo sc : scList) {
						if (c.getCameraNo().equals(sc.getCameraNo())) {
							isTrue = true;
						}
					}
					if (isTrue)
						jsTree += "{'text':'" + c.getCameraName() + "','id:':'camera_" + c.getCameraNo()
								+ "','state':{'opened':true,'selected': true}},";
					else
						jsTree += "{'text':'" + c.getCameraName() + "','id:':'" + c.getCameraNo() + "'},";

				}
				jsTree += "]},";
			}
			jsTree += "]},";
		}
		jsTree += "]}]";
		System.out.println(jsTree);

		return jsTree;
	}

}
