package com.shadt.backstage.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shadt.backstage.service.RoomService;
import com.shadt.backstage.vo.VideoVo;
import com.shadt.core.entity.User;
import com.shadt.core.model.Json;
import com.shadt.core.util.DateUtil;

/**
 * 说明：值班室视频信息列表
 * @author  岳云清
 * @version 1.0
 * @param
 * @date 2017年6月17日
 */
@Controller
@RequestMapping("/RoomC")
public class RoomC {

	@Autowired
	private RoomService roomService;
	
	/**
	 * 说明：分页显示数据
	 * @param aoData
	 * @param time
	 * @param session
	 * @return
	 * @author  岳云清
	 * @time：2017年6月17日 下午4:28:34
	 */
	@ResponseBody
	@RequestMapping(value = "/getAll")
	public String getAll(String aoData,String time,HttpSession session) {
		User user=(User)session.getAttribute("user");
		List<VideoVo> lst = null;
		JSONObject obj = new JSONObject();
		try{
			aoData = aoData.replaceAll("&quot;", "\"");
			String sEcho = null, term = "";
			int iDisplayStart = 0;
			int iDisplayLength = 0;
			JSONArray jarray = JSONArray.parseArray(aoData);
			for (int i = 0; i < jarray.size(); i++) {
				JSONObject o = jarray.getJSONObject(i);
				if (o.get("name").equals("sEcho")) {
					sEcho = o.getString("value");
				}
				if (o.get("name").equals("sSearch")) {
					term = o.getString("value");
				}
				if (o.get("name").equals("iDisplayStart")) {
					iDisplayStart = o.getInteger("value");
				}
				if (o.get("name").equals("iDisplayLength")) {
					iDisplayLength = o.getInteger("value");
				}
			}
			lst = roomService.findAllBySql(time,term, iDisplayStart,iDisplayLength,user);
			obj.put("sEcho", sEcho);  //搜索的关键字
			obj.put("iTotalRecords", lst.size());// 实际的行数
			obj.put("iTotalDisplayRecords", roomService.rowCount(term,time,user));// 显示的行数,这个要和上面写的一样
			obj.put("aaData", lst);// 要以JSON格式返回
		}catch(Exception e){
			System.err.println(new Exception(DateUtil.dataToStr(new Date(),"yyyy-MM-dd HH:mm:ss")+ ":获取视频信息列表出错！在\"VideoC.getAll()\"中"));
		}
		return JSONArray.toJSONString(obj);
	}
	
	
	/**
	 * 说明：删除数据
	 * @param id
	 * @return
	 * @author  岳云清
	 * @time：2017年6月17日 下午4:28:52
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteRow")
	public Json delete(String id) {
		Json j = new Json();
		String count = "";
		try {
			count = roomService.delete(id);
			j.setMsg("成功删除" + count + "条视频信息！");
			j.setSuccess(true);
		} catch (Exception e) {
			System.err.println(new Exception(DateUtil.dataToStr(new Date(),"yyyy-MM-dd HH:mm:ss") + ":删除视频信息出错！在\"videoC.delete()\"中"));
			j.setMsg("删除视频信息失败！");
			e.printStackTrace();
		}
		j.setObj(null);
		return j;
	}
}
