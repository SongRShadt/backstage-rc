package com.shadt.backstage.controller;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

public class test {
	public static void isAddressAvailable(String ip) {
		try {
			InetAddress address = InetAddress.getByName(ip);// ping this IP
			if (address.isReachable(5000)) {
				System.out.println("SUCCESS - ping " + ip + " with no interface specified");
			} else {
				System.out.println("FAILURE - ping " + ip + " with no interface specified");
				System.out.println("\n-------Trying different interfaces--------\n");
				Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();
				while (netInterfaces.hasMoreElements()) {
					NetworkInterface ni = netInterfaces.nextElement();
					System.out.println("Checking interface, DisplayName:" + ni.getDisplayName() + ", Name:" + ni.getName());
					if (address.isReachable(ni, 0, 5000)) {
						System.out.println("SUCCESS - ping " + ip);
					} else {
						System.out.println("FAILURE - ping " + ip);
					}

					Enumeration<InetAddress> ips = ni.getInetAddresses();
					while (ips.hasMoreElements()) {
						System.out.println("IP: " + ips.nextElement().getHostAddress());
					}
					System.out.println("-------------------------------------------");
				}
			}
		} catch (Exception e) {
			System.out.println("error occurs.");
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
		String url = "http://192.168.2.15";
		String[] ip = url.split("/");
		System.out.println(ip[2].split(":")[0]);
//		isAddressAvailable("");
	}
	
}
