package com.shadt.backstage.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shadt.backstage.service.VideoMettingService;
import com.shadt.backstage.vo.IpConfigVo;
import com.shadt.backstage.vo.MettingVideoVo;
import com.shadt.backstage.vo.TownVo;
import com.shadt.backstage.vo.VillageVo;
import com.shadt.core.controller.BaseController;
import com.shadt.core.model.Json;
/**
 * 视频会议控制器
 * @author SongR
 *
 */
@Controller
@RequestMapping(value="/videometting")
public class VideoMettingController extends BaseController{

	Logger log = LoggerFactory.getLogger(this.getClass());
	
	
	@Autowired
	VideoMettingService service;
	
	
	/**
	 * 获取所有视频会议
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/all",method={RequestMethod.GET,RequestMethod.POST})
	public Json getAll(HttpServletRequest request,HttpSession session,String aoData){
		Json j = new Json();
		if(!aoData.equals("")){
			aoData = aoData.replaceAll("&quot;", "\"");
			String sEcho=null,sSearch = "";
			int iDisplayStart = 0;// 起始索引
			int iDisplayLength = 0;// 每页显示行数
			JSONArray jarray = JSONArray.parseArray(aoData);
			for (int i = 0; i < jarray.size(); i++) {
				JSONObject o = jarray.getJSONObject(i);
				if (o.get("name").equals("sEcho")) {
					sEcho = o.getString("value");
				}
				if (o.get("name").equals("iDisplayStart")) {
					iDisplayStart = o.getInteger("value");
				}
				if (o.get("name").equals("iDisplayLength")) {
					iDisplayLength = o.getInteger("value");
				}
				if (o.get("name").equals("sSearch")) {
					sSearch = o.getString("value");
				}
			}
			try {
				List<MettingVideoVo> vos = service.getAll(sSearch, iDisplayStart, iDisplayLength);
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("sEcho",sEcho);
				map.put("iTotalRecords",vos.size());
				map.put("iTotalDisplayRecords",service.countAll(sSearch));
				map.put("aaData",vos);
				j.setMsg("成功获取视频会议！");
				j.setObj(map);
				j.setSuccess(true);
			} catch (Exception e) {
				log.error("接口异常！",e);
				j.setMsg("获取视频会议接口异常！");
				j.setObj(null);
			}
		}
		return j;
	}
	
	/**
	 * 添加视频会议
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/add",method={RequestMethod.POST})
	public Json add(MettingVideoVo vo,String village,String sewise){
		Json j = new Json();
		try {
			int count = service.add(vo,village,sewise);
			if(count>0){
				j.setMsg("视频会议添加成功！");
				j.setSuccess(true);
			}else{
				j.setMsg("视频会议添加失败！");
			}
		} catch (Exception e) {
			j.setMsg("添加视频会议失败");
			log.error("添加视频会议接口异常！",e);
		}
		return j;
	}
	
	/**
	 * 删除视频会议
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/delete",method={RequestMethod.POST})
	public Json delete(String id){
		Json j = new Json();
		try {
			int count = service.delete(id);
			if(count>0){
				j.setMsg("删除视频会议成功！");
				j.setSuccess(true);
			}else{
				j.setMsg("删除视频会议失败！");
			}
		} catch (Exception e) {
			j.setMsg("删除视频会议失败");
			log.error("删除视频会议接口异常！",e);
		}
		return j;
	}
	
	/**
	 * 获取视频会议
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/get",method={RequestMethod.GET,RequestMethod.POST})
	public Json get(String videoid){
		Json j = new Json();
		try {
			MettingVideoVo vo = service.get(videoid);
			j.setMsg("成功获取视频会议！");
			j.setObj(vo);
			j.setSuccess(true);
		} catch (Exception e) {
			j.setObj(null);
			j.setSuccess(false);
			j.setMsg("获取视频会议失败！");
			log.error("获取视频会议接口异常！",e);
		}
		return j;
	}
	/**
	 * 修改视频会议
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/update",method={RequestMethod.GET,RequestMethod.POST})
	public Json update(String id,String name,String rtsp,String sort,String villageid){
		Json j = new Json();
		try {
			int count = service.update(id,name,rtsp,sort,villageid);
			if(count>0){
				j.setMsg("修改视频会议成功！");
				j.setSuccess(true);
			}else{
				j.setMsg("修改视频会议失败！");
			}
		} catch (Exception e) {
			j.setMsg("修改视频会议失败");
			log.error("修改视频会议接口异常！",e);
		}
		return j;
	}
	
	/**
	 * 获取视频会议会场
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/town/all",method={RequestMethod.POST,RequestMethod.GET})
	public Json getAllTown(){
		Json j = new Json();
		try {
			List<VillageVo> vos = service.getAllTown();
			j.setMsg("成功获取视频会议会场！");
			j.setObj(vos);
			j.setSuccess(true);
		} catch (Exception e) {
			j.setObj(null);
			j.setSuccess(false);
			j.setMsg("获取视频会议会场失败！");
			log.error("获取视频会议会场接口异常！",e);
		}
		return j;
	}

	/**
	 * 获取流媒体服务器
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/sewise/all",method={RequestMethod.POST,RequestMethod.GET})
	public Json getAllSewise(){
		Json j = new Json();
		try {
			List<IpConfigVo> vos = service.getAllSewise();
			j.setMsg("成功获取推流服务器！");
			j.setObj(vos);
			j.setSuccess(true);
		} catch (Exception e) {
			j.setObj(null);
			j.setSuccess(false);
			j.setMsg("获取推流服务器失败！");
			log.error("获取推流服务器接口异常！",e);
		}
		return j;
	}
	
	/**
	 * 链接会议
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/start",method={RequestMethod.GET,RequestMethod.POST})
	public Json start(String videoid){
		Json j = new Json();
		try {
			service.start(videoid);
			j.setMsg("成功链接输入源!");
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("链接输入源失败!");
			log.error("链接输入源接口异常!",e);
		}
		return j;
	}
	
	/**
	 * 停止会议
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/stop",method={RequestMethod.GET,RequestMethod.POST})
	public Json stop(String videoid){
		Json j = new Json();
		try {
			service.stop(videoid);
			j.setMsg("成功断开输入源!");
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("断开输入源失败!");
			log.error("断开输入源接口异常!",e);
		}
		return j;
	}
	
	/**
	 * 获取绑定列表
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/bind/all",method={RequestMethod.GET,RequestMethod.POST})
	public Json getAllBind(String aoData,String videoid){
		Json j = new Json();
		if(!aoData.equals("")){
			aoData = aoData.replaceAll("&quot;", "\"");
			String sEcho=null,sSearch = "";
			int iDisplayStart = 0;// 起始索引
			int iDisplayLength = 0;// 每页显示行数
			JSONArray jarray = JSONArray.parseArray(aoData);
			for (int i = 0; i < jarray.size(); i++) {
				JSONObject o = jarray.getJSONObject(i);
				if (o.get("name").equals("sEcho")) {
					sEcho = o.getString("value");
				}
				if (o.get("name").equals("iDisplayStart")) {
					iDisplayStart = o.getInteger("value");
				}
				if (o.get("name").equals("iDisplayLength")) {
					iDisplayLength = o.getInteger("value");
				}
				if (o.get("name").equals("sSearch")) {
					sSearch = o.getString("value");
				}
			}
			try {
				List<VillageVo> vos = service.getAllBind(sSearch, iDisplayStart, iDisplayLength,videoid);
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("sEcho",sEcho);
				map.put("iTotalRecords",vos.size());
				map.put("iTotalDisplayRecords",service.countAllBind(sSearch,videoid));
				map.put("aaData",vos);
				j.setMsg("成功获取绑定的村！");
				j.setObj(map);
				j.setSuccess(true);
			} catch (Exception e) {
				log.error("接口异常！",e);
				j.setMsg("获取绑定的村失败！");
				j.setObj(null);
			}
		}
		return j;
	}
	
	/**
	 * 获取可以绑定的镇
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/bind/town",method={RequestMethod.GET,RequestMethod.POST})
	public Json getTownBind(){
		Json j = new Json();
		try {
			List<TownVo> vos = service.getTownBind();
			j.setMsg("成功获取镇信息！");
			j.setObj(vos);
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("获取镇信息失败！");
			log.error("获取镇信息接口异常！",e);
		}
		return j;
	}
	
	/**
	 * 获取可以绑定的村
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/bind/village",method={RequestMethod.GET,RequestMethod.POST})
	public Json getVillageBind(String townid,String videoid){
		Json j = new Json();
		try {
			List<VillageVo> vos = service.getVillageBind(townid,videoid);
			j.setMsg("成功获取未绑定的村！");
			j.setObj(vos);
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("获取未绑定的村失败！");
			log.error("获取未绑定的村接口异常！",e);
		}
		return j;
	}
	
	/**
	 * 绑定村
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/bind/add",method={RequestMethod.GET,RequestMethod.POST})
	public Json addBind(String vil,String videoid){
		Json j = new Json();
		try {
			int count = service.addBind(vil,videoid);
			if(count>0){
				j.setMsg("绑定成功！");
				j.setSuccess(true);
			}else{
				j.setMsg("绑定失败！");
			}
		} catch (Exception e) {
			j.setMsg("绑定失败！");
			log.error("绑定村权限接口异常！",e);
		}
		return j;
	}
	
	/**
	 * 删除绑定
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/bind/delete",method={RequestMethod.GET,RequestMethod.POST})
	public Json deleteBind(String videoid,String villageid){
		Json j = new Json();
		try {
			int count = service.deleteBind(videoid,villageid);
			if(count>0){
				j.setMsg("删除绑定成功！");
				j.setSuccess(true);
			}else{
				j.setMsg("删除绑定失败！");
			}
		} catch (Exception e) {
			j.setMsg("删除绑定失败！");
			log.error("删除村绑定接口异常！",e);
		}
		return j;
	}
	
}
