package com.shadt.backstage.controller;

import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shadt.backstage.service.CameraImgS;
import com.shadt.backstage.service.CameraS;
import com.shadt.backstage.vo.CameraImg;
import com.shadt.core.entity.User;
import com.shadt.core.model.Json;
import com.shadt.core.util.ConfigUtil;

@Controller
@RequestMapping("/cameraImg")
public class CameraImgAction {
	
	@Autowired 
	CameraS service;
	
	@Autowired
	CameraImgS cameraImgS;
	
	/**
	 * 说明：分页
	 * @param req
	 * @param aoData
	 * @param session
	 * @return
	 * @author  岳云清
	 * @time：2017年6月7日 上午8:50:21
	 */
	@ResponseBody
	@RequestMapping("/getByPaging")
	public String getByPaging(HttpServletRequest req, String aoData,HttpSession session) throws ParseException{
		User user=(User)session.getAttribute("user");
		aoData = aoData.replaceAll("&quot;", "\"");
		String sEcho=null,sSearch = "";
		int iDisplayStart = 0;// 起始索引
		int iDisplayLength = 0;// 每页显示行数
		JSONArray jarray = JSONArray.parseArray(aoData);
		for (int i = 0; i < jarray.size(); i++) {
			JSONObject o = jarray.getJSONObject(i);
			if (o.get("name").equals("sEcho")) {
				sEcho = o.getString("value");
			}
			if(o.get("name").equals("sSearch")){
				sSearch = o.getString("value");
			}
			if (o.get("name").equals("iDisplayStart")) {
				iDisplayStart = o.getInteger("value");
			}
			if (o.get("name").equals("iDisplayLength")) {
				iDisplayLength = o.getInteger("value");
			}
		}
		List<CameraImg> lst = cameraImgS.getAllBySearch(sSearch, iDisplayStart, iDisplayLength,user);
		JSONObject obj = new JSONObject();
		obj.put("sEcho", sEcho);
		obj.put("iTotalRecords",lst.size());// 实际的行数
		obj.put("iTotalDisplayRecords",cameraImgS.getCount(sSearch,user));// 显示的行数,这个要和上面写的一样
		obj.put("aaData", lst);// 要以JSON格式返回
		return JSONArray.toJSONString(obj);
	}
	/**
	 * 删除截图
	 * 
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public Json delete(String id) {
		System.out.println(id);
		System.out.println("进入摄像头截图删除页面");
		Json j = new Json();
		try {
			cameraImgS.delete(id);
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("删除截图信息失败！");
			e.printStackTrace();
		}
		j.setObj(null);
		return j;
	}

	
	/**
	 * 说明：下载图片
	 * @param url 图片路径
	 * @return
	 * @author  岳云清
	 * @time：2017年6月12日 下午2:05:24
	 */
	@ResponseBody
	@RequestMapping("/download")
	public Json download(String url) {
		String url2=ConfigUtil.get("imageUrl")+url;
		Json j = new Json();
		try {
			String download = cameraImgS.download(url2);
			if (download.equals("")) {
				j.setMsg("下载失败！");
			}else {
				j.setMsg("下载成功！下载到 E：");
				j.setSuccess(true);
			}
		} catch (Exception e) {
			j.setMsg("下载失败！");
			e.printStackTrace();
		}
		return j;
	}
}
