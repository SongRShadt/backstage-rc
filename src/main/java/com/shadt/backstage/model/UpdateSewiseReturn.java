package com.shadt.backstage.model;

public class UpdateSewiseReturn {
	private String errors;
	private boolean success;
	public String getErrors() {
		return errors;
	}
	public void setErrors(String errors) {
		this.errors = errors;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	 
	
}
