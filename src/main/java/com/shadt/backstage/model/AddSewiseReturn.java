package com.shadt.backstage.model;

public class AddSewiseReturn {
	private String errors;
	private boolean success;
	private String sourceid;
	private String rtmp;
	private String http;
	private String m3u8;
	private String rtsp;
	private String rtmppush	;
	public String getErrors() {
		return errors;
	}
	public void setErrors(String errors) {
		this.errors = errors;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getSourceid() {
		return sourceid;
	}
	public void setSourceid(String sourceid) {
		this.sourceid = sourceid;
	}
	public String getRtmp() {
		return rtmp;
	}
	public void setRtmp(String rtmp) {
		this.rtmp = rtmp;
	}
	public String getHttp() {
		return http;
	}
	public void setHttp(String http) {
		this.http = http;
	}
	public String getM3u8() {
		return m3u8;
	}
	public void setM3u8(String m3u8) {
		this.m3u8 = m3u8;
	}
	public String getRtsp() {
		return rtsp;
	}
	public void setRtsp(String rtsp) {
		this.rtsp = rtsp;
	}
	public String getRtmppush() {
		return rtmppush;
	}
	public void setRtmppush(String rtmppush) {
		this.rtmppush = rtmppush;
	}

}
