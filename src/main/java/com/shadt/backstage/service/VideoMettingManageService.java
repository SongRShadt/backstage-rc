package com.shadt.backstage.service;

import java.util.List;

import javax.persistence.Id;

import com.shadt.backstage.vo.TownVideoVo;
import com.shadt.backstage.vo.VideoMettingManageVo;
import com.shadt.backstage.vo.VideoMettingVo;
import com.shadt.backstage.vo.VillageVo;
import com.shadt.core.entity.User;


public interface VideoMettingManageService {

	/**
	 * 说明：分页
	 * @param sSearch
	 * @param iDisplayStart
	 * @param iDisplayLength
	 * @param user
	 * @return
	 * @author  岳云清
	 * @time：2017年6月27日 下午2:33:56
	 */
	List<VideoMettingManageVo> getAllBySearch(String sSearch, int iDisplayStart, int iDisplayLength, User user);

	/**
	 * 说明：获得总条数
	 * @param sSearch
	 * @param user
	 * @return
	 * @author  岳云清
	 * @time：2017年6月27日 下午2:34:05
	 */
	Object getCount(String sSearch, User user);

	/**
	 * 说明：添加
	 * @param videoMettingManageVo
	 * @return
	 * @author  岳云清
	 * @time：2017年6月27日 下午2:34:15
	 */
	String add(VideoMettingManageVo videoMettingManageVo);

	/**
	 * 说明：删除
	 * @param id
	 * @return
	 * @author  岳云清
	 * @time：2017年6月27日 下午2:34:23
	 */
	String delete(String id);

	/**
	 * 说明：查看
	 * @param sSearch
	 * @param iDisplayStart
	 * @param iDisplayLength
	 * @param user
	 * @return
	 * @author  岳云清
	 * @time：2017年6月27日 下午2:34:34
	 */
	List<VideoMettingManageVo> getVillage(String sSearch, int iDisplayStart, int iDisplayLength, User user,String id);

	/**
	 * 说明：获得条数
	 * @param sSearch
	 * @param user
	 * @param id
	 * @return
	 * @author  岳云清
	 * @time：2017年6月27日 下午5:33:54
	 */
	Object getCountVillage(String sSearch, User user,String id);

	/**
	 * 说明：删除查看按钮里的村
	 * @param id
	 * @return
	 * @author  岳云清
	 * @time：2017年6月27日 下午5:33:05
	 */
	String deleteVillage(String id);

	/**
	 * 说明：保存查看按钮中的村信息
	 * @param villeageId   村Id
	 * @param id    视频会议Id
	 * @author  岳云清
	 * @time：2017年6月29日 上午10:22:32
	 */
	void addVillageShow(String villeageId, String id);

	/**
	 * 说明：获取所有未绑定的村
	 * @param id
	 * @return
	 * @author  岳云清
	 * @time：2017年6月29日 下午1:16:19
	 */
	List<VillageVo> unbounded(String id);

	List<VideoMettingVo> getByMettingAndVideo();

	void addMettingAndVideoform(String id);


}
