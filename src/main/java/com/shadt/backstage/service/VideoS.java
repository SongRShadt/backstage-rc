package com.shadt.backstage.service;
import java.util.List;

import com.shadt.backstage.vo.VideoVo;
import com.shadt.core.entity.User;
/**
 * 视频信息业务逻辑层接口
 * @author hebin
 *
 */
public interface VideoS {

	List<VideoVo> findAllBySql(String time,String term,int start,int end,User user);
	
	String rowCount(String sql,String fullTime,User user);

	String delete(String id);
}
