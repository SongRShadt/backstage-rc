package com.shadt.backstage.service;

import java.util.List;

import com.shadt.backstage.vo.VideoVo;
import com.shadt.core.entity.User;

public interface RoomService {

	/**
	 * 说明：分页显示数据
	 * 
	 * @param aoData
	 * @param time
	 * @param session
	 * @return
	 * @author 岳云清
	 * @time：2017年6月17日 下午4:28:34
	 */
	List<VideoVo> findAllBySql(String time, String term, int start, int end, User user);

	/**
	 * 说明：
	 * @param sql
	 * @param fullTime
	 * @param user
	 * @return
	 * @author  岳云清
	 * @time：2017年6月17日 下午4:31:52
	 */
	String rowCount(String sql, String fullTime, User user);

	/**
	 * 说明：删除数据
	 * 
	 * @param id
	 * @return
	 * @author 岳云清
	 * @time：2017年6月17日 下午4:28:52
	 */
	String delete(String id);
}
