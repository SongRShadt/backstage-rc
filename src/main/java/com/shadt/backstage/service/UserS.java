package com.shadt.backstage.service;

import java.util.List;

import com.shadt.backstage.vo.UserVo;
import com.shadt.core.entity.User;


/**
 * 用户信息业务层接口
 * @author Lingbin
 *
 */
public interface UserS {

	/**
	 * 获取所有用户信息
	 * 
	 * @return
	 */
	List<UserVo> getAll(String sSearch, int iDisplayStart, int iDisplayLength);

	
	/**
	 * 按镇编号获取用户信息
	 * 
	 * @param id
	 * @return
	 */
	UserVo getById(String id);
	
	
	/**
	 * 添加用户信息
	 * @param userVo
	 * @return
	 */
	String add(UserVo userVo,String townId);

	/**
	 * 删除用户信息
	 * 
	 * @param ID
	 * @param townId 
	 * @return
	 */
	String delete(String ID);

	/**
	 * 修改用户信息
	 * 
	 * @param user
	 * @return
	 */
	String update(UserVo user,String townId);

	/**
	 * 按条件获取记录数
	 * 
	 * @param sSearch
	 * @return
	 */
	String getCount(String sSearch);
	
	/**
	 * 修改密码
	 * @param user
	 * @return
	 */
	String updPwd(User user,String value);
	/**
	 * 验证用户名重复性
	 * 
	 */
	boolean checkValue(String value);


}
