package com.shadt.backstage.service;

import java.util.List;
import com.shadt.backstage.vo.IpConfigVo;

/**
 * ip地址业务逻辑层接口
 * 
 * @author hebin
 *
 */
public interface IpConfigS {
	
	List<IpConfigVo> getBySql(String sql);
}
