package com.shadt.backstage.service;

import java.util.List;

import com.shadt.backstage.vo.GeneralMeetingVideoVO;

public interface GeneralMeetingVideoService {

	/**
	 * 获取翻页
	 * @param time
	 * @param term
	 * @param iDisplayStart
	 * @param iDisplayLength
	 * @return
	 */
	List<GeneralMeetingVideoVO> findAllBySql(String time, String term, int iDisplayStart, int iDisplayLength);

	/**
	 * 获取条数
	 * @param term
	 * @param time
	 * @return
	 */
	String rowCount(String term, String time);

	/**
	 * 删除数据
	 * @param id
	 * @return
	 */
	String delete(Integer id);

}
