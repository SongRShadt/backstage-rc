package com.shadt.backstage.service;

import java.text.ParseException;
import java.util.List;

import com.shadt.backstage.vo.CameraImg;
import com.shadt.core.entity.User;

public interface CameraImgS {

	

	/**
	 * 按条件分页
	 * @param sSearch
	 * @param iDisplayStart
	 * @param iDisplayLength
	 * @return
	 */
	List<CameraImg> getAllBySearch(String sSearch, int iDisplayStart, int iDisplayLength,User user)throws ParseException;
	
	

	/**
	 * 说明：删除截图
	 * @param id
	 * @return
	 * @author  岳云清
	 * @time：2017年6月8日 上午9:12:01
	 */
	String delete(String id);



	/**
	 * 说明：查询总数量
	 * @param sSearch
	 * @param user
	 * @return
	 * @author  岳云清
	 * @time：2017年6月8日 上午9:12:14
	 */
	String getCount(String sSearch, User user);



	/**
	 * 说明：下载图片
	 * @param url 图片路径
	 * @return
	 * @author  岳云清
	 * @time：2017年6月12日 下午2:05:24
	 */
	String download(String url);
}
