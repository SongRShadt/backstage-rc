package com.shadt.backstage.service;

import java.util.List;

import com.shadt.backstage.vo.IpConfigVo;
import com.shadt.backstage.vo.MettingVideoVo;
import com.shadt.backstage.vo.TownVo;
import com.shadt.backstage.vo.VillageVo;

/**
 * 视频会议业务逻辑层接口
 * @author SongR
 *
 */
public interface VideoMettingService {

	/**
	 * 获取所有视频会议
	 */
	List<MettingVideoVo> getAll(String sSearch, int iDisplayStart, int iDisplayLength) throws Exception;

	/**
	 * 获取视频会议数量 
	 */
	Object countAll(String sSearch) throws Exception;

	
	/**
	 * 获取视频会议会场
	 */
	List<VillageVo> getAllTown() throws Exception;

	/**
	 * 获取所有sewise服务器
	 */
	List<IpConfigVo> getAllSewise() throws Exception;

	/**
	 * 添加视频会议
	 */
	int add(MettingVideoVo vo, String village, String sewise) throws Exception;

	/**
	 * 删除视频会议
	 */
	int delete(String id) throws Exception;

	/**
	 * 获取镇列表
	 */
	List<TownVo> getTownBind() throws Exception;

	/**
	 * 获取未绑定的村
	 */
	List<VillageVo> getVillageBind(String townid, String videoid) throws Exception;

	/**
	 * 绑定村权限
	 */
	int addBind(String vil, String videoid) throws Exception;

	/**
	 * 获取绑定的村列表
	 */
	List<VillageVo> getAllBind(String sSearch, int iDisplayStart, int iDisplayLength, String videoid) throws Exception;

	/**
	 * 获取绑定的村数量
	 */
	int countAllBind(String sSearch,String videoid) throws Exception;

	/**
	 * 删除村绑定
	 */
	int deleteBind(String videoid, String villageid) throws Exception;

	/**
	 * 获取视频会议
	 */
	MettingVideoVo get(String videoid) throws Exception;

	/**
	 * 修改视频会议
	 */
	int update(String id, String name, String rtsp, String sort, String villageid) throws Exception;

	/**
	 * 链接输入源
	 */
	void start(String videoid) throws Exception;

	/**
	 * 断开输入源
	 */
	void stop(String videoid) throws Exception;


}
