package com.shadt.backstage.service;

import java.util.List;

import com.shadt.backstage.vo.MettingVideoVo;

/**
 * 视频会议业务逻辑层接口
 * @author SongR
 *
 */
public interface MettingVideoService {

	/**
	 * 查询该村绑定的视频会议列表
	 */
	List<MettingVideoVo> getByVillage(String sSearch, int iDisplayStart, int iDisplayLength, String villageId) throws Exception;

	
	/**
	 * 获取村绑定的视频会议数量
	 */
	int countByVillage(String sSearch,String villageId) throws Exception;

	/**
	 * 删除视频会议绑定
	 */
	int delete(String videoId, String villageId) throws Exception;

	/**
	 * 获取所有未绑定视频会议
	 */
	List<MettingVideoVo> getAll(String villageId) throws Exception;

	
	/**
	 * 添加视频会议绑定
	 */
	int add(String villageId, String videoId) throws Exception;
	
}
