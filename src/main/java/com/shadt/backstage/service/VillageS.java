package com.shadt.backstage.service;

import java.util.List;

import com.shadt.backstage.vo.VillageVo;
import com.shadt.core.entity.User;

/**
 * 村信息业务层接口
 * 
 * @author liusongren
 * 
 */
public interface VillageS {

	/**
	 * 获取所有村信息
	 * 
	 * @return
	 */
	List<VillageVo> getAll(String sSearch, int iDisplayStart, int iDisplayLength,User user) throws Exception;

	
	List<VillageVo> getAll(String townId);
	
	/**
	 * 按村编号获取村信息
	 * 
	 * @param id
	 * @return
	 */
	VillageVo getById(String id);

	/**
	 * 根据镇编号获取村列表
	 * 
	 * @param id
	 * @return
	 */
	List<VillageVo> getByTownId(String id);

	/**
	 * 添加村信息
	 * 
	 * @param village
	 * @return
	 */
	String add(VillageVo village);

	/**
	 * 删除村信息
	 * 
	 * @param id
	 * @param stbNo 
	 * @return
	 */
	String delete(String id, String stbNo);

	/**
	 * 修改村信息
	 * 
	 * @param village
	 * @return
	 */
	String update(VillageVo village);

	/**
	 * 按条件获取记录数
	 * 
	 * @param sSearch
	 * @return
	 */
	String getCount(String sSearch,User user);

	/**
	 * 验证值是否存在
	 * @param trim
	 * @return
	 */
	boolean checkValue(String value );


	List<VillageVo> getVillageByVideo(String townid);

}
