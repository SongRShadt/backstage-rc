package com.shadt.backstage.service;

import java.util.List;

import com.shadt.backstage.vo.TownVideoVo;
import com.shadt.backstage.vo.TownVo;
import com.shadt.core.entity.User;

/**
 * 镇信息业务逻辑层接口
 * @author liusongren
 *
 */
public interface TownS {

	/**
	 * 获取所有镇信息
	 * @return
	 */
	List<TownVo> getAll(User user);

	/**
	 * 根据镇编号获取镇信息
	 * @param id
	 * @return
	 */
	TownVo getById(String id);

	/**
	 * 添加镇信息
	 * @param town
	 * @return
	 */
	String add(TownVo town);

	/**
	 * 删除镇信息
	 * @param id
	 * @return
	 */
	String delete(String id);

	/**
	 * 修改镇信息
	 * @param town
	 * @return
	 */
	int updata(TownVo town);

	/**
	 * 验证值是否存在
	 * @param value
	 * @return
	 */
	boolean checkValue(String value);

	/**
	 * 按条件分页查询
	 * @param sSearch
	 * @param iDisplayStart
	 * @param iDisplayLength
	 * @return
	 */
	List<TownVo> getAllBySearch(String sSearch, int iDisplayStart, int iDisplayLength,User user) throws Exception;

	/**
	 * 按条件获取记录总数
	 * @param sSearch
	 * @return
	 */
	String getCount(String sSearch,User user);

	List<TownVideoVo> getByAllTown();

}
