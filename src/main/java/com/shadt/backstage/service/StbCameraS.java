package com.shadt.backstage.service;

import java.util.List;

import com.shadt.backstage.vo.StbCameraVo;

/**
 * 机顶盒与摄像头关系业务逻辑层接口
 * @author liusongren
 *
 */
public interface StbCameraS {

	/**
	 * 获取所有机顶盒与摄像头关系
	 * @return
	 */
	List<StbCameraVo> getAll();

	/**
	 * 根据机顶盒编号获取机顶盒与摄像头关系
	 * @param stbNo
	 * @return
	 */
	List<StbCameraVo> getByStbNo(String stbNo);

	/**
	 * 根据摄像头编号获取机顶盒与摄像头关系
	 * @param cameraNo
	 * @return
	 */
	List<StbCameraVo> getByCameraNo(String cameraNo,String stbNo);

	/**
	 * 添加机顶盒与摄像头关系
	 * @param stbCamera
	 * @return
	 */
	String add(StbCameraVo stbCamera);

	/**
	 * 删除机顶盒与摄像头关系
	 * @param id
	 * @return
	 */
	String delete(StbCameraVo stbCamera);

	/**
	 * 修改机顶盒与摄像头关系
	 * @param stbCamera
	 * @return
	 */
	String update(StbCameraVo stbCamera);

	/**
	 * 绑定摄像头
	 * @param type
	 * @param villageId
	 * @return
	 */
	String bindCamera(String type, String villageId);

}
