package com.shadt.backstage.service;

import com.shadt.backstage.vo.User_townVo;

public interface User_townS {

	/**
	 * 获取单个vo
	 */
	
	User_townVo get(String sql);
	
	/**
	 * 添加记录
	 */
	String add(User_townVo user_townVo);
}
