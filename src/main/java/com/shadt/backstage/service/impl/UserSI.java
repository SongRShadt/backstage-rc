package com.shadt.backstage.service.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.backstage.dao.TownD;
import com.shadt.backstage.dao.UserD;
import com.shadt.backstage.dao.User_townD;
import com.shadt.backstage.service.UserS;
import com.shadt.backstage.vo.TownVo;
import com.shadt.backstage.vo.UserVo;
import com.shadt.backstage.vo.User_townVo;
import com.shadt.core.entity.User;

/**
 * 用户信息业务层实现类
 * 
 * @author Lingbin
 * 
 */
@Service
public class UserSI implements UserS {

	@Autowired
	UserD dao;

	@Autowired
	User_townD udao;

	@Autowired
	TownD tdao;

	/**
	 * 分页获取所有用户信息
	 * 
	 * @param sSearch
	 * @param iDisplayStart
	 * @param iDisplayLength
	 * @return
	 */

	@Override
	public List<UserVo> getAll(String sSearch, int iDisplayStart,
			int iDisplayLength) {
		String sql = "select * from user where 1=1";
		if (!sSearch.trim().equals("")) {
			sql += " and name like '%" + sSearch + "%' or userName like '%"+ sSearch + "%'";
		}
		sql += " limit " + iDisplayStart + "," + iDisplayLength;
		List<UserVo> uList = dao.getBySql(sql);
		for (UserVo userVo : uList) {
			try {
				if (userVo.getLevel() == 1) {
					String utsql = "select * from user_town where userid='"+ userVo.getId() + "'";
					User_townVo uTownVo = udao.get(utsql);
					if (uTownVo != null) {
						String townId = uTownVo.getTownID();
						String tsql = "select * from town_info where townid='"+ townId + "'";
						TownVo townVo = tdao.getBySql(tsql).get(0);
						if (townVo != null) {
							String townName = townVo.getTownName();
							userVo.setTownName(townName);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return uList;
	}

	@Override
	public UserVo getById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 添加用户
	 * 
	 * @param userVo
	 * @param townId
	 * @return
	 */
	@Override
	public String add(UserVo userVo, String townId) {
		userVo.setId(UUID.randomUUID().toString());
		String sql = "INSERT INTO user VALUES('" + userVo.getId() + "',1,'" 
				+ userVo.getName() + "','" + userVo.getPassword() + "','','"
				+ userVo.getSex() + "','" + userVo.getUserName() + "')";
		String count = "";
		count = dao.update(sql);
		if (townId != null) {
			User_townVo user_townVo = new User_townVo();
			user_townVo.setId(UUID.randomUUID().toString());
			user_townVo.setTownID(townId);
			user_townVo.setUserID(userVo.getId());
			String utsql = "insert into user_town values('"
					+ user_townVo.getId() + "','" + user_townVo.getUserID()
					+ "','" + user_townVo.getTownID() + "')";
			udao.update(utsql);
		}
		return count;
	}

	/**
	 * 删除用户
	 * 
	 * @param ID
	 * @return
	 */
	@Override
	public String delete(String ID) {
		String sql = "delete from user where id='" + ID + "'";
		String utsql = "delete from user_town where userid='" + ID + "'";
		String count = "";
		count = dao.update(sql);
		if (Integer.parseInt(count) > 0) {
			udao.update(utsql);
		}
		return count;
	}

	/**
	 * 修改用户信息
	 * 
	 * @param user
	 * @return
	 */
	@Override
	public String update(UserVo user, String townId) {
		String sql = "update user set name='" + user.getName()
				+ "', password='" + user.getPassword() + "' where id='"
				+ user.getId() + "'";
		String utsql = "update user_town set townid='" + townId
				+ "' where userid='" + user.getId() + "'";
		String count = dao.update(sql);
		if (Integer.parseInt(count) > 0) {
			udao.update(utsql);
		}
		return count;
	}

	/**
	 * 获取记录数
	 * 
	 * @param sSearch
	 * @return
	 */
	@Override
	public String getCount(String sSearch) {
		String sql = "select count(*) from user where 1=1";
		if (!sSearch.trim().equals("")) {
			sql += "  and name like '%" + sSearch + "%' or userName like '%"
					+ sSearch + "%'";
		}
		return dao.getCount(sql);
	}

	/**
	 * 验证存在性
	 * 
	 * @param value
	 * @return
	 */
	@Override
	public boolean checkValue(String value) {
		String sql = "select count(*) from user where userName='" + value + "'";
		String count = dao.getCount(sql);
		if (Integer.parseInt(count) > 0) {
			return false;
		}
		return true;
	}

	@Override
	public String updPwd(User u,String value) {
		String sql = "update user set password='"+value+"' where id="+u.getId()+"";
		return dao.update(sql);
	}

}
