package com.shadt.backstage.service.impl;

import com.shadt.backstage.dao.CameraD;
import com.shadt.backstage.dao.StbCameraD;
import com.shadt.backstage.dao.StbD;
import com.shadt.backstage.dao.VillageD;
import com.shadt.backstage.service.StbCameraS;
import com.shadt.backstage.vo.CameraVo;
import com.shadt.backstage.vo.StbCameraVo;
import com.shadt.backstage.vo.StbVo;
import com.shadt.backstage.vo.VillageVo;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//@Service
public class StbCameraSI2 implements StbCameraS {

//	@Autowired
	StbCameraD dao;
//	@Autowired
	StbD sdao;
//	@Autowired
	CameraD cdao;
//	@Autowired
	VillageD vDao;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List getAll() {
		String sql = "select * from stb_camera_rlt where 1=1";
		List sclist = this.dao.getBySql(sql);
		Iterator var4 = sclist.iterator();

		while (var4.hasNext()) {
			StbCameraVo sc = (StbCameraVo) var4.next();
			List sl = this.sdao.getBySql("select * from stb_info where stbno=\'" + sc.getStbNo() + "\'");
			List cl = this.cdao.getBySql("select * from camera_info where camerano = \'" + sc.getCameraNo() + "\'");
			sc.setCameras(cl);
			sc.setStbs(sl);
		}

		return sclist;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List getByStbNo(String stbNo) {
		String sql = "select * from stb_camera_rlt where stbno=\'" + stbNo + "\'";
		List scList = this.dao.getBySql(sql);
		Iterator var5 = scList.iterator();

		while (var5.hasNext()) {
			StbCameraVo sc = (StbCameraVo) var5.next();
			List cList = this.cdao.getBySql("select * from camera_info where camerano = \'" + sc.getCameraNo() + "\'");
			if (cList.size() > 0) {
				sc.setCamera((CameraVo) cList.get(0));
			} else {
				System.err.println("没有此摄像头！摄像头编号为：" + sc.getCameraNo());
			}
		}

		return scList;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List getByCameraNo(String cameraNo, String stbNo) {
		String sql = "select * from stb_camera_rlt where camerano = \'" + cameraNo + "\' and stbNo = \'" + stbNo + "\'";
		return this.dao.getBySql(sql);
	}

	public String add(StbCameraVo stbCamera) {
		String sql = "insert into stb_camera_rlt(stbno,camerano,rlt_type) values(\'" + stbCamera.getStbNo() + "\',\'"
				+ stbCamera.getCameraNo() + "\',\'" + stbCamera.getRltType() + "\')";
		return this.dao.add(sql);
	}

	public String delete(StbCameraVo stbCamera) {
		String sql = "delete from stb_camera_rlt where stbno = \'" + stbCamera.getStbNo() + "\' and camerano =\'"
				+ stbCamera.getCameraNo() + "\'";
		return this.dao.delete(sql);
	}

	public String update(StbCameraVo stbCamera) {
		return null;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public String bindCamera(String type, String villageId) {
		List sList = this.sdao.getBySql("select * from stb_info where villageid = \'" + villageId + "\'");
		StbVo stb = new StbVo();
		if (sList.size() > 0) {
			stb = (StbVo) sList.get(0);
		} else {
			System.out.println("本村没有绑定摄像头！" + villageId);
		}

		this.dao.delete("delete from stb_camera_rlt where stbno=\'" + stb.getStbNo() + "\'");
		VillageVo village = (VillageVo) this.vDao
				.getBySql("select * from village_info where villageid = \'" + villageId + "\'").get(0);
		Object cList = new ArrayList();
		List isHasCameraNoList;
		Iterator var10;
		if (type.trim().equals("0")) {
			cList = this.cdao.getBySql(
					"select * from camera_info where camerano not in (\'90001\',\'90002\',\'90003\',\'90004\',\'99998\',\'99999\')");
		} else if (type.trim().equals("1")) {
			isHasCameraNoList = this.vDao
					.getBySql("select * from village_info where townid=\'" + village.getTownId() + "\'");
			var10 = isHasCameraNoList.iterator();

			while (var10.hasNext()) {
				VillageVo cameraVo = (VillageVo) var10.next();
				List cvo = this.cdao.getBySql("select * from camera_info where villageid=\'" + cameraVo.getVillageId()
						+ "\' and camerano not in (\'90001\',\'90002\',\'90004\',\'90003\',\'99998\',\'99999\')");
				Iterator var13 = cvo.iterator();

				while (var13.hasNext()) {
					CameraVo c = (CameraVo) var13.next();
					((List) cList).add(c);
				}
			}
		} else if (type.trim().equals("2")) {
			isHasCameraNoList = this.cdao
					.getBySql("select * from camera_info a,village_info b where a.villageid=b.villageid and b.townid=\'"
							+ village.getTownId() + "\' and b.orgcode<\'0\'");
			cList = this.cdao.getBySql("select * from camera_info where villageid= \'" + villageId
					+ "\' and camerano not in (\'90001\',\'90002\',\'90003\',\'90004\',\'99998\',\'99999\')");
			if (isHasCameraNoList.size() > 0) {
				((List) cList).add((CameraVo) isHasCameraNoList.get(0));
			}
		}

		Iterator cameraVo1 = ((List) cList).iterator();

		while (cameraVo1.hasNext()) {
			CameraVo isHasCameraNoList1 = (CameraVo) cameraVo1.next();
			this.dao.add("insert into stb_camera_rlt values(\'" + stb.getStbNo().trim() + "\',\'"
					+ isHasCameraNoList1.getCameraNo() + "\',\'1\');");
		}

		isHasCameraNoList = this.cdao.getBySql(
				"select * from camera_info where camerano in (\'90001\',\'90002\',\'90003\',\'99998\',\'90004\',\'99999\')");
		var10 = isHasCameraNoList.iterator();

		while (var10.hasNext()) {
			CameraVo cameraVo2 = (CameraVo) var10.next();
			this.dao.add("insert into stb_camera_rlt values(\'" + stb.getStbNo().trim() + "\',\'"
					+ cameraVo2.getCameraNo() + "\',\'0\');");
		}

		return String.valueOf(((List) cList).size());
	}
}