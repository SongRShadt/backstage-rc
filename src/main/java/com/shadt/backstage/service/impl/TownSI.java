package com.shadt.backstage.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.backstage.dao.CameraD;
import com.shadt.backstage.dao.TownD;
import com.shadt.backstage.dao.User_townD;
import com.shadt.backstage.dao.VillageD;
import com.shadt.backstage.service.TownS;
import com.shadt.backstage.vo.CameraVo;
import com.shadt.backstage.vo.TownVideoVo;
import com.shadt.backstage.vo.TownVo;
import com.shadt.backstage.vo.User_townVo;
import com.shadt.backstage.vo.VillageVo;
import com.shadt.core.entity.User;
import com.shadt.backstage.dao.VideoMettingManageDao;

/**
 * 镇信息业务逻辑层实现类
 * 
 * @author liusongren
 *
 */
@Service
public class TownSI implements TownS {

	@Autowired
	User_townD udao;
	
	@Autowired
	TownD dao;
	
	@Autowired
	VillageD vDao;
	
	@Autowired
	CameraD cDao;
	
	@Autowired
	VideoMettingManageDao VideoMettingManageDao;

	/**
	 * 实现获取所有镇信息
	 */
	public List<TownVo> getAll(User user) {
		String sql="";
		if(user.getLevel()==0){
			sql = "select * from town_info";
		}
		else{
			User_townVo uTownVo = udao.get("select * from user_town where userid='"+user.getId()+"'");
			sql="select * from town_info where townid='"+uTownVo.getTownID()+"'";
		}
		List<TownVo> tList = dao.getBySql(sql);
		for (TownVo town : tList) {
			List<VillageVo> vL=vDao.getBySql("select * from village_info where townid='"+town.getTownId()+"'");
			for (VillageVo village : vL) {
				List<CameraVo> cL = cDao.getBySql("select * from camera_info where villageid='"+village.getVillageId()+"' and camerano not in ('90001','90002','90003','99998','99999')");
				village.setCameras(cL);
			}
			town.setVillages(vL);
		}
		return tList;
	}

	/**
	 * 实现根据镇id获取镇信息
	 */
	public TownVo getById(String id) {
		String sql = "select * from town_info where townid='"+id+"'";
		List<TownVo> tList = dao.getBySql(sql);
		return tList.get(0);
	}

	/**
	 * 实现添加镇信息
	 */
	public String add(TownVo town) {
		if(null==town.getTownSort()){
			town.setTownSort(new Long(0));
		}
		String sql = "insert into town_info(townid,townname,townkeyvalue,townsort) values('" + UUID.randomUUID() + "','"
				+ town.getTownName() + "','" + town.getTownKeyValue() + "','"+town.getTownSort()+"')";
		return dao.update(sql) + "";
	}

	/**
	 * 实现删除镇信息
	 */
	public String delete(String id) {
		String sql = "delete from town_info where townid ='" + id + "'";
		return dao.update(sql) + "";
	}

	/**
	 * 实现修改镇信息
	 */
	public int updata(TownVo town) {
		String sql = "update  town_info set townname = '" + town.getTownName() + "',townkeyvalue='"
				+ town.getTownKeyValue() + "', townsort='"+town.getTownSort()+"' where townid='" + town.getTownId() + "'";
		return dao.update(sql);
	}

	/**
	 * 实现验证值是否存在
	 */
	public boolean checkValue(String value) {
		String sql = "select count(*) from town_info where townname='"+value+"'";
		int s = dao.checkValue(sql);
		if(s>0)
			return false;
		else
			return true;
	}

	/**
	 * 实现按条件分页获取镇信息
	 */
	public List<TownVo> getAllBySearch(String sSearch, int iDisplayStart, int iDisplayLength,User user)throws Exception {
		String sql="";
		if(user.getLevel()==0){
			sql = "select * from town_info where 1=1";
		}else{
			User_townVo uTownVo=udao.get("select * from user_town where userid='"+user.getId()+"'");
			sql= "select * from town_info where townid='"+uTownVo.getTownID()+"'";
		}
		List<TownVo> townList = new ArrayList<TownVo>();
		if(!sSearch.equals("")){
			sql+=" and townname like '%"+sSearch+"%'";
		}
		sql+= " order by townsort limit "+iDisplayStart +","+iDisplayLength;
		townList= dao.getBySql(sql);
		return townList;
	}

	/**
	 * 实现按条件获取记录总数
	 */
	public String getCount(String sSearch,User user) {
		String sql="";
		if(user.getLevel()==0){
			sql = "select count(*) from town_info where 1=1";
		}
		else {
			User_townVo uTownVo=udao.get("select * from user_town where userid='"+user.getId()+"'");
			sql="select count(*) from town_info where townid='"+uTownVo.getTownID()+"' ";
		}
		String count = "";
		if(!sSearch.equals("")){
			sql+=" and townname like '%"+sSearch+"%'";
		}
		try {
			count=dao.getCount(sql);
		} catch (Exception e) {
			System.err.println(new Exception("获取镇信息总记录出错！"));
		}
		return count;
	}

	@Override
	public List<TownVideoVo> getByAllTown() {
		List arrayList = new ArrayList<>();
		String sql="SELECT villageid,villagename FROM village_info WHERE villagelevel=0 OR villagelevel=1";
		List list = VideoMettingManageDao.getBySql(sql);
		Iterator it = list.iterator();
		if (list.size() > 0) {
			while (it.hasNext()) {
				Map m = (Map) it.next();
				TownVideoVo townVideoVo = new TownVideoVo();
				townVideoVo.setVillageId(m.get("villageid") != null ? m.get("villageid").toString() : "");
				townVideoVo.setVillageName(m.get("villagename") != null ? m.get("villagename").toString() : "");
				
				arrayList.add(townVideoVo);
			}
		}
		return arrayList;
	}

}
