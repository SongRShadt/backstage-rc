package com.shadt.backstage.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.shadt.backstage.dao.MettingVideoDao;
import com.shadt.backstage.dao.TownD;
import com.shadt.backstage.dao.VillageD;
import com.shadt.backstage.dao.ipConfigD;
import com.shadt.backstage.model.AddSewiseReturn;
import com.shadt.backstage.model.UpdateSewiseReturn;
import com.shadt.backstage.service.VideoMettingService;
import com.shadt.backstage.vo.IpConfigVo;
import com.shadt.backstage.vo.MettingVideoVo;
import com.shadt.backstage.vo.TownVo;
import com.shadt.backstage.vo.VillageVo;
import com.shadt.core.util.MD5Util;
import com.shadt.core.util.SendUtil;

/**
 * 视频会议业务逻辑层接口
 * 
 * @author SongR
 *
 */
@Service
public class VideoMettingServiceImpl implements VideoMettingService {
	@Autowired
	MettingVideoDao dao;
	@Autowired
	VillageD vdao;
	@Autowired
	ipConfigD idao;
	@Autowired
	TownD tdao;

	/**
	 * 获取所有视频会议
	 */
	@Override
	public List<MettingVideoVo> getAll(String sSearch, int iDisplayStart, int iDisplayLength) throws Exception {
		String sql = "select a.*,b.villagename from metting_video a,village_info b where a.village_id=b.villageid";
		if (!"".equals(sSearch)) {
			sql += " and a.video_name like'%" + sSearch + "%'";
		}
		sql += " order by a.video_sort limit " + iDisplayStart + "," + iDisplayLength;
		List<MettingVideoVo> vos = dao.getBySql(sql);
		return vos;
	}

	/**
	 * 获取视频会议数
	 */
	@Override
	public Object countAll(String sSearch) throws Exception {
		String sql = "select count(1) from metting_video a,village_info b where a.village_id=b.villageid ";
		if (!"".equals(sSearch)) {
			sql += " and a.video_name like'%" + sSearch + "%'";
		}
		int count = dao.countSql(sql);
		return count;
	}

	/**
	 * 获取视频会议会场
	 */
	@Override
	public List<VillageVo> getAllTown() throws Exception {
		String sql = "select * from village_info where  orgcode<=0 and villagestatus=0 order by villagesort";
		List<VillageVo> vos = vdao.getBySql(sql);
		return vos;
	}

	/**
	 * 获取所有sewise服务器
	 */
	@Override
	public List<IpConfigVo> getAllSewise() throws Exception {
		String sql = "select * from ipconfig";
		List<IpConfigVo> vos = idao.getBySql(sql);
		return vos;
	}

	/**
	 * 添加视频会议
	 */
	@Override
	public int add(MettingVideoVo vo, String village, String sewise) throws Exception {
		AddSewiseReturn sr = new AddSewiseReturn();
		String timestamp = (((new Date()).getTime()) / 1000) + "";
		Map<String, String> params = new HashMap<String, String>(0);
		List<IpConfigVo> ip = idao.getBySql("select * from ipconfig where id='" + sewise + "'");
		String accessid = ip.get(0).getAccessid();
		params.put("do", "stream");
		params.put("time", timestamp);
		params.put("code", MD5Util.md5(accessid + timestamp + "stream"));
		params.put("op", "add");
		params.put("type", "2");
		params.put("mode_type", "1");
		params.put("ip", vo.getRtsp());
		params.put("name", vo.getName() + "视频会议");
		String result = SendUtil.post(ip.get(0).getIpUrl(), params);
		sr = JSON.parseObject(result, AddSewiseReturn.class);
		int count = 0;
		if (sr != null && sr.isSuccess()) {
			String sourceid = sr.getSourceid();
			String url = ip.get(0).getCameraUrl()+"/" + sr.getSourceid()+".m3u8";
			String sql = "insert INTO metting_video (video_name,rtsp,video_sort,sourceid,video_status,video_type,video_url,village_id,sewiseid) "
					+ "values('" + vo.getName() + "','" + vo.getRtsp() + "','" + vo.getSort() + "','" + sourceid
					+ "','0','0','" + url + "','" + village + "','" + sewise + "')";
			count = dao.update(sql);
		}
		return count;
	}

	/**
	 * 删除视频会议
	 */
	@Override
	public int delete(String id) throws Exception {
		String sqla = "select * from metting_video where id="+id;
		MettingVideoVo vo = dao.getBySql(sqla).get(0);
		String sqli = "select * from ipconfig where id="+vo.getSewiseid();
		List<IpConfigVo> ip = idao.getBySql(sqli);
		String timestamp = (((new Date()).getTime()) / 1000) + "";
		Map<String, String> params = new HashMap<String, String>(0);
		params.put("do", "stream");
		params.put("time", timestamp);
		params.put("code", MD5Util.md5(ip.get(0).getAccessid() + timestamp + "stream" + vo.getSourceid()));
		params.put("op", "delete");
		params.put("sourceid", vo.getSourceid());
		String result = SendUtil.post(ip.get(0).getIpUrl(), params);
		UpdateSewiseReturn ur = JSON.parseObject(result, UpdateSewiseReturn.class);
		int count = 0;
		if (ur != null && ur.isSuccess()) {
			String sql = "delete from metting_video where id=" + id;
			count = dao.update(sql);
		}
		return count;
	}

	/**
	 * 获取镇列表
	 */
	@Override
	public List<TownVo> getTownBind() throws Exception {
		String sql = "select * from town_info";
		List<TownVo> vos = tdao.getBySql(sql);
		return vos;
	}

	/**
	 * 获取未绑定的村
	 */
	@Override
	public List<VillageVo> getVillageBind(String townid, String videoid) throws Exception {
		String sql = "select * from village_info where villageid not in (select villageid from metting_village_video where videoid="
				+ videoid + ") and townid= " + townid + " and orgcode>0 order by villageSort";
		List<VillageVo> vos = vdao.getBySql(sql);
		return vos;
	}

	/**
	 * 绑定村权限
	 */
	@Override
	public int addBind(String vil, String videoid) throws Exception {
		String sql = "insert into metting_village_video (videoid,villageid) values ('" + videoid + "','" + vil + "')";
		int count = dao.update(sql);
		return count;
	}

	/**
	 * 获取绑定的村列表
	 */
	@Override
	public List<VillageVo> getAllBind(String sSearch, int iDisplayStart, int iDisplayLength, String videoid)
			throws Exception {
		String sql = "select * from village_info a,metting_village_video b where a.villageid=b.villageid and b.videoid="
				+ videoid;
		if (!"".equals(sSearch)) {
			sql += " and a.villagename like '%" + sSearch + "%'";
		}
		sql += " limit " + iDisplayStart + "," + iDisplayLength;
		List<VillageVo> vos = vdao.getBySql(sql);
		return vos;
	}

	/**
	 * 获取绑定的村数量
	 */
	@Override
	public int countAllBind(String sSearch, String videoid) throws Exception {
		String sql = "select count(1) from village_info a,metting_village_video b where a.villageid=b.villageid and b.videoid="
				+ videoid;
		if (!"".equals(sSearch)) {
			sql += " and a.villagename like '%" + sSearch + "%'";
		}

		return dao.countSql(sql);
	}

	/**
	 * 删除村绑定
	 */
	@Override
	public int deleteBind(String videoid, String villageid) throws Exception {
		String sql = "delete from metting_village_video where villageid='" + villageid + "' and videoid=" + videoid;
		return dao.update(sql);
	}

	/**
	 * 获取视频会议
	 */
	@Override
	public MettingVideoVo get(String videoid) throws Exception {
		String sql = "select * from metting_video where id=" + videoid;
		List<MettingVideoVo> vos = dao.getBySql(sql);
		return vos.get(0);
	}

	/**
	 * 修改视频会议
	 */
	@Override
	public int update(String id, String name, String rtsp, String sort, String villageid) throws Exception {
		String sql = "select * from metting_video where id=" + id;
		MettingVideoVo vo = dao.getBySql(sql).get(0);
		List<IpConfigVo> ip = idao.getBySql("select * from ipconfig where id =" + vo.getSewiseid());
		String timestamp = (((new Date()).getTime()) / 1000) + "";
		Map<String, String> params = new HashMap<String, String>(0);
		params.put("do", "stream");
		params.put("time", timestamp);
		params.put("code", MD5Util.md5(ip.get(0).getAccessid() + timestamp + "stream" + vo.getSourceid()));
		params.put("op", "update");
		params.put("sourceid", vo.getSourceid());
		params.put("type", "2");
		params.put("mode_type", "1");
		params.put("ip", rtsp);
		params.put("name", name + "视频会议");
		String result = SendUtil.post(ip.get(0).getIpUrl(), params);
		UpdateSewiseReturn ur = JSON.parseObject(result, UpdateSewiseReturn.class);
		if (ur != null && ur.isSuccess()) {
			String sqla = "update metting_video set video_name='" + name + "',rtsp='" + rtsp + "',video_sort='" + sort
					+ "',village_id='" + villageid + "' where id=" + id;
			return dao.update(sqla);
		}
		return 0;
	}

	/**
	 * 链接输入源
	 */
	@Override
	public void start(String videoid) throws Exception {
		String sql = "select * from metting_video where id=" + videoid;
		MettingVideoVo vo = dao.getBySql(sql).get(0);
		List<IpConfigVo> ip = idao.getBySql("select * from ipconfig where id =" + vo.getSewiseid());
		String timestamp = (((new Date()).getTime()) / 1000) + "";
		Map<String, String> params = new HashMap<String, String>(0);
		params.put("do", "stream");
		params.put("time", timestamp);
		params.put("code", MD5Util.md5(ip.get(0).getAccessid() + timestamp + "stream" + vo.getSourceid()));
		params.put("op", "updatestatus");
		params.put("sourceid", vo.getSourceid());
		params.put("status", "1");
		String result = SendUtil.post(ip.get(0).getIpUrl(), params);
		UpdateSewiseReturn ur = JSON.parseObject(result, UpdateSewiseReturn.class);
		if (ur != null && ur.isSuccess()) {
			String sqla = "update metting_video set video_status='0' where id=" + videoid;
			dao.update(sqla);
		}
	}

	/**
	 * 断开输入源
	 */
	@Override
	public void stop(String videoid) throws Exception {
		String sql = "select * from metting_video where id=" + videoid;
		MettingVideoVo vo = dao.getBySql(sql).get(0);
		List<IpConfigVo> ip = idao.getBySql("select * from ipconfig where id =" + vo.getSewiseid());
		String timestamp = (((new Date()).getTime()) / 1000) + "";
		Map<String, String> params = new HashMap<String, String>(0);
		params.put("do", "stream");
		params.put("time", timestamp);
		params.put("code", MD5Util.md5(ip.get(0).getAccessid() + timestamp + "stream" + vo.getSourceid()));
		params.put("op", "updatestatus");
		params.put("sourceid", vo.getSourceid());
		params.put("status", "0");
		String result = SendUtil.post(ip.get(0).getIpUrl(), params);
		UpdateSewiseReturn ur = JSON.parseObject(result, UpdateSewiseReturn.class);
		if (ur != null && ur.isSuccess()) {
			String sqla = "update metting_video set video_status='1' where id=" + videoid;
			dao.update(sqla);
		}
	}

}
