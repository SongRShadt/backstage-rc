package com.shadt.backstage.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.backstage.dao.CameraD;
import com.shadt.backstage.dao.StbCameraD;
import com.shadt.backstage.dao.StbD;
import com.shadt.backstage.dao.VillageD;
import com.shadt.backstage.service.StbCameraS;
import com.shadt.backstage.vo.CameraVo;
import com.shadt.backstage.vo.StbCameraVo;
import com.shadt.backstage.vo.StbVo;
import com.shadt.backstage.vo.VillageVo;

/**
 * 机顶盒与摄像头关系业务逻辑层实现类
 * 
 * @author liusongren
 *
 */
@Service
public class StbCameraSI implements StbCameraS{
	@Autowired
	StbCameraD dao;

	@Autowired
	StbD sdao;

	@Autowired
	CameraD cdao;
	
	@Autowired
	VillageD vDao;

	/**
	 * 实现获取所有摄像头与机顶盒关系
	 */
	public List<StbCameraVo> getAll() {
		String sql = "select * from stb_camera_rlt where 1=1";
		List<StbCameraVo> sclist = dao.getBySql(sql);
		for (StbCameraVo sc : sclist) {
			List<StbVo> sl = sdao.getBySql("select * from stb_info where stbno='" + sc.getStbNo() + "'");
			List<CameraVo> cl = cdao.getBySql("select * from camera_info where camerano = '" + sc.getCameraNo() + "'");
			sc.setCameras(cl);
			sc.setStbs(sl);
		}
		return sclist;
	}

	/**
	 * 实现根据机顶盒编号获取摄像头与机顶盒关系
	 */
	public List<StbCameraVo> getByStbNo(String stbNo) {
		String sql = "select * from stb_camera_rlt where stbno='"+stbNo+"'";
		List<StbCameraVo> scList = dao.getBySql(sql);
		for (StbCameraVo sc : scList) {
			List<CameraVo> cList=cdao.getBySql("select * from camera_info where camerano = '"+sc.getCameraNo()+"'");
			if(cList.size()>0){
				sc.setCamera(cList.get(0));
			}else{
				System.err.println("没有此摄像头！摄像头编号为："+sc.getCameraNo());
			}
		}
		return scList;
	}

	/**
	 * 实现根据摄像头编号获取摄像头与机顶盒关系
	 */
	public List<StbCameraVo> getByCameraNo(String cameraNo, String stbNo) {
		String sql = "select * from stb_camera_rlt where camerano = '" + cameraNo + "' and stbNo = '" + stbNo + "'";
		return dao.getBySql(sql);
	}

	/**
	 * 实现添加摄像头与机顶盒关系
	 */
	public String add(StbCameraVo stbCamera) {
		String sql = "insert into stb_camera_rlt(stbno,camerano,rlt_type) values('" + stbCamera.getStbNo() + "','"
				+ stbCamera.getCameraNo() + "','" + stbCamera.getRltType() + "')";
		return dao.add(sql);
	}

	/**
	 * 实现删除摄像头与机顶盒关系
	 */
	public String delete(StbCameraVo stbCamera) {
		String sql = "delete from stb_camera_rlt where stbno = '" + stbCamera.getStbNo() + "' and camerano ='"
				+ stbCamera.getCameraNo() + "'";
		return dao.delete(sql);
	}

	/**
	 * 实现修改摄像头与机顶盒关系
	 */
	public String update(StbCameraVo stbCamera) {
		return null;
	}
	
	/**
	 * 实现绑定摄像头
	 */
	public String bindCamera(String type, String villageId) {
		List<StbVo> sList = sdao.getBySql("select * from stb_info where villageid = '"+villageId+"'");
		StbVo stb = new StbVo();
		if(sList.size()>0){
			stb = sList.get(0);
		}else{
			System.out.println("本村没有绑定摄像头！"+villageId);
		}
		@SuppressWarnings("unused")
		String dCount = dao.delete("delete from stb_camera_rlt where stbno='"+stb.getStbNo()+"'");
		VillageVo village = vDao.getBySql("select * from village_info where villageid = '"+villageId+"'").get(0);
		List<CameraVo> cList = new ArrayList<CameraVo>();
		if(type.trim().equals("0")){
			cList = cdao.getBySql("select * from camera_info where camerano not in ('90001','90002','90003','90004','99998','99999')");
		}else if(type.trim().equals("1")){
			List<VillageVo> vList = vDao.getBySql("select * from village_info where townid='"+village.getTownId()+"'");
			for (VillageVo v : vList) {
				List<CameraVo> cvo =cdao.getBySql("select * from camera_info where villageid='"+v.getVillageId()+"' and camerano not in ('90001','90002','90004','90003','99998','99999')");
				for (CameraVo c : cvo) {
					cList.add(c);
				}
			}
		}else if(type.trim().equals("2")){
			List<CameraVo> cv = cdao.getBySql("select * from camera_info a,village_info b where a.villageid=b.villageid and b.townid='"+village.getTownId()+"' and b.orgcode='-1'");
			cList = cdao.getBySql("select * from camera_info where villageid= '"+villageId+"' and camerano not in ('90001','90002','90003','90004','99998','99999')");
			if(cv.size()>0){
				cList.add(cv.get(0));
			}
		}
		for (CameraVo c : cList) {
			dao.add("insert into stb_camera_rlt (stbno,camerano,rlt_type,remark) values('"+stb.getStbNo().trim()+"','"+c.getCameraNo()+"','1','"+c.getCameraName()+"');");
		}
		List<CameraVo> isHasCameraNoList = cdao.getBySql("select * from camera_info where camerano in ('90001','90002','90003','99998','90004','99999')");
		for (CameraVo cameraVo : isHasCameraNoList) {
			dao.add("insert into stb_camera_rlt (stbno,camerano,rlt_type,remark) values('"+stb.getStbNo().trim()+"','"+cameraVo.getCameraNo()+"','0','"+cameraVo.getCameraName()+"');");
		}
		return (cList.size())+"";
	}

}
