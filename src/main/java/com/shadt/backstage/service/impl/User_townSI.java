package com.shadt.backstage.service.impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.backstage.dao.User_townD;
import com.shadt.backstage.service.User_townS;
import com.shadt.backstage.vo.User_townVo;

@Service
public class User_townSI implements User_townS {

	@Autowired
	User_townD udao;
	
	
	/**
	 * 获取VO
	 */
	@Override
	public User_townVo get(String sql){
		return udao.get(sql);
	}
	
	
	@Override
	public String add(User_townVo uTownVo){
		uTownVo.setId(UUID.randomUUID().toString());
		String sql="insert into user_town values('"+uTownVo.getId()+"','"+uTownVo.getUserID()+"','"+uTownVo.getTownID()+"')";
		String count="";
		count=udao.update(sql);
		return count;
	}

}
