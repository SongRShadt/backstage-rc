package com.shadt.backstage.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.shadt.backstage.dao.VideoMettingManageDao;
import com.shadt.backstage.dao.VillageD;
import com.shadt.backstage.dao.ipConfigD;
import com.shadt.backstage.entity.SewiseInfo;
import com.shadt.backstage.model.AddSewiseReturn;
import com.shadt.backstage.model.UpdateSewiseReturn;
import com.shadt.backstage.service.VideoMettingManageService;
import com.shadt.backstage.vo.CameraVo;
import com.shadt.backstage.vo.IpConfigVo;
import com.shadt.backstage.vo.VideoMettingManageVo;
import com.shadt.backstage.vo.VideoMettingVo;
import com.shadt.backstage.vo.VillageVo;
import com.shadt.core.entity.User;
import com.shadt.core.util.MD5Util;
import com.shadt.core.util.SendUtil;
/**
 * 视频会议业务逻辑层实现类
 * @author SongR
 *
 */
@Service
public class VideoMettingManageServiceImpl implements VideoMettingManageService {

	@Autowired
	private VideoMettingManageDao videoMettingManageDao;

	@Autowired
	private VillageD villageD;
	@Autowired
	ipConfigD iDao;
	@Override
	public List<VideoMettingManageVo> getAllBySearch(String sSearch, int iDisplayStart, int iDisplayLength, User user) {
		ArrayList<VideoMettingManageVo> arrayList = new ArrayList<>();
		String sql = "SELECT v.id,v.video_name,v.video_sort,v.video_url,v.village_id,t.townname FROM metting_video v, village_info i,town_info t WHERE v.village_id=i.villageid AND i.townid=t.townid AND (v.video_name LIKE '%"
				+ sSearch + "%' OR i.villagename LIKE '%" + sSearch + "%') LIMIT " + iDisplayStart + ","
				+ iDisplayLength;
		List videoMettingManageList = videoMettingManageDao.getBySql(sql);
		Iterator it = videoMettingManageList.iterator();
		if (videoMettingManageList.size() > 0) {
			while (it.hasNext()) {
				Map m = (Map) it.next();
				VideoMettingManageVo videoMettingManageVo = new VideoMettingManageVo();
				videoMettingManageVo.setId(Long.parseLong(m.get("id").toString()));
				videoMettingManageVo
						.setVideoMettingName(m.get("video_name") != null ? m.get("video_name").toString() : "");
				videoMettingManageVo.setUrl(m.get("video_url") != null ? m.get("video_url").toString() : "");
				videoMettingManageVo.setAreaName(m.get("townname") != null ? m.get("townname").toString() : "");
				if (m.get("video_sort") == null || m.get("video_sort").toString() == "") {
					videoMettingManageVo.setSort("");
				} else {
					videoMettingManageVo.setSort(m.get("video_sort").toString());
				}
				arrayList.add(videoMettingManageVo);
			}
		}
		return arrayList;
	}

	@Override
	public Object getCount(String sSearch, User user) {
		String sql = "SELECT COUNT(*) FROM metting_video v, village_info i,town_info t WHERE v.village_id=i.villageid AND i.townid=t.townid AND (v.video_name LIKE '%"+ sSearch + "%' OR i.villagename LIKE '%" + sSearch + "%')";
		String count = videoMettingManageDao.getCount(sql);
		return count;
	}

	@Override
	public String add(VideoMettingManageVo videoMettingManageVo) {
		String timestamp = (((new Date()).getTime()) / 1000)+"";
		Map<String,String> params = new HashMap<String,String>();
		List<IpConfigVo> ip = iDao.getBySql("select * from ipconfig where id='"+videoMettingManageVo.getIpId()+"'");
		String links = ip.get(0).getIpUrl();
		String accessid = ip.get(0).getAccessid();
		params.put("do", "stream");
		params.put("time", timestamp);
		String s = "";
		params.put("code", MD5Util.md5(accessid+timestamp+"stream"));
		params.put("op", "add");
		params.put("type", "2");
		params.put("mode_type", "1");
		params.put("ip", ip.get(0).getIpUrl());
		params.put("name", videoMettingManageVo.getVideoMettingName());
		s= SendUtil.post(links, params);
		AddSewiseReturn sr = new AddSewiseReturn();
		sr = JSON.parseObject(s,AddSewiseReturn.class);
		if(sr!=null &&sr.isSuccess()){
			String url = ip.get(0).getCameraUrl()+"/"+sr.getSourceid()+".m3u8";
			String sql = "INSERT INTO metting_video (video_name,rtsp,village_id,video_status,video_type,video_sort,sourceid,video_url) VALUES ('"
					+ videoMettingManageVo.getVideoMettingName() + "','" + videoMettingManageVo.getUrl() + "','"
					+ videoMettingManageVo.getVillageId() + "',0,0," + videoMettingManageVo.getSort() + ","+sr.getSourceid()+","+url+")";
			videoMettingManageDao.add(sql);
		}
		return "";
	}

	@Override
	public String delete(String id) {
		String sql = "DELETE FROM metting_video WHERE id=" + id;
		videoMettingManageDao.delete(sql);
		return "";
	}

	@Override
	public List<VideoMettingManageVo> getVillage(String sSearch, int iDisplayStart, int iDisplayLength, User user,
			String id) {
		List<VideoMettingManageVo> arrayList = new ArrayList<>();
		String sql = "SELECT i.villagename,v.id FROM metting_village_video v,village_info i WHERE v.videoid=" + id
				+ " AND v.villageid=i.villageid";
		List list = videoMettingManageDao.getBySql(sql);
		Iterator it = list.iterator();
		if (list.size() > 0) {
			while (it.hasNext()) {
				Map m = (Map) it.next();
				VideoMettingManageVo villageVo = new VideoMettingManageVo();
				villageVo.setId(Long.valueOf(m.get("id").toString()));
				villageVo.setVillageName(m.get("villagename") != null ? m.get("villagename").toString() : "");

				arrayList.add(villageVo);
			}
		}
		return arrayList;
	}

	@Override
	public Object getCountVillage(String sSearch, User user, String id) {
		String sql = "SELECT COUNT(*) FROM  metting_village_video WHERE videoid=" + id;
		String count = videoMettingManageDao.getCount(sql);
		return count;
	}

	@Override
	public String deleteVillage(String id) {
		String sql = "DELETE FROM metting_village_video WHERE id='" + id + "'";
		return villageD.update(sql);
	}

	@Override
	public void addVillageShow(String villeageId, String id) {
		if (villeageId != null && !villeageId.equals("")) {
			String sql = "INSERT INTO metting_village_video (videoid,villageid) VALUES (" + id + ",'" + villeageId
					+ "')";
			videoMettingManageDao.add(sql);
		}
	}

	@Override
	public List<VillageVo> unbounded(String id) {
		String sql = "select * from village_info a where villageid not in (select villageid from metting_village_video where videoid = "
				+ id + ") and villagestatus=0";
		return videoMettingManageDao.getBySql(sql);
	}

	@Override
	public List<VideoMettingVo> getByMettingAndVideo() {
		List<VideoMettingVo> arrayList = new ArrayList<>();
		String sql="SELECT	b.camerano,b.cameraname video_name,b.streamid sourceid FROM	village_info a,camera_info b,sewiseinfo c WHERE	a.villagelevel IN (0, 1) AND a.villagestatus = 0 AND a.villageid = b.villageid AND b.streamid = c.sourceid AND b.streamid not in (SELECT sourceid FROM metting_video where sourceid is not null);";
		List list = videoMettingManageDao.getBySql(sql);
		Iterator it = list.iterator();
		if (list.size() > 0) {
			while (it.hasNext()) {
				Map m = (Map) it.next();
				VideoMettingVo videoMettingVo = new VideoMettingVo();
				videoMettingVo.setId(m.get("camerano") != null ? m.get("camerano").toString() : "");
				videoMettingVo.setVideoName(m.get("video_name") != null ? m.get("video_name").toString() : "");
				arrayList.add(videoMettingVo);
			}
		}
		return arrayList;
	}

	@Override
	public void addMettingAndVideoform(String id) {
		if (id!=null) {
			String sql="select b.camerano,b.cameraname video_name,b.cameraurl video_url,b.streamid sourceid,b.villageid village_id, c.url  rtsp from village_info a,camera_info b,sewiseinfo c where a.villagelevel in(0,1) and a.villagestatus=0 and a.villageid=b.villageid and b.streamid=c.sourceid AND camerano='"+id+"'";
			List list = videoMettingManageDao.getBySql(sql);
			Iterator it = list.iterator();
			if (list.size() > 0) {
				while (it.hasNext()) {
					Map m = (Map) it.next();
					String sql3="SELECT MAX(video_sort) FROM metting_video";
					List max = videoMettingManageDao.getBySql(sql3);
					Iterator it1 = max.iterator();
					if (max.size() > 0) {
						while (it1.hasNext()) {
							Map m1 = (Map) it1.next();
							Long maxVideoSort =Long.valueOf(m1.get("MAX(video_sort)").toString())+1;
							String sql2="INSERT INTO metting_video (video_name,rtsp,video_sort,sourceid,video_status,video_type,video_url,village_id) VALUES('"+m.get("video_name").toString()+"','"+m.get("rtsp").toString()+"','"+maxVideoSort+"','"+m.get("sourceid").toString()+"',0,0,'"+m.get("video_url").toString()+"','"+m.get("village_id").toString()+"')";
							videoMettingManageDao.add(sql2);
						}
					}
				}
			}
		}
	}
}
