package com.shadt.backstage.service.impl;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.shadt.backstage.dao.CameraD;
import com.shadt.backstage.dao.User_townD;
import com.shadt.backstage.dao.VideoD;
import com.shadt.backstage.service.VideoS;
import com.shadt.backstage.vo.CameraVo;
import com.shadt.backstage.vo.User_townVo;
import com.shadt.backstage.vo.VideoVo;
import com.shadt.core.entity.User;
/**
 * 视频信息业务逻辑层实现类
 * 
 * @author hebin
 *
 */
@Service
public class VideoSI implements VideoS{
	
	@Resource
	User_townD udao;
	
	@Resource
	VideoD videoDao;
	
	@Resource
	CameraD cameraDao;
	
	public List<VideoVo> findAllBySql(String fullTime,String term,int start ,int end,User user) {
		List<VideoVo> list= null;
		String sql="";
		if(user.getLevel()==0){
		sql = "SELECT a.* FROM	video a,camera_info b WHERE	a.camerano = b.camerano and a.video_status = 0 and  (a.video_type <>2 or a.video_type is null)";
		}
		else{
			User_townVo uTownVo = udao.get("select * from user_town where userid='"+user.getId()+"'");
			sql="SELECT a.* from video a,camera_info b,village_info c,town_info d WHERE a.camerano=b.camerano AND b.villageid=c.villageid AND c.townid=d.townid AND d.townid='"+uTownVo.getTownID()+"' ";
		}
		List<CameraVo> cv = null;
		try{
			if(!"".equals(term)){
				term = term.trim();
				sql +="  and b.cameraname like '%"+term+"%'";
			}
			if(!fullTime.equals("undefined")&&!"".equals(fullTime)){
				String startTime = fullTime.substring(0,20);
				String endTime = fullTime.substring(22,41);
				sql+= " and a.starttime >='"+ startTime +"' and a.endtime <= '"+endTime+"'";
			}
			sql += " limit "+ start + ","+ end;
			list = videoDao.findAllBySql(sql);
			for (VideoVo videoVo : list) {
					cv = cameraDao.getBySql("select * from camera_info where camerano='"+videoVo.getCameraNo()+"'");	
				if(cv.size()>0){
					videoVo.setCameraVo(cv.get(0));
				}
			}
			for (VideoVo videoVo : list) {
			 if(videoVo.getCameraVo()!=null){	
//				 	String url = videoVo.getCameraVo().getCameraUrl().substring(0,24);
				 	String u[] = videoVo.getCameraVo().getCameraUrl().split("/");
				 	String url=u[0]+"//"+u[2];
				 	StringBuffer play = new StringBuffer(videoVo.getPlayUrl());
				 	play.insert(0,url);
				 	StringBuffer download = new StringBuffer(videoVo.getDownloadUrl());
				 	download.insert(0,url);
				 	videoVo.setDownloadUrl(download.toString());
				 	videoVo.setPlayUrl(play.toString());
				 
				 	String time = "";
				 	int duration = Integer.parseInt(videoVo.getDuration());
				 	int hour = duration/3600;
				 	int m = (duration%3600)/60;
				 	int s = (duration%3600)%60;
				 	if(hour>1){
				 		time+=hour+"小时"; 
				 	}
				 	if(m>1){
				 		time+=m+"分钟";
				 	}
				 	if(s>1){
				 		time+=s+"秒";
				 	}
				 	videoVo.setDuration(time);
			 	}
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		return list;
	}

	
	public String rowCount(String term,String fullTime,User user) {
		String count = "";
		String sql="";
		if(user.getLevel()==0){
			sql="select count(1) from video a,camera_info b where a.camerano = b.camerano and a.video_status = 0 and  (a.video_type <>2 or a.video_type is null)";
		}
		else{
			User_townVo uTownVo = udao.get("select * from user_town where userid='"+user.getId()+"'");
			sql="SELECT count(1) from video a,camera_info b,village_info c,town_info d WHERE a.camerano=b.camerano AND b.villageid=c.villageid AND c.townid=d.townid AND d.townid='"+uTownVo.getTownID()+"' ";
		}
		if(!"".equals(term)){
			sql += " and b.cameraname like '%"+term+"%'";
		}
		if(!fullTime.equals("undefined")&&!"".equals(fullTime)){
			String startTime = fullTime.substring(0,20);
			String endTime = fullTime.substring(22,41);
			sql+= " and a.starttime >='"+ startTime +"' and a.endtime <= '"+endTime+"'";
		}
		try{
			count = videoDao.rowCount(sql);
		}catch(Exception e){
			e.printStackTrace();
		}
		return count;
	}

	public String delete(String id) {
		String sql = "delete from video where id ='" + id + "'";
		String value = "";
		try{
			value = videoDao.delete(sql) + "";
		}catch(Exception e){
			e.printStackTrace();
		}
		return value;
	}
	
}
