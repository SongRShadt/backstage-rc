package com.shadt.backstage.service.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.backstage.dao.StbCameraD;
import com.shadt.backstage.dao.StbD;
import com.shadt.backstage.dao.TownD;
import com.shadt.backstage.dao.User_townD;
import com.shadt.backstage.dao.VillageD;
import com.shadt.backstage.service.VillageS;
import com.shadt.backstage.vo.StbCameraVo;
import com.shadt.backstage.vo.StbVo;
import com.shadt.backstage.vo.TownVo;
import com.shadt.backstage.vo.User_townVo;
import com.shadt.backstage.vo.VillageVo;
import com.shadt.core.entity.User;

/**
 * 村信息业务层实现类
 * 
 * @author liusongren
 * 
 */
@Service
public class VillageSI implements VillageS {
	
	@Autowired
	User_townD udao;
	
	@Autowired
	VillageD dao;
	@Autowired
	StbD stbDao;
	@Autowired
	TownD townDao;
	@Autowired
	StbCameraD scDao;

	/**
	 * 实现获取所有村信息分页信息
	 */
	public List<VillageVo> getAll(String sSearch, int iDisplayStart,int iDisplayLength,User user)throws Exception {
		String sql="select * from village_info where 1=1";
		if(user.getLevel()!=0){
			User_townVo uTownVo=udao.get("select * from user_town where userid='"+user.getId()+"'");
			if(null!=uTownVo){
				sql="select * from village_info where townid='"+uTownVo.getTownID()+"' ";
			}else{
				return null;
			}
		}
		if (!sSearch.trim().equals("")) {
			sql += " and villagename like '%" + sSearch + "%' or orgcode like '%"+sSearch+"%'";
			String stbSql = "select * from stb_info where stbno like'%"+ sSearch+ "%'";
			List<StbVo> stbL = stbDao.getBySql(stbSql);
			for (StbVo stbVo : stbL) {
				sql+=" or villageid like '%"+stbVo.getVillageId()+"%'";
			}
			String townSql = "select * from town_info where townname like '%"+sSearch+"%'";
			List<TownVo> townL = townDao.getBySql(townSql);
			for (TownVo townVo : townL) {
				sql+=" or townid like '%"+townVo.getTownId()+"%'";
			}
		}
		sql += " order by villageSort limit " + iDisplayStart + "," + iDisplayLength;
		List<VillageVo> villageList = dao.getBySql(sql);
		for (VillageVo v : villageList) {
			String stbSql = "select * from stb_info where villageid='"+ v.getVillageId() + "'";
			List<StbVo> stbList = stbDao.getBySql(stbSql);
			v.setStb(stbList.size() > 0 ?stbList.get(0) : null);
			String townSql = "select * from town_info where townid='"+ v.getTownId() + "'";
			List<TownVo> tList = townDao.getBySql(townSql);
			v.setTown(tList.size()>0?tList.get(0):null);
			if(null!=v.getStb()){
				String rltSql ="select * from stb_camera_rlt where stbno = '"+v.getStb().getStbNo()+"'";
				List<StbCameraVo> stbcList = scDao.getBySql(rltSql);
				if(stbcList.size()>=1){
					v.setBindInfo("1");
				}else {
					v.setBindInfo("0");
				} 
			}else{
				System.out.println(v.getVillageId());
			}
			
		}
		return villageList;
	}

	
	/**
	 * 实现获取所有村信息
	 */
	public List<VillageVo> getAll(String townId) {
		String sql = "select * from village_info where 1=1";
		if (!townId.equals("")&&townId!=null) {
			sql += " and townid = '" + townId + "'";
		}
		List<VillageVo> villageList = dao.getBySql(sql);
		for (VillageVo v : villageList) {
			String stbSql = "select * from stb_info where villageid='"
					+ v.getVillageId() + "'";
			v.setStb(stbDao.getBySql(stbSql).size() > 0 ? stbDao.getBySql(
					stbSql).get(0) : null);
			String townSql = "select * from town_info where townid='"
					+ v.getTownId() + "'";
			v.setTown(townDao.getBySql(townSql).get(0));
		}
		return villageList;
	}
	
	
	
	/**
	 * 实现根据村编号获取村信息
	 */
	public VillageVo getById(String id) {
		return null;
	}

	/**
	 * 实现根据镇编号获取村信息
	 */
	public List<VillageVo> getByTownId(String id) {
		return null;
	}

	/**
	 * 实现添加村信息
	 */
	public String add(VillageVo village) {
		village.setVillageId(UUID.randomUUID().toString());
		String sql = "INSERT INTO village_info (villageid,villagename,villagekeyvalue,townid,orgcode,villagesort)values('"
				+ village.getVillageId()+ "','"+ village.getVillageName()+ "','"+ village.getVillageKeyValue()+ "','"
				+ village.getTownId()+ "','" + village.getOrgCode() + "',"+village.getVillageSort()+")";
		if(village.getStbNo()!=null&&village.getStbNo()!=""){
			String sSql = "insert into stb_info(stbno,villageid)values('"+village.getStbNo()+"','"+village.getVillageId()+"')";
			String stbDeleteSql = "delete from stb_info where stbno='"+village.getStbNo()+"'";
			stbDao.update(stbDeleteSql);
			stbDao.update(sSql);
		}
		String count = dao.update(sql);
		return count;
	}

	/**
	 * 实现删除村信息
	 */
	public String delete(String id, String stbNo) {
		String vSql = "delete from village_info where villageid ='" + id + "'";
		String sSql = "delete from stb_info where stbNo ='" + stbNo + "'";
		String scSql = "delete from stb_camera_rlt where stbNo='" + stbNo + "'";
		String count = dao.update(vSql);
		if (Integer.parseInt(count) > 0) {
			stbDao.update(sSql);
			scDao.delete(scSql);
		}
		return count;
	}

	/**
	 * 实现修改村信息
	 */
	public String update(VillageVo village) {
		String vSql = "update village_info SET villagename='"
				+ village.getVillageName() + "',villagekeyvalue='"
				+ village.getVillageKeyValue() + "',townid='"
				+ village.getTownId() + "',orgcode='" + village.getOrgCode()
				+ "',villagesort='"+village.getVillageSort()+"' where villageid='" + village.getVillageId() + "'";
		String sSql = "UPDATE stb_info SET stbno ='" + village.getStbNo()
				+ "' where villageid='" + village.getVillageId() + "'";
		String vCount = dao.update(vSql);
		if (Integer.parseInt(vCount) > 0) {
			stbDao.update(sSql);
		}
		return vCount;
	}

	/**
	 * 实现按条件获取记录数
	 */
	public String getCount(String sSearch,User user) {
		String sql="";
		if(user.getLevel()==0){
			sql = "select count(*) from village_info where 1=1";
		}
		else{
			User_townVo uTownVo=udao.get("select * from user_town where userid='"+user.getId()+"'");
			sql="select count(*) from village_info where townid='"+uTownVo.getTownID()+"' ";
		}
		if (!sSearch.trim().equals("")) {
			sql += " and villagename like '%" + sSearch + "%' and orgcode like '%"+sSearch+"%'";
			String stbSql = "select * from stb_info where stbno like'%"+ sSearch+ "%'";
			List<StbVo> stbL = stbDao.getBySql(stbSql);
			for (StbVo stbVo : stbL) {
				sql+=" or villageid like '%"+stbVo.getVillageId()+"%'";
			}
			String townSql = "select * from town_info where townname like '%"+sSearch+"%'";
			List<TownVo> townL = townDao.getBySql(townSql);
			for (TownVo townVo : townL) {
				sql+=" or townid like '%"+townVo.getTownId()+"%'";
			}
		}
		return dao.getCount(sql);
	}

	/**
	 * 实现验证值是否存在
	 */
	public boolean checkValue(String value) {
		String sql = "select count(*) from village_info where villagename='"+value+"'";
		String s = dao.getCount(sql);
		if(Integer.parseInt(s)>0)
			return false;
		else
			return true;
	}

	
	public List<VillageVo> getVillageByVideo(String townid) {
		String sql = "select c.* from metting_video a,metting_village_video b, village_info c where a.id=b.videoid and b.villageid != c.villageid and c.townid='"+townid+"' and c.villagelevel=2 and c.villagestatus=0 ORDER BY if(isnull(c.villageSort),1,0),c.villageSort";
		List<VillageVo> villageList = dao.getBySql(sql);
		return villageList;
	}

}
