package com.shadt.backstage.service.impl;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.shadt.backstage.dao.ipConfigD;
import com.shadt.backstage.service.IpConfigS;
import com.shadt.backstage.vo.IpConfigVo;
/**
 * ip地址业务逻辑层实现类
 * 
 * @author hebin
 *
 */
@Service
public class IpConfigSI implements IpConfigS{
	
	@Resource
	ipConfigD iDao;
	
	public List<IpConfigVo> getBySql(String sql) {
		List<IpConfigVo> list=  null;
		try{
			list = iDao.getBySql(sql);
		}catch(Exception e){
			e.printStackTrace();
		}
		return list;
	}

	
	
}
