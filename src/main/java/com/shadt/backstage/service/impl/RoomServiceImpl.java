package com.shadt.backstage.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.shadt.backstage.dao.CameraD;
import com.shadt.backstage.dao.User_townD;
import com.shadt.backstage.dao.VideoD;
import com.shadt.backstage.service.RoomService;
import com.shadt.backstage.vo.CameraVo;
import com.shadt.backstage.vo.VideoVo;
import com.shadt.core.entity.User;

@Service
public class RoomServiceImpl implements RoomService {

	
	@Resource
	User_townD udao;
	
	@Resource
	VideoD videoDao;
	
	@Resource
	CameraD cameraDao;
	
	@Override
	public List<VideoVo> findAllBySql(String time, String term, int start, int end, User user) {
		String sql="select a.* from video a,camera_info b where a.camerano = b.camerano and a.video_type=2 AND a.video_status=0";
		List<VideoVo> list = videoDao.findAllBySql(sql);
		for (VideoVo videoVo : list) {
			List<CameraVo> cv = cameraDao.getBySql("select * from camera_info where camerano='"+videoVo.getCameraNo()+"'");	
			
		if(cv.size()>0){
			videoVo.setCameraVo(cv.get(0));
		}
	}
		for (VideoVo videoVo : list) {
			 if(videoVo.getCameraVo()!=null){	
//				 	String url = videoVo.getCameraVo().getCameraUrl().substring(0,24);
				 	String u[] = videoVo.getCameraVo().getCameraUrl().split("/");
				 	String url=u[0]+"//"+u[2];
				 	StringBuffer play = new StringBuffer(videoVo.getPlayUrl());
				 	play.insert(0,url);
				 	StringBuffer download = new StringBuffer(videoVo.getDownloadUrl());
				 	download.insert(0,url);
				 	videoVo.setDownloadUrl(download.toString());
				 	videoVo.setPlayUrl(play.toString());
				 
				 	String time1 = "";
				 	int duration = Integer.parseInt(videoVo.getDuration());
				 	int hour = duration/3600;
				 	int m = (duration%3600)/60;
				 	int s = (duration%3600)%60;
				 	if(hour>1){
				 		time1+=hour+"小时"; 
				 	}
				 	if(m>1){
				 		time1+=m+"分钟";
				 	}
				 	if(s>1){
				 		time1+=s+"秒";
				 	}
				 	videoVo.setDuration(time1);
			 	}
			}
		return list;

	}

	@Override
	public String rowCount(String term, String fullTime, User user) {
		String count = "";
		String sql="";
			sql="select count(1) from video a,camera_info b where a.camerano = b.camerano and a.video_type=2 AND a.video_status=0";
		if(!"".equals(term)){
			sql += " and b.cameraname like '%"+term+"%'";
		}
		if(!fullTime.equals("undefined")&&!"".equals(fullTime)){
			String startTime = fullTime.substring(0,20);
			String endTime = fullTime.substring(22,41);
			sql+= " and a.starttime >='"+ startTime +"' and a.endtime <= '"+endTime+"'";
		}
		try{
			count = videoDao.rowCount(sql);
		}catch(Exception e){
			e.printStackTrace();
		}
		return count;
	}

	@Override
	public String delete(String id) {
		String sql = "delete from video where id ='" + id + "'";
		String value = "";
		try{
			value = videoDao.delete(sql) + "";
		}catch(Exception e){
			e.printStackTrace();
		}
		return value;
	}

}
