package com.shadt.backstage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.backstage.dao.GeneralMeetingVideoDao;
import com.shadt.backstage.service.GeneralMeetingVideoService;
import com.shadt.backstage.vo.GeneralMeetingVideoVO;

@Service
public class GeneralMeetingVideoServiceImpl implements GeneralMeetingVideoService {

	@Autowired
	GeneralMeetingVideoDao dao;
	
	@Override
	public List<GeneralMeetingVideoVO> findAllBySql(String fullTime,String term,int start ,int end) {
		List<GeneralMeetingVideoVO> list= null;
		String sql="select m.meeting_id,m.meeting_name,m.camera_type,m.orgcode,v.starttime,v.endtime,v.duration,v.play_url,v.download_url from general_meeting m,general_meeting_video v where m.taskid = v.taskid and m.meeting_status = 1";
		try{
			if(!"".equals(term)){
				term = term.trim();
				sql +="  and m.meeting_name like '%"+term+"%'";
			}
			if(!fullTime.equals("undefined")&&!"".equals(fullTime)){
				String startTime = fullTime.substring(0,20);
				String endTime = fullTime.substring(22,41);
				sql+= " and v.starttime >='"+ startTime +"' and v.endtime <= '"+endTime+"'";
			}
			sql += " limit "+ start + ","+ end;
			list = dao.findAllBySql(sql);
			for (GeneralMeetingVideoVO videoVo : list) {
				 String time = "";
				 int duration = Integer.parseInt(videoVo.getMeetingTime());
				 int hour = duration/3600;
				 int m = (duration%3600)/60;
				 int s = (duration%3600)%60;
				 if(hour>1){
				 	time+=hour+"小时"; 
				 }
				 if(m>1){
				 	time+=m+"分钟";
				 }
				 if(s>1){
				 	time+=s+"秒";
				 }
				 videoVo.setMeetingTime(time);
			 }
		}catch(Exception e){
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public String rowCount(String term,String fullTime) {
		String count = "";
		String sql="select count(1) from general_meeting m,general_meeting_video v where m.taskid = v.taskid and m.meeting_status = 1";
		if(!"".equals(term)){
			sql += " and m.meeting_name like '%"+term+"%'";
		}
		if(!fullTime.equals("undefined")&&!"".equals(fullTime)){
			String startTime = fullTime.substring(0,20);
			String endTime = fullTime.substring(22,41);
			sql+= " and v.starttime >='"+ startTime +"' and v.endtime <= '"+endTime+"'";
		}
		try{
			count = dao.rowCount(sql);
		}catch(Exception e){
			e.printStackTrace();
		}
		return count;
	}

	@Override
	public String delete(Integer id) {
		String sql = "delete from general_meeting where meeting_id="+id+"";
		String sql2 = "delete from general_meeting_video where taskid=(SELECT taskid FROM general_meeting WHERE meeting_id="+id+")";
		String value = "";
		try{
			value = (dao.delete(sql2) + dao.delete(sql)) + "";
		}catch(Exception e){
			e.printStackTrace();
		}
		return value;
	}

}
