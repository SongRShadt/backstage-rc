package com.shadt.backstage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.backstage.dao.StbD;
import com.shadt.backstage.dao.VillageD;
import com.shadt.backstage.service.StbS;
import com.shadt.backstage.vo.StbVo;
import com.shadt.backstage.vo.VillageVo;

/**
 * 机顶盒信息业务逻辑层实现类
 * 
 * @author liusongren
 *
 */
@Service
public class StbSI implements StbS {

	@Autowired
	StbD dao;

	@Autowired
	VillageD vdao;

	/**
	 * 实现获取所有机顶盒信息
	 */
	public List<StbVo> getAll() {
		String sql = "select * from stb_info where 1=1";
		List<StbVo> stbList = dao.getBySql(sql);
		for (StbVo s : stbList) {
			List<VillageVo> vl = vdao.getBySql("select * from village_info where villageid='" + s.getVillageId() + "'");
			if(vl.size()<1){
				System.out.println("不存在此村！villageid=="+s.getVillageId());
			}else{
				s.setVillage(vl.get(0));
			}
		}
		return stbList;
	}

	/**
	 * 实现根据机顶盒编号获取机顶盒信息
	 */
	public StbVo getById(String id) {
		String sql = "select * from stb_info where 1=1 and stbno='" + id + "'";
		List<StbVo> stbList = dao.getBySql(sql);
		for (StbVo s : stbList) {
			List<VillageVo> vl = vdao.getBySql("select * from village_info where villageid='" + s.getVillageId() + "'");
			if(vl.size()>0){
				s.setVillage(vl.get(0));
			}else{
				System.out.println("不存在此村！villageid=="+s.getVillageId());
			}
		}
		return stbList.get(0);
	}

	/**
	 * 实现根据村编号获取机顶盒信息
	 */
	public StbVo getByVillageId(String id) {
		String sql = "select * from stb_info where 1=1 and villageid='" + id + "'";
		List<StbVo> stbList = dao.getBySql(sql);
		for (StbVo s : stbList) {
			List<VillageVo> vl = vdao.getBySql("select * from village_info where villageid='" + id + "'");
			if(vl.size()>0){
				s.setVillage(vl.get(0));
			}else{
				System.out.println("不存在此村！villageid=="+s.getVillageId());
			}
		}
		return stbList.size()>0?stbList.get(0):null;
	}

	/**
	 * 实现添加机顶盒信息
	 */
	public String add(StbVo stb) {
		String sql = "insert into stb_info(stbno,villageid,remark) values" + "('" + stb.getStbNo() + "','"
				+ stb.getVillageId() + "','" + stb.getRemark() + "')";
		return dao.add(sql);
	}

	/**
	 * 实现删除机顶盒信息
	 */
	public String delete(String id) {
		String sql = "delete from stb_info where stbno = '" + id + "'";
		return dao.delete(sql);
	}

	/**
	 * 实现修改机顶盒信息
	 */
	public String update(StbVo stb) {
		String sql = "update stb_info set villageid='" + stb.getVillageId() + "',remark='" + stb.getRemark()
				+ "' where stbno='"+stb.getStbNo()+"'";
		return dao.update(sql);
	}
	
	/**
	 * 实现验证值是否存在
	 */
	public boolean checkValue(String value) {
		String sql = "select count(*) from stb_info where stbno='"+value+"'";
		String s = dao.getCount(sql);
		if(Integer.parseInt(s)>0)
			return false;
		else
			return true;
	}

}
