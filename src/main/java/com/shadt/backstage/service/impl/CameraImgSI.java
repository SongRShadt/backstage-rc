package com.shadt.backstage.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.backstage.dao.CameraD;
import com.shadt.backstage.dao.CameraImgD;
import com.shadt.backstage.dao.User_townD;
import com.shadt.backstage.service.CameraImgS;
import com.shadt.backstage.vo.CameraImg;
import com.shadt.backstage.vo.CameraVo;
import com.shadt.core.entity.User;
import com.shadt.core.util.ConfigUtil;

@Service
public class CameraImgSI implements CameraImgS {

	@Autowired
	CameraImgD cameraImgD;

	@Autowired
	User_townD udao;

	@Autowired
	CameraD cameraD;

	/**
	 * 按条件分页
	 * 
	 * @param sSearch
	 * @param iDisplayStart
	 * @param iDisplayLength
	 * @return
	 */
	@Override
	public List<CameraImg> getAllBySearch(String sSearch, int iDisplayStart, int iDisplayLength, User user)
			throws ParseException {
		String sql = "SELECT * from camera_image_info LIMIT " + iDisplayStart + "," + iDisplayLength;
		List<CameraImg> cameraImgList = new ArrayList<>();
		List<CameraImg> cameraList = cameraImgD.getBySql(sql);

		for (CameraImg cameraImg : cameraList) {
			CameraImg cameraImg2 = new CameraImg();
			String id = cameraImg.getId();
			cameraImg2.setId(id);
			String cid = cameraImg.getCid();
			if (cid == null || cid == "") {
				cameraImg2.setCameraVoName("");
				String url = cameraImg.getUrl();
				if (url == null || url == "") {
					SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String creattime = cameraImg.getCreattime();
					if (creattime == null || creattime == "") {
						String s = "";
						cameraImg2.setUrl("");
						cameraImg2.setCreattime(s);
						cameraImgList.add(cameraImg2);
					} else {
						Date date = fmt.parse(creattime);
						String s = fmt.format(date);
						cameraImg2.setUrl(url);
						cameraImg2.setCreattime(s);
						cameraImgList.add(cameraImg2);
					}
				} else {
					SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String creattime = cameraImg.getCreattime();
					if (creattime == null || creattime == "") {
						String s = "";
						cameraImg2.setUrl(url);
						cameraImg2.setCreattime(s);
						cameraImgList.add(cameraImg2);
					} else {
						Date date = fmt.parse(creattime); // 将数据库读出来的时间类型，转换为java的Date类型

						String s = fmt.format(date);
						cameraImg2.setUrl(url);
						cameraImg2.setCreattime(s);
						cameraImgList.add(cameraImg2);
					}
				}
			} else {
				String sql2 = "SELECT i.cameraname FROM camera_info i WHERE i.camerano=" + "'" + cid + "' AND i.cameraname LIKE '%"+sSearch+"%'";
				List<CameraVo> cameraVoList = cameraD.getBySql(sql2);
				for (CameraVo cameraVo : cameraVoList) {
					String cameraName = cameraVo.getCameraName();
					if (cameraName == null || cameraName == "") {
						cameraImg2.setCameraVoName("");
					} else {
						cameraImg2.setCameraVoName(cameraName);
					}
					String url = cameraImg.getUrl();
					if (url == null || url == "") {
						SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

						String creattime = cameraImg.getCreattime();
						if (creattime == null || creattime == "") {
							String s = "";
							cameraImg2.setUrl("");
							cameraImg2.setCreattime(s);
							cameraImgList.add(cameraImg2);
						} else {
							Date date = fmt.parse(creattime);
							String s = fmt.format(date);
							cameraImg2.setUrl(url);
							cameraImg2.setCreattime(s);
							cameraImgList.add(cameraImg2);
						}
					} else {
						SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

						String creattime = cameraImg.getCreattime();
						if (creattime == null || creattime == "") {
							String s = "";
							cameraImg2.setUrl(url);
							cameraImg2.setCreattime(s);
							cameraImgList.add(cameraImg2);
						} else {
							Date date = fmt.parse(creattime);
							String s = fmt.format(date);
							cameraImg2.setUrl(url);
							cameraImg2.setCreattime(s);
							cameraImgList.add(cameraImg2);
						}
					}
				}
			}
		}
		return cameraImgList;
	}

	/**
	 * 说明：删除截图
	 * 
	 * @param id
	 * @return
	 * @author 岳云清
	 * @time：2017年6月8日 上午9:12:01
	 */
	@Override
	public String delete(String id) {
		String sql = "DELETE FROM camera_image_info WHERE id=" + id;
		cameraImgD.delete(sql);
		return "";
	}

	/**
	 * 说明：查询总数量
	 * 
	 * @param sSearch
	 * @param user
	 * @return
	 * @author 岳云清
	 * @time：2017年6月8日 上午9:12:14
	 */
	@Override
	public String getCount(String sSearch, User user) {
		String sql = "SELECT COUNT(*) FROM camera_image_info a,camera_info b WHERE a.camera_id=b.camerano AND b.cameraname LIKE '%"+sSearch+"%'";
		String update = cameraImgD.update(sql);
		return update;
	}

	/**
	 * 说明：下载图片
	 * 
	 * @param url
	 *            图片路径
	 * @return
	 * @author 岳云清
	 * @time：2017年6月12日 下午2:05:24
	 */
	@Override
	public String download(String url) {
		if (url != null && !url.equals(ConfigUtil.get("imageUrl"))) {
			try {
				// 构造URL
				URL url1 = new URL(url);
				// 打开连接
				URLConnection con = url1.openConnection();
				// 输入流
				InputStream is = con.getInputStream();
				// 1K的数据缓冲
				byte[] bs = new byte[1024];
				// 读取到的数据长度
				int len;
				// 输出的文件流
				File tempFile = new File(url.trim());

				String fileName = tempFile.getName();

				OutputStream os = new FileOutputStream("E:\\" + fileName);// 保存路
				// 开始读取
				while ((len = is.read(bs)) != -1) {
					os.write(bs, 0, len);
				}
				// 完毕，关闭所有链接
				os.close();
				is.close();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			return "";
		}

		return "1";
	}

}
