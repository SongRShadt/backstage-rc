package com.shadt.backstage.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.shadt.backstage.dao.CameraD;
import com.shadt.backstage.dao.SewiseInfoDao;
import com.shadt.backstage.dao.User_townD;
import com.shadt.backstage.dao.VillageD;
import com.shadt.backstage.dao.ipConfigD;
import com.shadt.backstage.entity.SewiseInfo;
import com.shadt.backstage.model.AddSewiseReturn;
import com.shadt.backstage.model.UpdateSewiseReturn;
import com.shadt.backstage.service.CameraS;
import com.shadt.backstage.vo.CameraVo;
import com.shadt.backstage.vo.IpConfigVo;
import com.shadt.backstage.vo.User_townVo;
import com.shadt.backstage.vo.VillageVo;
import com.shadt.core.entity.User;
import com.shadt.core.util.MD5Util;
import com.shadt.core.util.SendUtil;

/**
 * 摄像头业务逻辑层实现类
 * 
 * @author liusongren
 *
 */
@Service
public class CameraSI implements CameraS {

	@Autowired
	User_townD udao;
	
	@Autowired
	CameraD dao;
	
	@Autowired
	VillageD vdao;

	@Autowired
	SewiseInfoDao sDao;
	
	@Autowired
	ipConfigD iDao;
	
	/**
	 * 获取所有摄像头信息
	 */
	public List<CameraVo> getAll() {
		List<CameraVo> cList =dao.getBySql("select * from camera_info");
		for (CameraVo c : cList) {
			List<VillageVo> vl = vdao.getBySql("select * from village_info where villageid='"+c.getVillageId()+"'");
			if(vl.size()>0){
				c.setVillage(vl.get(0));
			}
		}
		return cList;
	}

	/**
	 * 根据摄像头编号获取摄像头信息
	 */
	public CameraVo getById(String id) {
		List<CameraVo> cList = dao.getBySql("select * from camera_info where camerano = '" + id + "'");
		for (CameraVo c : cList) {
			List<VillageVo> vl = vdao.getBySql("select * from village_info where villageid='"+c.getVillageId()+"'");
			c.setVillage(vl.get(0));
		}
		CameraVo camera = new CameraVo();
		if(cList.size()>0){
			camera = cList.get(0);
			SewiseInfo sInfo = sDao.get("from SewiseInfo where sourceid='"+camera.getStreamId()+"'");
			if(sInfo!=null)
				camera.setS_ipurl(sInfo.getUrl());
		}
		return camera;
	}

	/**
	 * 根据村编号获取摄像头信息
	 */
	public CameraVo getByVillageId(String id) {
		List<CameraVo> cList = dao.getBySql("select * from camera_info where villageid = '" + id + "'");
		for (CameraVo c : cList) {
			List<VillageVo> vl = vdao.getBySql("select * from village_info where villageid='"+id+"'");
			c.setVillage(vl.get(0));
		}
		return cList.get(0);
	}

	/**
	 * 添加摄像头信息
	 */
	public String add(CameraVo camera) {
		AddSewiseReturn sr = new AddSewiseReturn();
		UpdateSewiseReturn ur = new UpdateSewiseReturn();
		String timestamp = (((new Date()).getTime()) / 1000)+"";
		Map<String,String> params = new HashMap<String,String>();
		List<IpConfigVo> ip = iDao.getBySql("select * from ipconfig where id='"+camera.getIpId()+"'");
		String links = ip.get(0).getIpUrl();
		String accessid = ip.get(0).getAccessid();
		params.put("do", "stream");
		params.put("time", timestamp);
		String s = "";
		if(camera.getCameraNo().equals("")){
			params.put("code", MD5Util.md5(accessid+timestamp+"stream"));
			params.put("op", "add");
			params.put("type", "2");
			params.put("mode_type", "1");
			params.put("ip", camera.getS_ipurl());
			params.put("name", camera.getCameraName());
			s= SendUtil.post(links, params);
			sr = JSON.parseObject(s,AddSewiseReturn.class);
			if(sr!=null &&sr.isSuccess()){
				sDao.saveOrUpdate(new SewiseInfo(sr.getSourceid(),camera.getS_ipurl()));
				camera.setCameraUrl(ip.get(0).getCameraUrl()+"/"+sr.getSourceid()+".m3u8");	
				String sql = "insert into camera_info(camerano,cameraname,cameraurl,cameratype,imgurl,streamid,villageid) values" + "('" + UUID.randomUUID().toString() + "','" + camera.getCameraName() + "','" + camera.getCameraUrl() + "','"+ camera.getCameraType() + "','" + camera.getImgUrl() + "','" + sr.getSourceid() + "','"+ camera.getVillageId() + "')";
				return dao.add(sql);
			}
		}else{
			CameraVo cv = this.getById(camera.getCameraNo());
			params.put("code", MD5Util.md5(accessid+timestamp+"stream"+cv.getStreamId()));
			params.put("op", "update");
			params.put("sourceid", cv.getStreamId());
			params.put("type", "2");
			params.put("mode_type", "1");
			params.put("ip", camera.getS_ipurl());
			params.put("name", camera.getCameraName());
			if(!camera.getS_ipurl().equals("")){
				s =  SendUtil.post(links, params);
			}
			ur = JSON.parseObject(s,UpdateSewiseReturn.class);
			if(camera.getS_ipurl().equals("")){
				ur.setSuccess(true);
			}
			if(ur!=null&&ur.isSuccess()){
				camera.setCameraUrl(ip.get(0).getCameraUrl()+"/"+camera.getStreamId()+".m3u8");
				SewiseInfo sInfo = sDao.get("from SewiseInfo where sourceid='"+cv.getStreamId()+"'");
				sInfo.setUrl(camera.getS_ipurl());
				sDao.saveOrUpdate(sInfo);
			}
			String sql = "update camera_info set cameraname = '" + camera.getCameraName() + "' ,cameraurl='"
					+ cv.getCameraUrl() + "',cameratype='" + camera.getCameraType() + "',imgurl='" + camera.getImgUrl()
					+ "',streamid='" + cv.getStreamId() + "',villageid='" + camera.getVillageId() + "' where camerano='"
					+ cv.getCameraNo() + "'";
			return dao.add(sql);
		}
		return "";
	}

	/**
	 * 修改摄像头信息
	 */
	public String update(CameraVo camera) {
		CameraVo cv = this.getById(camera.getCameraNo());
		String links = "";
		if(cv.getCameraUrl().split(":").length>1){
			links = cv.getCameraUrl().split(":")[1];
		}
		List<IpConfigVo> ip = iDao.getBySql("select * from ipconfig where ipurl like '%"+links+"%'");
		UpdateSewiseReturn ur = new UpdateSewiseReturn();
		String accessid = ip.get(0).getAccessid();
		links=ip.get(0).getIpUrl();
		String s = "";
		String timestamp = (((new Date()).getTime()) / 1000)+"";
		Map<String,String> params = new HashMap<String,String>();
		params.put("do", "stream");
		params.put("time", timestamp);
		params.put("code", MD5Util.md5(accessid+timestamp+"stream"+cv.getStreamId()));
		params.put("op", "update");
		params.put("sourceid", cv.getStreamId());
		params.put("type", "2");
		params.put("mode_type", "1");
		params.put("ip", camera.getS_ipurl());
		params.put("name", camera.getCameraName());
		s =  SendUtil.post(links, params);
		ur = JSON.parseObject(s,UpdateSewiseReturn.class);
		if(ur!=null&&ur.isSuccess()){
			SewiseInfo sInfo = sDao.get("from SewiseInfo where sourceid='"+cv.getStreamId()+"'");
			sInfo.setUrl(camera.getS_ipurl());
			sDao.update(sInfo);
		}
		String sql = "update camera_info set cameraname = '" + camera.getCameraName() + "' ,cameraurl='"
				+ cv.getCameraUrl() + "',cameratype='" + camera.getCameraType() + "',imgurl='" + camera.getImgUrl()
				+ "',streamid='" + cv.getStreamId() + "',villageid='" + camera.getVillageId() + "' where camerano='"
				+ cv.getCameraNo() + "'";
		return dao.add(sql);
	}

	/**
	 * 删除摄像头信息
	 */
	public String delete(String id) {
		CameraVo cameraVo = this.getById(id);
//		String links = cameraVo.getCameraUrl().substring(0,19);
		String links = "";
		if(cameraVo.getCameraUrl().split(":").length>1){
			links = cameraVo.getCameraUrl().split(":")[1];
		}
		List<IpConfigVo> ip = iDao.getBySql("select * from ipconfig where ipurl like '%"+links+"%'");
		if(ip.size()>0){
			String accessid = ip.get(0).getAccessid();
			String timestamp = (((new Date()).getTime()) / 1000)+"";
			Map<String,String> params = new HashMap<String,String>();
			params.put("do", "stream");
			params.put("time", timestamp);
			params.put("code", MD5Util.md5(accessid+timestamp+"stream"+cameraVo.getStreamId()));
			params.put("op", "delete");
			params.put("sourceid",  cameraVo.getStreamId() );
			String s =  SendUtil.post(ip.get(0).getIpUrl(), params);
			UpdateSewiseReturn ur = JSON.parseObject(s,UpdateSewiseReturn.class);
			if(ur.isSuccess()){
				return dao.delete("delete from camera_info where camerano = '" + id + "'");
			}
		}
		return "";
	}
	
	
	/**
	 * 按条件分页获取摄像头信息
	 */
	public List<CameraVo> getAllBySearch(String sSearch, int iDisplayStart, int iDisplayLength,User user) {
		String sql="";
		if(user.getLevel()==0){
			sql = "select * from camera_info where 1=1";
		}
		else{
			User_townVo uTownVo=udao.get("select * from user_town where userid='"+user.getId()+"'");
			sql= "select * from camera_info where villageid in (SELECT villageid from village_info a where a.townid='"+uTownVo.getTownID()+"')";
		}
		List<CameraVo> cameraList = new ArrayList<CameraVo>();
		if(!sSearch.equals("")){
			sql+=" and cameraname like '%"+sSearch+"%' or villageid in(select villageid from village_info where townid in(select townid from town_info where townname like '%"+sSearch+"%'))";
		}
		sql+= " limit "+iDisplayStart +","+iDisplayLength;
		try {
			cameraList= dao.getBySql(sql);
			for (CameraVo c : cameraList) {
				List<VillageVo> vl = vdao.getBySql("select * from village_info where villageid='"+c.getVillageId()+"'");
				if(vl.size()>0){
					c.setVillage(vl.get(0));
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return cameraList;
	}

	/**
	 * 按条件获取记录数
	 */
	public String getCount(String sSearch,User user) {
		String sql="";
		if(user.getLevel()==0){
			sql = "select count(*) from camera_info where 1=1";
		}
		else {
			User_townVo uTownVo=udao.get("select * from user_town where userid='"+user.getId()+"'");
			sql="select count(*) from camera_info where villageid in(SELECT villageid from village_info a where a.townid='"+uTownVo.getTownID()+"' )";
		}
		String count = "";
		if(!sSearch.equals("")){
			sql+=" and cameraname like '%"+sSearch+"%' or villageid in(select villageid from village_info where townid in(select townid from town_info where townname like '%"+sSearch+"%'))";
		}
		try {
			count=dao.update(sql);
		} catch (Exception e) {
			System.err.println(new Exception("获取摄像头信息总记录出错！"));
			e.printStackTrace();
		}
		return count;
	}

}