package com.shadt.backstage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.backstage.dao.MettingVideoDao;
import com.shadt.backstage.service.MettingVideoService;
import com.shadt.backstage.vo.MettingVideoVo;

/**
 * 视频会议业务逻辑层实现类
 * @author SongR
 *
 */
@Service
public class MettingVideoServiceImpl implements MettingVideoService {

	
	@Autowired
	MettingVideoDao dao;
	
	/**
	 * 实现查找村绑定的视频会议列表
	 */
	public List<MettingVideoVo> getByVillage(String sSearch, int iDisplayStart, int iDisplayLength,
			String villageId) throws Exception {
		String sql = "select a.*,c.villagename from metting_video a,metting_village_video b,village_info c where  a.video_status=0 and a.video_type=0 and a.village_id = c.villageid and a.id=b.videoid and b.villageid='"+villageId+"'";
		if(!sSearch.equals("")){
			sql +=" and video_name like '%"+sSearch+"%'";
		}
		sql+= " order by a.video_sort limit "+iDisplayStart +","+iDisplayLength;
		List<MettingVideoVo> vos = dao.getBySql(sql);
		return vos;
	}

	/**
	 * 实心获取村绑定的视频会议数量
	 */
	public int countByVillage(String sSearch, String villageId) throws Exception {
		String sql = "select count(1) from metting_video a,metting_village_video b where a.id=b.videoid and a.video_status=0 and a.video_type=0 and b.villageid='"+villageId+"'";
		if(!sSearch.equals("")){
			sql +=" and video_name like '%"+sSearch+"%'";
		}
		int count = dao.countSql(sql);
		return count;
	}

	/**
	 * 实现删除视频会议绑定
	 */
	public int delete(String videoId, String villageId) throws Exception {
		String sql = "delete from metting_village_video where videoid='"+videoId+"' and villageid='"+villageId+"'";
		return dao.update(sql);
	}

	/**
	 * 实现获取所有未绑定视频会议
	 */
	public List<MettingVideoVo> getAll(String villageId) throws Exception {
		String sql = "select * from metting_video where id not in (select videoid from metting_village_video where villageid='"+villageId+"') and video_status=0 and video_type=0;";
		return dao.getBySql(sql);
	}

	/**
	 * 实现添加视频会议绑定
	 */
	public int add(String villageId, String videoId) throws Exception {
		String s ="select count(1) from metting_village_video where videoid='"+videoId+"' and villageid='"+villageId+"'";
		int ss = dao.countSql(s);
		String sql = "insert into metting_village_video (videoid,villageid) values('"+videoId+"','"+villageId+"')";
		int count =0;
		if(ss<=0){
			count = dao.update(sql);
		}
		return count;
	}

}
