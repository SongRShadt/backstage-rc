package com.shadt.backstage.service;

import java.util.List;

import com.shadt.backstage.vo.CameraVo;
import com.shadt.core.entity.User;

/**
 * 摄像头信息业务逻辑层接口
 * @author liusongren
 *
 */
public interface CameraS {

	/**
	 * 获取所有摄像头信息（超级管理员所看）
	 * @return
	 */
	List<CameraVo> getAll();
	
	
	/**
	 * 根据摄像头编号获取摄像头信息
	 * @param id
	 * @return
	 */
	CameraVo getById(String id);

	/**
	 * 根据村编号获取摄像头信息
	 * @param id
	 * @return
	 */
	CameraVo getByVillageId(String id);

	/**
	 * 添加摄像头信息
	 * @param camera
	 * @return
	 */
	String add(CameraVo camera);

	/**
	 * 修改摄像头信息
	 * @param camera
	 * @return
	 */
	String update(CameraVo camera);

	/**
	 * 删除摄像头信息
	 * @param id
	 * @return
	 */
	String delete(String id);

	/**
	 * 按条件分页获取摄像头信息
	 * @param sSearch
	 * @param iDisplayStart
	 * @param iDisplayLength
	 * @return
	 */
	List<CameraVo> getAllBySearch(String sSearch, int iDisplayStart, int iDisplayLength,User user);
	
	/**
	 * 按条件获取摄像头记录数
	 * @param sSearch
	 * @return
	 */
	String getCount(String sSearch,User user);

}
