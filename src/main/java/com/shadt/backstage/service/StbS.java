package com.shadt.backstage.service;

import java.util.List;

import com.shadt.backstage.vo.StbVo;

public interface StbS {

	List<StbVo> getAll();

	StbVo getById(String id);

	StbVo getByVillageId(String id);

	String add(StbVo stb);

	String delete(String id);

	String update(StbVo stb);
	
	/**
	 * 验证值是否存在
	 * @param trim
	 * @return
	 */
	boolean checkValue(String value );

}
