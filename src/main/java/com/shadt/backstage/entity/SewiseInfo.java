package com.shadt.backstage.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SewiseInfo {
	@Id
	private String sourceid;
	private String url;

	public SewiseInfo() {
	}

	public SewiseInfo(String sourceid, String url) {
		this.sourceid = sourceid;
		this.url = url;
	}
	public String getSourceid() {
		return sourceid;
	}

	public void setSourceid(String sourceid) {
		this.sourceid = sourceid;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
