package com.shadt.backstage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class ReadTxt {
	/**
	 * 功能：Java读取txt文件的内容 步骤：1：先获得文件句柄 2：获得文件句柄当做是输入一个字节码流，需要对这个输入流进行读取
	 * 3：读取到输入流后，需要读取生成字节流 4：一行一行的输出。readline()。 备注：需要考虑的是异常情况
	 * 
	 * @param filePath
	 */
	public static void readTxtFile(String filePath) {
		try {
			String encoding = "Unicode";
			File file = new File(filePath);
			if (file.isFile() && file.exists()) { // 判断文件是否存在
				InputStreamReader read = new InputStreamReader(new FileInputStream(file), encoding);// 考虑到编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				String sql = "";
				int i = 72;
				while ((lineTxt = bufferedReader.readLine()) != null) {
					String []s = lineTxt.split("\t");
					if(s.length!=1&&s.length>=6){
						sql+="('0000"+i+"','"+s[1]+"会议室','http://172.29.0.111:5080/hls/"+s[6]+".m3u8'"
								+ ",'1',NULL,'"+s[6]+"','"+s[3]+"'),\n";
						i++;
					}
					
				}
				System.out.println(sql);
				read.close();
			} else {
				System.out.println("找不到指定的文件");
			}
		} catch (Exception e) {
			System.out.println("读取文件内容出错");
			e.printStackTrace();
		}
	}
	public static void main(String argv[]) {
		String filePath = "/Users/liusongren/Desktop/a.txt";
		readTxtFile(filePath);
	}
}
