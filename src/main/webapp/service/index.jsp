<%@page import="com.shadt.core.entity.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%

	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/";
	request.setAttribute("path", basePath);
	User u = (User)request.getSession().getAttribute("user");
	if(null==u){
		response.sendRedirect("login.jsp");
	}
	
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8" />
<title>视频会议管理系统</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link rel="stylesheet" href="assets/global/css/lightbox.css" media="screen"/>
<link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css" />
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css" />
<link rel="stylesheet" type="text/css" href="assets/global/plugins/jstree/dist/themes/default/style.min.css"/>

<!-- END PAGE LEVEL PLUGIN STYLES -->

<!-- BEGIN PAGE STYLES -->
<link href="assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css" />

<link href="assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/><!-- 表单样式 -->
<link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css" />
<!-- END PAGE STYLES -->

<!-- BEGIN THEME STYLES -->
<link href="assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css" />
<link href="assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color" />

<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-closed-hide-logo page-sidebar-closed-hide-logo">
	<!-- BEGIN HEADER -->
	<div class="page-header navbar navbar-fixed-top">
		<!-- BEGIN HEADER INNER -->
		<div class="page-header-inner">
			
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="index.jsp" style="font-size: 30px;"> 
					<img src="logo.png" alt="logo" width="180px" style="margin-top: 16px" class="logo-default"/>
				</a>
				<div class="menu-toggler sidebar-toggler">
					<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
				</div>
			</div>
			<!-- END LOGO -->
			
			<!-- BEGIN PAGE TOP -->
			<div class="page-top">
				<!-- BEGIN HEADER SEARCH BOX -->
				<!-- END HEADER SEARCH BOX -->
				<!-- BEGIN TOP NAVIGATION MENU -->
				<div class="top-menu">
					<ul class="nav navbar-nav pull-right">
						<li class="separator hide"></li>
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown dropdown-user dropdown-dark">
							<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> 
								<span class="username username-hide-on-mobile"><%=u==null?"":u.getName() %></span>
								<img alt="" class="img-circle" src="assets/admin/layout4/img/avatar9.jpg" />
							</a>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
				</div>
				<!-- END TOP NAVIGATION MENU -->
			</div>
			<!-- END PAGE TOP -->
			
		</div>
		<!-- END HEADER INNER -->
	</div>
	<!-- END HEADER -->
	
	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container " >
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar-wrapper">
			<div class="page-sidebar navbar-collapse collapse">
				<!-- BEGIN SIDEBAR MENU -->
				<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
					<li class="start active ">
							<a class="ajaxify" id="default" href="pages/welcome.jsp">
							<i class="icon-home"></i> 
							<span class="title">首页</span>
						</a>
					</li>
					<% if(null!=u&&u.getLevel()==0){ %>
					<li>
						<a href="javascript:;"> 
							<i class="icon-film"></i> 
							<span class="title">村镇信息管理</span> 
							<span class="arrow "></span>
						</a>
						<ul class="sub-menu">
								<li>
								<a class="ajaxify" href="${path}service/pages/town.jsp"><i class="icon-flag"></i>镇信息列表</a>
								</li>
							<li>
								<a class="ajaxify" href="${path}service/pages/village.jsp"><i class="icon-direction"></i>村信息列表</a>
							</li>
						</ul>
					</li>
					
					<li>
						<a href="javascript:;"> 
							<i class="icon-camcorder"></i> 
							<span class="title">摄像头信息管理</span> 
							<span class="arrow "></span>
						</a>
						<ul class="sub-menu">
							<li>
								<a class="ajaxify" href="../service/pages/camera.jsp"><i class="glyphicon glyphicon-facetime-video"></i>摄像头信息管理</a>
							</li>
							<li>
								<a class="ajaxify" href="../service/pages/mettingvideo.jsp"><i class="glyphicon glyphicon-facetime-video"></i>视频会议管理</a>
							</li>
							<li>
								<a class="ajaxify" href="../service/pages/cameraimg.jsp"><i class="glyphicon glyphicon-facetime-video"></i>摄像头截图管理</a>
							</li>
						</ul>
					</li>
					<%} %>
					<li>
						<a href="javascript:;"> 
							<i class="icon-film"></i> 
							<span class="title">视频管理</span> 
							<span class="arrow "></span>
						</a>
						<ul class="sub-menu">
							<li>
								<a class="ajaxify" href="${path}service/pages/video.jsp"><i class="glyphicon glyphicon glyphicon-film"></i>会议室视频列表</a>
							</li>
							<li>
								<a class="ajaxify" href="${path}service/pages/room.jsp"><i class="glyphicon glyphicon glyphicon-film"></i>值班室视频列表</a>
							</li>
							<% if(null!=u&&u.getUserName().equals("admin")){ %>
							<li>
								<a class="ajaxify" href="${path}service/pages/generalmeetingvideo.jsp"><i class="glyphicon glyphicon glyphicon-film"></i>换届会议视频列表</a>
							</li>
							<%} %>
						</ul>
					</li>
					<% if(null!=u&&u.getLevel()==0){ %>
					<li>
						<a href="javascript:;"> 
							<i class="icon-user"></i> 
							<span class="title">用户管理</span> 
							<span class="arrow "></span>
						</a>
						<ul class="sub-menu">
							<li>
								<a class="ajaxify" href="${path}service/pages/user.jsp"><i class="glyphicon glyphicon glyphicon-user"></i>用户信息列表</a>
							</li>
						</ul>
					</li>
					<%} if(null!=u&&u.getLevel()==0){%>
						<li>
						<a href="javascript:;"> 
							<i class="icon-user"></i> 
							<span class="title">个人信息</span> 
							<span class="arrow "></span>
						</a>
						<ul class="sub-menu">
							<li>
								<a class="ajaxify" href="${path}service/pages/personal.jsp"><i class="glyphicon glyphicon glyphicon-user"></i>个人信息</a>
							</li>
						</ul>
					</li>
					<%} %>
					<li>
						<a onclick="logout()" >
						<i class="glyphicon glyphicon-off"></i>
						<span class="title">注销登录</span> 
						</a>
					</li>
				</ul>
				<!-- END SIDEBAR MENU -->
			</div>
		</div>
		<!-- END SIDEBAR -->
		
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<div class="page-content-body"></div>
			</div>
		</div>
		<!-- END CONTENT -->
		
	</div>
	<!-- END CONTAINER -->
	
	<!-- BEGIN FOOTER -->
	<div class="page-footer">
		<div class="page-footer-inner">2017 &copy; 党建管理系统 by
			<a href="http://www.shadt.com/"  target="_Blank">千城智联(上海)网络科技有限公司</a>.</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN CORE PLUGINS -->
	<script type="text/javascript" src="assets/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
	<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<script src="assets/lightbox-2.6.min.js"></script>
	<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
	<!-- END CORE PLUGINS -->
	
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<!-- END PAGE LEVEL PLUGINS -->
	
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
	<script src="assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
	<script src="assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
	<script type="text/javascript" src="pages/script/index.js"></script>
	
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
		jQuery(document).ready(function() {
			Metronic.init(); // init metronic core componets
			Layout.init(); // init layout
			Demo.init(); // init demo features 
			$('#default').click();
			
		});
		
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>