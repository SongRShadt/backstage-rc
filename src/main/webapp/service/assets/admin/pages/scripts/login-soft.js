var Login = function() {

	var handleLogin = function() {
//		$("#login_submit").on("click",function(e){
//			
//		});
//		
		
		$('.login-form').validate({
			errorElement : 'span', // default input error message container
			errorClass : 'help-block', // default input error message class
			focusInvalid : false, // do not focus the last invalid input
			rules : {
				userName : {
					required : true
				},
				password : {
					required : true
				} 
			},

			messages : {
				userName : {
					required : "用户名不能为空"
				},
				password : {
					required : "密码不能为空"
				}
			},
			invalidHandler : function(event, validator) { // display error
				$('.alert-danger', $('.login-form')).show();
			},
			highlight : function(element) { // hightlight error inputs
				$(element).closest('.form-group').addClass('has-error'); // set
			},
			success : function(label) {
				label.closest('.form-group').removeClass('has-error');
				label.remove();
			},
			errorPlacement : function(error, element) {
				error.insertAfter(element.closest('.input-icon'));
			},
			submitHandler : function(form) {
				$.ajax({
					url:"/backstage/user/login",
					type:"post",
					dataType:"json",
					data:$(".login-form").serialize(),
					success:function(data){
						console.info(data);
						if(data.success){
							window.location.href="index.jsp";
						}else{
							$('.alert-danger span').html("用户名或密码不正确");
							$('.alert-danger', $('.login-form')).show();
						}
					},error : function(msg){
						console.info();
					}
				});
//				form.submit();
			}
		});
		$('.login-form input').keypress(function(e) {
			if (e.which == 13) {
				if ($('.login-form').validate().form()) {
					$('.login-form').submit();
				}
				return false;
			}
		});
	}

	return {
		init : function() {
			handleLogin();
		}
	};
}();