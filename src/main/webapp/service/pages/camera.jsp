<%@page import="com.shadt.core.entity.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/";
	request.setAttribute("path", basePath);
		User u = (User)request.getSession().getAttribute("user");
	if(null==u){
		response.sendRedirect("login.jsp");
	}
%>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-edit"></i>摄像头信息
				</div>
				<div class="tools">
					<a href="javascript:;" class="reload"> </a> 
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-4">
							<div class="btn-group">
								<a href="#addCamera"class="btn green" data-toggle="modal">
									添加 <i class="fa fa-plus"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
				<table class="table table-striped table-hover table-bordered" id="cameraTable">
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT -->

<div id="addCamera" class="modal fade modal-scroll"  tabindex="-1" data-backdrop="static" data-replace="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="addCameraClose"></button>
				<h4 class="modal-title">摄像头信息</h4>
			</div>
			<div class="modal-body">
				<!-- BEGIN FORM-->
				<form id="addCameraForm" class="form-horizontal">
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3"> 
								摄像头名称<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<input type="text" id="cameraName" name="cameraName" class="form-control" />
								<input type="hidden" id="cameraNo" name="cameraNo" value=""/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								视频源路径<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<input type="text" id="s_ipurl" name="s_ipurl" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								摄像头类型
							</label>
							<div class="col-md-7">
								<label><input type="radio" name="cameraType" id="cameraType" value="0" checked="checked">会议室</label>
								<label><input type="radio" name="cameraType" id="cameraType" value="1">公示栏</label>
								<label><input type="radio" name="cameraType" id="cameraType" value="2">值班室</label>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								所属村<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<select name="town" id="town"class="form-control input-small col-md-3">
								</select>
								<select name="villageId" id="villageId"class="form-control input-small col-md-3">
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								推流服务器<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<select name="ipId" id="ip"class="form-control input-medium col-md-3">
								</select>
							</div>
						</div>
					</div>
				</form>
				<!-- END FORM-->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn red" id="formSubmit">提交</button>
				<button type="button" data-dismiss="modal" class="btn green" id="formCancel">取消</button>
			</div>
		</div>
	</div>
</div>

<div id="editCamera" class="modal fade modal-scroll"  tabindex="-1" data-backdrop="static" data-replace="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="editCameraClose"></button>
				<h4 class="modal-title">摄像头信息</h4>
			</div>
			<div class="modal-body">
				<!-- BEGIN FORM-->
				<form id="editCameraForm" class="form-horizontal">
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3"> 
								摄像头名称<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<input type="text" id="editCameraName" name="cameraName" class="form-control" />
								<input type="hidden" id="editCameraNo" name="cameraNo" value=""/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								视频源路径<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<input type="text" id="edit_s_ipurl" name="s_ipurl" class="form-control" />
							</div>
						</div>
						<div class="form-group"  >
							<label class="control-label col-md-3">
								摄像头类型
							</label>
							<div class="col-md-7">
								<label><input type="radio" name="cameraType" id="editCameraType" value="0"/>会议室</label>
								<label><input type="radio" name="cameraType" id="editCameraType" value="1"/>公示栏</label>
								<label><input type="radio" name="cameraType" id="cameraType" value="2">值班室</label>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								所属村<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<select name="town" id="editTown"class="form-control input-small col-md-3">
								</select>
								<select name="villageId" id="editVillageId"class="form-control input-small col-md-3">
								</select>
							</div>
						</div>
					</div>
				</form>
				<!-- END FORM-->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn red" id="editFormSubmit">修改</button>
				<button type="button" data-dismiss="modal" class="btn green" id="editFormCancel">取消</button>
			</div>
		</div>
	</div>
</div>

			
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL STYLES -->
<script src="pages/script/camera.js"></script>
<!-- END PAGE LEVEL STYLES -->
<script>
jQuery(document).ready(function() {   
   Camera.init();
});
</script>
<!-- END JAVASCRIPTS -->