<%@page import="com.shadt.core.entity.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/";
	request.setAttribute("path", basePath);
		User u = (User)request.getSession().getAttribute("user");
	if(null==u){
		response.sendRedirect("login.jsp");
	}
%>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-edit"></i>视频会议管理
				</div>
				<div class="tools">
					<a href="javascript:;" class="reload"> </a> 
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-4">
							<div class="btn-group">
								<a href="#addVideoMetting"class="btn green" data-toggle="modal">添加 <i class="fa fa-plus"></i></a>
							</div>
						</div>
					</div>
				</div>
				<table class="table table-striped table-hover table-bordered videomettingTable"></table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT -->

<div id="addVideoMetting" class="modal fade modal-scroll"  tabindex="-1" data-backdrop="static" data-replace="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="addCameraClose"></button>
				<h4 class="modal-title">视频会议信息</h4>
			</div>
			<div class="modal-body">
				<!-- BEGIN FORM-->
				<form id="addVideoMettingForm" class="form-horizontal">
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3"> 
								视频会议名称<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<input type="text" id="name" name="name" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								视频源路径<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<input type="text" id="rtsp" name="rtsp" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">
								排序
							</label>
							<div class="col-md-7">
								<input type="text" id="sort" name="sort" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">
								所属会场<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<select name="village" id="village"class="form-control input-medium col-md-3">
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								推流服务器<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<select name="sewise" id="sewise"class="form-control input-medium col-md-3">
								</select>
							</div>
						</div>
					</div>
				</form>
				<!-- END FORM-->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn red" id="addSubmit">提交</button>
				<button type="button" data-dismiss="modal" class="btn green" id="formCancel">取消</button>
			</div>
		</div>
	</div>
</div>



<div id="editVideoMetting" class="modal fade modal-scroll"  tabindex="-1" data-backdrop="static" data-replace="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="addCameraClose"></button>
				<h4 class="modal-title">视频会议信息</h4>
			</div>
			<div class="modal-body">
				<!-- BEGIN FORM-->
				<form id="editVideoMettingForm" class="form-horizontal">
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3"> 
								视频会议名称<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<input type="hidden" id="edit_id" name="edit_id" class="form-control" />
								<input type="text" id="edit_name" name="edit_name" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								视频源路径<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<input type="text" id="edit_rtsp" name="edit_rtsp" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">
								排序
							</label>
							<div class="col-md-7">
								<input type="text" id="edit_sort" name="edit_sort" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">
								所属会场<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<select name="edit_village" id="edit_village"class="form-control input-medium col-md-3">
								</select>
							</div>
						</div>
					</div>
				</form>
				<!-- END FORM-->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn red" id="editSubmit">提交</button>
				<button type="button" data-dismiss="modal" class="btn green" id="editformCancel">取消</button>
			</div>
		</div>
	</div>
</div>











<div id="mettingVideoBind" class="modal fade modal-scroll"  tabindex="-1" data-backdrop="static" data-replace="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="mettingVideoCancel"></button>
				<h4 class="modal-title">查看村绑定</h4>
			</div>
			<div class="modal-body">
			<form id="addVillageForm" class="form-horizontal">
			<div class="table-toolbar">
					<div class="form-group">
							<label class="control-label col-md-2">
								选择镇村<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<select name="town" id="town"class="form-control input-small col-md-3">
								</select>
								<select name="vil" id="vil" class="form-control input-small col-md-3">
								</select>
							</div>
						<span class="input-group-btn">
							<button id="bindVillage" class="btn btn-success" type="button">
								<i class="fa fa-arrow-left fa-fw" /></i>添加
							</button>
						</span>
					</div>
				</div>
				</form>
				<table class="table table-striped table-hover table-bordered villageBind">
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn green" id="mettingVideoCancel">关闭</button>
			</div>
		</div>
	</div>
</div>
		
		
		
			
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL STYLES -->
<script src="pages/script/mettingvideo.js"></script>
<!-- END PAGE LEVEL STYLES -->
<script>
jQuery(document).ready(function() {   
	Mettingvideo.init();
});
</script>
<!-- END JAVASCRIPTS -->