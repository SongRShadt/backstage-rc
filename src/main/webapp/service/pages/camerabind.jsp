<%@page import="com.shadt.core.entity.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/";
	request.setAttribute("path", basePath);
		User u = (User)request.getSession().getAttribute("user");
	if(null==u){
		response.sendRedirect("login.jsp");
	}
%>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-edit"></i>村信息
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"> </a> 
					<a href="#portlet-config" data-toggle="modal" class="config"> </a> 
					<a href="javascript:;" class="reload"> </a> 
					<a href="javascript:;" class="remove"> </a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-4">
							<div class="btn-group">
								<a  href="#addTown"class="btn green" data-toggle="modal">
									添加 <i class="fa fa-plus"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
				<table class="table table-striped table-hover table-bordered" id="villageTable">
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL STYLES -->
<script src="pages/script/village.js"></script>
<!-- END PAGE LEVEL STYLES -->
<script>
jQuery(document).ready(function() {   
   Village.init();
});
</script>
<!-- END JAVASCRIPTS -->
