<%@page import="com.shadt.core.entity.User"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/";
	request.setAttribute("path", basePath);
		User u = (User)request.getSession().getAttribute("user");
	if(null==u){
		response.sendRedirect("login.jsp");
	}
%>
<style type="text/css">
td {
overflow: hidden;
white-space: nowrap;
text-overflow: ellipsis;
}
</style>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-edit"></i>视频信息
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"> </a> 
					<a href="#portlet-config" data-toggle="modal" class="config"> </a> 
					<a href="javascript:;" class="reload"> </a> 
					<a href="javascript:;" class="remove"> </a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-4">
							<div id="mytoolbox"></div>
						</div>
					</div>
				</div>
				<table class="table table-striped table-hover table-bordered" id="videoTable">
				</table>
				<tbody></tbody>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>


<script type="text/javascript" src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL STYLES -->
<script src="pages/script/video.js"></script>
<!-- END PAGE LEVEL STYLES -->
<script>
jQuery(document).ready(function() {   
  	Video.init();
  	
  	$("div[class='span9']").attr("class","col-md-6 col-sm-12");
  	$("div[class='span3']").attr("class","col-md-6 col-sm-12");
  	$("div[class='span6']").attr("class","col-md-6 col-sm-12");
});
