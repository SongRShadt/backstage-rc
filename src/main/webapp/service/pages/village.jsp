<%@page import="com.shadt.core.entity.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/";
	request.setAttribute("path", basePath);
		User u = (User)request.getSession().getAttribute("user");
	if(null==u){
		response.sendRedirect("login.jsp");
	}
%>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-edit"></i>村信息
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"> </a> 
					<a href="#portlet-config" data-toggle="modal" class="config"> </a> 
					<a href="javascript:;" class="reload"> </a> 
					<a href="javascript:;" class="remove"> </a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-4">
							<div class="btn-group">
								<a  href="#addVillage"class="btn green" data-toggle="modal">
									添加 <i class="fa fa-plus"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
				<table class="table table-striped table-hover table-bordered" id="villageTable">
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT -->

<div id="addVillage" class="modal fade modal-scroll"  tabindex="-1" data-backdrop="static" data-replace="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">添加村信息</h4>
			</div>
			<div class="modal-body">
				<!-- BEGIN FORM-->
				<form id="addVillageForm" class="form-horizontal">
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3"> 
								村名称<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<input type="text" id="villageName" name="villageName" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">村Key</label>
							<div class="col-md-7">
								<input type="text" name="villageKeyValue" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								所属镇<span class="required">*</span>
							</label>
							<div class="col-md-7" id="town">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								机顶盒编号<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<input type="text" name="stbNo" id="stbNo" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								村组织编号<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<input type="text" name="orgCode" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">排序</label>
							<div class="col-md-7">
								<input type="text" name="villageSort" class="form-control" />
							</div>
						</div>
					</div>
				</form>
				<!-- END FORM-->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn red" id="formSubmit">提交</button>
				<button type="button" data-dismiss="modal" class="btn green" id="formCancel">取消</button>
			</div>
		</div>
	</div>
</div>


<div id="bindCamera" class="modal fade modal-scroll"  tabindex="-1" data-backdrop="static" data-replace="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">绑定摄像头</h4>
			</div>
			<div class="modal-body">
				<button type="button" class="btn red" id="bindAll">绑定全部</button>
				<button type="button" class="btn blue" id="bindTown">绑定本镇</button>
				<button type="button" class="btn yellow" id="bindVillage">绑定本村</button>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn green" id="bindCameraCancel">关闭</button>
			</div>
		</div>
	</div>
</div>


<div id="lookCamera" class="modal fade modal-scroll"  tabindex="-1" data-backdrop="static" data-replace="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">查看摄像头绑定</h4>
			</div>
			<div class="modal-body">
				<div id="jstree" >
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn green" id="formCancel">关闭</button>
			</div>
		</div>
	</div>
</div>



<div id="lookMettingVideo" class="modal fade modal-scroll"  tabindex="-1" data-backdrop="static" data-replace="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="mettingVideoCancel"></button>
				<h4 class="modal-title">查看视频会议绑定</h4>
			</div>
			<div class="modal-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="form-group">
							<div class="col-md-9">
								<div class="input-group">
									<div class="input-icon">
										<i class="fa fa-fw"></i>
										<select class="form-control" id="bindMettingVideo"></select>
									</div>
									<span class="input-group-btn">
									<button id="addMettingVideo" class="btn btn-success" type="button"><i class="fa fa-arrow-left fa-fw"/></i>添加</button>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<table class="table table-striped table-hover table-bordered" id="mettingVideo">
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn green" id="mettingVideoCancel">关闭</button>
			</div>
		</div>
	</div>
</div>




			
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/global/plugins/jstree/dist/jstree.min.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="pages/script/village.js"></script>
<!-- END PAGE LEVEL STYLES -->
<script>
jQuery(document).ready(function() {   
   Village.init();
});
</script>
<!-- END JAVASCRIPTS -->
