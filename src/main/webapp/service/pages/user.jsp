<%@page import="com.shadt.core.entity.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/";
	request.setAttribute("path", basePath);
	//	User u = (User)request.getSession().getAttribute("user");
	//if(null==u){
		//response.sendRedirect("login.jsp");
	//}
%>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-edit"></i>用户信息
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"> </a> 
					<a href="#portlet-config" data-toggle="modal" class="config"> </a> 
					<a href="javascript:;" class="reload"> </a> 
					<a href="javascript:;" class="remove"> </a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-4">
							<div class="btn-group">
								<a  href="#addUser"class="btn green" data-toggle="modal">
									添加用户 <i class="fa fa-plus"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
				<table class="table table-striped table-hover table-bordered" id="userTable">
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT -->

<div id="addUser" class="modal fade modal-scroll"  tabindex="-1" data-backdrop="static" data-replace="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">添加用户信息</h4>
			</div>
			<div class="modal-body">
				<!-- BEGIN FORM-->
				<form id="addUserForm" class="form-horizontal">
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3"> 
								用户名<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<input type="text" id="userName" name="userName" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								设置密码<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<input type="password" name="password" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								姓名<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<input type="text" name="name" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								性别<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<select name="sex" class="form-control">
									<option value="男">男</option>
									<option value="女">女</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								所属镇<span class="required">*</span>
							</label>
							<div class="col-md-7" id="town">
							</div>
						</div>
					</div>
				</form>
				<!-- END FORM-->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn red" id="formSubmit">提交</button>
				<button type="button" data-dismiss="modal" class="btn green" id="formCancel">取消</button>
			</div>
		</div>
	</div>
</div>








			
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/global/plugins/jstree/dist/jstree.min.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="pages/script/user.js"></script>
<!-- END PAGE LEVEL STYLES -->
<script>
jQuery(document).ready(function() {   
	User.init();
});
</script>
<!-- END JAVASCRIPTS -->
