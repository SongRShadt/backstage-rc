<%@page import="com.shadt.core.entity.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/";
	request.setAttribute("path", basePath);
	User u = (User)request.getSession().getAttribute("user");
	//if(null==u){
		//response.sendRedirect("login.jsp");
	//}
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-edit"></i>个人信息
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"> </a> 
					<a href="#portlet-config" data-toggle="modal" class="config"> </a> 
					<a href="javascript:;" class="reload"> </a> 
					<a href="javascript:;" class="remove"> </a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-4">
						</div>
					</div>
				</div>
				<table class="table table-striped table-hover table-bordered" id="userTable">
					<tr>
						<th>姓名</th>
						<th>登录名</th>
						<th>性别</th>
						<th>头像</th>
						<th>操作</th>
				<c:if test="${user!=null }">
					</tr>
					<td>${user.name }</td>
					<td>${user.userName }</td>
					<td>${user.sex }</td>
					<td>${user.portrait }</td>
					<td><a href="javascript:;" data-toggle="modal" data-target="#modifyPwd"  data-container="body" data-placement="top" data-html="true" data-original-title="修改密码" class="btn blue tooltips edit"><i class="fa fa-edit"></i></a></td>
					</tr>
				</c:if>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT -->

<div id="modifyPwd" class="modal fade modal-scroll"  tabindex="-1" data-backdrop="static" data-replace="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">修改密码</h4>
			</div>
			<div class="modal-body">
				<!-- BEGIN FORM-->
				<form id="updatePwdForm" class="form-horizontal">
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3"> 
								请输入新密码：<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<input type="password" id="newPwd" name="newPwd" class="form-control" placeholder="密码由6-16位字母或数字组成"  /></br>
								<span id="npwdInfo"></span>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3"> 
								请确认新密码：<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<input type="password" id="againPwd" name="againPwd" class="form-control" placeholder="请保持两次密码一致"  /></br>
								<span id="apwdInfo"></span>
							</div>
						</div>
						
					</div>
				</form>
				<!-- END FORM-->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn red" id="formSubmit" disabled="true">提交</button>
				<button type="button" data-dismiss="modal" class="btn green" id="formCancel">取消</button>
			</div>
		</div>
	</div>
</div>

			
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/global/plugins/jstree/dist/jstree.min.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- END PAGE LEVEL STYLES -->
<script>
	jQuery(document).ready(function() {
		 $("#formSubmit").on("click",function(e){
			 var npwd= $("#newPwd").val();
			 $.ajax({
				 url:"/backstage/userC/updatePwd",
				 data:{newPwd:npwd},
				 success:function(result){
					 result=eval("("+result+")");
					 alert(result.msg);
					 $("#modifyPwd").modal("hide");
				 }
			 })
		 })
		 
		 $("#newPwd").blur(function(){
			 var re =/[0-9a-zA-Z]{6,16}/;
			 var va = $("#newPwd").val();
			 if(re.test(va)){
				 $("#npwdInfo").html("");
			 }else{
				 $("#npwdInfo").html("<font color='red' size='3'>你输入的密码不符合要求！</font>");
			 }
		 })
		 
		 $("#againPwd").blur(function(){
			 var re=$("#newPwd").val();
			 var va=$("#againPwd").val();
			 if(re==va){
				 $("#apwdInfo").html("");
				 $("#formSubmit").attr("disabled",false);
			 }else{
				 $("#apwdInfo").html("<font color='red' size='3'>两次密码不一致！</font>");
				 $("#formSubmit").attr("disabled",true);
			 }
		 })
	});

</script>
<!-- END JAVASCRIPTS -->
