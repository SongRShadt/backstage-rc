/**
 * 摄像头信息
 */
var Mettingvideo = function(){
	/**
	 * 初始化视频会议数据表格
	 */
	var handleTable = function(){
		var vmTable = $(".videomettingTable");
		var vvTable = vmTable.dataTable({
	        "lengthMenu": [
	               [10,20,30,40,50,100],
	               [10,20,30,40,50,100]
	        ],
	        "pageLength": 10,
	        "language": {
	        	"processing": "数据加载中...",
	            "emptyTable": "暂无数据",
	            "info": "第_START_到_END_条记录,共_TOTAL_条记录",
	            "infoEmpty": "没有找到记录",
	            "infoFiltered": "",
	            "lengthMenu": "每页显示 _MENU_ 条记录",
	            "search": "搜索:",
	            "zeroRecords": "未找到匹配记录",
	            "paginate": {
	                "previous":"上一页",
	                "next": "下一页",
	                "last": "末页",
	                "first": "首页"
	            }
	        },
	        "columnDefs": [
	            {'orderable': false,"data":"id",'targets': [0],'Sortable': false,"visible": false},
	            {'orderable': false,"data":"name",'targets': [1],'Sortable': false,"title":"视频会议名称",
	            	"render":function(data, type, full){
	                	if(full.name===""){
	                		return"<span style='color:red;'>无</span>";
	                    }else{
	                    	return full.name;
	                    }
	                }
	            },
	            {'orderable': false,"data":"sort",'targets': [2],"title":"排序",
	                "render":function(data, type, full){
	                	if(full.sort===""){
	                		return"<span style='color:red;'>无</span>";
	                    }else{
	                    	return full.sort;
	                    }
	                }
	            },{'orderable': false,"data":"villageName",'targets': [3],"title":"所属会场",
	                "render":function(data, type, full){
	                	if(full.villageName===""){
	                		return"<span style='color:red;'>无</span>";
	                    }else{
	                    	return full.villageName;
	                    }
	                }
	            },{'orderable': false,"data":"rtsp",'targets': [4],"title":"视频源地址",
	                "render":function(data, type, full){
	                	if(full.rtsp===""||full.rtsp===undefined){
	                		return"<span style='color:red;'>无</span>";
	                    }else{
	                    	return full.rtsp;
	                    }
	                }
	            },{'orderable': false,"data":"videourl",'targets': [5],"title":"视频播放地址",
	                "render":function(data, type, full){
	                	if(full.videourl===""||full.videourl===undefined){
	                		return"<span style='color:red;'>无</span>";
	                    }else{
	                    	return full.videourl;
	                    }
	                }
	            },{'orderable': false,"data":"status",'targets': [6],"title":"状态",
	                "render":function(data, type, full){
	                	if(Number(full.status)===0){
	                		return "<span class='label label-success'>链接</span>";
	                    }else if(Number(full.status)===1){
	                    	return "<span class='label label-danger'>断开</span>";
	                    }
	                }
	            },{'orderable': false,'targets': [7],"title":"操作",
	          	 "render":function(data,type,full){
	          		 //0、链接 1、断开  
	          		var btn = "";
	          		    btn +="&nbsp;<button class='btn btn-primary' type='button' id='bindBtn'><i class='fa fa-search'></i> 绑定</button>";
	          			btn +="&nbsp;<button class='btn btn-info'    type='button' id='editBtn'>  <i class='fa fa-edit'></i> 编辑</button>";
	          		if(Number(full.status)===0){
	          			btn +="&nbsp;<button class='btn btn-warning'  type='button' id='offBtn'> <i class='fa fa-pause'></i> 断开</button>";
	          		}else if(Number(full.status)===1){
	          			btn +="&nbsp;<button class='btn btn-success' type='button' id='linkBtn'>  <i class='fa fa-play'></i> 链接</button>";
	          		}
	          			btn +="&nbsp;<button class='btn btn-danger'  type='button' id='delBtn'><i class='fa fa-trash-o'></i> 删除</button>";
	          		 return btn;
	          	 }
	           },
	        ],
	        "bAutoWidth":false,
	        "bProcessing": true, // 是否显示取数据时的那个等待提示
		    "bServerSide": true,//这个用来指明是通过服务端来取数据
	        "sAjaxSource":"../videometting/all",//请求的地址
	        "fnServerData": function(sSource,aoData,fnCallback){				  // 获取数据的处理函数
	        	$.ajax({
	        		url:sSource,
	        		type : "post",
	        		data:{
	        			"aoData":JSON.stringify(aoData)
	        		},
	        		dataType : "json",
	        		async:false,
	        		success:function(result){
	        			if(result.success){
	        				fnCallback(result.obj);
	        				
	        			}else{
	        			}
	        		},error:function(msg){
	        		}
	        	});
	        } 
	    });
		
		//绑定村权限
		vmTable.on("click","#bindBtn",function(e){
			e.preventDefault();
			var nRow = $(this).parents('tr');
	    	var dData = vvTable.fnGetData(nRow);
			var townSelect = "";
			$.ajax({
				url:"../videometting/bind/town",
        		type : "post",
        		dataType : "json",
        		async:false,
        		success:function(result){
        			if(result.success){
        				$.each(result.obj,function(i){
        					townSelect+="<option id='"+this.townId+"'>"+this.townName+"</option>";
        				});
        			}else{
        			}
        		},error:function(msg){
        		}
			});
			$("#town").html(townSelect);
			var handleVillage = function(){
				var villageSelect="";
				$.ajax({
					url:"../videometting/bind/village",
	        		type : "post",
	        		dataType : "json",
	        		data:{
	        			townid:$("#town").find("option:checked").attr("id"),
	        			videoid:dData.id
	        		},async:false,
	        		success:function(result){
	        			if(result.success){
	        				if(result.obj.length<=0){
	        					villageSelect+="<option>该镇已全部绑定</option>";
	        					$("#bindVillage").addClass("disabled");
	        				}
	        				$.each(result.obj,function(i){
	        					villageSelect+="<option id='"+this.villageId+"'>"+this.villageName+"</option>";
	        				});
	        			}else{
	        			}
	        		},error:function(msg){
	        		}
				});
				$("#vil").html(villageSelect);
			};
			handleVillage();
			$("#town").on("change",function(e){
				$("#bindVillage").removeClass("disabled");
				handleVillage();
			});
			$("#bindVillage").on("click",function(e){
				$.ajax({
					url:"../videometting/bind/add",
	        		type : "post",
	        		dataType : "json",
	        		data:{
	        			vil:$("#vil").find("option:checked").attr("id"),
	        			videoid:dData.id
	        		},async:false,
	        		success:function(result){
	        			if(result.success){
	        				handleVillage();
	        				villageTable.fnDraw(true);
	        			}else{
	        			}
	        		},error:function(msg){
	        		}
				});
			});
			
			
			$("#mettingVideoBind").modal();
			var villageTable = $(".villageBind");
			var villageDataTable = villageTable.dataTable({
		        "lengthMenu": [
    	               [10,20,30,40,50,100],
    	               [10,20,30,40,50,100]
    	        ],
    	        "pageLength": 10,
    	        "language": {
    	        	"processing": "数据加载中...",
    	            "emptyTable": "暂无数据",
    	            "info": "第_START_到_END_条记录,共_TOTAL_条记录",
    	            "infoEmpty": "没有找到记录",
    	            "infoFiltered": "",
    	            "search": "搜索:",
    	            "lengthMenu": "每页显示 _MENU_ 条记录",
    	            "zeroRecords": "未找到匹配记录",
    	            "paginate": {
    	                "previous":"上一页",
    	                "next": "下一页",
    	                "last": "末页",
    	                "first": "首页"
    	            }
    	        },
    	        "columnDefs": [
    	            {'orderable': false,"data":"villageId",'targets': [0],'Sortable': false,"visible": false},
    	            {'orderable': false,"data":"villageName",'targets': [1],'Sortable': false,"title":"村名称",
    	            	"render":function(data, type, full){
    	                	if(full.villageName===""){
    	                		return"<span style='color:red;'>无</span>";
    	                    }else{
    	                    	return full.villageName;
    	                    }
    	                }
    	            },{'orderable': false,'targets': [2],"title":"操作",
    	          	 "render":function(data,type,full){
    	          		var btn = "&nbsp;<button class='btn btn-danger' type='button' id='delBindBtn'><i class='fa fa-trash-o'></i> 删除</button>";
    	          		return btn;
    	          	 }
    	           },
    	        ],"bAutoWidth":false,"bProcessing": true,"bServerSide": true,
    	        "sAjaxSource":"../videometting/bind/all",//请求的地址
    	        "fnServerData": function(sSource,aoData,fnCallback){
    	        	$.ajax({
    	        		url:sSource,
    	        		type : "post",
    	        		data:{
    	        			"videoid":dData.id,
    	        			"aoData":JSON.stringify(aoData)
    	        		},
    	        		dataType : "json",
    	        		async:false,
    	        		success:function(result){
    	        			if(result.success){
    	        				fnCallback(result.obj);
    	        			}else{
    	        			}
    	        		},error:function(msg){
    	        		}
    	        	});
    	        } 
    	    });
			
			villageTable.on("click","#delBindBtn",function(e){
				e.preventDefault();
				var vRow = $(this).parents('tr');
		    	var vData = villageTable.fnGetData(vRow);
				$.ajax({
	        		url:"../videometting/bind/delete",
	        		type : "post",
	        		data:{
	        			"videoid":dData.id,
	        			"villageid":vData.villageId
	        		},
	        		dataType : "json",
	        		async:false,
	        		success:function(result){
	        			if(result.success){
	        				handleVillage();
	        				villageTable.fnDraw(true);
	        			}else{
	        			}
	        		},error:function(msg){
	        		}
	        	});
				
			});
		});
		//编辑视频会议
		vmTable.on("click","#editBtn",function(e){
			e.preventDefault();	
			var nRow = $(this).parents('tr');
	    	var dData = vvTable.fnGetData(nRow);
	    	var handlEditModal=function(){
		    	var villageid="";
		    	$.ajax({
		    		url:"../videometting/get",
		    		type : "post",
		    		data:{
		    			"videoid":dData.id
		    		},
		    		dataType : "json",
		    		async:false,
		    		success:function(result){
		    			if(result.success){
		    				$("#edit_id").val(result.obj.id);
		    				$("#edit_name").val(result.obj.name);
		    				$("#edit_rtsp").val(result.obj.rtsp);
		    				$("#edit_sort").val(result.obj.sort);
		    				villageid=result.obj.villageId;
		    			}else{
		    			}
		    		},error:function(msg){
		    		}
		    	});
		    	var villageSelect="";
		    	$.ajax({
					url:"../videometting/town/all",
		    		type : "post",
		    		dataType : "json",
		    		async:false,
		    		success:function(result){
		    			if(result.success){
		    				$.each(result.obj,function(i){
		    					if(villageid==this.villageId){
		    						villageSelect+="<option value='"+this.villageId+"' checked>"+this.villageName+"</option>";
		    					}else{
		    						villageSelect+="<option value='"+this.villageId+"'>"+this.villageName+"</option>";
		    					}
		    				});
		    			}else{
		    			}
		    		},error:function(msg){
		    		}
				});
				$("#edit_village").html(villageSelect);
	    	};
	    	handlEditModal();
	    	$("#editVideoMetting").modal();
	    	$("#editSubmit").on("click",function(e){
				if($("#editVideoMettingForm").validate().form()){
					$.ajax({
						url:"../videometting/update",
			    		type : "post",
			    		dataType : "json",
			    		data :{
			    			id:$("#edit_id").val(),
			    			name:$("#edit_name").val(),
			    			rtsp:$("#edit_rtsp").val(),
			    			sort:$("#edit_sort").val(),
			    			villageid:$("#edit_village").val()
			    		},async:false,
			    		success:function(result){
			    			console.info(result);
			    			if(result.success){
			    				$("#editVideoMetting").modal('hide');
			    				handlEditModal();
			    				vmTable.fnDraw(true);
			    			}
			    		},error:function(msg){
			    		}
					});
				}
				$("#editformCancel").on("click",function(e){
					handlEditModal();
				});
	    	});
	    	
		});
		//断开sewise
		vmTable.on("click","#offBtn",function(e){
			e.preventDefault();	
			var nRow = $(this).parents('tr');
	    	var dData = vvTable.fnGetData(nRow);
	    	console.info(dData.id);
	    	$.ajax({
	    		url:"../videometting/stop",
	    		type : "post",
	    		dataType : "json",
	    		data :{
	    			videoid:dData.id
	    		},
	    		async:false,
	    		success:function(result){
	    			if(result.success){
	    				vmTable.fnDraw(true);
	    			}
	    		},error:function(msg){
	    		}
	    	});
		});
		//链接sewise
		vmTable.on("click","#linkBtn",function(e){
			e.preventDefault();	
			var nRow = $(this).parents('tr');
	    	var dData = vvTable.fnGetData(nRow);
	    	$.ajax({
	    		url:"../videometting/start",
	    		type : "post",
	    		dataType : "json",
	    		data :{
	    			videoid:dData.id
	    		},
	    		async:false,
	    		success:function(result){
	    			if(result.success){
	    				vmTable.fnDraw(true);
	    			}
	    		},error:function(msg){
	    		}
	    	});
		});
		//删除视频会议
		vmTable.on("click","#delBtn",function(e){
			e.preventDefault();	
			var nRow = $(this).parents('tr');
	    	var dData = vvTable.fnGetData(nRow);
	    	$.ajax({
	    		url:"../videometting/delete",
	    		type : "post",
	    		dataType : "json",
	    		data :{
	    			id:dData.id
	    		},
	    		async:false,
	    		success:function(result){
	    			if(result.success){
	    				vmTable.fnDraw(true);
	    			}
	    		},error:function(msg){
	    		}
	    	});
	    	
		});
		
		//添加视频会议表单提交按钮事件
		$("#addSubmit").on("click",function(){
			if($("#addVideoMettingForm").validate().form()){
				$.ajax({
					url:"../videometting/add",
		    		type : "post",
		    		dataType : "json",
		    		data :$("#addVideoMettingForm").serialize(),
		    		async:false,
		    		success:function(result){
		    			console.info(result);
		    			if(result.success){
		    				$("#addVideoMetting").modal('hide');
		    				$("#addVideoMettingForm")[0].reset();
		    				vmTable.fnDraw(true);
		    			}
		    		},error:function(msg){
		    		}
				});
			}
		});
		$("#formCancel").on("click",function(e){
			$("#addVideoMettingForm")[0].reset();
		});
	};
	
	/**
	 * 初始化视频会议添加表格
	 */
	var handleAddVideoMettingForm = function(){
		var villageSelect = "";
		$.ajax({
			url:"../videometting/town/all",
    		type : "post",
    		dataType : "json",
    		async:false,
    		success:function(result){
    			if(result.success){
    				$.each(result.obj,function(i){
    					villageSelect+="<option value='"+this.villageId+"'>"+this.villageName+"</option>";
    				});
    			}else{
    			}
    		},error:function(msg){
    		}
		});
		$("#village").html(villageSelect);
		var sewiseSelect = "";
		$.ajax({
			url:"../videometting/sewise/all",
    		type : "post",
    		dataType : "json",
    		async:false,
    		success:function(result){
    			if(result.success){
    				$.each(result.obj,function(i){
    					sewiseSelect+="<option value='"+this.id+"'>"+this.name+"</option>";
    				});
    			}else{
    			}
    		},error:function(msg){
    		}
		});
		$("#sewise").html(sewiseSelect);
	};
	
	/**
	 * 初始化添加视频会议表格验证
	 */
	var handleAddVideoMettingFormValidate = function(){
		 $('#addVideoMettingForm').validate({
            errorElement: 'span', 
            errorClass: 'help-block help-block-error', 
            focusInvalid: false,ignore: "",  
            debug:true,
            rules: {
            	name: {
                    required: true,
                },
                rtsp : {
                	required:true
                },
            },messages:{
            	name:{
            		required:"视频会议名称不能为空",
            	},
            	rtsp : {
            		required:"视频源不能为空"
                }
            },
            highlight: function (element) { 
                $(element).closest('.form-group').addClass('has-error'); 
            },
            unhighlight: function (element) { 
                $(element).closest('.form-group').removeClass('has-error'); 
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); 
            } 
        });
	};
	

	/**
	 * 初始化编辑视频会议表格验证
	 */
	var handleEditVideoMettingFormValidate = function(){
		 $('#editVideoMettingForm').validate({
            errorElement: 'span', 
            errorClass: 'help-block help-block-error', 
            focusInvalid: false,ignore: "",  
            debug:true,
            rules: {
            	edit_name: {
                    required: true,
                },
                edit_rtsp : {
                	required:true
                },
            },messages:{
            	edit_name:{
            		required:"视频会议名称不能为空",
            	},
            	edit_rtsp : {
            		required:"视频源不能为空"
                }
            },
            highlight: function (element) { 
                $(element).closest('.form-group').addClass('has-error'); 
            },
            unhighlight: function (element) { 
                $(element).closest('.form-group').removeClass('has-error'); 
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); 
            } 
        });
	};
   
    
    
	return {
		init : function(){
			handleTable();
			handleAddVideoMettingForm();
			handleAddVideoMettingFormValidate();
		},
	}
}();