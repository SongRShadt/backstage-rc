var Town = function () {
	/**
	 * 获取项目根路径
	 * 
	 * @returns
	 */
	var getPath = function() {
		var curWwwPath = window.document.location.href;// 获取当前网址，如： http://localhost:8080/ems/Pages/Basic/Person.jsp
		var pathName = window.document.location.pathname; // 获取主机地址之后的目录，如： /ems/Pages/Basic/Person.jsp
		var pos = curWwwPath.indexOf(pathName);
		var localhostPath = curWwwPath.substring(0, pos); // 获取主机地址，如： http://localhost:8080
		var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);// 获取带"/"的项目名，如：/ems
		return (localhostPath + projectName);
	};
	
	/**
	 * 添加表单验证
	 */
    var handleValidation = function() {
       $('#addTownForm').validate({
            errorElement: 'span', 
            errorClass: 'help-block help-block-error', 
            focusInvalid: false,ignore: "",  
            rules: {
            	townName: {
                    minlength: 2,
                    required: true,
                    remote:{//自定义验证//后台验证镇名称是否存在
                    	type:"post",
                    	url:getPath()+"/townC/checkValue",
                    	data : {
                    		"value": function() {return $("#townName").val();}
                    	}
                    }
                },townSort: {
                	number: true
                }
            },messages:{
            	townName:{
            		minlength:"镇名称长度不能小于2！",
            		required:"镇名称不能为空！",
            		remote:"此镇已存在！"
            	},townSort:{
            		number:"请输入数字！"
            	}
            },
            highlight: function (element) { 
                $(element).closest('.form-group').addClass('has-error'); 
            },
            unhighlight: function (element) { 
                $(element).closest('.form-group').removeClass('has-error'); 
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); 
            } 
        });
    };

    /**
     * 初始化表格
     */
    var handleTable = function () {
    	$(".reload").bind("click",function(){
    		oTable.fnDraw(true);
    	});
    	
    	
        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }
            oTable.fnDraw();
        }

        /**
         * 编辑行
         */
        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            var btn = "<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-html=\"true\" data-original-title=\"保存\" class=\"btn yellow tooltips edit\"><i class=\"fa fa-save\"></i></a>";
     		btn+="<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-original-title=\"取消\" class=\"btn green tooltips cancel\"><i class=\"fa fa-history\"></i></a>";
            jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData.townName + '">';
            jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData.townKeyValue + '">';
            jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData.townSort + '">';
            jqTds[3].innerHTML = btn;
        }
        
        /**
         * 保存行
         */
        function saveRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            var aData = oTable.fnGetData(nRow);
            $.ajax({
            	url : getPath()+"/townC/update",
            	type:"post",
            	dataType:"json", 
            	data:{
            		"townId":aData.townId,
            		"townName":jqInputs[0].value,
            		"townKeyValue":jqInputs[1].value,
            		"townSort":jqInputs[2].value
            	},
            	success:function(data){
            		if(data.success){
            			oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
        	            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
        	            var btn = "<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-html=\"true\" data-original-title=\"保存\" class=\"btn yellow tooltips edit\"><i class=\"fa fa-edit\"></i></a>";
        	    		btn+="<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-original-title=\"删除\" class=\"btn red tooltips delete\"><i class=\"fa fa-trash-o\"></i></a>";
        	            oTable.fnUpdate(btn, nRow, 3, false);
        	            oTable.fnDraw();
            		}
            	},error:function(msg){
            	}
            });
        };

        /**
         * 取消编辑行
         */
        function cancelEditRow(oTable, nRow) {
            oTable.fnDraw();
        };

        /**
         * 数据表格
         */
        var table = $('#townTable');
        var oTable = table.dataTable({
            "lengthMenu": [
                [10,20,30,40,50,100],
                [10,20,30,40,50,100]
            ],
            "pageLength": 10,
            "language": {
            	"processing": "数据加载中...",
                "emptyTable": "暂无数据",
                "info": "第_START_到_END_条记录,共_TOTAL_条记录",
                "infoEmpty": "没有找到记录",
                "infoFiltered": "",
                "lengthMenu": "显示 _MENU_ 条记录",
                "search": "搜索:",
                "zeroRecords": "未找到匹配记录",
                "paginate": {
                    "previous":"上一页",
                    "next": "下一页",
                    "last": "末页",
                    "first": "首页"
                }
            },
            "columnDefs": [
                {"targets":[0],"data":"townId",'Sortable': false,"visible": false,
                	"render":function(data,type,full){
                		if(full.townId==undefined)
                			return "";
                		else
                			return full.townId;
                	}
                },
                {'orderable': false,"data":"townName",    'targets': [1],"title":"镇名称",},
                {'orderable': false,"data":"townKeyValue",'targets': [2],"title":"镇key",
                    "render":function(data, type, full){
                    	if(full.townKeyValue==="")
                    		return"<span style='color:red;'>无</span>";
                        else
                        	return full.townKeyValue;
                    }
                },{'orderable': false,"data":"townSort",'targets': [3],"title":"排序",
                    "render":function(data, type, full){
                    	if(full.townSort===0)
                    		return"<span style='color:red;'>无</span>";
                        else
                        	return full.townSort;
                    }
                },
               {'orderable': false,'targets': [4],"title":"操作",
              	 "render":function(data,type,full){
              		 var btn = "<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-html=\"true\" data-original-title=\"编辑\" class=\"btn blue tooltips edit\"><i class=\"fa fa-edit\"></i></a>";
              		 btn+="<a href=\"javascript:alert(1);\ data-container=\"body\" data-placement=\"top\" data-original-title=\"删除\" class=\"btn red  tooltips delete\"><i class=\"fa fa-trash-o\"></i></a>";
              		 return btn;
              	 }
               },
            ],
            "bAutoWidth":false,
            "bProcessing": true, // 是否显示取数据时的那个等待提示
		    "bServerSide": true,//这个用来指明是通过服务端来取数据
            "sAjaxSource": getPath()+"/townC/getToDataTable",//请求的地址
            "fnServerData": function(sSource,aoData,fnCallback){				  // 获取数据的处理函数
            	$.ajax({
            		url:sSource,
            		type : "post",
            		data:{
            			"aoData":JSON.stringify(aoData)
            		},
            		dataType : "json",
            		async:false,
            		success:function(result){
            			result=eval("("+result+")");
            			fnCallback(result);
            		},error:function(msg){
            			console.info(msg);
            		}
            	});
            }
        });
        var tableWrapper = $("#townTable_wrapper");
        tableWrapper.find(".dataTables_length select").select2({
            showSearchInput: false 
        }); 
        var nEditing = null;
        var nNew = false;
        /**
         * 绑定删除按钮事件
         */
        table.on('click', '.delete', function (e) {
            e.preventDefault();
            if (confirm("你确定要删除这一条记录吗?") == false) {
                return;
            }
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            $.ajax({
            	url : getPath()+"/townC/delete",
            	type:"post",
            	dataType:"json",
            	data:{
            		"id":aData.townId
            	},
            	success : function(data){
            		if(data.success){
            			 oTable.fnDeleteRow(nRow);
            		}
            	},error:function(msg){
            		
            	}
            });
        });

        /**
         * 绑定取消按钮事件
         */
        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        /**
         * 绑定编辑按钮事件
         */
        table.on('click', '.edit', function (e) {
            e.preventDefault();
            var nRow = $(this).parents('tr')[0];
            if (nEditing !== null && nEditing != nRow) {
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML=="<i class=\"fa fa-save\"></i>") {
                saveRow(oTable, nEditing);
                nEditing = null;
            } else {
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });
    };

    
    var handleForm = function(){
    	$("#formSubmit").on("click",function(e){
    		if($("#addTownForm").validate().form()){
				$.ajax({
					url:getPath()+"/townC/add",
					type:"post",
					dataType:"json",
					data:$("#addTownForm").serialize(),
					success:function(data){
						$('#townTable').dataTable().fnDraw();
					},error : function(msg){
						console.info();
					}
				});
				$("#formCancel").click();
    		}
    	});
    	$("#formCancel").on("click",function(e){
    		$("#addTownForm")[0].reset();
    		$(".form-group").closest('.form-group').removeClass('has-error'); 
    		$("#townName-error").html("");
    	});
    };
    
    return {
        init: function () {
            handleValidation();
            handleTable();
            handleForm();
        }
    };

}();