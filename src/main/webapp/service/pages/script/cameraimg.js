/**
 * 摄像头信息
 */
var Cameraimg = function(){
	

	
	/**
	 * 获取项目根路径
	 * 
	 * @returns
	 */
	var getPath = function() {
		var curWwwPath = window.document.location.href;// 获取当前网址，如： http://localhost:8080/ems/Pages/Basic/Person.jsp
		var pathName = window.document.location.pathname; // 获取主机地址之后的目录，如： /ems/Pages/Basic/Person.jsp
		var pos = curWwwPath.indexOf(pathName);
		var localhostPath = curWwwPath.substring(0, pos); // 获取主机地址，如： http://localhost:8080
		var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);// 获取带"/"的项目名，如：/ems
		return (localhostPath + projectName);
	};
	
	/**
     * 初始化表格
     */
    var handleTable = function () {
        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }
            oTable.fnDraw(true);
        }

      

        /**
         * 数据表格
         */
        var table = $('#cameraTable');
        
        var oTable = table.dataTable({
            "lengthMenu": [
                [10, 20, 30,40, 50,100],
                [10, 20, 30, 40,50,100] 
            ],
            "pageLength": 10,
            "language": {
            	"processing": "数据加载中...",
                "emptyTable": "暂无数据",
                "info": "第_START_到_END_条记录,共_TOTAL_条记录",
                "infoEmpty": "没有找到记录",
                "infoFiltered": "",
                "lengthMenu": "显示 _MENU_ 条记录",
                "search": "搜索",
                "zeroRecords": "未找到匹配记录",
                "paginate": {
                    "previous":"上一页",
                    "next": "下一页",
                    "last": "末页",
                    "first": "首页"
                }
            },
            "columnDefs": [
                {"targets":[0],"data":"id",'Sortable': false,"visible": false,
                	"render":function(data,type,full){
                		return full.id==undefined?"":full.id;
                	}
                },{'orderable': false,"data":"cameraVoName", 'targets': [1],"title":"名称",
                    "render":function(data, type, full){
                    	return full.cameraVoName!=""?full.cameraVoName:"<span style='color:red'>无</span>";
                    }
                },
                {'orderable': false,"data":"creattime", 'targets': [2],"title":"创建时间",
                    "render":function(data, type, full){
                    	return full.creattime!=""?full.creattime:"<span style='color:red'>无</span>";
                    }
                }
                ,{'orderable': false,"data":"url", 'targets': [3],"title":"图片显示",
                    "render":function(data, type, full){
                    	return full.url!=""?"<a class='example-image-link' href='"+full.url+"' data-lightbox='example-1'><img alt='加载失败' class='example-image' src='"+full.url+"' style='width:55px;height:40px;'/>":"<span style='color:red'>无</span>";
                    }
                },{'orderable': false,'targets': [4],"title":"操作",
              	 "render":function(data,type,full){
              		 var btn = "<a href=\"javascript:;\" data-container=\"body\" data-placement=\"top\" data-html=\"true\" data-original-title=\"下载\" class=\"btn blue tooltips download\"><i class=\"fa fa-cloud-download\"></i></a>";
              		 btn+="<a href=\"javascript:;\" data-container=\"body\" data-placement=\"top\" data-original-title=\"删除\" class=\"btn red  tooltips delete\"><i class=\"fa fa-trash-o\"></i></a>";
              		 return btn;
              	 }
               },
            ],
            "bAutoWidth":false,
            "bProcessing": true, // 是否显示取数据时的那个等待提示
		    "bServerSide": true,//这个用来指明是通过服务端来取数据
            "sAjaxSource":"../cameraImg/getByPaging",//请求的地址
            "fnServerData": function(sSource,aoData,fnCallback){				  // 获取数据的处理函数
            	
            	$.ajax({
            		url:sSource,
            		type : "post",
            		data:{
            			"aoData":JSON.stringify(aoData)
            		},
            		dataType : "json",
            		async:false,
            		success:function(result){
            			result=eval("("+result+")");
            			console.info(result);
            			fnCallback(result);
            		} 
            	});
            } ,
            "order": [
                [0, "asc"]
            ] 
            
        });
        var tableWrapper = $("#townTable_wrapper");
        tableWrapper.find(".dataTables_length select").select2({
            showSearchInput: false 
        }); 
        var nEditing = null;
        var nNew = false;
        
        /**
         * 绑定下载图片按钮事件
         */
        table.on('click', '.download', function (e) {
       	 var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            $.ajax({
            	url : getPath()+"/cameraImg/download",
            	type:"post",
            	dataType:"json",
            	data:{
            		"url":aData.url
            	},
            	success : function(data){
            		   alert(data.msg);
            		if(data.success){
            			 oTable.fnDeleteRow(nRow);
            		}
            	},error:function(msg){
            	}
            });
       });
        /**
         * 绑定删除按钮事件
         */
        table.on('click', '.delete', function (e) {
            e.preventDefault();
            if (!confirm("你确定要删除这一条记录吗?")) {
                return;
            }
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            console.info(aData);
            $.ajax({
            	url : getPath()+"/cameraImg/delete",
            	type:"post",
            	dataType:"json",
            	data:{
            		"id":aData.id
            	},
            	success : function(data){
            		if(data.success){
            			 oTable.fnDeleteRow(nRow);
            		}
            	},error:function(msg){
            	}
            });
        });

       

        /**
         * 绑定编辑按钮事件
         *//*
        table.on('click', '.edit', function (e) {
        	e.preventDefault();
    	  	var nRow = $(this).parents('tr')[0];
    	  	var aData = oTable.fnGetData(nRow);
    	  	$("#editTown option[value='"+aData.village.townId+"']").attr("selected",true);
    	  	$.getJSON(getPath()+"/villageC/getByAll",{"townId":aData.village.townId},function(data){
        		if(data.success){
        			var villages ="<option value=''>---请选择村---</option>";
        			$.each(data.obj,function(i){
    					villages+="<option value='"+this.villageId+"'>"+this.villageName+"</option>";
        			});
        			$("#editVillageId").html(villages);
        			$("#editVillageId option[value='"+aData.village.villageId+"']").attr("selected",true);
        		}
        	});
    	  	$('#editCameraType[value="'+aData.cameraType+'"]').parent('span').addClass('checked');
    	 	$('#editCameraType[value="'+aData.cameraType+'"]').attr("checked",true);
    	  	$("#editCameraNo").attr("value",aData.cameraNo);
    	  	$("#editCameraName").attr("value",aData.cameraName);
    	  	$.getJSON(getPath()+"/cameraC/getById",{"id":aData.cameraNo},function(data){
    	  		if(data.success){
    	  			$("#edit_s_ipurl").attr("value",data.obj.s_ipurl);
    	  		}
    	  	});
        	$("#editCamera").modal('show');
        });*/
    };

    
    
	return {
		init : function(){
			handleTable();
		},
	}
	
}();