/**
 * 视频信息
 */
var Video = function(){
	
	var getPath = function() {
		var curWwwPath = window.document.location.href;// 获取当前网址，如： http://localhost:8080/ems/Pages/Basic/Person.jsp
		var pathName = window.document.location.pathname; // 获取主机地址之后的目录，如： /ems/Pages/Basic/Person.jsp
		var pos = curWwwPath.indexOf(pathName);
		var localhostPath = curWwwPath.substring(0, pos); // 获取主机地址，如： http://localhost:8080
		var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);// 获取带"/"的项目名，如：/ems
		return (localhostPath + projectName);
	};
	
	
	
    var handleTable = function () {
        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }
            oTable.fnDraw();
        }
        var table = $('#videoTable');
        var oTable = table.dataTable({
            "lengthMenu": [
                [10,20,30,40,50,100],
                [10,20,30,40,50,100] 
            ],
            "pageLength": 10,
            "language": {
            	"processing": "数据加载中...",
                "emptyTable": "暂无数据",
                "info": "第_START_到_END_条记录,共_TOTAL_条记录",
                "infoEmpty": "没有找到记录",
                "infoFiltered": "",
                "lengthMenu": "显示 _MENU_ 条记录",
                "search": "搜索:",
                "zeroRecords": "未找到匹配记录",
                "paginate": {
                    "previous":"上一页",
                    "next": "下一页",
                    "last": "末页",
                    "first": "首页"
                }
            },
            "columnDefs": [
                {"targets":[0],"data":"id",'Sortable': false,"visible": false,
                	"render":function(data,type,full){
                		return full.id==undefined?"":full.id;
                	}
                },{'orderable': false,"data":"cameraVo",'targets': [1],"title":"摄像头名称",
                	"render":function(data,type,full){
                		return full.cameraVo!=""&&full.cameraVo!=null?full.cameraVo.cameraName:"<span style='color:red'>无</span>";
                	}
                },{'orderable': false,"data":"taskId", 'targets': [2],"title":"任务编号",
                    "render":function(data, type, full){
                    	return full.taskId!=""?full.taskId:"<span style='color:red'>无</span>";
                    }
                },{'orderable': false,"data":"playUrl", 'targets': [3],"title":"播放路径",
                    "render":function(data, type, full){
                    	return full.playUrl!=""?full.playUrl:"<span style='color:red'>无</span>";
                    }
                },{'orderable': false,"data":"startTime", 'targets': [4],"title":"开始时间",
                    "render":function(data, type, full){
                    	return full.startTime!=""?full.startTime:"<span style='color:red'>无</span>";
                    }
                },{'orderable': false,"data":"endTime", 'targets': [5],"title":"结束时间",
                    "render":function(data, type, full){
                    	return full.endTime!=""?full.endTime:"<span style='color:red'>无</span>";
                    }
                },{'orderable': false,"data":"duration", 'targets': [6],"title":"视频时长",
                    "render":function(data, type, full){
                    	return full.duration!=""?full.duration:"<span style='color:red'>无</span>";
                    }
                },{'orderable': false,'targets': [7],"title":"操作",
              	 "render":function(data,type,full){
              		 var btn = "<a href=\"javascript:;\" data-container=\"body\" data-placement=\"top\" data-html=\"true\" data-original-title=\"下载\" class=\"btn blue tooltips download\"><i class=\"fa fa-cloud-download\"></i></a>";
              		 btn+="<a href=\"javascript:;\" data-container=\"body\" data-placement=\"top\" data-original-title=\"删除\" class=\"btn red  tooltips delete\"><i class=\"fa fa-trash-o\"></i></a>";
              		 return btn;
              	 }
               },
            ],
            "bFilter":true,
            "bAutoWidth":true,
            "bProcessing": true, // 是否显示取数据时的那个等待提示
		    "bServerSide": true,//这个用来指明是通过服务端来取数据
            "sAjaxSource": getPath()+"/videoC/getAll",//请求的地址
            "fnServerData": function(sSource,aoData,fnCallback){ // 获取数据的处理函数
            	var times = $('#reportrange span').html(); //获得时间控件的值
            	$.ajax({
            		url:sSource,
            		type : "post",
            		data : "aoData="+JSON.stringify(aoData)+"&time="+times,
            		dataType : "json",
            		async:false,
            		success:function(result){
            			result=eval("("+result+")");
            			fnCallback(result);
            		} 
            	});
            } ,
            "order": [
                [0, "asc"]
            ],
           initComplete: function(){
        	   var dataPlugin = '<div id="reportrange" class="pull-left dateRange" style="width:400px;margin-left: -1px"> '+'日期：<i class="glyphicon glyphicon-calendar fa fa-calendar"></i> '+ '<span id="searchDateRange"></span> '+'<b class="caret"></b></div> ';
           $('#mytoolbox').append(dataPlugin);
           //时间插件
         /*  $('#reportrange span').html(moment().subtract('hours', 1).format('YYYY-MM-DD HH:mm:ss') + ' - ' + moment().format('YYYY-MM-DD HH:mm:ss'));*/
     
           $('#reportrange').daterangepicker(
                   {
                       // startDate: moment().startOf('day'),
                       //endDate: moment(),
                       //minDate: '01/01/2012',    //最小时间
                       maxDate : moment(), //最大时间
                      // dateLimit : {
                     //      days : 30
                     //  }, //起止时间的最大间隔
                       showDropdowns : true,
                       showWeekNumbers : false, //是否显示第几周
                       timePicker : true, //是否显示小时和分钟
                       timePickerIncrement : 60, //时间的增量，单位为分钟
                       timePicker12Hour : false, //是否使用12小时制来显示时间
                       ranges : {
                           //'最近1小时': [moment().subtract('hours',1), moment()],
                           '今日': [moment().startOf('day'), moment()],
                           '昨日': [moment().subtract('days', 1).startOf('day'), moment().subtract('days', 1).endOf('day')],
                           '最近7日': [moment().subtract('days', 6), moment()],
                           '最近30日': [moment().subtract('days', 29), moment()]
                       },
                       opens : 'right', //日期选择框的弹出位置
                       buttonClasses : [ 'btn btn-default' ],
                       applyClass : 'btn-small btn-primary blue',
                       cancelClass : 'btn-small',
                       format : 'YYYY-MM-DD HH:mm:ss', //控件中from和to 显示的日期格式
                       separator : ' to ',
                       locale : {
                           applyLabel : '确定',
                           cancelLabel : '取消',
                           fromLabel : '起始时间',
                           toLabel : '结束时间',
                           customRangeLabel : '自定义',
                           daysOfWeek : [ '日', '一', '二', '三', '四', '五', '六' ],
                           monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月',
                               '七月', '八月', '九月', '十月', '十一月', '十二月' ],
                           firstDay : 1
                       }
                   }, function(start, end, label) {//格式化日期显示框
     
                       $('#reportrange span').html(start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss'));
                   });
     
           //设置日期菜单被选项  --开始--
           var dateOption ;
           if("${riqi}"=='day') {
               dateOption = "今日";
           }else if("${riqi}"=='yday') {
               dateOption = "昨日";
           }else if("${riqi}"=='week'){
               dateOption ="最近7日";
           }else if("${riqi}"=='month'){
               dateOption ="最近30日";
           }else if("${riqi}"=='year'){
               dateOption ="最近一年";
           }else{
               dateOption = "自定义";
           }
           $(".daterangepicker").find("li").each(function (){
               if($(this).hasClass("active")){
                   $(this).removeClass("active");
               }
               if(dateOption==$(this).html()){
                   $(this).addClass("active");
               }
           });
           //设置日期菜单被选项  --结束--
     
     
           //选择时间后触发重新加载的方法
           $("#reportrange").on('apply.daterangepicker',function(){
        	   	oTable.fnDraw();
           });
     
           function getParam(url) {
               var data = decodeURI(url).split("?")[1];
               var param = {};
               var strs = data.split("&");
     
               for(var i = 0; i<strs.length; i++){
                   param[strs[i].split("=")[0]] = strs[i].split("=")[1];
               }
               return param;
           }
           }
        });
        
        table.on('click', '.delete', function (e) {
            e.preventDefault();
            if (!confirm("你确定要删除这一条记录吗?")) {
                return;
            }
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            $.ajax({
            	url : getPath()+"/videoC/deleteRow",
            	type:"post",
            	dataType:"json",
            	data:{
            		"id":aData.id
            	},
            	success : function(data){
            		if(data.success){
            			 oTable.fnDeleteRow(nRow);
            		}
            	},error:function(msg){
            		alert(msg);
            	}
            });
        });
        
        table.on('click', '.download', function (e) {
        	 var nRow = $(this).parents('tr')[0];
             var aData = oTable.fnGetData(nRow);
             window.open(aData.downloadUrl);
        });
    };
    
  
    

	return {
		init : function(){
			handleTable();
		},
	}
}();