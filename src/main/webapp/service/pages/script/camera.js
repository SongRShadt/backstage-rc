/**
 * 摄像头信息
 */
var Camera = function(){
	/**
	 * 获取项目根路径
	 * 
	 * @returns
	 */
	var getPath = function() {
		var curWwwPath = window.document.location.href;// 获取当前网址，如： http://localhost:8080/ems/Pages/Basic/Person.jsp
		var pathName = window.document.location.pathname; // 获取主机地址之后的目录，如： /ems/Pages/Basic/Person.jsp
		var pos = curWwwPath.indexOf(pathName);
		var localhostPath = curWwwPath.substring(0, pos); // 获取主机地址，如： http://localhost:8080
		var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);// 获取带"/"的项目名，如：/ems
		return (localhostPath + projectName);
	};
	
	/**
     * 初始化表格
     */
    var handleTable = function () {
        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }
            oTable.fnDraw();
        }

        /**
         * 编辑行
         */
        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            var cameraTypes = "<select id='cameraTypes' class='form-control input-small'>"; 
            if(aData.cameraType==0){
            	cameraTypes+="<option value='0' selected>会议室</option><option value='1'>公示栏</option></select>";
            }
            if(aData.cameraType==1){
            	cameraTypes+="<option value='0'>会议室</option><option value='1' selected>公示栏</option></select>";
            }
            if(aData.cameraType==2){
            	cameraTypes+="<option value='0'>会议室</option><option value='1'>公示栏</option><option value='2' selected>值班室</option></select>";
            }
            var villages = "<select id='villages' class='form-control input-small'>";
            $.ajax({
            	url : getPath()+"/villageC/getByAll",
            	type:"post",
            	dataType:"json", 
            	async: false,
            	success:function(data){
            		if(data.success){
            			$.each(data.obj,function(i){
            				if(this.villageName=== aData.village.villageName )
            					villages+="<option value='"+this.villageId+"' selected>"+this.villageName+"</option>"
	        				else
	        					villages+="<option value='"+this.villageId+"'>"+this.villageName+"</option>"
            			})
            		}
            	},error:function(msg){
            	}
            });
            villages+="</select>";
            var btn = "<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-html=\"true\" data-original-title=\"保存\" class=\"btn yellow tooltips edit\"><i class=\"fa fa-save\"></i></a>";
     		btn+="<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-original-title=\"取消\" class=\"btn green tooltips cancel\"><i class=\"fa fa-history\"></i></a>";
     		jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData.cameraName + '">';
            jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData.streamId + '">';
            jqTds[3].innerHTML = cameraTypes; 
            jqTds[4].innerHTML = villages;
            jqTds[5].innerHTML = btn;
        }
        
        /**
         * 保存行
         */
        function saveRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            var aData = oTable.fnGetData(nRow);
            $.ajax({
            	url : getPath()+"/cameraC/update",
            	type:"post",
            	dataType:"json", 
            	data:{
            		"cameraNo":aData.cameraNo,
            		"cameraName":jqInputs[0].value,
            		"streamId":jqInputs[1].value,
            		"cameraType":$("#cameraTypes").val(),
            		"villageId":$("#villages").val()
            	},
            	success:function(data){
            		if(data.success){
            			var btn = "<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-html=\"true\" data-original-title=\"保存\" class=\"btn yellow tooltips edit\"><i class=\"fa fa-edit\"></i></a>";
            			btn+="<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-original-title=\"删除\" class=\"btn red tooltips delete\"><i class=\"fa fa-trash-o\"></i></a>";
            			oTable.fnUpdate(jqInputs[0].value,nRow, 1, false);
            			oTable.fnUpdate(jqInputs[1].value,nRow, 2, false);
            			oTable.fnUpdate($("#cameraTypes").val(),nRow, 3, false); 
            			oTable.fnUpdate($("#villages").val(),nRow, 4, false);
            			oTable.fnUpdate(btn,nRow, 5, false);
            			oTable.fnDraw();
            		}
            	} 
            });
        };

        /**
         * 取消编辑行
         */
        function cancelEditRow(oTable, nRow) {
            oTable.fnDraw(true);
        };

        /**
         * 数据表格
         */
        var table = $('#cameraTable');
        var oTable = table.dataTable({
            "lengthMenu": [
                [10, 20, 30,40, 50,100],
                [10, 20, 30, 40,50,100] 
            ],
            "pageLength": 10,
            "language": {
            	"processing": "数据加载中...",
                "emptyTable": "暂无数据",
                "info": "第_START_到_END_条记录,共_TOTAL_条记录",
                "infoEmpty": "没有找到记录",
                "infoFiltered": "",
                "lengthMenu": "显示 _MENU_ 条记录",
                "search": "搜索:",
                "zeroRecords": "未找到匹配记录",
                "paginate": {
                    "previous":"上一页",
                    "next": "下一页",
                    "last": "末页",
                    "first": "首页"
                }
            },
            "columnDefs": [
                {"targets":[0],"data":"cameraNo",'Sortable': false,"visible": false,
                	"render":function(data,type,full){
                		return full.cameraNo==undefined?"":full.cameraNo;
                	}
                },{'orderable': false,"data":"cameraName",'targets': [1],"title":"摄像头名称",
                	"render":function(data,type,full){
                		return full.cameraName!=""?full.cameraName:"<span style='color:red'>无</span>";
                	}
                },{'orderable': false,"data":"cameraUrl", 'targets': [2],"title":"流编号",
                    "render":function(data, type, full){
                    	return full.streamId!=""?full.streamId:"<span style='color:red'>无</span>";
                    }
                }
                ,{'orderable': false,"data":"cameraUrl", 'targets': [3],"title":"播放路径",
                    "render":function(data, type, full){
                    	return full.cameraUrl!=""?full.cameraUrl:"<span style='color:red'>无</span>";
                    }
                },{'orderable': false,"data":"cameraType",'targets': [4],"title":"摄像头类型",
                    "render":function(data, type, full){
                    	var cameraType="";
                    	if(full.cameraType=="0"){
                    		cameraType="会议室";
                    	}
                    	if(full.cameraType=="1"){
                    		cameraType="公示栏";
                    	}
                    	if(full.cameraType=="2"){
                    		cameraType="值班室";
                    	}
                    	if(full.cameraNo=="90001"||full.cameraNo=="90002"||full.cameraNo=="90003"||full.cameraNo=="99998"||full.cameraNo=="99999"){
                    		cameraType="主会场";
                    	}
                    	return cameraType;
                    }
                },{'orderable': false,"data":"village.villageName",'targets': [5],"title":"所属村",
                    "render":function(data, type, full){
                    	return full.village!=undefined?full.village.villageName:"<span style='color:red;'>无</span>";
                    }
                },{'orderable': false,'targets': [6],"title":"操作",
              	 "render":function(data,type,full){
              		 var btn = "<a href=\"javascript:;\" data-container=\"body\" data-placement=\"top\" data-html=\"true\" data-original-title=\"编辑\" class=\"btn blue tooltips edit\"><i class=\"fa fa-edit\"></i></a>";
              		 btn+="<a href=\"javascript:;\" data-container=\"body\" data-placement=\"top\" data-original-title=\"删除\" class=\"btn red  tooltips delete\"><i class=\"fa fa-trash-o\"></i></a>";
              		 return btn;
              	 }
               },
            ],
            "bAutoWidth":false,
            "bProcessing": true, // 是否显示取数据时的那个等待提示
		    "bServerSide": true,//这个用来指明是通过服务端来取数据
            "sAjaxSource": getPath()+"/cameraC/getByPaging",//请求的地址
            "fnServerData": function(sSource,aoData,fnCallback){				  // 获取数据的处理函数
            	$.ajax({
            		url:sSource,
            		type : "post",
            		data:{
            			"aoData":JSON.stringify(aoData)
            		},
            		dataType : "json",
            		async:false,
            		success:function(result){
            			result=eval("("+result+")");
            			fnCallback(result);
            		} 
            	});
            } ,
            "order": [
                [0, "asc"]
            ] 
        });
        var tableWrapper = $("#townTable_wrapper");
        tableWrapper.find(".dataTables_length select").select2({
            showSearchInput: false 
        }); 
        var nEditing = null;
        var nNew = false;
        /**
         * 绑定删除按钮事件
         */
        table.on('click', '.delete', function (e) {
            e.preventDefault();
            if (!confirm("你确定要删除这一条记录吗?")) {
                return;
            }
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            $.ajax({
            	url : getPath()+"/cameraC/delete",
            	type:"post",
            	dataType:"json",
            	data:{
            		"id":aData.cameraNo
            	},
            	success : function(data){
            		if(data.success){
            			 oTable.fnDeleteRow(nRow);
            		}
            	},error:function(msg){
            	}
            });
        });

        /**
         * 绑定取消按钮事件
         */
        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        /**
         * 绑定编辑按钮事件
         */
        table.on('click', '.edit', function (e) {
        	e.preventDefault();
    	  	var nRow = $(this).parents('tr')[0];
    	  	var aData = oTable.fnGetData(nRow);
    	  	$("#editTown option[value='"+aData.village.townId+"']").attr("selected",true);
    	  	$.getJSON(getPath()+"/villageC/getByAll",{"townId":aData.village.townId},function(data){
        		if(data.success){
        			var villages ="<option value=''>---请选择村---</option>";
        			$.each(data.obj,function(i){
    					villages+="<option value='"+this.villageId+"'>"+this.villageName+"</option>";
        			});
        			$("#editVillageId").html(villages);
        			$("#editVillageId option[value='"+aData.village.villageId+"']").attr("selected",true);
        		}
        	});
    	  	$('#editCameraType[value="'+aData.cameraType+'"]').parent('span').addClass('checked');
    	 	$('#editCameraType[value="'+aData.cameraType+'"]').attr("checked",true);
    	  	$("#editCameraNo").attr("value",aData.cameraNo);
    	  	$("#editCameraName").attr("value",aData.cameraName);
    	  	$.getJSON(getPath()+"/cameraC/getById",{"id":aData.cameraNo},function(data){
    	  		if(data.success){
    	  			$("#edit_s_ipurl").attr("value",data.obj.s_ipurl);
    	  		}
    	  	});
        	$("#editCamera").modal('show');
        });
    };

    /**
	 * 添加表单验证
	 */
    var handleValidation = function() {
       $('#addCameraForm').validate({
            errorElement: 'span', 
            errorClass: 'help-block help-block-error', 
            focusInvalid: false,ignore: "",  
            debug:true,
            rules: {
            	cameraName: {
                    minlength: 2,
                    required: true
                },
                s_ipurl:{
                	required:true
                },
                streamId : {
                	required:true
                },
                villageId:{
                	required:true,
                },
                ipId : {
                	required:true
                }
            },messages:{
            	cameraName:{
            		minlength:"摄像头名称长度不能小于2！",
            		required:"摄像头名称不能为空！",
            	},  s_ipurl:{
                	required:"视频源不能为空"
                },
            	streamId : {
            		required:"流编号不能为空！"
                },
                villageId:{
                	required:"请选择所属村！"
                },
                ipId :{
                	required:"请选择服务器!"
                }
                
            },
            highlight: function (element) { 
                $(element).closest('.form-group').addClass('has-error'); 
            },
            unhighlight: function (element) { 
                $(element).closest('.form-group').removeClass('has-error'); 
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); 
            } 
        });
       
       $('#editCameraForm').validate({
           errorElement: 'span', 
           errorClass: 'help-block help-block-error', 
           focusInvalid: false,ignore: "",  
           debug:true,
           rules: {
        	   cameraName: {
                   minlength: 2,
                   required: true
               },
               s_ipurl:{
               	required:true
               },
               villageId:{
               	required:true,
               },
           },messages:{
        	   cameraName:{
           		minlength:"摄像头名称长度不能小于2！",
           		required:"摄像头名称不能为空！",
           		},  s_ipurl:{
               	required:"视频源不能为空"
               },
               villageId:{
               	required:"请选择所属村！"
               },
           },
           highlight: function (element) { 
               $(element).closest('.form-group').addClass('has-error'); 
           },
           unhighlight: function (element) { 
               $(element).closest('.form-group').removeClass('has-error'); 
           },
           success: function (label) {
               label.closest('.form-group').removeClass('has-error'); 
           } 
       });
    };
    var handleForm = function(){
    	$("#town").on("change",function(){
    		$.ajax({
     			url : getPath()+"/villageC/getByAll",
            	type:"post",
            	dataType:"json",
            	data:{
            		"townId":$("#town").val()
            	},
            	async: false,
            	success:function(data){
            		if(data.success){
            			var villages ="<option value=''>---请选择村---</option>";
            			$.each(data.obj,function(i){
        					villages+="<option value='"+this.villageId+"'>"+this.villageName+"</option>";
            			})
            			$("#villageId").html(villages)
            		}
            	},error:function(msg){
            	}
     		});
    	});
    	
    	$("#editTown").on("change",function(){
    		$.ajax({
     			url : getPath()+"/villageC/getByAll",
            	type:"post",
            	dataType:"json",
            	data:{
            		"townId":$("#editTown").val()
            	},
            	async: false,
            	success:function(data){
            		if(data.success){
            			var villages ="<option value=''>---请选择村---</option>";
            			$.each(data.obj,function(i){
        					villages+="<option value='"+this.villageId+"'>"+this.villageName+"</option>";
            			})
            			$("#editVillageId").html(villages)
            		}
            	},error:function(msg){
            	}
     		});
    	});
    	$.ajax({
    		url : getPath() +"/townC/getAll",
    		type:"post",
    		dataType:"json",
    		async:false,
    		success:function(data){
    			if(data.success){
    				$("#town").append("<option value=''>---请选择镇---</option>");
    				$("#editTown").append("<option value=''>---请选择镇---</option>");
    				$("#villageId").append("<option value=''>---请选择村---</option>");
    				$("#editVillageId").append("<option value=''>---请选择村---</option>");
    				$.each(data.obj,function(i){
    					$("#town").append("<option value='"+this.townId+"'>"+this.townName+"</option>");
    					$("#editTown").append("<option value='"+this.townId+"'>"+this.townName+"</option>");
        			})
    			}
    		} 
    	});
    	
    	$.ajax({
    		url : getPath()+"/ipConfigC/getAll",
    		type:"post",
    		dataType:"json",
    		async:false,
    		success:function(data){
    			if(data.success){
    				$("#ip").append("<option value=''>---请选择服务器---</option>");
    				var html;
    				var list = data.obj;
    				for(var i=0;i<list.length;i++){
    					html+="<option value='"+list[i].id+"'>"+list[i].name+"</option>";
    				}
    				$("#ip").append(html);
    			}
    		}
    	});
    	
    	
    	$("#formSubmit").on("click",function(e){
    		if($("#addCameraForm").validate().form()){
				$.ajax({
					url:getPath()+"/cameraC/add",
					type:"post",
					dataType:"json",
					data:$("#addCameraForm").serialize(),
					success:function(data){
						$('#cameraTable').dataTable().fnDraw(true);
					}
				});
				$("#formCancel").click();
    		}
    	});
    	
    	$("#editFormSubmit").on("click",function(e){
    		if($("#editCameraForm").validate().form()){
				$.ajax({
					url:getPath()+"/cameraC/update",
					type:"post",
					dataType:"json",
					data:$("#editCameraForm").serialize(),
					success:function(data){
						$('#cameraTable').dataTable().fnDraw(true);
					}
				});
				$("#editFormCancel").click();
    		}
    	});
    	
    	$("#addCameraClose").on("click",function(){
    		$("#formCancel").click();
    	});
    	
    	
    	$("#editCameraClose").on("click",function(){
    		$("#editFormCancel").click();
    	});
    	
    	$("#formCancel").on("click",function(e){
    		$("#cameraName").attr("value","");
    		$("#cameraNo").attr("value","");
    		$("#s_ipurl").attr("value","");
    		$("#town").find("option:selected").removeAttr("selected");
    		$("#villageId").find("option:selected").removeAttr("selected");
    		$('#cameraType[value="1"').parent('span').removeClass('checked');
    		$('#cameraType[value="2"').parent('span').removeClass('checked');
    		$('#cameraType[value="0"]').parent('span').addClass('checked');
    		$("#addCameraForm")[0].reset();
    		$(".form-group").closest('.form-group').removeClass('has-error'); 
    		$("#cameraName-error").html("");
    		$("#villageId-error").html("");
    		$("#s_ipurl-error").html("");
    		$("#ip-error").html("");
    	});
    	
    	
    	 $("#editFormCancel").on("click",function(e){ 
    		 $("#editCameraType[value='2']").parent('span').removeClass('checked');
    		 $("#editCameraType[value='1']").parent('span').removeClass('checked');
    		 $("#editCameraType[value='0']").parent('span').removeClass('checked');
    		 $(".form-group").closest('.form-group').removeClass('has-error'); 
    		 $("#editCameraForm")[0].reset();
    		 $("#editCameraName-error").html("");
    		 $("#editVillageId-error").html("");
    		 $("#edit_s_ipurl").attr("value","");
    	 });
    };
    

    
   
    
    
	return {
		init : function(){
			handleTable();
			handleForm();
			handleValidation();
		},
	}
}();