var logout = function(){
	
	var getPath = function() {
		var curWwwPath = window.document.location.href;// 获取当前网址
		var pathName = window.document.location.pathname; // 获取主机地址之后的目录
		var pos = curWwwPath.indexOf(pathName);
		var localhostPath = curWwwPath.substring(0, pos); // 获取主机地址
		var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
		return (localhostPath + projectName);
	};
	
	$.ajax({
		url:getPath()+"/user/logout",
		type:"post",
		dataType: 'JSON',
		success:function(data){
			console.info(data);
			if(data.success){
				window.location="login.jsp";
			}
			else{
				console.info(data.msg);
			}
		}
	});
};