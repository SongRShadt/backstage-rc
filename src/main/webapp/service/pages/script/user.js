
var User = function(){
    /**
     * 获取项目根路径
     * 
     * @returns

     */
    var getPath = function() {
        var curWwwPath = window.document.location.href;// 获取当前网址，如： http://localhost:8080/ems/Pages/Basic/Person.jsp

        var pathName = window.document.location.pathname; // 获取主机地址之后的目录，如： /ems/Pages/Basic/Person.jsp

        var pos = curWwwPath.indexOf(pathName);
        var localhostPath = curWwwPath.substring(0, pos); // 获取主机地址，如： http://localhost:8080

        var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);// 获取带"/"的项目名，如：/ems

        return (localhostPath + projectName);
    };
    /**

     * 初始化表格

     */
    var handleTable = function () {
        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }
            oTable.fnDraw();
        }
        /**

         * 编辑行

         */
        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            var btn = "<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-html=\"true\" data-original-title=\"保存\" class=\"btn yellow tooltips edit\"><i class=\"fa fa-save\"></i></a>";
                btn+= "<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-original-title=\"取消\" class=\"btn green tooltips cancel\"><i class=\"fa fa-history\"></i></a>";
            var townSelect = "<select id='town' class='form-control input-small'>";
            $.ajax({
                url : getPath()+"/townC/getAll",
                type:"post",
                dataType:"json", 
                async: false,
                success:function(data){
                    if(data.success){
                        $.each(data.obj,function(i){
                            if(this.townName=== aData.townName )
                                townSelect+="<option value='"+this.townId+"' selected>"+this.townName+"</option>"
                            else
                                townSelect+="<option value='"+this.townId+"'>"+this.townName+"</option>"
                        })
                    }
                },error:function(msg){
                }
            });
            townSelect+="</select>";
            jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData.name + '">';
            jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData.password + '">';
            /*jqTds[1].innerHTML='<input type="text" class="form-control input-small" value="' + aData.password + '">';*/
            jqTds[4].innerHTML = townSelect; 
          /*  jqTds[3].innerHTML = '<input type="text" class="form-control input-small" value="' + aData.stb.stbNo + '">';

            jqTds[4].innerHTML = '<input type="text" class="form-control input-small" value="' + aData.orgCode + '">';*/
            jqTds[5].innerHTML = btn;
        }
        
        /**

         * 保存行

         */
        function saveRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            var jqSelect = $('select', nRow);
            var aData = oTable.fnGetData(nRow);
            $.ajax({
                url : getPath()+"/userC/update",
                type:"post",
                dataType:"json", 
                data:{
                    "townId":jqSelect[0].value,
                    "id":aData.id,
                    "level":aData.level,
                    "name":jqInputs[0].value,
                    "password":jqInputs[1].value,
                    "portrait":aData.portrait,
                    "sex":aData.sex,
                },
                success:function(data){
                    if(data.success){
                        oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
                        oTable.fnUpdate(jqInputs[1].value, nRow, 3, false);
                        oTable.fnUpdate(jqSelect[0].value, nRow, 5, false);
                        var btn = "<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-html=\"true\" data-original-title=\"保存\" class=\"btn yellow tooltips edit\"><i class=\"fa fa-edit\"></i></a>";
                        btn+="<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-original-title=\"删除\" class=\"btn red tooltips delete\"><i class=\"fa fa-trash-o\"></i></a>";
                        oTable.fnUpdate(btn, nRow, 6, false);
                        oTable.fnDraw();
                    }
                },error:function(msg){
                }
            });
        };

        /**

         * 取消编辑行

         */
        function cancelEditRow(oTable, nRow) {
            oTable.fnDraw();
        };

        /**

         * 数据表格

         */
        var table = $('#userTable');
        var oTable = table.dataTable({
            "lengthMenu": [
                   [10,20,30,40,50,100],
                   [10,20,30,40,50,100]
            ],
            "pageLength": 10,
            "language": {
                "processing": "数据加载中...",
                "emptyTable": "暂无数据",
                "info": "第_START_到_END_条记录,共_TOTAL_条记录",
                "infoEmpty": "没有找到记录",
                "infoFiltered": "",
                "lengthMenu": "每页显示 _MENU_ 条记录",
                "search": "搜索:",
                "zeroRecords": "未找到匹配记录",
                "paginate": {
                    "previous":"上一页",
                    "next": "下一页",
                    "last": "末页",
                    "first": "首页"
                }
            },
            "columnDefs": [
                {"targets":[0],"data":"id",'Sortable': false,"visible": false,
                    "render":function(data,type,full){
                        if(full.id==undefined){
                            return "";
                        }else{
                            return full.id;
                        }
                    }
                },
                {'targets': [1],"data":"level",'Sortable': false,"visible": false,},
                {'orderable': false,"data":"name",'targets': [2],"title":"姓名", "render":function(a,b,full){
                        if(full.name!=null){
                            return full.name;
                        }else{
                            return "";
                        }
                    } 
                },
                {'orderable': false,"data":"userName",'targets': [3],"title":"登录名",   },
                {'orderable': false,"data":"password",'targets': [4],"title":"密码",   }, 
                {'targets': [5],"data":"portrait",'Sortable': false,"visible": false,},
                {'orderable': false,"data":"sex",'targets': [6],"title":"性别",   },
                {'orderable': false,"data":"townName",'targets': [7],"title":"所属镇",   },
                {'orderable': false,'targets': [8],"title":"操作",
                     "render":function(data,type,full){
                         var btn = "<a href=\"javascript:;\" data-container=\"body\" data-placement=\"top\" data-html=\"true\" data-original-title=\"编辑\" class=\"btn blue tooltips edit\"><i class=\"fa fa-edit\"></i></a>";
                         btn+="<a href=\"javascript:;\" data-container=\"body\" data-placement=\"top\" data-original-title=\"删除\" class=\"btn red  tooltips delete\"><i class=\"fa fa-trash-o\"></i></a>";
                         return btn;
                     }
               },
            ],
            "bAutoWidth":false,
            "bProcessing": true, // 是否显示取数据时的那个等待提示

            "bServerSide": true,//这个用来指明是通过服务端来取数据

            "sAjaxSource": getPath()+"/userC/getAll",//请求的地址

            "fnServerData": function(sSource,aoData,fnCallback){                  // 获取数据的处理函数

                $.ajax({
                    url:sSource,
                    type : "post",
                    data:{
                        "aoData":JSON.stringify(aoData)
                    },
                    dataType : "json",
                    async:false,
                    success:function(result){
                        result=eval("("+result+")");
                        fnCallback(result);
                    },error:function(msg){
                    }
                });
            } 
        });
      
        
        var tableWrapper = $("#townTable_wrapper");
        tableWrapper.find(".dataTables_length select").select2({
            showSearchInput: false 
        }); 
        
        var nEditing = null;
        var nNew = false;
        var sData = null;
        /**

         * 绑定删除按钮事件

         */
        table.on('click', '.delete', function (e) {
            e.preventDefault();
            if (!confirm("你确定要删除这一条记录吗?")) {
                return;
            }
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            $.ajax({
                url : getPath()+"/userC/delete",
                type:"post",
                dataType:"json",
                data:{
                    "id":aData.id,
                },
                success : function(data){
                    console.log(data);
                    if(data.success){
                         oTable.fnDeleteRow(nRow);
                    }
                },error:function(msg){
                }
            });
        });

        
        /**

         * 绑定取消按钮事件

         */
        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        /**

         * 绑定编辑按钮事件

         */
        table.on('click', '.edit', function (e) {
            e.preventDefault();
/*            console.log("测试 ");*/
            var nRow = $(this).parents('tr')[0];
            if (nEditing !== null && nEditing != nRow) {
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML=="<i class=\"fa fa-save\"></i>") {
                saveRow(oTable, nEditing);
                nEditing = null;
            } else {
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });
    };
    
    
    var jstree = function(){
        $("#tree_4").jstree({
            "core" : {
                "themes" : {
                    "responsive": false
                }, 
                // so that create works


                "check_callback" : true,
                'data' : {
                    'url' : function (node) {
                      return 'demo/jstree_ajax_data.php';
                    },
                    'data' : function (node) {
                      return { 'parent' : node.id };
                    }
                }
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder icon-state-warning icon-lg"
                },
                "file" : {
                    "icon" : "fa fa-file icon-state-warning icon-lg"
                }
            },
            "state" : { "key" : "demo3" },
            "plugins" : [ "dnd", "state", "types" ]
        });
    };
    /**

     * 添加表单验证

     */
    var handleValidation = function() {
       $('#addUserForm').validate({
            errorElement: 'span', 
            errorClass: 'help-block help-block-error', 
            focusInvalid: false,ignore: "",  
            debug:true,
            rules: {
                userName: {
                    minlength: 2,
                    required: true,
                    remote:{//自定义验证//后台验证用户名是否存在

                        type:"post",
                        url:getPath()+"/userC/checkValue",
                        data : {
                            "value": function() {return $("#userName").val();},
                        }
                    }
                },
                password : {
                    minlength: 2,
                    maxlength: 18,
                    required:true
                },
                name:{
                    required:true                
                }             
            },messages:{
                userName:{
                    minlength:"登录用户名长度不能小于2！",
                    required:"登录用户名称不能为空！",
                    remote:"登录用户名已存在！"
                },
                password : {
                    minlength:"密码长度不能小于2！",
                    maxlength:"密码长度不能大于18！",
                    required:"密码不能为空！"
                },
                name:{
                    required:"姓名不能为空！"
                }                
            },
            highlight: function (element) { 
                $(element).closest('.form-group').addClass('has-error'); 
            },
            unhighlight: function (element) { 
                $(element).closest('.form-group').removeClass('has-error'); 
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); 
            } 
        });
    };

    var handleForm = function(){
       var townSelect = "<select id='townId' name='townId' class='form-control'><option value=''>---请选择---</option>";
       $.ajax({
           url : getPath()+"/townC/getAll",
           type:"post",
           dataType:"json", 
           async: false,
           success:function(data){
               if(data.success){
                   $.each(data.obj,function(i){
                       townSelect+="<option value='"+this.townId+"'>"+this.townName+"</option>"
                   })
               }
           },error:function(msg){
           }
       });
       townSelect+="</select>";
       $("#town").html(townSelect);
        $("#formSubmit").on("click",function(e){
            if($("#addUserForm").validate().form()){
                $.ajax({
                    url:getPath()+"/userC/add",
                    type:"post",
                    dataType:"json",
                    data:$("#addUserForm").serialize(),
                    success:function(data){
                        $('#userTable').dataTable().fnDraw();
                    },error : function(msg){
                        console.info();
                    }
                });
                $("#formCancel").click();
            }
        });
        $("#addUserForm").on("click",function(e){
            $("#addTownForm")[0].reset();
            $(".form-group").closest('.form-group').removeClass('has-error'); 
            $("#userName-error").html("");
            $("#town-error").html("");
            $("#name-error").html("");
        });
    };
    
    
    return {
        init : function(){
            handleTable();
            handleForm();
            handleValidation();
        },
    };
    
}();

