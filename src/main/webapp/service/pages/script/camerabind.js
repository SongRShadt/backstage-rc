/**
 * 村信息
 */
var Village = function(){
	/**
     * 初始化表格
     */
    var handleTable = function () {
        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }
            oTable.fnDraw();
        }

        /**
         * 编辑行
         */
        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            var btn = "<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-html=\"true\" data-original-title=\"保存\" class=\"btn yellow tooltips edit\"><i class=\"fa fa-save\"></i></a>";
     		btn+="<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-original-title=\"取消\" class=\"btn green tooltips cancel\"><i class=\"fa fa-history\"></i></a>";
            jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData.townName + '">';
            jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData.townKey + '">';
            jqTds[2].innerHTML = btn;
        }
        
        /**
         * 保存行
         */
        function saveRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            var aData = oTable.fnGetData(nRow);
            $.ajax({
            	url : "/PartyMemberEducation/townC/update",
            	type:"post",
            	dataType:"json", 
            	data:{
            		"townId":aData.townId,
            		"townName":jqInputs[0].value,
            		"townKeyValue":jqInputs[1].value
            	},
            	success:function(data){
            		if(data.success){
            			 oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            	            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            	            var btn = "<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-html=\"true\" data-original-title=\"保存\" class=\"btn yellow tooltips edit\"><i class=\"fa fa-edit\"></i></a>";
            	    		btn+="<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-original-title=\"删除\" class=\"btn red tooltips delete\"><i class=\"fa fa-trash-o\"></i></a>";
            	            oTable.fnUpdate(btn, nRow, 3, false);
            	            oTable.fnDraw();
            		}
            	} 
            });
        };

        /**
         * 取消编辑行
         */
        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            oTable.fnDraw();
        };

        /**
         * 数据表格
         */
        var table = $('#villageTable');
        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] 
            ],
            "pageLength": 5,
            "language": {
            	"processing": "数据加载中...",
                "emptyTable": "暂无数据",
                "info": "第_START_到_END_条记录,共_TOTAL_条记录",
                "infoEmpty": "没有找到记录",
                "infoFiltered": "",
                "lengthMenu": "显示 _MENU_ 条记录",
                "search": "搜索:",
                "zeroRecords": "未找到匹配记录",
                "paginate": {
                    "previous":"上一页",
                    "next": "下一页",
                    "last": "末页",
                    "first": "首页"
                }
            },
            "columnDefs": [
                {"targets":[0],"data":"villageId",'Sortable': false,"visible": false,
                	"render":function(data,type,full){
                		if(full.villageId==undefined){
                			return "";
                		}else{
                			return full.villageId;
                		}
                	}
                },
                {'orderable': false,"data":"villageName",'targets': [1],"title":"村名称",},
                {'orderable': false,"data":"villageKeyValue",'targets': [2],"title":"村key",
                    "render":function(data, type, full){
                    	if(full.villageKeyValue===""){
                    		return"<span style='color:red;'>无</span>";
                        }else{
                        	return full.villageKeyValue;
                        }
                    }
                },
                {'orderable': false,"data":"town.townName",'targets': [3],"title":"所属镇",
                    "render":function(data, type, full){
                    	if(full.town==undefined){
                    		return"<span style='color:red;'>无</span>";
                        }else{
                        	return full.town.townName;
                        }
                    }
                }, {'orderable': false,"data":"stb.stbNo",'targets': [4],"title":"对应机顶盒",
                    "render":function(data, type, full){
                    	if(full.stb!=undefined){
                        	return full.stb.stbNo;
                        }else{
                        	return"<span style='color:red;'>无</span>";
                        }
                    }
                },
                {'orderable': false,"data":"orgCode",'targets': [5],"title":"村组织编号",
                    "render":function(data, type, full){
                    	if(full.orgCode===""){
                    		return"<span style='color:red;'>无</span>";
                        }else if(full.orgCode=="-1"){
                        	return "<span style='color:green;'>镇级(标识-1)</span>";
                        }else if(full.orgCode=="0"){
                        	return "<span style='color:#FF7F24;'>县级(标识0)</span>";
                        }else{
                        	return  "<span style='color:#E066FF;'>村级(标识"+full.orgCode+")</span>";
                        }
                    }
                },
               {'orderable': false,'targets': [6],"title":"操作",
              	 "render":function(data,type,full){
              		 var btn = "<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-html=\"true\" data-original-title=\"编辑\" class=\"btn blue tooltips edit\"><i class=\"fa fa-edit\"></i></a>";
              		 btn+="<a href=\"javascript:alert(1);\ data-container=\"body\" data-placement=\"top\" data-original-title=\"删除\" class=\"btn red  tooltips delete\"><i class=\"fa fa-trash-o\"></i></a>";
              		 return btn;
              	 }
               },
            ],
            "bProcessing": true, // 是否显示取数据时的那个等待提示
		    "bServerSide": true,//这个用来指明是通过服务端来取数据
            "sAjaxSource": "/PartyMemberEducation/villageC/getAll",//请求的地址
            "fnServerData": function(sSource,aoData,fnCallback){				  // 获取数据的处理函数
            	$.ajax({
            		url:sSource,
            		type : "post",
            		data:{
            			"aoData":JSON.stringify(aoData)
            		},
            		dataType : "json",
            		async:false,
            		success:function(result){
            			result=eval("("+result+")");
            			fnCallback(result);
            		},error:function(msg){
            			console.info(msg);
            		}
            	});
            } ,
            "order": [
                [0, "asc"]
            ] 
        });
        var tableWrapper = $("#townTable_wrapper");
        tableWrapper.find(".dataTables_length select").select2({
            showSearchInput: false 
        }); 
        var nEditing = null;
        var nNew = false;
        /**
         * 绑定删除按钮事件
         */
        table.on('click', '.delete', function (e) {
            e.preventDefault();
            if (confirm("你确定要删除这一条记录吗?") == false) {
                return;
            }
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            $.ajax({
            	url : "/PartyMemberEducation/townC/delete",
            	type:"post",
            	dataType:"json",
            	data:{
            		"id":aData.townId
            	},
            	success : function(data){
            		if(data.success){
            			 oTable.fnDeleteRow(nRow);
            		}
            	},error:function(msg){
            		
            	}
            });
        });

        /**
         * 绑定取消按钮事件
         */
        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        /**
         * 绑定编辑按钮事件
         */
        table.on('click', '.edit', function (e) {
            e.preventDefault();
            var nRow = $(this).parents('tr')[0];
            if (nEditing !== null && nEditing != nRow) {
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML=="<i class=\"fa fa-save\"></i>") {
                saveRow(oTable, nEditing);
                nEditing = null;
            } else {
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });
    };

	return {
		init : function(){
			handleTable();
		},
	}
}();