/**
 * 村信息
 */
var Village = function(){
	/**
	 * 获取项目根路径
	 * 
	 * @returns
	 */
	var getPath = function() {
		var curWwwPath = window.document.location.href;// 获取当前网址，如： http://localhost:8080/ems/Pages/Basic/Person.jsp
		var pathName = window.document.location.pathname; // 获取主机地址之后的目录，如： /ems/Pages/Basic/Person.jsp
		var pos = curWwwPath.indexOf(pathName);
		var localhostPath = curWwwPath.substring(0, pos); // 获取主机地址，如： http://localhost:8080
		var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);// 获取带"/"的项目名，如：/ems
		return (localhostPath + projectName);
	};
	/**
     * 初始化表格
     */
    var handleTable = function () {
        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }
            oTable.fnDraw(true);
        }
        /**
         * 编辑行
         */
        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            var btn = "<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-html=\"true\" data-original-title=\"保存\" class=\"btn yellow tooltips edit\"><i class=\"fa fa-save\"></i></a>";
     		    btn+= "<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-original-title=\"取消\" class=\"btn green tooltips cancel\"><i class=\"fa fa-history\"></i></a>";
     		var townSelect = "<select id='town' class='form-control input-small'>";
     		$.ajax({
     			url : getPath()+"/townC/getAll",
            	type:"post",
            	dataType:"json", 
            	async: false,
            	success:function(data){
            		if(data.success){
            			$.each(data.obj,function(i){
            				if(aData.town!=undefined&&this.townName=== aData.town.townName)
            					townSelect+="<option value='"+this.townId+"' selected>"+this.townName+"</option>"
	        				else
            					townSelect+="<option value='"+this.townId+"'>"+this.townName+"</option>"
            			})
            		}
            	},error:function(msg){
            	}
     		});
     		townSelect+="</select>";
     		jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData.villageName + '">';
            jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData.villageKeyValue + '">';
            jqTds[2].innerHTML = townSelect; 
            jqTds[3].innerHTML = '<input type="text" class="form-control input-small" value="' + aData.stb.stbNo + '">';
            jqTds[4].innerHTML = '<input type="text" class="form-control input-small" value="' + aData.orgCode + '">';
            jqTds[6].innerHTML = '<input type="text" class="form-control input-small" value="' + (aData.villageSort==undefined?"":aData.villageSort) + '">';
            jqTds[7].innerHTML = btn;
        }
        
        /**
         * 保存行
         */
        function saveRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            var jqSelect = $('select', nRow);
            var aData = oTable.fnGetData(nRow);
            $.ajax({
            	url : getPath()+"/villageC/updata",
            	type:"post",
            	dataType:"json", 
            	data:{
            		"townId":jqSelect[0].value,
            		"villageId":aData.villageId,
            		"villageName":jqInputs[0].value,
            		"villageKeyValue":jqInputs[1].value,
            		"stbNo":jqInputs[2].value,
            		"orgCode":jqInputs[3].value,
            		"villageSort":jqInputs[4].value,
            	},
            	success:function(data){
            		if(data.success){
            			oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
        	            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
        	            oTable.fnUpdate(jqSelect[0].value, nRow, 3, false);
        	            oTable.fnUpdate(jqInputs[2].value, nRow, 4, false);
        	            oTable.fnUpdate(jqInputs[3].value, nRow, 5, false);
        	            var btn = "<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-html=\"true\" data-original-title=\"保存\" class=\"btn yellow tooltips edit\"><i class=\"fa fa-edit\"></i></a>";
        	    		btn+="<a href=\"javascript:;\ data-container=\"body\" data-placement=\"top\" data-original-title=\"删除\" class=\"btn red tooltips delete\"><i class=\"fa fa-trash-o\"></i></a>";
        	            oTable.fnUpdate(btn, nRow, 6, false);
        	            oTable.fnDraw(true);
            		}
            	},error:function(msg){
            	}
            });
        };

        /**
         * 取消编辑行
         */
        function cancelEditRow(oTable, nRow) {
        	var aData = oTable.fnGetData(nRow);
        	var jqTds = $('>td', nRow);
            oTable.fnDraw(true);
        };

        /**
         * 数据表格
         */
        var table = $('#villageTable');
        var oTable = table.dataTable({
            "lengthMenu": [
                   [10,20,30,40,50,100],
                   [10,20,30,40,50,100]
            ],
            "pageLength": 10,
            "language": {
            	"processing": "数据加载中...",
                "emptyTable": "暂无数据",
                "info": "第_START_到_END_条记录,共_TOTAL_条记录",
                "infoEmpty": "没有找到记录",
                "infoFiltered": "",
                "lengthMenu": "每页显示 _MENU_ 条记录",
                "search": "搜索:",
                "zeroRecords": "未找到匹配记录",
                "paginate": {
                    "previous":"上一页",
                    "next": "下一页",
                    "last": "末页",
                    "first": "首页"
                }
            },
            "columnDefs": [
                {"targets":[0],"data":"villageId",'Sortable': false,"visible": false,
                	"render":function(data,type,full){
                		if(full.villageId==undefined){
                			return "";
                		}else{
                			return full.villageId;
                		}
                	}
                },
                {'orderable': false,"data":"villageName",'targets': [1],"title":"村名称",},
                {'orderable': false,"data":"villageKeyValue",'targets': [2],"title":"村key",
                    "render":function(data, type, full){
                    	if(full.villageKeyValue===""){
                    		return"<span style='color:red;'>无</span>";
                        }else{
                        	return full.villageKeyValue;
                        }
                    }
                },
                {'orderable': false,"data":"town.townName",'targets': [3],"title":"所属镇",
                    "render":function(data, type, full){
                    	if(full.town==undefined){
                    		return"<span style='color:red;'>无</span>";
                        }else{
                        	return full.town.townName;
                        }
                    }
                }, {'orderable': false,"data":"stb.stbNo",'targets': [4],"title":"对应机顶盒",
                    "render":function(data, type, full){
                    	if(full.stb!=undefined){
                        	return full.stb.stbNo;
                        }else{
                        	return"<span style='color:red;'>无</span>";
                        }
                    }
                },
                {'orderable': false,"data":"orgCode",'targets': [5],"title":"村组织编号",
                    "render":function(data, type, full){
                    	if(full.orgCode===""){
                    		return"<span style='color:red;'>无</span>";
                        }else if(full.orgCode=="-1"){
                        	return "<span style='color:green;'>镇级(标识"+full.orgCode+")</span>"; 
                        }else if(full.orgCode=="0"){
                        	return "<span style='color:#FF7F24;'>县级(标识"+full.orgCode+")</span>"; 
                        }else{
                        	return  "<span style='color:#E066FF;'>村级(标识"+full.orgCode+")</span>";
                        }
                    }
                },
                {'orderable': false,"data":"bindInfo",'targets': [6],"title":"绑定情况",
                	"render":function(data,type,full){
                		if(full.bindInfo=="0"){
                			return "<span style='color:red;'>未绑定摄像头</span>";
                		}else{
                			return "<span style='color:blue;'>已绑定摄像头</span>";
                		}
                	}
                }, {'orderable': false,"data":"villageSort",'targets': [7],"title":"排序",
                	"render":function(data,type,full){
                		if(full.villageSort==null){
                			return "<span style='color:red;'>无</span>";
                		}else{
                			return full.villageSort;
                		}
                	}
                },
               {'orderable': false,'targets': [8],"title":"操作",
              	 "render":function(data,type,full){
              		 var btn = "<a href=\"javascript:;\" data-container=\"body\" data-placement=\"top\" data-html=\"true\" data-original-title=\"编辑\" class=\"btn blue tooltips edit\"><i class=\"fa fa-edit\"></i></a>";
              		 btn+="<a href=\"javascript:;\" data-container=\"body\" data-placement=\"top\" data-original-title=\"删除\" class=\"btn red  tooltips delete\"><i class=\"fa fa-trash-o\"></i></a>";
              		 btn+="<a href=\"#bindCamera\"data-toggle=\"modal\"id=\"cameraBind\" data-container=\"body\" data-placement=\"top\" data-original-title=\"绑定摄像头\" class=\"btn yellow  tooltips\"><i class=\"fa fa-chain\"></i></a>";
              		 btn+="<a href=\"#lookCamera\"data-toggle=\"modal\"id=\"cameraLook\" data-container=\"body\" data-placement=\"top\" data-original-title=\"查看绑定摄像头信息\" class=\"btn green tooltips\"><i class=\"fa fa-search\"></i></a>";
              		 btn+="<a href=\"#lookMettingVideo\"data-toggle=\"modal\"id=\"mettingVideoLook\" data-container=\"body\" data-placement=\"top\" data-original-title=\"查看绑定视频会议信息\" class=\"btn blue tooltips\"><i class=\"fa fa-search\"></i></a>";
              		 return btn;
              	 }
               },
            ],
            "bAutoWidth":false,
            "bProcessing": true, // 是否显示取数据时的那个等待提示
		    "bServerSide": true,//这个用来指明是通过服务端来取数据
            "sAjaxSource": getPath()+"/villageC/getAll",//请求的地址
            "fnServerData": function(sSource,aoData,fnCallback){				  // 获取数据的处理函数
            	$.ajax({
            		url:sSource,
            		type : "post",
            		data:{
            			"aoData":JSON.stringify(aoData)
            		},
            		dataType : "json",
            		async:false,
            		success:function(result){
            			result=eval("("+result+")");
            			fnCallback(result);
            		},error:function(msg){
            		}
            	});
            } 
        });
        var tableWrapper = $("#townTable_wrapper");
        tableWrapper.find(".dataTables_length select").select2({
            showSearchInput: false 
        }); 
        var nEditing = null;
        var nNew = false;
        var sData = null;
        
        /**
         * 视频会议表格
         */
        var villageId = null;
        var t = $("#mettingVideo");//视频会议表格
        var vTable = t.dataTable({
    		"lengthMenu": [
                [20,30,40,50,100],
                [20,30,40,50,100] 
            ],
            "bSort": false ,
            "order": [[0, "asc"]],
            "pageLength": 20,
            "searching" : false,
            "language": {
            	"processing": "数据加载中...",
                "emptyTable": "暂无数据",
                "info": "第_START_到_END_条记录,共_TOTAL_条记录",
                "infoEmpty": "没有找到记录",
                "infoFiltered": "",
                "lengthMenu": "显示 _MENU_ 条记录",
                "search": "搜索:",
                "zeroRecords": "未找到匹配记录",
                "paginate": {
                    "previous":"上一页",
                    "next": "下一页",
                    "last": "末页",
                    "first": "首页"
                }
            },"columnDefs": [
                {"targets":[0],"data":"id","visible": false,
                	"render":function(data,type,full){
                		return full.id==undefined?"":full.id;
                	}
                },{"targets":[1],"data":"name","title":"视频会议名称",
                	"render":function(data,type,full){
                		return full.name==undefined?"":full.name;
                	}
             	},{"targets":[2],"data":"sort","title":"排序",
                	"render":function(data,type,full){
                		return full.sort==undefined?"":full.sort;
                	}
             	},{"targets":[3],"title":"操作",
                	"render":function(data,type,full){
                		var btn="<a href=\"javascript:;\" id='deleteMettingVideo' data-container=\"body\" data-placement=\"top\" data-original-title=\"删除\" class=\"btn red  tooltips delete\"><i class=\"fa fa-trash-o\"></i></a>";
                		return btn;
                	}
             	}, {"targets":[4],"data":"villageId",'Sortable': false,"visible": false,
                	"render":function(data,type,full){
                		return full.villageId==undefined?"":full.villageId;
                	}
                }
            ],
            "bFilter":true,
            "bAutoWidth":true,
            "bProcessing": true, // 是否显示取数据时的那个等待提示
		    "bServerSide": true,//这个用来指明是通过服务端来取数据
            "fnServerData": function(sSource,aoData,fnCallback){ // 获取数据的处理函数
            	$.ajax({
            		url: "../mettingvideo/get/bind/"+villageId,
            		type : "post",
            		data:{
            			"aoData":JSON.stringify(aoData)
            		},
            		dataType : "json",
            		async:false,
            		success:function(result){
            			result=eval("("+result+")");
            			fnCallback(result);
            		} 
            	});
            } 
    	});
        /**
         * 删除视频会议绑定
         */
        t.on("click","#deleteMettingVideo",function(e){
        	e.preventDefault();	
        	if (!confirm("你确定要删除这一条记录吗?")) {
                return;
            }
        	var nRow = $(this).parents('tr');
        	var dData = vTable.fnGetData(nRow);
        	$.ajax({
        		url :"../mettingvideo/delete/"+dData.id+"/"+villageId,
        		type : "post",
        		dataType : "json",
        		success:function(result){
        			if(result.success){
        				handleVideoSelect();
        				vTable.fnDraw(true);
        			}else{
        				alert("删除失败！");
        			}
        		},error:function(a,b,c){
        			alert("系统异常！");
        		}
        	});
        });
        
        /**
         * 绑定查看视频会议按钮事件
         */
        table.on("click","#mettingVideoLook",function(e){
        	e.preventDefault();	
        	var nRow = $(this).parents('tr');
        	var dData = oTable.fnGetData(nRow);
        	villageId= dData.villageId;
	    	vTable.fnDraw(true);
	    	handleVideoSelect();
        });
        
        $("#addMettingVideo").on("click",function(){
        	$.ajax({
        		url :"../mettingvideo/add/"+villageId+"/"+$("#bindMettingVideo option:selected").val(),
        		type : "post",
        		dataType : "json",
        		success:function(result){
        			if(result.success){
        				vTable.fnDraw(true);
        				handleVideoSelect();
        			}else{
        				alert("添加失败！");
        			}
        		},error:function(a,b,c){
        			alert("系统异常！");
        		}
        	});
        	handleVideoSelect();
        });
        
        var handleVideoSelect = function(){
        	var videoSelect = "";
	    	$.ajax({
	    		url:"../mettingvideo/all/"+villageId,
	    		async:false,
	    		type : "post",
        		dataType : "json",
        		success:function(result){
        			if(result.success){
        				if(result.obj!=undefined){
        					$('#addMettingVideo').removeAttr("disabled");
        					$.each(result.obj,function(i){
        						videoSelect+="<option value='"+this.id+"'>"+this.name+"</option>"
        					});
        				}else{
        					videoSelect+="<option >已绑定所有视频会议</option>"
    						$('#addMettingVideo').attr('disabled',"true");
        				}
        			} 
        		},error:function(a,b,c){
        			alert("系统异常！");
        		}
	    	});
	    	$("#bindMettingVideo").html(videoSelect);
        };
        
        
        table.on("click","#cameraBind",function(e){
        	e.preventDefault();	
        	var nRow = $(this).parents('tr');
        	sData = oTable.fnGetData(nRow);
        });
        $("#bindAll").on("click",function(e){
    		$.ajax({
    			url : getPath()+"/stbCameraC/bindCamera",
    			dataType:"json",
    			type:"post",
    			data:{
    				"type":"0",
    				"villageId":sData.villageId
    			},
    			success:function(data){
    				if(data.success){
    					alert("绑定成功！");
    				}else{
    					alert("绑定失败！");
    				}
    				$("#bindCameraCancel").click();
    			}
    		});
    	});
    	$("#bindTown").on("click",function(e){
    		$.ajax({
    			url : getPath()+"/stbCameraC/bindCamera",
    			dataType:"json",
    			type:"post",
    			data:{
    				"type":"1",
    				"villageId":sData.villageId
    			},
    			success:function(data){
    				if(data.success){
    					alert("绑定成功！");
    				}else{
    					alert("绑定失败！");
    				}
    				$("#bindCameraCancel").click();
    			}
    		});
    	});
    	$("#bindVillage").on("click",function(e){
    		$.ajax({
    			url : getPath()+"/stbCameraC/bindCamera",
    			dataType:"json",
    			type:"post",
    			data:{
    				"type":"2",
    				"villageId":sData.villageId
    			},
    			success:function(data){
    				if(data.success){
    					alert("绑定成功！");
    				}else{
    					alert("绑定失败！");
    				}
    				$("#bindCameraCancel").click();
    			}
    		});
    	});
    	
    	
    	table.on("click","#cameraLook",function(e){
        	e.preventDefault();	
        	var nRow = $(this).parents('tr');
        	var ssData = oTable.fnGetData(nRow);
        	$.ajax({
        		url:getPath()+"/stbCameraC/handleJsTree",
        		type:"post",
        		dataType:"json",
        		data:{
        			"stbNo":ssData.stb.stbNo
        		},
        		success:function(data){ 
        			data=eval("("+data+")");
        			$('#jstree').jstree({
	                    'plugins': ["wholerow","checkbox","types"],
	                    'core': {
	                        "themes" : {
	                            "responsive": false
	                        },    
	                        'data':data
	                    }
	                });
        		}
        	});
        });
    	
        /**
         * 绑定删除按钮事件
         */
        table.on('click', '.delete', function (e) {
            e.preventDefault();
            if (!confirm("你确定要删除这一条记录吗?")) {
                return;
            }
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            $.ajax({
            	url : getPath()+"/villageC/delete",
            	type:"post",
            	dataType:"json",
            	data:{
            		"id":aData.villageId,
            		"stbNo":aData.stb==undefined?"":aData.stb.stbNo
            	},
            	success : function(data){
            		if(data.success){
//            			 oTable.fnDeleteRow(nRow);
            			 oTable.fnDraw(true);
            		}
            	},error:function(msg){
            	}
            });
        });

        /**
         * 绑定取消按钮事件
         */
        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        /**
         * 绑定编辑按钮事件
         */
        table.on('click', '.edit', function (e) {
            e.preventDefault();
            
            var nRow = $(this).parents('tr')[0];
            if (nEditing !== null && nEditing != nRow) {
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML=="<i class=\"fa fa-save\"></i>") {
                saveRow(oTable, nEditing);
                nEditing = null;
            } else {
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });
    };
    
    
    var jstree = function(){
    	$("#tree_4").jstree({
            "core" : {
                "themes" : {
                    "responsive": false
                }, 
                // so that create works
                "check_callback" : true,
                'data' : {
                    'url' : function (node) {
                      return 'demo/jstree_ajax_data.php';
                    },
                    'data' : function (node) {
                      return { 'parent' : node.id };
                    }
                }
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder icon-state-warning icon-lg"
                },
                "file" : {
                    "icon" : "fa fa-file icon-state-warning icon-lg"
                }
            },
            "state" : { "key" : "demo3" },
            "plugins" : [ "dnd", "state", "types" ]
        });
    };
    /**
	 * 添加表单验证
	 */
    var handleValidation = function() {
       $('#addVillageForm').validate({
            errorElement: 'span', 
            errorClass: 'help-block help-block-error', 
            focusInvalid: false,ignore: "",  
            debug:true,
            rules: {
            	villageName: {
                    minlength: 2,
                    required: true,
                    remote:{//自定义验证//后台验证镇名称是否存在
                    	type:"post",
                    	url:getPath()+"/villageC/checkValue",
                    	data : {
                    		"value": function() {return $("#villageName").val();},
                    	}
                    }
                },
                townId : {
                	required:true
                },
                stbNo:{
                	required:true,
                	remote:{
                		type:"post",
                		url:getPath()+"/stbC/checkValue",
                		data:{
                			"value":function(){return $("#stbNo").val()}
                		}
                	}
                },
                orgCode:{
                	required:true,
                	number:true
                }
            },messages:{
            	villageName:{
            		minlength:"村名称长度不能小于2！",
            		required:"村名称不能为空！",
            		remote:"此村已存在！"
            	},
            	townId : {
            		required:"请选择所属镇"
                },
                stbNo:{
                	required:"机顶盒编号不能为空",
                	remote:"机顶盒编号已存在"
                },
                orgCode:{
                	required:"村组织编号不能为空",
                	number:"村组织编号为数字"
                }
            },
            highlight: function (element) { 
                $(element).closest('.form-group').addClass('has-error'); 
            },
            unhighlight: function (element) { 
                $(element).closest('.form-group').removeClass('has-error'); 
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); 
            } 
        });
    };

    var handleForm = function(){
    	var townSelect = "<select id='townId' name='townId' class='form-control'><option value=''>---请选择---</option>";
 		$.ajax({
 			url : getPath()+"/townC/getAll",
        	type:"post",
        	dataType:"json", 
        	async: false,
        	success:function(data){
        		if(data.success){
        			$.each(data.obj,function(i){
    					townSelect+="<option value='"+this.townId+"'>"+this.townName+"</option>";
        			})
        		}
        	},error:function(msg){
        	}
 		});
 		townSelect+="</select>";
 		$("#town").html(townSelect);
    	$("#formSubmit").on("click",function(e){
    		if($("#addVillageForm").validate().form()){
				$.ajax({
					url:getPath()+"/villageC/add",
					type:"post",
					dataType:"json",
					data:$("#addVillageForm").serialize(),
					success:function(data){
						$('#villageTable').dataTable().fnDraw(true);
					},error : function(msg){
						console.info();
					}
				});
				$("#formCancel").click();
    		}
    	});
    	$("#addVillageForm").on("click",function(e){
    		$("#addTownForm")[0].reset();
    		$(".form-group").closest('.form-group').removeClass('has-error'); 
    		$("#villageName-error").html("");
    		$("#town-error").html("");
    		$("#stbNo-error").html("");
    		$("#orgCode-error").html("");
    	});
    };
    
    
	return {
		init : function(){
			handleTable();
			handleForm();
			handleValidation();
		},
	}
}();