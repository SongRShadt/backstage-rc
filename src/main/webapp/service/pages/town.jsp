<%@page import="com.shadt.core.entity.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	request.setAttribute("path", basePath);
		User u = (User)request.getSession().getAttribute("user");
	if(null==u){
		response.sendRedirect("login.jsp");
	}
%>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-edit"></i>镇信息管理
				</div>
				<div class="tools">
					<a href="javascript:;" class="reload"> </a> 
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-4">
							<div class="btn-group">
								<a  href="#addTown"class="btn green" data-toggle="modal">
									添加 <i class="fa fa-plus"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
				<table class="table table-striped table-hover table-bordered" id="townTable">
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
	<!-- END PAGE CONTENT -->

<div id="addTown" class="modal fade modal-scroll"  tabindex="-1" data-backdrop="static" data-replace="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">添加镇信息</h4>
			</div>
			<div class="modal-body">
				<!-- BEGIN FORM-->
				<form id="addTownForm" class="form-horizontal">
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3"> 
								镇名称<span class="required">*</span>
							</label>
							<div class="col-md-7">
								<input type="text" id="townName" name="townName" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"> 镇Key</label>
							<div class="col-md-7">
								<input type="text" name="townKeyValue" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">排序</label>
							<div class="col-md-7">
								<input type="text" id="townSort" name="townSort" class="form-control" />
							</div>
						</div>
					</div>
				</form>
				<!-- END FORM-->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn red" id="formSubmit">提交</button>
				<button type="button" data-dismiss="modal" class="btn green" id="formCancel">取消</button>
			</div>
		</div>
	</div>
</div>


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL STYLES -->
<script src="pages/script/town.js"></script>
<!-- END PAGE LEVEL STYLES -->
<script>
	jQuery(document).ready(function() {
		Town.init();
// 		TableEditable.init();
	});
</script>
<!-- END JAVASCRIPTS -->
