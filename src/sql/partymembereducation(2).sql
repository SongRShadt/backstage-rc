/*
Navicat MySQL Data Transfer

Source Server         : qf
Source Server Version : 50151
Source Host           : localhost:3306
Source Database       : partymembereducation

Target Server Type    : MYSQL
Target Server Version : 50151
File Encoding         : 65001

Date: 2015-12-21 11:01:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `camera_info`
-- ----------------------------
DROP TABLE IF EXISTS `camera_info`;
CREATE TABLE `camera_info` (
  `streamid` varchar(55) DEFAULT NULL,
  `camerano` varchar(30) NOT NULL COMMENT '摄像头编号',
  `cameraname` varchar(60) NOT NULL COMMENT '摄像头名称',
  `cameraurl` varchar(100) DEFAULT NULL COMMENT '摄像头播放地址',
  `cameratype` int(2) NOT NULL DEFAULT '0' COMMENT '摄像头类型:0-会议室摄像头;1-监控摄像头;',
  `imgurl` varchar(300) DEFAULT NULL,
  `villageid` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`camerano`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of camera_info
-- ----------------------------
INSERT INTO `camera_info` VALUES ('dsd6vfk7', '00001', '书院会议室', 'http://172.29.0.111:5080/hls/dsd6vfk7.m3u8', '0', null, '001002');
INSERT INTO `camera_info` VALUES ('wzsdtcac', '00002', '宫家村会议室', 'http://172.29.0.111:5080/hls/wzsdtcac.m3u8', '0', null, '001003');
INSERT INTO `camera_info` VALUES ('td5m22pi', '00003', '毕家村会议室', 'http://172.29.0.111:5080/hls/td5m22pi.m3u8', '0', null, '001004');
INSERT INTO `camera_info` VALUES ('6xhwswr3', '00004', '鲁城会议室', 'http://172.29.0.111:5080/hls/6xhwswr3.m3u8', '0', null, '002003');
INSERT INTO `camera_info` VALUES ('cq53xk9i', '00005', '池涯会议室', 'http://172.29.0.111:5080/hls/cq53xk9i.m3u8', '0', null, '002005');
INSERT INTO `camera_info` VALUES ('n553jy33', '00006', '仓巷会议室', 'http://172.29.0.111:5080/hls/n553jy33.m3u8', '0', null, '002004');
INSERT INTO `camera_info` VALUES (null, '90001', '主会场一', 'http://172.29.0.111:5080/hls/n553jy33.m3u8', '0', 'VideoConference\\image\\img\\1.png', null);
INSERT INTO `camera_info` VALUES (null, '90002', '主会场二', 'http://172.29.0.111:5080/hls/dsd6vfk7.m3u8', '0', 'VideoConference\\image\\img\\2.png', null);
INSERT INTO `camera_info` VALUES (null, '90003', '主会场三', 'http://172.29.0.111:5080/hls/dsd6vfk7.m3u8', '0', 'VideoConference\\image\\img\\3.png', null);
INSERT INTO `camera_info` VALUES (null, '99998', '站点监控', 'lsdj/hyhf_zxx.jsp', '0', 'VideoConference\\image\\img\\1.png', null);

-- ----------------------------
-- Table structure for `camera_video_list`
-- ----------------------------
DROP TABLE IF EXISTS `camera_video_list`;
CREATE TABLE `camera_video_list` (
  `camerano` varchar(20) DEFAULT NULL COMMENT '摄像头编号',
  `videoid` int(8) NOT NULL DEFAULT '0' COMMENT '频编号视',
  `video_desc` varchar(80) DEFAULT NULL COMMENT '视频描述',
  `starttime` datetime DEFAULT NULL COMMENT '视频开始时间',
  `endtime` datetime DEFAULT NULL COMMENT '视频结束时间',
  `video_url` varchar(200) DEFAULT NULL COMMENT '频视播放地址',
  PRIMARY KEY (`videoid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of camera_video_list
-- ----------------------------
INSERT INTO `camera_video_list` VALUES ('90001', '0', '10月党员会议', '2015-10-15 02:53:39', '2015-10-15 02:53:48', 'http://172.29.0.108:1935/vod/_definst_/mp4:nvod/jxyeyjk/2015-09-16/jxyeyjk.stream_360p_20-08-12.mp4/playlist.m3u8');
INSERT INTO `camera_video_list` VALUES ('90001', '1', '9月党员会议', '2015-09-15 02:55:25', '2015-09-15 02:55:34', 'http://172.29.0.108:1935/vod/_definst_/mp4:nvod/jxyeyjk/2015-09-16/jxyeyjk.stream_360p_20-08-12.mp4/playlist.m3u8');

-- ----------------------------
-- Table structure for `stb_camera_rlt`
-- ----------------------------
DROP TABLE IF EXISTS `stb_camera_rlt`;
CREATE TABLE `stb_camera_rlt` (
  `stbno` varchar(20) NOT NULL COMMENT '机顶盒编号',
  `camerano` varchar(20) NOT NULL COMMENT '摄像头编号',
  `rlt_type` int(1) DEFAULT NULL COMMENT '关系类型0-县级直播会议关系;1-村镇直播会议关系'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stb_camera_rlt
-- ----------------------------
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:D1', '00002', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:D1', '00003', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:D1', '90003', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:D1', '00001', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:95', '00002', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:95', '90002', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:95', '90003', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:2D', '99998', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:2D', '00001', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:D0', '90002', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:D0', '90001', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:D0', '90003', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:D0', '00005', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:D0', '00006', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:D0', '99998', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:95', '90001', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:2D', '90001', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:2D', '90002', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:2D', '90003', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:E4', '90001', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:E4', '90002', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:E4', '90003', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:A1', '90001', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:A1', '90002', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:A1', '90003', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:A1', '99998', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:A1', '00005', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:E1', '90001', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:E1', '90002', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:E1', '90003', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:E1', '99998', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:E1', '00006', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:7D:7D:10', '90002', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:7D:7D:10', '90003', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:7D:7D:10', '99998', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:7D:7D:10', '90001', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:7D:7D:10', '00006', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:2D', '00002', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:D1', '90001', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:D1', '90002', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:2D', '00003', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:2D', '00004', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:2D', '00005', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:2D', '00006', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:7D:7D:0B', '90001', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:7D:7D:0B', '90002', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:7D:7D:0B', '90003', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:7D:7D:0B', '00001', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:7D:7D:0B', '00002', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:7D:7D:0B', '00003', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:7D:7D:0B', '99998', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:7D:7D:0B', '00005', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:7D:7D:0B', '00006', '1');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:E4', '00003', '0');
INSERT INTO `stb_camera_rlt` VALUES ('BC:20:BA:84:E9:E4', '99998', '0');
INSERT INTO `stb_camera_rlt` VALUES ('a089e4dc0e9d', '00001', '1');
INSERT INTO `stb_camera_rlt` VALUES ('a089e4dc0e9d', '00002', '1');
INSERT INTO `stb_camera_rlt` VALUES ('a089e4dc0e9d', '00003', '1');
INSERT INTO `stb_camera_rlt` VALUES ('a089e4dc0e9d', '90001', '0');
INSERT INTO `stb_camera_rlt` VALUES ('a089e4dc0e9d', '90002', '0');
INSERT INTO `stb_camera_rlt` VALUES ('a089e4dc0e9d', '90003', '0');
INSERT INTO `stb_camera_rlt` VALUES ('a089e4dc0e9d', '99998', '0');

-- ----------------------------
-- Table structure for `stb_info`
-- ----------------------------
DROP TABLE IF EXISTS `stb_info`;
CREATE TABLE `stb_info` (
  `stbno` varchar(20) NOT NULL COMMENT '机顶盒编号',
  `villageid` varchar(20) DEFAULT NULL COMMENT '村编号',
  PRIMARY KEY (`stbno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stb_info
-- ----------------------------
INSERT INTO `stb_info` VALUES ('a089e4dc0e9d', '001001');
INSERT INTO `stb_info` VALUES ('BC:20:BA:84:E9:2D', '001003');
INSERT INTO `stb_info` VALUES ('BC:20:BA:84:E9:A1', '002005');
INSERT INTO `stb_info` VALUES ('BC:20:BA:84:E9:D0', '002003');
INSERT INTO `stb_info` VALUES ('BC:20:BA:84:E9:D1', '001002');
INSERT INTO `stb_info` VALUES ('BC:20:BA:84:E9:E1', '002004');
INSERT INTO `stb_info` VALUES ('BC:20:BA:84:E9:E4', '001004');

-- ----------------------------
-- Table structure for `town_info`
-- ----------------------------
DROP TABLE IF EXISTS `town_info`;
CREATE TABLE `town_info` (
  `townid` varchar(20) NOT NULL COMMENT '镇编号',
  `townname` varchar(60) NOT NULL COMMENT '镇名称',
  `townkeyvalue` varchar(20) DEFAULT NULL COMMENT '镇对应到智慧城市栏目的key值',
  PRIMARY KEY (`townid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of town_info
-- ----------------------------
INSERT INTO `town_info` VALUES ('001', '书院街道', null);
INSERT INTO `town_info` VALUES ('002', '鲁城街道', null);

-- ----------------------------
-- Table structure for `video`
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video` (
  `id` varchar(45) NOT NULL,
  `starttime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `camerano` varchar(45) DEFAULT NULL,
  `taskid` varchar(45) DEFAULT NULL,
  `download_url` varchar(45) DEFAULT NULL,
  `play_url` varchar(45) DEFAULT NULL,
  `duration` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of video
-- ----------------------------
INSERT INTO `video` VALUES ('670b8ff0-e859-459a-871d-ab2e4f2db0b4', '2015-12-14 11:04:12', '2015-12-14 11:04:19', '00002', 'swTqPjov', '/record/201512/14/swTqPjov/swTqPjov.mp4', '/record/201512/14/swTqPjov/swTqPjov.m3u8', '7');
INSERT INTO `video` VALUES ('fdaf8f82-30bb-4fcf-ae8c-988c1a7432bb', '2015-12-14 12:43:27', '2015-12-14 12:43:36', '00002', 'NIzBGn0H', '/record/201512/14/NIzBGn0H/NIzBGn0H.mp4', '/record/201512/14/NIzBGn0H/NIzBGn0H.m3u8', '9');
INSERT INTO `video` VALUES ('5e57fc03-4731-4695-b858-e22849d2038d', '2015-12-14 11:16:31', '2015-12-14 19:04:24', '00002', 'WOIplGXB', '/record/201512/14/WOIplGXB/WOIplGXB.mp4', '/record/201512/14/WOIplGXB/WOIplGXB.m3u8', '28073');
INSERT INTO `video` VALUES ('17901ad7-4f59-4246-8f96-c6c22ccd6d28', '2015-12-14 10:53:07', '2015-12-14 18:55:22', '00002', 'NNfe3eJa', '/record/201512/14/NNfe3eJa/NNfe3eJa.mp4', '/record/201512/14/NNfe3eJa/NNfe3eJa.m3u8', '28935');
INSERT INTO `video` VALUES ('ceca2393-5600-4ec0-92ac-9f2b5bb98ad7', '2015-12-14 12:01:11', '2015-12-14 20:00:45', '00002', 'jdsiSzQS', '/record/201512/14/jdsiSzQS/jdsiSzQS.mp4', '/record/201512/14/jdsiSzQS/jdsiSzQS.m3u8', '28774');
INSERT INTO `video` VALUES ('31ea70dd-3ce6-4ef5-ab9f-1f7bdf168600', '2015-12-14 13:03:32', '2015-12-14 19:25:06', '00002', 'zIQo56sO', '/record/201512/14/zIQo56sO/zIQo56sO.mp4', '/record/201512/14/zIQo56sO/zIQo56sO.m3u8', '22894');
INSERT INTO `video` VALUES ('7640e4cf-3a12-426d-b3a3-813db87007b9', '2015-12-14 13:14:50', '2015-12-14 19:30:56', '00002', 'SwGmPnut', '/record/201512/14/SwGmPnut/SwGmPnut.mp4', '/record/201512/14/SwGmPnut/SwGmPnut.m3u8', '22566');
INSERT INTO `video` VALUES ('59f51d7a-feed-4377-a932-eb585d157e5a', '2015-12-14 13:19:56', '2015-12-14 19:41:12', '00002', 'ROIApQQD', '/record/201512/14/ROIApQQD/ROIApQQD.mp4', '/record/201512/14/ROIApQQD/ROIApQQD.m3u8', '22876');
INSERT INTO `video` VALUES ('aeae7d31-49e9-46cc-b079-af6bc6427191', '2015-12-14 11:34:21', '2015-12-14 19:29:52', '00002', 'qTevFCr4', '/record/201512/14/qTevFCr4/qTevFCr4.mp4', '/record/201512/14/qTevFCr4/qTevFCr4.m3u8', '28531');
INSERT INTO `video` VALUES ('e5c09401-3aab-4901-9dbc-8c27a1175cbd', '2015-12-14 11:36:27', '2015-12-14 19:45:38', '00002', 'CevzWFzO', '/record/201512/14/CevzWFzO/CevzWFzO.mp4', '/record/201512/14/CevzWFzO/CevzWFzO.m3u8', '29351');
INSERT INTO `video` VALUES ('253f1f1a-cab5-417f-b06b-7bdaa79e1d30', '2015-12-14 11:16:09', '2015-12-14 19:19:20', '00002', 'XeiGUluf', '/record/201512/14/XeiGUluf/XeiGUluf.mp4', '/record/201512/14/XeiGUluf/XeiGUluf.m3u8', '28991');
INSERT INTO `video` VALUES ('b69a06cd-5326-47aa-9966-355c905c22b6', '2015-12-14 13:33:15', '2015-12-14 19:45:33', '00002', 'p9ABXNXc', '/record/201512/14/p9ABXNXc/p9ABXNXc.mp4', '/record/201512/14/p9ABXNXc/p9ABXNXc.m3u8', '22338');
INSERT INTO `video` VALUES ('60ed3456-4ab4-40c9-ad9f-6a9fad2447fe', '2015-12-14 11:42:27', '2015-12-14 19:53:03', '00002', 'nFH13TsE', '/record/201512/14/nFH13TsE/nFH13TsE.mp4', '/record/201512/14/nFH13TsE/nFH13TsE.m3u8', '29436');
INSERT INTO `video` VALUES ('77a7bece-7af2-4e04-bde2-8fbfd884bc7b', '2015-12-14 11:54:38', '2015-12-14 19:58:38', '00002', 'OSMQafDq', '/record/201512/14/OSMQafDq/OSMQafDq.mp4', '/record/201512/14/OSMQafDq/OSMQafDq.m3u8', '29040');
INSERT INTO `video` VALUES ('645316b0-711c-4388-aa6f-8f7ade2a15cd', '2015-12-14 11:57:25', '2015-12-14 19:57:24', '00002', 'pw2iQNvm', '/record/201512/14/pw2iQNvm/pw2iQNvm.mp4', '/record/201512/14/pw2iQNvm/pw2iQNvm.m3u8', '28799');
INSERT INTO `video` VALUES ('56926d71-cb4e-4716-bfb6-823232da67c0', '2015-12-14 11:24:42', '2015-12-14 19:22:27', '00002', 'ZtDKwzq2', '/record/201512/14/ZtDKwzq2/ZtDKwzq2.mp4', '/record/201512/14/ZtDKwzq2/ZtDKwzq2.m3u8', '28665');
INSERT INTO `video` VALUES ('c6cb4561-2770-426d-9ca3-f494dd37bb87', '2015-12-14 13:33:05', '2015-12-14 19:46:55', '00002', 'euYdHQ4z', '/record/201512/14/euYdHQ4z/euYdHQ4z.mp4', '/record/201512/14/euYdHQ4z/euYdHQ4z.m3u8', '22430');
INSERT INTO `video` VALUES ('88b7479b-662e-4f2c-bfd6-025f892e599f', '2015-12-14 11:52:38', '2015-12-14 19:50:17', '00002', 'vNWF2ufM', '/record/201512/14/vNWF2ufM/vNWF2ufM.mp4', '/record/201512/14/vNWF2ufM/vNWF2ufM.m3u8', '28659');
INSERT INTO `video` VALUES ('19d37b87-f9ef-4d51-94f2-fb8b1b78b07e', '2015-12-14 13:39:29', '2015-12-14 19:54:07', '00002', 'CnBWgtel', '/record/201512/14/CnBWgtel/CnBWgtel.mp4', '/record/201512/14/CnBWgtel/CnBWgtel.m3u8', '22478');
INSERT INTO `video` VALUES ('f342837e-be03-4538-838f-ea3034a62c8b', '2015-12-14 12:02:14', '2015-12-14 20:00:45', '00002', 'TrAjoTeM', '/record/201512/14/TrAjoTeM/TrAjoTeM.mp4', '/record/201512/14/TrAjoTeM/TrAjoTeM.m3u8', '28711');
INSERT INTO `video` VALUES ('bf6571f6-8bd3-4817-8897-a72a9c822bcf', '2015-12-14 11:45:10', '2015-12-14 19:53:05', '00002', 'MsNfEEJ4', '/record/201512/14/MsNfEEJ4/MsNfEEJ4.mp4', '/record/201512/14/MsNfEEJ4/MsNfEEJ4.m3u8', '29275');
INSERT INTO `video` VALUES ('c35c0fe9-4393-411e-b1b7-6d94e7db0315', '2015-12-14 13:38:51', '2015-12-14 19:58:44', '00002', 'iRAjjMOg', '/record/201512/14/iRAjjMOg/iRAjjMOg.mp4', '/record/201512/14/iRAjjMOg/iRAjjMOg.m3u8', '22793');
INSERT INTO `video` VALUES ('77a6f18f-a7d3-4229-85c2-5cbb32c04932', '2015-12-14 12:08:46', '2015-12-14 20:07:26', '00002', 'm5lHmlCr', '/record/201512/14/m5lHmlCr/m5lHmlCr.mp4', '/record/201512/14/m5lHmlCr/m5lHmlCr.m3u8', '28720');
INSERT INTO `video` VALUES ('5e5ab153-019a-45e8-8326-6ba00bc11723', '2015-12-14 13:41:14', '2015-12-14 19:56:54', '00002', 'w8I9gqlC', '/record/201512/14/w8I9gqlC/w8I9gqlC.mp4', '/record/201512/14/w8I9gqlC/w8I9gqlC.m3u8', '22540');
INSERT INTO `video` VALUES ('a283f5c1-e9a1-46b3-9bd4-c23c68573b40', '2015-12-14 11:12:32', '2015-12-14 20:05:56', '00002', '9sN5TjAE', '/record/201512/14/9sN5TjAE/9sN5TjAE.mp4', '/record/201512/14/9sN5TjAE/9sN5TjAE.m3u8', '32004');
INSERT INTO `video` VALUES ('46355c57-e0a6-44ac-874c-c88b6809e2c9', '2015-12-14 13:57:25', '2015-12-14 20:11:16', '00002', 'EAuArNF5', '/record/201512/14/EAuArNF5/EAuArNF5.mp4', '/record/201512/14/EAuArNF5/EAuArNF5.m3u8', '22431');
INSERT INTO `video` VALUES ('f6a4a564-9d1e-488e-b9ec-fabc888e5058', '2015-12-14 12:57:36', '2015-12-14 19:16:24', '00002', 'WLiT2616', '/record/201512/14/WLiT2616/WLiT2616.mp4', '/record/201512/14/WLiT2616/WLiT2616.m3u8', '22728');
INSERT INTO `video` VALUES ('a26b2daf-49c4-4a93-b8e7-64f84aa57b7d', '2015-12-14 11:00:07', '2015-12-14 19:07:26', '00002', 'QSirzYkf', '/record/201512/14/QSirzYkf/QSirzYkf.mp4', '/record/201512/14/QSirzYkf/QSirzYkf.m3u8', '29239');
INSERT INTO `video` VALUES ('a8f60c90-9ab8-431c-a8d6-736c33c49a9d', '2015-12-14 11:07:59', '2015-12-14 19:14:23', '00002', 'tMg1kSVX', '/record/201512/14/tMg1kSVX/tMg1kSVX.mp4', '/record/201512/14/tMg1kSVX/tMg1kSVX.m3u8', '29184');
INSERT INTO `video` VALUES ('856072f5-1b7a-4a1b-9edb-49974d7ce170', '2015-12-14 11:06:29', '2015-12-14 19:12:37', '00002', '01yicQta', '/record/201512/14/01yicQta/01yicQta.mp4', '/record/201512/14/01yicQta/01yicQta.m3u8', '29168');
INSERT INTO `video` VALUES ('64289a7e-66d6-48fa-bc39-d371f637c339', '2015-12-14 10:04:32', '2015-12-14 20:44:51', '00003', 'AdUcA4gS', '/record/201512/14/AdUcA4gS/AdUcA4gS.mp4', '/record/201512/14/AdUcA4gS/AdUcA4gS.m3u8', '38419');
INSERT INTO `video` VALUES ('78931d33-1ecd-4c13-a928-2d13e17cf70a', '2015-12-14 09:53:31', '2015-12-14 20:29:42', '00003', 'VkbgI9Mt', '/record/201512/14/VkbgI9Mt/VkbgI9Mt.mp4', '/record/201512/14/VkbgI9Mt/VkbgI9Mt.m3u8', '38171');
INSERT INTO `video` VALUES ('725ba0aa-de3e-4e31-95a8-739a246103bf', '2015-12-14 09:47:42', '2015-12-14 20:27:25', '00003', 'eLsBeKrM', '/record/201512/14/eLsBeKrM/eLsBeKrM.mp4', '/record/201512/14/eLsBeKrM/eLsBeKrM.m3u8', '38383');

-- ----------------------------
-- Table structure for `village_info`
-- ----------------------------
DROP TABLE IF EXISTS `village_info`;
CREATE TABLE `village_info` (
  `villageid` varchar(20) NOT NULL COMMENT '村编号',
  `villagename` varchar(60) NOT NULL COMMENT '镇名称',
  `villagekeyvalue` varchar(20) DEFAULT NULL COMMENT '村对应到智慧城市栏目的key值',
  `townid` varchar(20) DEFAULT NULL COMMENT '村所属镇编号',
  `orgcode` varchar(30) DEFAULT NULL COMMENT '村组织编号',
  PRIMARY KEY (`villageid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of village_info
-- ----------------------------
INSERT INTO `village_info` VALUES ('001001', '六个党建', null, '001', '-1');
INSERT INTO `village_info` VALUES ('001002', '书院街道', null, '001', '2');
INSERT INTO `village_info` VALUES ('001003', '宫家村', null, '001', '3');
INSERT INTO `village_info` VALUES ('001004', '毕家村', null, '001', '4');
INSERT INTO `village_info` VALUES ('002003', '鲁城街道', null, '002', '5');
INSERT INTO `village_info` VALUES ('002004', '仓巷社区', null, '002', '6');
INSERT INTO `village_info` VALUES ('002005', '池涯社区', null, '002', '7');

-- ----------------------------
-- Table structure for `we_sys_dayweatherinfo`
-- ----------------------------
DROP TABLE IF EXISTS `we_sys_dayweatherinfo`;
CREATE TABLE `we_sys_dayweatherinfo` (
  `ID` varchar(36) NOT NULL,
  `CODE_D` varchar(255) DEFAULT NULL,
  `CODE_N` varchar(255) DEFAULT NULL,
  `DATE` varchar(255) DEFAULT NULL,
  `DEG` varchar(255) DEFAULT NULL,
  `DIR` varchar(255) DEFAULT NULL,
  `HUM` varchar(255) DEFAULT NULL,
  `PCPN` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `PRES` varchar(255) DEFAULT NULL,
  `SC` varchar(255) DEFAULT NULL,
  `SPD` varchar(255) DEFAULT NULL,
  `SR` varchar(255) DEFAULT NULL,
  `SS` varchar(255) DEFAULT NULL,
  `TMPMAX` varchar(255) DEFAULT NULL,
  `TMPMIN` varchar(255) DEFAULT NULL,
  `TXT_D` varchar(255) DEFAULT NULL,
  `TXT_N` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `VIS` varchar(255) DEFAULT NULL,
  `WEEK` varchar(255) DEFAULT NULL,
  `W_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_gl4idf60pms87p60nn5n55i7n` (`W_ID`),
  CONSTRAINT `FK_gl4idf60pms87p60nn5n55i7n` FOREIGN KEY (`W_ID`) REFERENCES `we_sys_weather` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of we_sys_dayweatherinfo
-- ----------------------------
INSERT INTO `we_sys_dayweatherinfo` VALUES ('002c2cbb-6b74-4ee3-bc41-9f7749e3b080', '100', '101', '2015-10-13', '199', '西南风', '19', '0.0', '0', '1022', '3-4', '11', '06:17', '17:41', '25', '10', '晴', '多云', 'images/qing.png', '10', '星期二', '43201297-f8ee-4025-add1-d9eab2862d49');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('01b87a36-bf25-4d05-bedc-67d51aaa0fc4', '100', '100', '2015-10-16', '175', '无持续风向', '22', '0.0', '0', '1018', '微风', '9', '06:19', '17:37', '27', '12', '晴', '晴', 'images/qing.png', '10', '星期五', '7a856cb3-5b40-4a67-a5d0-a1230768bcfd');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('03557c14-7b76-46d1-a558-fe8bbd4192f3', '101', '100', '2015-10-14', '197', '无持续风向', '20', '0.0', '0', '1017', '微风', '4', '06:18', '17:40', '26', '12', '多云', '晴', 'images/duoyun.png', '10', '星期三', '0d1be87e-2d44-44b6-ba0d-e920994f72b2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('04099bcb-5f42-4f2b-a368-0af1936cedfb', '100', '101', '2015-10-14', '191', '南风', '20', '0.0', '0', '1017', '微风', '4', '06:18', '17:40', '28', '12', '晴', '多云', 'images/qing.png', '10', '星期三', 'ccfc76c5-6669-4372-8007-0705a8a014f3');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('042b7fd9-2536-44ef-8ff4-666c513f76ca', '101', '100', '2015-10-15', '29', '北风', '28', '0.0', '0', '1016', '微风', '4', '06:19', '17:38', '26', '10', '多云', '晴', 'images/duoyun.png', '10', '星期四', '37b7ea55-bc41-4c39-973e-0f0b137239f6');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('0525a1e9-830e-4728-8738-3320b5360640', '100', '100', '2015-10-15', '221', '东南风', '26', '0.0', '0', '1015', '微风', '8', '06:19', '17:38', '26', '11', '晴', '晴', 'images/qing.png', '10', '星期四', '26c065b2-ed18-429c-877e-7ab41586af9d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('05446213-5671-4f17-bbfd-e0b1d9e21d22', '100', '100', '2015-10-19', '165', '无持续风向', '35', '0.0', '0', '1017', '微风', '11', '06:22', '17:33', '26', '15', '晴', '晴', 'images/qing.png', '10', '星期一', 'c50b611e-6dc2-47df-9f06-d7c1a64d2931');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('05797504-8d62-425f-b354-e0bc8612db9a', '100', '100', '2015-10-16', '150', '南风', '22', '0.0', '0', '1018', '微风', '7', '06:19', '17:37', '27', '10', '晴', '晴', 'images/qing.png', '10', '星期五', 'f377f019-0348-4d84-a93a-0ed0b2706a2d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('05835735-0a5d-4735-8c63-0ebc95aec4fc', '305', '104', '2015-09-24', '115', '无持续风向', '42', '0.0', '39', '1009', '微风', '4', '06:02', '18:08', '25', '20', '小雨', '阴', 'images/xiaoyu.png', '10', '星期四', 'c3278f94-6e0f-4ead-a27e-374c09465276');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('0672afbc-d472-400c-bd93-3af7b0da604d', '100', '100', '2015-10-11', '322', '北风', '22', '0.0', '0', '1019', '微风', '7', '06:15', '17:44', '22', '6', '晴', '晴', 'images/qing.png', '10', '星期日', 'bdcdbd4b-097a-4112-8a70-33bf9be5b1c0');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('081ae643-9655-42e7-84cb-1a4a41f046bd', '101', '100', '2015-10-16', '43', '无持续风向', '20', '0.0', '0', '1018', '微风', '11', '06:19', '17:37', '28', '11', '多云', '晴', 'images/duoyun.png', '10', '星期五', '4ccc3e88-7bd3-487b-9988-7a74fa271fb0');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('085305e1-b2f0-499c-8449-1ddc1a77e4c1', '100', '100', '2015-10-17', '184', '无持续风向', '28', '0.0', '0', '1017', '微风', '4', '06:20', '17:36', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期六', '8801860c-99dc-43b9-bee2-66686e2f9ff2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('097e63ef-44d8-4951-aec7-c7bd4cbfe6c4', '100', '100', '2015-10-16', '125', '无持续风向', '24', '0.0', '0', '1018', '微风', '4', '06:19', '17:37', '27', '13', '晴', '晴', 'images/qing.png', '10', '星期五', 'd6be07ca-b1e5-4509-b1d5-623717739ac9');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('0982d7a7-2a61-4278-beab-33efb4989c9b', '100', '100', '2015-10-11', '335', '北风', '25', '0.0', '0', '1020', '3-4', '7', '06:15', '17:44', '21', '8', '晴', '晴', 'images/qing.png', '10', '星期日', '4d4e5d4a-097c-4b93-bf9c-7792cc9f75a2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('0a2bb70e-2992-49a4-b174-7d60236a7c5c', '101', '100', '2015-10-15', '332', '无持续风向', '22', '0.0', '0', '1016', '微风', '11', '06:19', '17:38', '27', '11', '多云', '晴', 'images/duoyun.png', '10', '星期四', 'c50b611e-6dc2-47df-9f06-d7c1a64d2931');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('0a4ae638-59f8-4a70-badd-9eaf15fe3fe5', '100', '100', '2015-10-16', '168', '无持续风向', '29', '0.0', '0', '1018', '微风', '7', '06:19', '17:37', '27', '15', '晴', '晴', 'images/qing.png', '10', '星期五', '6e2c44a5-9575-4379-bcea-1674f3175048');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('0a9c88e1-41ce-4deb-86a5-9f8b1d686bea', '101', '100', '2015-10-15', '332', '无持续风向', '22', '0.0', '0', '1016', '微风', '11', '06:19', '17:38', '27', '11', '多云', '晴', 'images/duoyun.png', '10', '星期四', 'b5992b35-8896-4d2e-96b0-deee3f288109');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('0b13188f-00e9-4d5c-9821-5f6b11a570f7', '100', '100', '2015-10-17', '182', '无持续风向', '29', '0.0', '0', '1017', '微风', '4', '06:20', '17:36', '27', '12', '晴', '晴', 'images/qing.png', '10', '星期六', '20a31c2b-fc46-4ac0-bee1-2207e3b226f9');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('0d5c45f4-0075-4298-ae95-faeeb699a719', '100', '100', '2015-10-16', '125', '无持续风向', '24', '0.0', '0', '1018', '微风', '4', '06:19', '17:37', '27', '13', '晴', '晴', 'images/qing.png', '10', '星期五', '2792d049-e8e0-49a1-a988-4efc974f61be');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('0da8ab39-bfa8-4163-878b-923af7831942', '100', '100', '2015-10-19', '147', '无持续风向', '25', '0.0', '0', '1016', '微风', '4', '06:22', '17:33', '26', '15', '晴', '晴', 'images/qing.png', '10', '星期一', '3a73898d-f5b1-4228-9e4a-9205e09c6afb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('0e292f6d-4bf6-451f-98b5-c8cecab61d39', '101', '101', '2015-09-23', '51', '南风', '41', '0.0', '0', '1009', '微风', '4', '06:01', '18:09', '29', '19', '多云', '多云', 'images/duoyun.png', '10', '星期三', 'e1c44614-22bb-40d8-9db2-ce03556fc806');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('0e67af29-ae5e-4605-8261-d92919e89a77', '100', '100', '2015-10-12', '355', '南风', '19', '0.0', '0', '1022', '微风', '7', '06:16', '17:42', '24', '6', '晴', '晴', 'images/qing.png', '10', '星期一', '1ebbf1f6-ff2f-454a-ab1d-c8735cf1cfdf');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('0f02c8eb-b250-4bf9-a231-aaf112751882', '100', '100', '2015-10-11', '314', '北风', '19', '0.0', '0', '1019', '3-4', '11', '06:15', '17:44', '21', '6', '晴', '晴', 'images/qing.png', '10', '星期日', '37b62595-c596-47d2-aa85-affcacde22b5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('0f29f568-a3ae-40a7-84a7-4364b17f4ca7', '101', '101', '2015-10-14', '200', '无持续风向', '21', '0.0', '0', '1018', '微风', '11', '06:18', '17:40', '25', '13', '多云', '多云', 'images/duoyun.png', '10', '星期三', '040a0a72-be00-4f7a-909d-43b07e68eccb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('0f577ba9-c656-48e5-a454-a71619dec5ae', '100', '100', '2015-10-16', '168', '无持续风向', '29', '0.0', '0', '1018', '微风', '7', '06:19', '17:37', '27', '15', '晴', '晴', 'images/qing.png', '10', '星期五', 'e3f1ac1d-f5b5-4e71-bbaf-5b935378b6cb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('0f87ec2d-1de8-47da-adec-2949c38a861d', '100', '100', '2015-10-17', '191', '无持续风向', '28', '0.0', '0', '1018', '微风', '11', '06:20', '17:36', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期六', '4ccc3e88-7bd3-487b-9988-7a74fa271fb0');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('10c7a93f-8eb3-4190-bdd6-6ddc8d9634e5', '101', '100', '2015-09-25', '55', '无持续风向', '38', '0.0', '19', '1009', '微风', '4', '06:02', '18:06', '28', '18', '多云', '晴', 'images/duoyun.png', '10', '星期五', '8d23507d-9c63-4bac-bbfc-59914cc07a81');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('11524095-0262-4e0e-b956-f2986198b798', '100', '101', '2015-10-18', '57', '无持续风向', '29', '0.0', '0', '1019', '微风', '8', '06:21', '17:34', '23', '15', '晴', '多云', 'images/qing.png', '10', '星期日', '26c065b2-ed18-429c-877e-7ab41586af9d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('1162f008-5055-4bb0-ab30-151bcdc48063', '101', '100', '2015-10-16', '88', '无持续风向', '21', '0.0', '0', '1018', '微风', '15', '06:19', '17:37', '28', '11', '多云', '晴', 'images/duoyun.png', '10', '星期五', 'd6bdba54-752b-4f62-beb1-f02622b11619');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('116a471e-704a-450b-9ee1-e9590f597e75', '100', '100', '2015-10-11', '314', '北风', '19', '0.0', '0', '1019', '3-4', '11', '06:15', '17:44', '21', '6', '晴', '晴', 'images/qing.png', '10', '星期日', '43201297-f8ee-4025-add1-d9eab2862d49');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('11c9ad5a-4ed2-45b9-892e-2b2052e26df6', '100', '100', '2015-10-20', '83', '无持续风向', '31', '0.0', '0', '1014', '微风', '11', '06:23', '17:32', '27', '15', '晴', '晴', 'images/qing.png', '10', '星期二', 'b5992b35-8896-4d2e-96b0-deee3f288109');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('1337787c-bd59-4c3a-b6b2-368d1291399a', '100', '100', '2015-10-19', '165', '无持续风向', '35', '0.0', '0', '1017', '微风', '11', '06:22', '17:33', '26', '15', '晴', '晴', 'images/qing.png', '10', '星期一', 'b5992b35-8896-4d2e-96b0-deee3f288109');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('13c327ca-cee5-4450-b9f9-fe30dee3971d', '100', '101', '2015-10-20', '167', '无持续风向', '33', '0.0', '0', '1015', '微风', '4', '06:23', '17:32', '26', '15', '晴', '多云', 'images/qing.png', '10', '星期二', '8dd107b6-6144-4450-b42f-2dbd104b433b');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('13cb4978-23e9-4c6d-a4bd-fa8a1f35e8c8', '100', '100', '2015-10-12', '3', '西南风', '20', '0.0', '0', '1023', '3-4', '11', '06:16', '17:42', '24', '8', '晴', '晴', 'images/qing.png', '10', '星期一', '69f9f736-24bf-499a-bafb-418e3a87a0ec');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('140303ea-9edf-4d06-bb32-babf203d0f5b', '100', '100', '2015-10-16', '168', '无持续风向', '29', '0.0', '0', '1018', '微风', '7', '06:19', '17:37', '27', '15', '晴', '晴', 'images/qing.png', '10', '星期五', 'dfd10274-dc82-4f34-bbd7-8fd758890ae5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('150b1792-ac01-4430-9afc-9ae8add7273c', '100', '100', '2015-10-17', '181', '无持续风向', '23', '0.0', '0', '1016', '微风', '11', '06:20', '17:36', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期六', '040a0a72-be00-4f7a-909d-43b07e68eccb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('18f5a001-1eaa-4fae-bb9d-4986f1126d44', '100', '100', '2015-10-12', '3', '西南风', '20', '0.0', '0', '1023', '3-4', '11', '06:16', '17:42', '24', '8', '晴', '晴', 'images/qing.png', '10', '星期一', '43201297-f8ee-4025-add1-d9eab2862d49');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('1926417a-4def-45ae-baaa-6868aeb3533f', '101', '101', '2015-10-21', '143', '无持续风向', '32', '0.0', '0', '1016', '微风', '4', '06:24', '17:31', '24', '15', '多云', '多云', 'images/duoyun.png', '10', '星期三', '8dd107b6-6144-4450-b42f-2dbd104b433b');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('19b9bf25-f5ab-4576-acd9-477c9c7f4804', '100', '101', '2015-10-13', '199', '西南风', '19', '0.0', '0', '1022', '3-4', '11', '06:17', '17:41', '25', '10', '晴', '多云', 'images/qing.png', '10', '星期二', '69f9f736-24bf-499a-bafb-418e3a87a0ec');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('19e36d54-1e4f-4f78-8988-d4c5fffae399', '100', '101', '2015-10-20', '150', '无持续风向', '30', '0.0', '0', '1015', '微风', '4', '06:23', '17:32', '26', '15', '晴', '多云', 'images/qing.png', '10', '星期二', '3a73898d-f5b1-4228-9e4a-9205e09c6afb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('1a4eb55e-9753-41d1-aaa7-36af7529db40', '100', '100', '2015-10-13', '200', '南风', '17', '0.0', '0', '1021', '微风', '4', '06:17', '17:41', '29', '8', '晴', '晴', 'images/qing.png', '10', '星期二', '8801860c-99dc-43b9-bee2-66686e2f9ff2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('1a975b4a-f197-44cf-ae93-14f03c1f5b69', '100', '101', '2015-10-18', '69', '无持续风向', '27', '0.0', '0', '1019', '微风', '7', '06:21', '17:34', '23', '15', '晴', '多云', 'images/qing.png', '10', '星期日', 'dfd10274-dc82-4f34-bbd7-8fd758890ae5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('1aa0f9e2-7373-4ad7-99d0-965bc81c929f', '101', '100', '2015-10-13', '220', '无持续风向', '16', '0.0', '0', '1021', '微风', '7', '06:17', '17:41', '25', '11', '多云', '晴', 'images/duoyun.png', '10', '星期二', 'e2caa130-4580-436c-b647-aa5a2667fbde');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('1ad10d08-09da-4a93-9168-2e6058d8aa25', '100', '100', '2015-10-18', '16', '无持续风向', '31', '0.0', '0', '1019', '微风', '4', '06:21', '17:34', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期日', 'ba8c15ba-41f4-40e9-abfb-a81a2dcb0324');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('1b2c77d0-7f61-4cfa-b29c-eedff403b9b0', '100', '100', '2015-10-16', '171', '无持续风向', '25', '0.0', '0', '1018', '微风', '4', '06:19', '17:37', '28', '14', '晴', '晴', 'images/qing.png', '10', '星期五', 'cc663d03-3cfd-439b-9203-bf0b99b43a85');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('1c251c00-effd-43bc-85e9-846ef4d95355', '101', '100', '2015-10-16', '43', '无持续风向', '20', '0.0', '0', '1018', '微风', '11', '06:19', '17:37', '28', '11', '多云', '晴', 'images/duoyun.png', '10', '星期五', 'c50b611e-6dc2-47df-9f06-d7c1a64d2931');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('1c9cb4a8-c0a5-4ee1-b32a-4871e022f237', '101', '101', '2015-10-17', '180', '无持续风向', '31', '0.0', '0', '1017', '微风', '4', '06:20', '17:36', '27', '15', '多云', '多云', 'images/duoyun.png', '10', '星期六', '3a73898d-f5b1-4228-9e4a-9205e09c6afb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('1cf273d3-09bb-4f28-b68d-5a0d09d0b0fa', '101', '101', '2015-10-17', '180', '无持续风向', '31', '0.0', '0', '1017', '微风', '4', '06:20', '17:36', '27', '15', '多云', '多云', 'images/duoyun.png', '10', '星期六', 'ccfc76c5-6669-4372-8007-0705a8a014f3');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('1d4d432a-9401-4015-a47b-e676aa01cfb8', '100', '100', '2015-10-11', '314', '北风', '19', '0.0', '0', '1019', '3-4', '11', '06:15', '17:44', '21', '6', '晴', '晴', 'images/qing.png', '10', '星期日', '040a0a72-be00-4f7a-909d-43b07e68eccb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('1e8b0fd9-8712-4bc8-b0b3-c24b87805a5d', '100', '100', '2015-10-15', '203', '无持续风向', '25', '0.0', '0', '1017', '微风', '4', '06:19', '17:38', '26', '13', '晴', '晴', 'images/qing.png', '10', '星期四', '0378b4ff-3867-40ba-8364-5f65c0543063');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('1eb5171a-3860-4a0f-aad2-8759184d5d36', '100', '101', '2015-10-13', '199', '南风', '16', '0.0', '0', '1021', '微风', '7', '06:17', '17:41', '26', '11', '晴', '多云', 'images/qing.png', '10', '星期二', 'dfd10274-dc82-4f34-bbd7-8fd758890ae5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('1edf567e-0160-4bf3-b10d-a366683242e1', '101', '101', '2015-10-18', '84', '无持续风向', '28', '0.0', '0', '1020', '微风', '4', '06:21', '17:34', '25', '14', '多云', '多云', 'images/duoyun.png', '10', '星期日', '37b7ea55-bc41-4c39-973e-0f0b137239f6');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('1f79c863-de5b-44df-93cc-4d98fe2d15cc', '305', '104', '2015-09-24', '115', '无持续风向', '42', '0.0', '39', '1009', '微风', '4', '06:02', '18:08', '25', '20', '小雨', '阴', 'images/xiaoyu.png', '10', '星期四', 'beeca77c-46dd-42d8-915a-a5a33863bb61');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('21125a57-5f51-46a8-a183-caacb7c676e8', '101', '100', '2015-10-15', '343', '无持续风向', '25', '0.0', '0', '1016', '微风', '4', '06:19', '17:38', '26', '12', '多云', '晴', 'images/duoyun.png', '10', '星期四', '978b086a-5382-4552-b95b-c759bc7619dc');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('23521638-3919-4cef-a8f4-08a66bba3f18', '100', '100', '2015-10-20', '83', '无持续风向', '31', '0.0', '0', '1014', '微风', '11', '06:23', '17:32', '27', '15', '晴', '晴', 'images/qing.png', '10', '星期二', 'c50b611e-6dc2-47df-9f06-d7c1a64d2931');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('23977058-d82a-436b-8f05-d5eea1143135', '100', '100', '2015-10-12', '355', '南风', '19', '0.0', '0', '1022', '微风', '7', '06:16', '17:42', '24', '6', '晴', '晴', 'images/qing.png', '10', '星期一', '6300d30d-7c05-4709-b0c9-b870d90e00c7');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('261d4808-430f-4a22-9a70-89f5a90860f6', '101', '100', '2015-10-17', '182', '无持续风向', '29', '0.0', '0', '1017', '微风', '7', '06:20', '17:36', '24', '13', '多云', '晴', 'images/duoyun.png', '10', '星期六', '6e2c44a5-9575-4379-bcea-1674f3175048');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('262083c8-48ad-43e2-aee1-c1c89b0bd41e', '101', '101', '2015-10-17', '169', '无持续风向', '31', '0.0', '0', '1016', '微风', '7', '06:20', '17:36', '28', '14', '多云', '多云', 'images/duoyun.png', '10', '星期六', 'bdcdbd4b-097a-4112-8a70-33bf9be5b1c0');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('28f8c9c2-34a1-452b-8260-89fe425ca221', '101', '101', '2015-09-21', '165', '南风', '36', '0.0', '0', '1011', '微风', '4', '05:59', '18:12', '29', '17', '多云', '多云', 'images/duoyun.png', '10', '星期一', '05a3b4ac-1fd3-47e3-a9fa-9e4f66cbaf15');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('2982f190-240b-401c-83f8-c7f54c304a94', '100', '100', '2015-10-12', '4', '南风', '19', '0.0', '0', '1022', '微风', '4', '06:16', '17:42', '25', '6', '晴', '晴', 'images/qing.png', '10', '星期一', 'a7a5d3f9-bff1-4f11-b54c-e3c05ddd3b9e');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('2a1d2e0c-91c1-4b92-8832-9724ad37ce6a', '100', '101', '2015-10-18', '123', '无持续风向', '31', '0.0', '0', '1018', '微风', '4', '06:21', '17:34', '23', '15', '晴', '多云', 'images/qing.png', '10', '星期日', '0378b4ff-3867-40ba-8364-5f65c0543063');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('2a7cdad5-b7d7-4964-a20e-d4087c6ae391', '100', '100', '2015-10-20', '95', '无持续风向', '35', '0.0', '0', '1016', '微风', '7', '06:23', '17:32', '25', '15', '晴', '晴', 'images/qing.png', '10', '星期二', '4a3b6c50-4402-43b6-b0e8-ab10128c275e');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('2b36cf6b-08da-4c33-93da-6a6ddc003fd8', '101', '101', '2015-10-17', '183', '无持续风向', '26', '0.0', '0', '1016', '微风', '4', '06:20', '17:36', '28', '14', '多云', '多云', 'images/duoyun.png', '10', '星期六', 'ba8c15ba-41f4-40e9-abfb-a81a2dcb0324');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('2b4fe259-92c8-49ad-b782-cc28d701fddb', '100', '100', '2015-10-10', '324', '北风', '18', '0.0', '0', '1018', '3-4', '7', '06:14', '17:45', '19', '7', '晴', '晴', 'images/qing.png', '10', '星期六', '4d4e5d4a-097c-4b93-bf9c-7792cc9f75a2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('2e224047-e991-4550-a157-48b7629b2cf0', '101', '101', '2015-10-14', '193', '南风', '20', '0.0', '0', '1017', '微风', '7', '06:18', '17:40', '25', '11', '多云', '多云', 'images/duoyun.png', '10', '星期三', '6e2c44a5-9575-4379-bcea-1674f3175048');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('2f6cc202-cc2e-4e00-b825-ca62dfb09288', '100', '100', '2015-10-16', '175', '无持续风向', '30', '0.0', '0', '1018', '微风', '7', '06:19', '17:37', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期五', 'e2caa130-4580-436c-b647-aa5a2667fbde');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('31b73305-d554-413e-b4db-caae075aff92', '100', '101', '2015-10-14', '191', '南风', '20', '0.0', '0', '1017', '微风', '4', '06:18', '17:40', '28', '12', '晴', '多云', 'images/qing.png', '10', '星期三', '978b086a-5382-4552-b95b-c759bc7619dc');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('33b47eaa-63df-4f92-8657-c43595beb7d3', '100', '100', '2015-10-12', '4', '南风', '19', '0.0', '0', '1022', '微风', '4', '06:16', '17:42', '25', '6', '晴', '晴', 'images/qing.png', '10', '星期一', '97d962ec-aeec-45d4-b9ca-191d3b12d0f1');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('33f8e461-8d01-438f-b8c9-5af783e6b57b', '100', '101', '2015-09-26', '89', '无持续风向', '32', '0.0', '0', '1012', '微风', '4', '06:03', '18:05', '29', '18', '晴', '多云', 'images/qing.png', '10', '星期六', '05a3b4ac-1fd3-47e3-a9fa-9e4f66cbaf15');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('348a1331-ac09-4e40-8562-b024a914786a', '100', '100', '2015-10-18', '16', '无持续风向', '31', '0.0', '0', '1019', '微风', '4', '06:21', '17:34', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期日', '8977b5b4-7db1-4131-a180-29f07fba47ab');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('3495a7fe-8ce7-4a2d-9961-0c1f82fc65c1', '101', '100', '2015-09-25', '55', '无持续风向', '38', '0.0', '19', '1009', '微风', '4', '06:02', '18:06', '28', '18', '多云', '晴', 'images/duoyun.png', '10', '星期五', 'c3278f94-6e0f-4ead-a27e-374c09465276');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('3674fc76-8d1f-4b12-b1b9-9205309cc942', '100', '305', '2015-10-20', '124', '无持续风向', '28', '0.0', '0', '1014', '微风', '15', '06:23', '17:32', '25', '15', '晴', '小雨', 'images/qing.png', '10', '星期二', 'd6bdba54-752b-4f62-beb1-f02622b11619');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('36ab84a5-ddda-40fa-9065-c57c3ae35350', '100', '101', '2015-10-20', '150', '无持续风向', '30', '0.0', '0', '1015', '微风', '4', '06:23', '17:32', '26', '15', '晴', '多云', 'images/qing.png', '10', '星期二', '978b086a-5382-4552-b95b-c759bc7619dc');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('37985855-b92f-40c0-8cd3-d050a9e5f8cb', '101', '100', '2015-10-15', '343', '无持续风向', '25', '0.0', '0', '1016', '微风', '4', '06:19', '17:38', '26', '12', '多云', '晴', 'images/duoyun.png', '10', '星期四', '2792d049-e8e0-49a1-a988-4efc974f61be');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('39301d6a-f436-4127-8b36-7cea78717701', '101', '101', '2015-10-14', '196', '东南风', '21', '0.0', '0', '1018', '微风', '4', '06:18', '17:40', '25', '12', '多云', '多云', 'images/duoyun.png', '10', '星期三', '0378b4ff-3867-40ba-8364-5f65c0543063');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('3962da96-fc9c-4ee1-b2b7-d92cd856d68f', '101', '101', '2015-10-18', '72', '无持续风向', '27', '0.0', '0', '1019', '微风', '9', '06:21', '17:34', '25', '14', '多云', '多云', 'images/duoyun.png', '10', '星期日', '7a856cb3-5b40-4a67-a5d0-a1230768bcfd');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('3a6a446c-6b32-4323-b38c-a08e8ebb8f75', '100', '100', '2015-10-15', '208', '无持续风向', '23', '0.0', '0', '1017', '微风', '7', '06:19', '17:38', '27', '13', '晴', '晴', 'images/qing.png', '10', '星期四', '1ebbf1f6-ff2f-454a-ab1d-c8735cf1cfdf');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('3b096bd4-a2f5-4c6f-9494-01c570b1558e', '101', '100', '2015-10-17', '178', '无持续风向', '26', '0.0', '0', '1016', '微风', '4', '06:20', '17:36', '24', '13', '多云', '晴', 'images/duoyun.png', '10', '星期六', '97d962ec-aeec-45d4-b9ca-191d3b12d0f1');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('3c2ed5f8-c7e8-4e20-b1b1-e5f7f95b8c9f', '101', '100', '2015-10-13', '203', '无持续风向', '17', '0.0', '0', '1021', '微风', '4', '06:17', '17:41', '25', '10', '多云', '晴', 'images/duoyun.png', '10', '星期二', '71147a0f-6fba-4e15-a6e7-f29099056b55');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('3c3b1675-3ed9-42fb-83f2-4f7c3c60edee', '101', '101', '2015-10-14', '200', '无持续风向', '21', '0.0', '0', '1018', '微风', '11', '06:18', '17:40', '25', '13', '多云', '多云', 'images/duoyun.png', '10', '星期三', '69f9f736-24bf-499a-bafb-418e3a87a0ec');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('3cd20e3e-2aa3-4746-9e6e-002c76b8d906', '101', '100', '2015-10-14', '197', '无持续风向', '20', '0.0', '0', '1017', '微风', '4', '06:18', '17:40', '26', '12', '多云', '晴', 'images/duoyun.png', '10', '星期三', 'ba8c15ba-41f4-40e9-abfb-a81a2dcb0324');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('3d0123aa-3192-4da2-ba87-8ecd34dd756b', '100', '100', '2015-10-15', '1', '北风', '28', '0.0', '0', '1016', '微风', '7', '06:19', '17:38', '28', '10', '晴', '晴', 'images/qing.png', '10', '星期四', 'f377f019-0348-4d84-a93a-0ed0b2706a2d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('3ddf581e-3a9f-4978-adb4-f44633f6f55e', '101', '101', '2015-10-17', '183', '无持续风向', '26', '0.0', '0', '1016', '微风', '4', '06:20', '17:36', '28', '14', '多云', '多云', 'images/duoyun.png', '10', '星期六', '0d1be87e-2d44-44b6-ba0d-e920994f72b2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('3e0eb14d-a77d-4988-afeb-c47b86805c7b', '100', '100', '2015-10-19', '147', '无持续风向', '25', '0.0', '0', '1016', '微风', '4', '06:22', '17:33', '26', '15', '晴', '晴', 'images/qing.png', '10', '星期一', '2792d049-e8e0-49a1-a988-4efc974f61be');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('3ea396f0-18a6-45e3-ac11-d61706339a7e', '101', '101', '2015-10-17', '169', '无持续风向', '31', '0.0', '0', '1016', '微风', '7', '06:20', '17:36', '28', '14', '多云', '多云', 'images/duoyun.png', '10', '星期六', '1ebbf1f6-ff2f-454a-ab1d-c8735cf1cfdf');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('3fcf59e8-8714-418a-840f-81458f990a18', '101', '100', '2015-10-17', '182', '无持续风向', '29', '0.0', '0', '1017', '微风', '7', '06:20', '17:36', '24', '13', '多云', '晴', 'images/duoyun.png', '10', '星期六', 'dfd10274-dc82-4f34-bbd7-8fd758890ae5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('404456be-4fe9-4c70-8e11-31465162673e', '100', '100', '2015-10-17', '191', '无持续风向', '28', '0.0', '0', '1018', '微风', '11', '06:20', '17:36', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期六', 'c50b611e-6dc2-47df-9f06-d7c1a64d2931');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('40c87486-1557-4668-8030-061686a3bbd4', '100', '100', '2015-09-27', '131', '无持续风向', '18', '0.0', '1', '1014', '微风', '4', '06:04', '18:03', '28', '18', '晴', '晴', 'images/qing.png', '10', '星期日', '8d23507d-9c63-4bac-bbfc-59914cc07a81');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('416133a0-8487-406d-af6d-b3058b8b1202', '101', '300', '2015-09-22', '164', '南风', '44', '1.5', '70', '1011', '微风', '4', '06:00', '18:11', '28', '18', '多云', '阵雨', 'images/duoyun.png', '10', '星期二', '05a3b4ac-1fd3-47e3-a9fa-9e4f66cbaf15');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('41d168a5-6bfd-4bf7-b132-e85765de1153', '100', '100', '2015-10-19', '147', '无持续风向', '25', '0.0', '0', '1016', '微风', '4', '06:22', '17:33', '26', '15', '晴', '晴', 'images/qing.png', '10', '星期一', '6b291c64-06d5-4965-b535-488f8d011865');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('42f8b5df-5338-49d3-b701-e24c175a0079', '101', '300', '2015-09-22', '164', '南风', '44', '1.5', '70', '1011', '微风', '4', '06:00', '18:11', '28', '18', '多云', '阵雨', 'images/duoyun.png', '10', '星期二', 'c3278f94-6e0f-4ead-a27e-374c09465276');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('4397a032-0b55-408c-9a81-6332b9f99ebe', '101', '100', '2015-10-14', '203', '无持续风向', '18', '0.0', '0', '1019', '微风', '7', '06:18', '17:40', '26', '14', '多云', '晴', 'images/duoyun.png', '10', '星期三', '6300d30d-7c05-4709-b0c9-b870d90e00c7');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('43cc544b-bdf6-4fff-82b8-dec615855c83', '101', '100', '2015-10-16', '43', '无持续风向', '20', '0.0', '0', '1018', '微风', '11', '06:19', '17:37', '28', '11', '多云', '晴', 'images/duoyun.png', '10', '星期五', 'c0f4f0ee-dec1-4b38-81c6-09964fb630c5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('440e1b1d-6b6f-4877-bf27-ba5a4653fbb4', '100', '100', '2015-10-15', '229', '无持续风向', '27', '0.0', '0', '1016', '微风', '4', '06:19', '17:38', '27', '13', '晴', '晴', 'images/qing.png', '10', '星期四', 'b411b524-5a8d-4e25-ad6c-25aa9aa9bb04');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('44ab6247-98b6-4e6d-a33e-f185849a9ce8', '100', '101', '2015-10-20', '150', '无持续风向', '30', '0.0', '0', '1015', '微风', '4', '06:23', '17:32', '26', '15', '晴', '多云', 'images/qing.png', '10', '星期二', '2792d049-e8e0-49a1-a988-4efc974f61be');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('464495ad-c0a7-4027-8533-e9161bdc557b', '100', '101', '2015-10-18', '56', '无持续风向', '28', '0.0', '0', '1020', '微风', '7', '06:21', '17:34', '26', '15', '晴', '多云', 'images/qing.png', '10', '星期日', '4a3b6c50-4402-43b6-b0e8-ab10128c275e');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('46bd930b-6bb5-42bd-9eef-ccb2df4f5fd3', '100', '100', '2015-10-19', '243', '无持续风向', '38', '0.0', '0', '1016', '微风', '4', '06:22', '17:33', '26', '15', '晴', '晴', 'images/qing.png', '10', '星期一', '8801860c-99dc-43b9-bee2-66686e2f9ff2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('47ff6db5-ca3b-4679-b1ab-195c7f8c1027', '100', '100', '2015-10-16', '125', '无持续风向', '24', '0.0', '0', '1018', '微风', '4', '06:19', '17:37', '27', '13', '晴', '晴', 'images/qing.png', '10', '星期五', '978b086a-5382-4552-b95b-c759bc7619dc');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('4863539b-a22d-4b6f-8816-105b2e4de649', '100', '100', '2015-10-14', '204', '无持续风向', '21', '0.0', '0', '1018', '微风', '14', '06:18', '17:40', '26', '13', '晴', '晴', 'images/qing.png', '10', '星期三', 'e4551817-b109-4ad0-9525-ad004d91d100');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('4918d597-bcb1-45a5-ae24-d8f4251f578e', '100', '100', '2015-10-16', '125', '无持续风向', '24', '0.0', '0', '1018', '微风', '4', '06:19', '17:37', '27', '13', '晴', '晴', 'images/qing.png', '10', '星期五', 'ccfc76c5-6669-4372-8007-0705a8a014f3');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('4a001893-e68f-4f66-af55-dc5faa1d7c67', '100', '100', '2015-10-17', '182', '无持续风向', '29', '0.0', '0', '1017', '微风', '9', '06:20', '17:36', '27', '12', '晴', '晴', 'images/qing.png', '10', '星期六', 'd5eab9bd-91e4-40a9-a370-4beeaffeb3ae');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('4a89cc66-3757-40d2-969b-588c3b9cb113', '101', '101', '2015-10-14', '200', '无持续风向', '21', '0.0', '0', '1018', '微风', '11', '06:18', '17:40', '25', '13', '多云', '多云', 'images/duoyun.png', '10', '星期三', '37b62595-c596-47d2-aa85-affcacde22b5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('4c046cd9-a534-4d16-97fc-53055571823a', '101', '100', '2015-10-21', '147', '无持续风向', '28', '0.0', '0', '1017', '微风', '7', '06:24', '17:31', '24', '15', '多云', '晴', 'images/duoyun.png', '10', '星期三', 'f377f019-0348-4d84-a93a-0ed0b2706a2d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('4ccfc36e-2664-4d01-8822-51771239da71', '100', '100', '2015-10-16', '173', '无持续风向', '23', '0.0', '0', '1017', '微风', '4', '06:19', '17:37', '28', '15', '晴', '晴', 'images/qing.png', '10', '星期五', 'ba8c15ba-41f4-40e9-abfb-a81a2dcb0324');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('4ce6456b-066d-42bf-9d32-f7af35b5412e', '100', '100', '2015-10-16', '171', '无持续风向', '25', '0.0', '0', '1018', '微风', '4', '06:19', '17:37', '28', '14', '晴', '晴', 'images/qing.png', '10', '星期五', '0eb20599-8c3a-477b-bd2c-1cf53883f82a');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('4d34fcb4-f887-4e4c-b758-f819884287a9', '100', '101', '2015-10-15', '202', '无持续风向', '25', '0.0', '0', '1016', '微风', '11', '06:19', '17:38', '26', '12', '晴', '多云', 'images/qing.png', '10', '星期四', '040a0a72-be00-4f7a-909d-43b07e68eccb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('4d894bcb-2ae7-44e5-9237-e3c7e3befe61', '100', '100', '2015-10-15', '51', '无持续风向', '22', '0.0', '0', '1016', '微风', '4', '06:19', '17:38', '27', '13', '晴', '晴', 'images/qing.png', '10', '星期四', '71147a0f-6fba-4e15-a6e7-f29099056b55');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('4e02d806-a8a1-4455-a111-10f395b94bc4', '101', '100', '2015-10-15', '343', '无持续风向', '25', '0.0', '0', '1016', '微风', '4', '06:19', '17:38', '26', '12', '多云', '晴', 'images/duoyun.png', '10', '星期四', 'd6be07ca-b1e5-4509-b1d5-623717739ac9');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('4ec61679-506e-4370-927b-e3b4891740ff', '100', '100', '2015-10-16', '125', '无持续风向', '24', '0.0', '0', '1018', '微风', '4', '06:19', '17:37', '27', '13', '晴', '晴', 'images/qing.png', '10', '星期五', '3a73898d-f5b1-4228-9e4a-9205e09c6afb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('4f697b8c-cfe0-45aa-8726-dc6c29459c8f', '101', '100', '2015-10-13', '203', '无持续风向', '17', '0.0', '0', '1021', '微风', '4', '06:17', '17:41', '25', '10', '多云', '晴', 'images/duoyun.png', '10', '星期二', '0d1be87e-2d44-44b6-ba0d-e920994f72b2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('4fb7cd67-9d9a-4224-bd0d-548f599cf0b4', '101', '101', '2015-10-14', '195', '南风', '20', '0.0', '0', '1017', '微风', '4', '06:18', '17:40', '27', '10', '多云', '多云', 'images/duoyun.png', '10', '星期三', '0eb20599-8c3a-477b-bd2c-1cf53883f82a');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('503087b5-4d29-48da-b36d-b5659f873243', '101', '100', '2015-10-14', '197', '无持续风向', '22', '0.0', '0', '1016', '微风', '11', '06:18', '17:40', '27', '11', '多云', '晴', 'images/duoyun.png', '10', '星期三', 'c50b611e-6dc2-47df-9f06-d7c1a64d2931');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('5059a750-4415-4fc1-9287-91e6b32e45b3', '101', '100', '2015-10-14', '196', '无持续风向', '20', '0.0', '0', '1017', '微风', '4', '06:18', '17:40', '26', '12', '多云', '晴', 'images/duoyun.png', '10', '星期三', 'b411b524-5a8d-4e25-ad6c-25aa9aa9bb04');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('50e4d784-51dc-4d83-acd8-1c9d73c0f492', '101', '101', '2015-09-23', '51', '南风', '41', '0.0', '0', '1009', '微风', '4', '06:01', '18:09', '29', '19', '多云', '多云', 'images/duoyun.png', '10', '星期三', '30382c9c-8088-447a-b604-4267fe87461d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('51051fea-059a-47bd-900a-bbce5f64b778', '100', '101', '2015-10-13', '199', '南风', '16', '0.0', '0', '1021', '微风', '7', '06:17', '17:41', '26', '11', '晴', '多云', 'images/qing.png', '10', '星期二', '9004e631-74f7-4ba0-aa4c-3ab0d43eb8db');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('5129f12f-9b8b-4a95-9bc5-42eb914bd03d', '100', '100', '2015-10-15', '51', '无持续风向', '22', '0.0', '0', '1016', '微风', '4', '06:19', '17:38', '27', '13', '晴', '晴', 'images/qing.png', '10', '星期四', '0d1be87e-2d44-44b6-ba0d-e920994f72b2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('53a90b9f-8c14-4c53-ac8a-f92246c631d5', '100', '100', '2015-10-17', '185', '无持续风向', '29', '0.0', '0', '1018', '微风', '15', '06:20', '17:36', '26', '14', '晴', '晴', 'images/qing.png', '10', '星期六', 'd6bdba54-752b-4f62-beb1-f02622b11619');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('53d5e302-f62c-4932-b80e-d2f4110d3142', '100', '100', '2015-10-16', '148', '无持续风向', '22', '0.0', '0', '1018', '微风', '4', '06:19', '17:37', '27', '12', '晴', '晴', 'images/qing.png', '10', '星期五', '8dd107b6-6144-4450-b42f-2dbd104b433b');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('54121545-6171-4aba-b3f5-f145948bc8e0', '100', '100', '2015-10-17', '191', '无持续风向', '28', '0.0', '0', '1018', '微风', '11', '06:20', '17:36', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期六', 'c0f4f0ee-dec1-4b38-81c6-09964fb630c5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('545869e6-74c4-4348-91a9-6ce5f5bd9b34', '100', '100', '2015-10-16', '175', '无持续风向', '24', '0.0', '0', '1018', '微风', '7', '06:19', '17:37', '28', '15', '晴', '晴', 'images/qing.png', '10', '星期五', '6300d30d-7c05-4709-b0c9-b870d90e00c7');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('580eb8f8-3960-4bca-b85c-650b54a1a55a', '100', '100', '2015-10-16', '175', '无持续风向', '22', '0.0', '0', '1018', '微风', '9', '06:19', '17:37', '27', '12', '晴', '晴', 'images/qing.png', '10', '星期五', 'd5eab9bd-91e4-40a9-a370-4beeaffeb3ae');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('58d54107-6506-4942-afa8-0661eeb70c9b', '101', '101', '2015-10-14', '193', '南风', '20', '0.0', '0', '1017', '微风', '7', '06:18', '17:40', '25', '11', '多云', '多云', 'images/duoyun.png', '10', '星期三', 'dfd10274-dc82-4f34-bbd7-8fd758890ae5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('591d434d-00bf-475f-8b14-ee9ef0ba19b2', '100', '100', '2015-10-16', '184', '无持续风向', '22', '0.0', '0', '1017', '微风', '4', '06:19', '17:37', '28', '15', '晴', '晴', 'images/qing.png', '10', '星期五', 'b411b524-5a8d-4e25-ad6c-25aa9aa9bb04');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('5a6cb104-5a5f-47b7-ac3b-bcb32fe24592', '101', '300', '2015-09-22', '164', '南风', '44', '1.5', '70', '1011', '微风', '4', '06:00', '18:11', '28', '18', '多云', '阵雨', 'images/duoyun.png', '10', '星期二', 'e1c44614-22bb-40d8-9db2-ce03556fc806');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('5b32ad72-dd2e-4f8d-93b5-ae1249581cde', '100', '100', '2015-10-16', '173', '无持续风向', '23', '0.0', '0', '1017', '微风', '4', '06:19', '17:37', '28', '15', '晴', '晴', 'images/qing.png', '10', '星期五', '0d1be87e-2d44-44b6-ba0d-e920994f72b2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('5b52ca9d-213c-41c8-b322-f0af0df6f68b', '101', '101', '2015-10-18', '84', '无持续风向', '28', '0.0', '0', '1020', '微风', '4', '06:21', '17:34', '25', '14', '多云', '多云', 'images/duoyun.png', '10', '星期日', '8dd107b6-6144-4450-b42f-2dbd104b433b');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('5b7c18dc-eee3-4abc-8690-28f993427911', '101', '100', '2015-10-17', '178', '无持续风向', '26', '0.0', '0', '1016', '微风', '4', '06:20', '17:36', '24', '13', '多云', '晴', 'images/duoyun.png', '10', '星期六', 'a7a5d3f9-bff1-4f11-b54c-e3c05ddd3b9e');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('5bfdfa84-f484-41d5-93d6-c8e958cc2771', '101', '101', '2015-10-18', '58', '无持续风向', '28', '0.0', '0', '1020', '微风', '11', '06:21', '17:34', '26', '15', '多云', '多云', 'images/duoyun.png', '10', '星期日', '4ccc3e88-7bd3-487b-9988-7a74fa271fb0');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('5c33a8e5-6ed3-4ca6-bf71-d3862dc474f9', '100', '101', '2015-10-20', '164', '无持续风向', '25', '0.0', '0', '1014', '微风', '9', '06:23', '17:32', '26', '15', '晴', '多云', 'images/qing.png', '10', '星期二', 'd5eab9bd-91e4-40a9-a370-4beeaffeb3ae');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('5c664a04-74c8-407c-831e-d7a0a9ecf47f', '100', '101', '2015-10-18', '123', '无持续风向', '31', '0.0', '0', '1018', '微风', '4', '06:21', '17:34', '23', '15', '晴', '多云', 'images/qing.png', '10', '星期日', '97d962ec-aeec-45d4-b9ca-191d3b12d0f1');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('5d0fe3ca-e4ec-4076-af46-a7664d095269', '100', '100', '2015-10-19', '243', '无持续风向', '38', '0.0', '0', '1016', '微风', '4', '06:22', '17:33', '26', '15', '晴', '晴', 'images/qing.png', '10', '星期一', '0eb20599-8c3a-477b-bd2c-1cf53883f82a');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('5e92416c-89c4-4b1d-8813-4913961fdea6', '100', '100', '2015-10-10', '324', '北风', '18', '0.0', '0', '1018', '3-4', '7', '06:14', '17:45', '19', '7', '晴', '晴', 'images/qing.png', '10', '星期六', 'e2caa130-4580-436c-b647-aa5a2667fbde');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('5fb5f99c-28b5-4c21-b592-346c5f491c39', '101', '101', '2015-10-19', '192', '无持续风向', '35', '0.0', '0', '1016', '微风', '7', '06:22', '17:33', '26', '15', '多云', '多云', 'images/duoyun.png', '10', '星期一', '9004e631-74f7-4ba0-aa4c-3ab0d43eb8db');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('5fc1980d-0be3-4d7d-a91a-65878ba07663', '100', '100', '2015-10-17', '184', '无持续风向', '28', '0.0', '0', '1017', '微风', '4', '06:20', '17:36', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期六', '0eb20599-8c3a-477b-bd2c-1cf53883f82a');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('629ce100-c4d8-46a5-a753-b38fb6a91625', '305', '104', '2015-09-24', '115', '无持续风向', '42', '0.0', '39', '1009', '微风', '4', '06:02', '18:08', '25', '20', '小雨', '阴', 'images/xiaoyu.png', '10', '星期四', '30382c9c-8088-447a-b604-4267fe87461d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('6464990a-85de-4e8c-a4a8-da74f591ca54', '100', '101', '2015-09-26', '89', '无持续风向', '32', '0.0', '0', '1012', '微风', '4', '06:03', '18:05', '29', '18', '晴', '多云', 'images/qing.png', '10', '星期六', '8d23507d-9c63-4bac-bbfc-59914cc07a81');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('64ce085b-3b31-4647-8118-a561908dbbc7', '100', '100', '2015-10-15', '1', '北风', '28', '0.0', '0', '1016', '微风', '7', '06:19', '17:38', '28', '10', '晴', '晴', 'images/qing.png', '10', '星期四', '4a3b6c50-4402-43b6-b0e8-ab10128c275e');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('65b26ff1-0884-43c1-8e52-77210b469256', '101', '101', '2015-10-21', '143', '无持续风向', '32', '0.0', '0', '1016', '微风', '4', '06:24', '17:31', '24', '15', '多云', '多云', 'images/duoyun.png', '10', '星期三', '20a31c2b-fc46-4ac0-bee1-2207e3b226f9');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('66541ad3-0292-4a60-983a-4d10cbeec7ad', '100', '100', '2015-10-19', '151', '无持续风向', '36', '0.0', '0', '1017', '微风', '4', '06:22', '17:33', '26', '15', '晴', '晴', 'images/qing.png', '10', '星期一', '8dd107b6-6144-4450-b42f-2dbd104b433b');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('666a557e-39ec-4e90-aeca-4d705700a3d3', '101', '101', '2015-09-23', '51', '南风', '41', '0.0', '0', '1009', '微风', '4', '06:01', '18:09', '29', '19', '多云', '多云', 'images/duoyun.png', '10', '星期三', 'dc0dbb0c-0f76-4544-bd61-1a65c3ee80e5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('66dc0029-bf8b-4915-b773-bff1317b8494', '100', '100', '2015-10-18', '16', '无持续风向', '31', '0.0', '0', '1019', '微风', '4', '06:21', '17:34', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期日', '71147a0f-6fba-4e15-a6e7-f29099056b55');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('6768f0c1-f4cf-40b7-b85a-2b52182f3d21', '100', '100', '2015-10-11', '314', '北风', '19', '0.0', '0', '1019', '3-4', '11', '06:15', '17:44', '21', '6', '晴', '晴', 'images/qing.png', '10', '星期日', '69f9f736-24bf-499a-bafb-418e3a87a0ec');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('677526ad-c073-4d1c-9b30-8b3cd52e3691', '100', '101', '2015-10-15', '202', '无持续风向', '25', '0.0', '0', '1016', '微风', '11', '06:19', '17:38', '26', '12', '晴', '多云', 'images/qing.png', '10', '星期四', '69f9f736-24bf-499a-bafb-418e3a87a0ec');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('681521d1-79e0-4bfc-91c3-64e0c9ecc963', '100', '100', '2015-10-11', '322', '北风', '22', '0.0', '0', '1019', '微风', '7', '06:15', '17:44', '22', '6', '晴', '晴', 'images/qing.png', '10', '星期日', '1ebbf1f6-ff2f-454a-ab1d-c8735cf1cfdf');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('689cec8f-11fd-458a-8d9e-35da44f6638a', '101', '101', '2015-09-21', '165', '南风', '36', '0.0', '0', '1011', '微风', '4', '05:59', '18:12', '29', '17', '多云', '多云', 'images/duoyun.png', '10', '星期一', '3dbc7e93-e5ca-4606-a44d-42f43d499e2a');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('68e91586-96a3-42bb-82b9-89e6f0a6285a', '101', '101', '2015-10-18', '92', '无持续风向', '27', '0.0', '0', '1019', '微风', '4', '06:21', '17:34', '25', '14', '多云', '多云', 'images/duoyun.png', '10', '星期日', 'ccfc76c5-6669-4372-8007-0705a8a014f3');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('6aae8077-bde8-4626-9267-4c832e1f3bf8', '101', '101', '2015-10-18', '84', '无持续风向', '28', '0.0', '0', '1020', '微风', '4', '06:21', '17:34', '25', '14', '多云', '多云', 'images/duoyun.png', '10', '星期日', '20a31c2b-fc46-4ac0-bee1-2207e3b226f9');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('6abc1cd7-c6f3-44b6-8c2e-4d6a45b0c246', '101', '100', '2015-10-12', '331', '无持续风向', '18', '0.0', '0', '1022', '微风', '4', '06:16', '17:42', '24', '7', '多云', '晴', 'images/duoyun.png', '10', '星期一', '0d1be87e-2d44-44b6-ba0d-e920994f72b2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('6bf744bf-fc5f-48d5-84c6-a65c0e187d2a', '101', '101', '2015-10-14', '200', '无持续风向', '21', '0.0', '0', '1018', '微风', '11', '06:18', '17:40', '25', '13', '多云', '多云', 'images/duoyun.png', '10', '星期三', '43201297-f8ee-4025-add1-d9eab2862d49');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('6cdf4c1d-6d0c-4ec6-8430-e72786fab836', '100', '100', '2015-10-15', '68', '北风', '28', '0.0', '0', '1016', '微风', '4', '06:19', '17:38', '26', '8', '晴', '晴', 'images/qing.png', '10', '星期四', 'cc663d03-3cfd-439b-9203-bf0b99b43a85');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('6da842be-8366-4dcd-84f4-7cde75dae5fc', '100', '101', '2015-09-26', '89', '无持续风向', '32', '0.0', '0', '1012', '微风', '4', '06:03', '18:05', '29', '18', '晴', '多云', 'images/qing.png', '10', '星期六', 'e1c44614-22bb-40d8-9db2-ce03556fc806');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('72a1c158-c32f-42e8-9f57-c8d3eb749453', '100', '101', '2015-10-14', '191', '南风', '20', '0.0', '0', '1017', '微风', '4', '06:18', '17:40', '28', '12', '晴', '多云', 'images/qing.png', '10', '星期三', 'd6be07ca-b1e5-4509-b1d5-623717739ac9');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('73287a70-f7f8-45e4-9626-79258ec7794b', '100', '100', '2015-10-11', '322', '北风', '22', '0.0', '0', '1019', '微风', '7', '06:15', '17:44', '22', '6', '晴', '晴', 'images/qing.png', '10', '星期日', '6300d30d-7c05-4709-b0c9-b870d90e00c7');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('73ae697d-5b7e-478e-960c-7d25d68c4d85', '101', '101', '2015-10-14', '213', '无持续风向', '21', '0.0', '0', '1019', '微风', '7', '06:18', '17:40', '25', '13', '多云', '多云', 'images/duoyun.png', '10', '星期三', '4d4e5d4a-097c-4b93-bf9c-7792cc9f75a2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('74547820-34d1-4cd0-86a5-b185f71bf2c6', '101', '101', '2015-10-18', '92', '无持续风向', '27', '0.0', '0', '1019', '微风', '4', '06:21', '17:34', '25', '14', '多云', '多云', 'images/duoyun.png', '10', '星期日', '3a73898d-f5b1-4228-9e4a-9205e09c6afb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('754ae76c-7952-4105-a60a-ed99e09d8c07', '101', '101', '2015-10-18', '72', '无持续风向', '27', '0.0', '0', '1019', '微风', '9', '06:21', '17:34', '25', '14', '多云', '多云', 'images/duoyun.png', '10', '星期日', 'd5eab9bd-91e4-40a9-a370-4beeaffeb3ae');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('75503880-7a64-4c7b-89d1-cac91768d8b0', '100', '100', '2015-10-19', '147', '无持续风向', '25', '0.0', '0', '1016', '微风', '4', '06:22', '17:33', '26', '15', '晴', '晴', 'images/qing.png', '10', '星期一', 'ccfc76c5-6669-4372-8007-0705a8a014f3');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('75dc6505-7f80-4460-9b77-6a1f8789de84', '100', '100', '2015-10-16', '192', '无持续风向', '24', '0.0', '0', '1017', '微风', '11', '06:19', '17:37', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期五', '040a0a72-be00-4f7a-909d-43b07e68eccb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('774cfa8c-1800-4d83-b28b-751c1b2152d7', '101', '100', '2015-10-13', '203', '无持续风向', '17', '0.0', '0', '1021', '微风', '4', '06:17', '17:41', '25', '10', '多云', '晴', 'images/duoyun.png', '10', '星期二', '8977b5b4-7db1-4131-a180-29f07fba47ab');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('77e27ada-fa50-44e4-bee9-3c0524f38a06', '101', '100', '2015-10-15', '29', '北风', '27', '0.0', '0', '1016', '微风', '9', '06:19', '17:38', '26', '10', '多云', '晴', 'images/duoyun.png', '10', '星期四', '7a856cb3-5b40-4a67-a5d0-a1230768bcfd');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('79ade095-51d7-4a53-ad00-48f33e27b20f', '101', '101', '2015-10-14', '196', '东南风', '21', '0.0', '0', '1018', '微风', '4', '06:18', '17:40', '25', '12', '多云', '多云', 'images/duoyun.png', '10', '星期三', '97d962ec-aeec-45d4-b9ca-191d3b12d0f1');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('7a537a87-9c19-4cfd-a784-895d663f2e84', '100', '101', '2015-09-26', '89', '无持续风向', '32', '0.0', '0', '1012', '微风', '4', '06:03', '18:05', '29', '18', '晴', '多云', 'images/qing.png', '10', '星期六', 'beeca77c-46dd-42d8-915a-a5a33863bb61');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('7bdb0914-b41b-4162-aaea-fdb155c4cccd', '100', '101', '2015-10-20', '164', '无持续风向', '25', '0.0', '0', '1014', '微风', '9', '06:23', '17:32', '26', '15', '晴', '多云', 'images/qing.png', '10', '星期二', '7a856cb3-5b40-4a67-a5d0-a1230768bcfd');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('7bf51f14-c3ff-424d-a3d2-aade6c0744a2', '101', '100', '2015-09-25', '55', '无持续风向', '38', '0.0', '19', '1009', '微风', '4', '06:02', '18:06', '28', '18', '多云', '晴', 'images/duoyun.png', '10', '星期五', '30382c9c-8088-447a-b604-4267fe87461d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('7c08fe12-bbe9-4951-aca6-89480742431a', '100', '101', '2015-10-18', '69', '无持续风向', '27', '0.0', '0', '1019', '微风', '7', '06:21', '17:34', '23', '15', '晴', '多云', 'images/qing.png', '10', '星期日', 'e3f1ac1d-f5b5-4e71-bbaf-5b935378b6cb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('7c527ec5-eace-44d3-84db-6a46faedc9cf', '101', '101', '2015-10-17', '186', '南风', '29', '0.0', '0', '1018', '微风', '7', '06:20', '17:36', '27', '12', '多云', '多云', 'images/duoyun.png', '10', '星期六', '4a3b6c50-4402-43b6-b0e8-ab10128c275e');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('7d16c2d7-a667-4149-810e-1faa0093838d', '101', '101', '2015-10-17', '188', '无持续风向', '26', '0.0', '0', '1016', '微风', '4', '06:20', '17:36', '28', '14', '多云', '多云', 'images/duoyun.png', '10', '星期六', 'b411b524-5a8d-4e25-ad6c-25aa9aa9bb04');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('7d2136b3-1ea1-458a-b876-199f14aa3b1a', '100', '100', '2015-10-13', '216', '南风', '18', '0.0', '0', '1022', '微风', '7', '06:17', '17:41', '25', '8', '晴', '晴', 'images/qing.png', '10', '星期二', '6300d30d-7c05-4709-b0c9-b870d90e00c7');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('7dda7dc8-75e6-49e5-9b34-fac9a76fe751', '100', '100', '2015-10-15', '208', '无持续风向', '23', '0.0', '0', '1017', '微风', '7', '06:19', '17:38', '27', '13', '晴', '晴', 'images/qing.png', '10', '星期四', '6300d30d-7c05-4709-b0c9-b870d90e00c7');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('7e668ccb-5413-438d-a0ca-04b2839bfcf3', '100', '101', '2015-10-13', '199', '南风', '16', '0.0', '0', '1021', '微风', '8', '06:17', '17:41', '26', '11', '晴', '多云', 'images/qing.png', '10', '星期二', '26c065b2-ed18-429c-877e-7ab41586af9d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('7fe73be9-deac-459d-a78b-39d81c2ab59a', '100', '100', '2015-10-12', '330', '南风', '19', '0.0', '0', '1023', '3-4', '7', '06:16', '17:42', '24', '10', '晴', '晴', 'images/qing.png', '10', '星期一', '4d4e5d4a-097c-4b93-bf9c-7792cc9f75a2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('80532dc9-80a9-4742-8a19-c3e7f6d36f2e', '100', '100', '2015-10-15', '51', '无持续风向', '22', '0.0', '0', '1016', '微风', '4', '06:19', '17:38', '27', '13', '晴', '晴', 'images/qing.png', '10', '星期四', '8977b5b4-7db1-4131-a180-29f07fba47ab');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('80be591f-50aa-4197-a979-02cf06ae6b20', '100', '100', '2015-10-17', '182', '无持续风向', '29', '0.0', '0', '1017', '微风', '4', '06:20', '17:36', '27', '12', '晴', '晴', 'images/qing.png', '10', '星期六', '8dd107b6-6144-4450-b42f-2dbd104b433b');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('8257987d-01e8-41fb-bd38-8a9f5769fa70', '101', '101', '2015-10-19', '192', '无持续风向', '35', '0.0', '0', '1016', '微风', '7', '06:22', '17:33', '26', '15', '多云', '多云', 'images/duoyun.png', '10', '星期一', 'dfd10274-dc82-4f34-bbd7-8fd758890ae5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('8266c194-1cfa-48df-9702-f944d5c8de6d', '100', '101', '2015-09-26', '89', '无持续风向', '32', '0.0', '0', '1012', '微风', '4', '06:03', '18:05', '29', '18', '晴', '多云', 'images/qing.png', '10', '星期六', 'c3278f94-6e0f-4ead-a27e-374c09465276');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('82b4ad24-00d1-4d8c-a4c2-94d609c7efcf', '101', '101', '2015-10-18', '42', '无持续风向', '30', '0.0', '0', '1019', '微风', '4', '06:21', '17:34', '26', '15', '多云', '多云', 'images/duoyun.png', '10', '星期日', '0eb20599-8c3a-477b-bd2c-1cf53883f82a');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('859f73db-9827-4942-9c52-27f5710349eb', '100', '100', '2015-10-19', '282', '无持续风向', '31', '0.0', '0', '1017', '微风', '7', '06:22', '17:33', '25', '14', '晴', '晴', 'images/qing.png', '10', '星期一', 'f377f019-0348-4d84-a93a-0ed0b2706a2d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('85afee14-c677-42e5-b345-cc1cf24c3fa5', '100', '100', '2015-10-17', '181', '无持续风向', '23', '0.0', '0', '1016', '微风', '11', '06:20', '17:36', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期六', '37b62595-c596-47d2-aa85-affcacde22b5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('85c48899-c937-42e6-bbdd-4e31706d523a', '101', '101', '2015-10-21', '122', '无持续风向', '27', '0.0', '0', '1016', '微风', '9', '06:24', '17:31', '24', '15', '多云', '多云', 'images/duoyun.png', '10', '星期三', '7a856cb3-5b40-4a67-a5d0-a1230768bcfd');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('87c69904-1ba9-49b8-a671-e8af2a1038be', '100', '100', '2015-10-16', '192', '无持续风向', '24', '0.0', '0', '1017', '微风', '11', '06:19', '17:37', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期五', '37b62595-c596-47d2-aa85-affcacde22b5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('88bf4147-1ce5-4eff-a510-3a4a7f6d1ff3', '100', '100', '2015-10-12', '3', '西南风', '20', '0.0', '0', '1023', '3-4', '11', '06:16', '17:42', '24', '8', '晴', '晴', 'images/qing.png', '10', '星期一', '040a0a72-be00-4f7a-909d-43b07e68eccb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('88eb9ecc-126b-4d72-a566-b614b51c62a0', '100', '100', '2015-10-16', '175', '无持续风向', '24', '0.0', '0', '1018', '微风', '7', '06:19', '17:37', '28', '15', '晴', '晴', 'images/qing.png', '10', '星期五', '1ebbf1f6-ff2f-454a-ab1d-c8735cf1cfdf');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('892ce3ea-e6e3-40c1-8941-5cbe73342ce9', '101', '101', '2015-10-21', '143', '无持续风向', '32', '0.0', '0', '1016', '微风', '4', '06:24', '17:31', '24', '15', '多云', '多云', 'images/duoyun.png', '10', '星期三', '37b7ea55-bc41-4c39-973e-0f0b137239f6');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('8930fc54-3052-46ac-8344-998134cb76fd', '101', '101', '2015-10-21', '122', '无持续风向', '27', '0.0', '0', '1016', '微风', '9', '06:24', '17:31', '24', '15', '多云', '多云', 'images/duoyun.png', '10', '星期三', 'd5eab9bd-91e4-40a9-a370-4beeaffeb3ae');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('898e5a7c-83b9-4b59-9e5e-532c7bf44040', '100', '100', '2015-10-13', '216', '南风', '18', '0.0', '0', '1022', '微风', '7', '06:17', '17:41', '25', '8', '晴', '晴', 'images/qing.png', '10', '星期二', 'bdcdbd4b-097a-4112-8a70-33bf9be5b1c0');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('8996d6bd-b9a7-4d78-aac9-8527bbc8825f', '101', '100', '2015-10-17', '182', '无持续风向', '29', '0.0', '0', '1017', '微风', '7', '06:20', '17:36', '24', '13', '多云', '晴', 'images/duoyun.png', '10', '星期六', 'e3f1ac1d-f5b5-4e71-bbaf-5b935378b6cb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('8a35dd9a-1a5d-4b70-8b1b-af00142170e5', '100', '101', '2015-10-15', '202', '无持续风向', '25', '0.0', '0', '1016', '微风', '11', '06:19', '17:38', '26', '12', '晴', '多云', 'images/qing.png', '10', '星期四', '37b62595-c596-47d2-aa85-affcacde22b5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('8a9e5858-be50-4bdc-b4dd-ebb4ff45ca55', '305', '104', '2015-09-24', '115', '无持续风向', '42', '0.0', '39', '1009', '微风', '4', '06:02', '18:08', '25', '20', '小雨', '阴', 'images/xiaoyu.png', '10', '星期四', 'dc0dbb0c-0f76-4544-bd61-1a65c3ee80e5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('8ab8d8ae-e4c7-409f-b6db-f26d5b3ad342', '100', '100', '2015-09-27', '131', '无持续风向', '18', '0.0', '1', '1014', '微风', '4', '06:04', '18:03', '28', '18', '晴', '晴', 'images/qing.png', '10', '星期日', 'e1c44614-22bb-40d8-9db2-ce03556fc806');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('8ac0d670-05ce-4acd-9c46-27dfd8f48d6a', '101', '300', '2015-09-22', '164', '南风', '44', '1.5', '70', '1011', '微风', '4', '06:00', '18:11', '28', '18', '多云', '阵雨', 'images/duoyun.png', '10', '星期二', 'beeca77c-46dd-42d8-915a-a5a33863bb61');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('8b297849-de5a-416e-92b8-72639a005267', '101', '100', '2015-09-25', '55', '无持续风向', '38', '0.0', '19', '1009', '微风', '4', '06:02', '18:06', '28', '18', '多云', '晴', 'images/duoyun.png', '10', '星期五', '05a3b4ac-1fd3-47e3-a9fa-9e4f66cbaf15');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('8b47c2e7-7347-4622-8a60-c5ba2ebd58df', '100', '100', '2015-10-16', '150', '南风', '22', '0.0', '0', '1018', '微风', '7', '06:19', '17:37', '27', '10', '晴', '晴', 'images/qing.png', '10', '星期五', '4a3b6c50-4402-43b6-b0e8-ab10128c275e');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('8bd13786-dd22-4a68-8aca-f6bbc6fabe88', '100', '101', '2015-09-26', '89', '无持续风向', '32', '0.0', '0', '1012', '微风', '4', '06:03', '18:05', '29', '18', '晴', '多云', 'images/qing.png', '10', '星期六', '3dbc7e93-e5ca-4606-a44d-42f43d499e2a');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('8c254ea3-172a-436f-85f9-0dc1d4db26e0', '100', '101', '2015-10-15', '195', '无持续风向', '23', '0.0', '0', '1018', '微风', '7', '06:19', '17:38', '26', '12', '晴', '多云', 'images/qing.png', '10', '星期四', 'e2caa130-4580-436c-b647-aa5a2667fbde');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('8c6bca02-6a55-436a-a4b3-40557c8032de', '101', '100', '2015-10-14', '197', '无持续风向', '20', '0.0', '0', '1017', '微风', '4', '06:18', '17:40', '26', '12', '多云', '晴', 'images/duoyun.png', '10', '星期三', '8977b5b4-7db1-4131-a180-29f07fba47ab');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('8d175302-a15d-4a08-b9aa-01728469929d', '101', '101', '2015-10-18', '58', '无持续风向', '28', '0.0', '0', '1020', '微风', '11', '06:21', '17:34', '26', '15', '多云', '多云', 'images/duoyun.png', '10', '星期日', 'c0f4f0ee-dec1-4b38-81c6-09964fb630c5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('8e18481f-b1a6-4292-b222-05f643cfee90', '100', '100', '2015-09-27', '131', '无持续风向', '18', '0.0', '1', '1014', '微风', '4', '06:04', '18:03', '28', '18', '晴', '晴', 'images/qing.png', '10', '星期日', '05a3b4ac-1fd3-47e3-a9fa-9e4f66cbaf15');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('8ea51790-8649-4271-90fb-07cbf657461a', '100', '100', '2015-10-19', '159', '无持续风向', '19', '0.0', '0', '1015', '微风', '15', '06:22', '17:33', '27', '15', '晴', '晴', 'images/qing.png', '10', '星期一', 'd6bdba54-752b-4f62-beb1-f02622b11619');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('8f4be659-973c-4607-aed3-9c9438e3a870', '100', '101', '2015-10-18', '56', '无持续风向', '28', '0.0', '0', '1020', '微风', '7', '06:21', '17:34', '26', '15', '晴', '多云', 'images/qing.png', '10', '星期日', 'f377f019-0348-4d84-a93a-0ed0b2706a2d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('905506a6-33d5-4c66-a0ff-a20d045ed6e4', '100', '101', '2015-10-20', '150', '无持续风向', '30', '0.0', '0', '1015', '微风', '4', '06:23', '17:32', '26', '15', '晴', '多云', 'images/qing.png', '10', '星期二', '6b291c64-06d5-4965-b535-488f8d011865');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('90a13d1e-9e7c-4835-8afe-bcfa0b8aabd4', '101', '101', '2015-09-23', '51', '南风', '41', '0.0', '0', '1009', '微风', '4', '06:01', '18:09', '29', '19', '多云', '多云', 'images/duoyun.png', '10', '星期三', 'c3278f94-6e0f-4ead-a27e-374c09465276');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('90ca3406-bc6a-4d59-9771-d01ca8831c3d', '100', '100', '2015-10-16', '125', '无持续风向', '24', '0.0', '0', '1018', '微风', '4', '06:19', '17:37', '27', '13', '晴', '晴', 'images/qing.png', '10', '星期五', '6b291c64-06d5-4965-b535-488f8d011865');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('919c6382-e3bd-406c-8ebd-b1d22be6babb', '100', '100', '2015-10-16', '173', '无持续风向', '23', '0.0', '0', '1017', '微风', '4', '06:19', '17:37', '28', '15', '晴', '晴', 'images/qing.png', '10', '星期五', '71147a0f-6fba-4e15-a6e7-f29099056b55');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('91c0a3cb-14c2-42ad-a1f8-895663a45d14', '100', '100', '2015-10-16', '192', '无持续风向', '25', '0.0', '0', '1017', '微风', '4', '06:19', '17:37', '27', '15', '晴', '晴', 'images/qing.png', '10', '星期五', '0378b4ff-3867-40ba-8364-5f65c0543063');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('926f6ba8-8877-47eb-a9ad-0becb3da3da8', '101', '101', '2015-09-23', '51', '南风', '41', '0.0', '0', '1009', '微风', '4', '06:01', '18:09', '29', '19', '多云', '多云', 'images/duoyun.png', '10', '星期三', '8d23507d-9c63-4bac-bbfc-59914cc07a81');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('9322bab2-daaf-4115-a98b-eba427806564', '101', '101', '2015-09-21', '165', '南风', '36', '0.0', '0', '1011', '微风', '4', '05:59', '18:12', '29', '17', '多云', '多云', 'images/duoyun.png', '10', '星期一', 'c3278f94-6e0f-4ead-a27e-374c09465276');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('9373d7ac-cd50-4553-8708-82165a7ba10f', '100', '101', '2015-10-14', '191', '南风', '20', '0.0', '0', '1017', '微风', '4', '06:18', '17:40', '28', '12', '晴', '多云', 'images/qing.png', '10', '星期三', '6b291c64-06d5-4965-b535-488f8d011865');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('9381cff3-f64d-40bc-ac4e-e967e77b6722', '101', '100', '2015-10-12', '346', '无持续风向', '19', '0.0', '0', '1022', '微风', '4', '06:16', '17:42', '24', '7', '多云', '晴', 'images/duoyun.png', '10', '星期一', 'b411b524-5a8d-4e25-ad6c-25aa9aa9bb04');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('964d155d-5abf-4cc3-9b15-aa03e0299378', '100', '100', '2015-10-11', '335', '北风', '25', '0.0', '0', '1020', '3-4', '7', '06:15', '17:44', '21', '8', '晴', '晴', 'images/qing.png', '10', '星期日', 'e2caa130-4580-436c-b647-aa5a2667fbde');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('978a0361-c480-4cec-8434-5f53296574e9', '100', '100', '2015-10-19', '147', '无持续风向', '25', '0.0', '0', '1016', '微风', '4', '06:22', '17:33', '26', '15', '晴', '晴', 'images/qing.png', '10', '星期一', 'd6be07ca-b1e5-4509-b1d5-623717739ac9');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('9828bca9-20ed-41f5-8b1c-252c8c217228', '101', '101', '2015-09-23', '51', '南风', '41', '0.0', '0', '1009', '微风', '4', '06:01', '18:09', '29', '19', '多云', '多云', 'images/duoyun.png', '10', '星期三', '05a3b4ac-1fd3-47e3-a9fa-9e4f66cbaf15');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('987945d3-7979-4986-bd63-1b46f1c1f292', '100', '101', '2015-10-20', '150', '无持续风向', '30', '0.0', '0', '1015', '微风', '4', '06:23', '17:32', '26', '15', '晴', '多云', 'images/qing.png', '10', '星期二', 'd6be07ca-b1e5-4509-b1d5-623717739ac9');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('98aad3b6-5b1c-40b9-ba97-8f11e7b8b24d', '100', '100', '2015-10-16', '183', '无持续风向', '26', '0.0', '0', '1018', '微风', '14', '06:19', '17:37', '26', '12', '晴', '晴', 'images/qing.png', '10', '星期五', 'e4551817-b109-4ad0-9525-ad004d91d100');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('9976478e-04cd-452e-a44e-bf976dcab186', '101', '100', '2015-09-25', '55', '无持续风向', '38', '0.0', '19', '1009', '微风', '4', '06:02', '18:06', '28', '18', '多云', '晴', 'images/duoyun.png', '10', '星期五', 'dc0dbb0c-0f76-4544-bd61-1a65c3ee80e5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('9a2b1755-f3f2-42e7-8df1-65315987363c', '101', '101', '2015-10-19', '206', '无持续风向', '36', '0.0', '1', '1015', '微风', '8', '06:22', '17:33', '26', '15', '多云', '多云', 'images/duoyun.png', '10', '星期一', '26c065b2-ed18-429c-877e-7ab41586af9d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('9cc6e213-3407-41ab-b89b-22a6fc7a9aca', '101', '101', '2015-10-14', '195', '南风', '20', '0.0', '0', '1017', '微风', '4', '06:18', '17:40', '27', '10', '多云', '多云', 'images/duoyun.png', '10', '星期三', 'cc663d03-3cfd-439b-9203-bf0b99b43a85');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('9d1fd75f-2d13-4f85-bbd5-63cdacc4222e', '100', '100', '2015-10-15', '177', '东南风', '26', '0.0', '0', '1016', '微风', '7', '06:19', '17:38', '26', '11', '晴', '晴', 'images/qing.png', '10', '星期四', 'dfd10274-dc82-4f34-bbd7-8fd758890ae5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('9d687fe5-09a0-4e68-be51-fd6582718937', '101', '100', '2015-10-16', '43', '无持续风向', '20', '0.0', '0', '1018', '微风', '11', '06:19', '17:37', '28', '11', '多云', '晴', 'images/duoyun.png', '10', '星期五', 'b5992b35-8896-4d2e-96b0-deee3f288109');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('9f82dc0d-62a1-458d-8020-4e51758cd743', '100', '100', '2015-10-19', '151', '无持续风向', '36', '0.0', '0', '1017', '微风', '4', '06:22', '17:33', '26', '15', '晴', '晴', 'images/qing.png', '10', '星期一', '20a31c2b-fc46-4ac0-bee1-2207e3b226f9');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('9fc35207-8a52-4226-9cfe-c66844e51af4', '101', '101', '2015-10-17', '180', '无持续风向', '31', '0.0', '0', '1017', '微风', '4', '06:20', '17:36', '27', '15', '多云', '多云', 'images/duoyun.png', '10', '星期六', '978b086a-5382-4552-b95b-c759bc7619dc');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a072d50b-c948-4e01-871b-01b9fcd8a980', '101', '100', '2015-10-12', '331', '无持续风向', '18', '0.0', '0', '1022', '微风', '4', '06:16', '17:42', '24', '7', '多云', '晴', 'images/duoyun.png', '10', '星期一', '71147a0f-6fba-4e15-a6e7-f29099056b55');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a07d05ec-0981-4317-a82c-5b24dd75249b', '101', '100', '2015-10-13', '203', '无持续风向', '17', '0.0', '0', '1021', '微风', '4', '06:17', '17:41', '25', '10', '多云', '晴', 'images/duoyun.png', '10', '星期二', 'ba8c15ba-41f4-40e9-abfb-a81a2dcb0324');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a08cf855-b247-4f75-b75e-4bd8245f8184', '100', '100', '2015-10-16', '162', '无持续风向', '28', '0.0', '0', '1017', '微风', '8', '06:19', '17:37', '27', '15', '晴', '晴', 'images/qing.png', '10', '星期五', '26c065b2-ed18-429c-877e-7ab41586af9d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a10cadb9-8548-478b-bd35-1695e461b3e6', '100', '100', '2015-10-17', '184', '无持续风向', '28', '0.0', '0', '1017', '微风', '4', '06:20', '17:36', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期六', 'cc663d03-3cfd-439b-9203-bf0b99b43a85');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a13b03c5-1f9a-44c6-a461-dfdeb7bbde1a', '101', '100', '2015-10-15', '332', '无持续风向', '22', '0.0', '0', '1016', '微风', '11', '06:19', '17:38', '27', '11', '多云', '晴', 'images/duoyun.png', '10', '星期四', 'c0f4f0ee-dec1-4b38-81c6-09964fb630c5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a27ef626-93d7-4bfc-9fd7-4e1fe5c86181', '101', '100', '2015-10-15', '29', '北风', '27', '0.0', '0', '1016', '微风', '9', '06:19', '17:38', '26', '10', '多云', '晴', 'images/duoyun.png', '10', '星期四', 'd5eab9bd-91e4-40a9-a370-4beeaffeb3ae');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a2c73a50-e7f3-4844-8a6b-9d1feda77654', '100', '100', '2015-10-16', '192', '无持续风向', '25', '0.0', '0', '1017', '微风', '4', '06:19', '17:37', '27', '15', '晴', '晴', 'images/qing.png', '10', '星期五', '97d962ec-aeec-45d4-b9ca-191d3b12d0f1');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a352eb11-66c2-4279-8fbe-1c88871e72ad', '100', '100', '2015-09-27', '131', '无持续风向', '18', '0.0', '1', '1014', '微风', '4', '06:04', '18:03', '28', '18', '晴', '晴', 'images/qing.png', '10', '星期日', '30382c9c-8088-447a-b604-4267fe87461d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a4c7693a-a5ea-4c36-9378-227166c97072', '100', '100', '2015-10-19', '170', '无持续风向', '22', '0.0', '0', '1016', '微风', '9', '06:22', '17:33', '26', '15', '晴', '晴', 'images/qing.png', '10', '星期一', '7a856cb3-5b40-4a67-a5d0-a1230768bcfd');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a4fb4cf0-495d-4f58-b69d-f669b961bca8', '100', '100', '2015-10-13', '200', '南风', '17', '0.0', '0', '1021', '微风', '4', '06:17', '17:41', '29', '8', '晴', '晴', 'images/qing.png', '10', '星期二', 'cc663d03-3cfd-439b-9203-bf0b99b43a85');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a5524ffa-9130-4beb-a0b2-11042474e0cf', '100', '101', '2015-10-13', '199', '南风', '16', '0.0', '0', '1021', '微风', '7', '06:17', '17:41', '26', '11', '晴', '多云', 'images/qing.png', '10', '星期二', '6e2c44a5-9575-4379-bcea-1674f3175048');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a56b69db-5060-4917-aada-e29385f65cad', '101', '101', '2015-10-17', '186', '南风', '29', '0.0', '0', '1018', '微风', '7', '06:20', '17:36', '27', '12', '多云', '多云', 'images/duoyun.png', '10', '星期六', 'f377f019-0348-4d84-a93a-0ed0b2706a2d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a60546f1-a5c7-4382-a3f6-b6313291ee6f', '100', '100', '2015-10-17', '181', '无持续风向', '23', '0.0', '0', '1016', '微风', '11', '06:20', '17:36', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期六', '43201297-f8ee-4025-add1-d9eab2862d49');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a64b59b2-66e3-47ca-8a62-38b2e1d0e61f', '101', '101', '2015-10-19', '192', '无持续风向', '35', '0.0', '0', '1016', '微风', '7', '06:22', '17:33', '26', '15', '多云', '多云', 'images/duoyun.png', '10', '星期一', 'e3f1ac1d-f5b5-4e71-bbaf-5b935378b6cb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a694001b-a61b-4d3d-af6e-ee29f9f891a1', '101', '100', '2015-10-17', '182', '无持续风向', '29', '0.0', '0', '1017', '微风', '7', '06:20', '17:36', '24', '13', '多云', '晴', 'images/duoyun.png', '10', '星期六', '9004e631-74f7-4ba0-aa4c-3ab0d43eb8db');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a73f274b-bcb3-49d3-b879-0b68784d6968', '101', '101', '2015-09-21', '165', '南风', '36', '0.0', '0', '1011', '微风', '4', '05:59', '18:12', '29', '17', '多云', '多云', 'images/duoyun.png', '10', '星期一', 'e1c44614-22bb-40d8-9db2-ce03556fc806');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a76535e2-475b-472a-9eb8-4a5aafd0ed5b', '100', '100', '2015-10-18', '79', '无持续风向', '29', '0.0', '0', '1019', '微风', '4', '06:21', '17:34', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期日', 'b411b524-5a8d-4e25-ad6c-25aa9aa9bb04');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a8db2384-2a3a-4701-ad04-95bb14746cce', '100', '100', '2015-10-11', '325', '北风', '23', '0.0', '0', '1019', '微风', '14', '06:15', '17:44', '21', '7', '晴', '晴', 'images/qing.png', '10', '星期日', 'e4551817-b109-4ad0-9525-ad004d91d100');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('a9806b8e-eaa2-4f17-8c47-5f04207ef63b', '100', '101', '2015-10-13', '199', '南风', '16', '0.0', '0', '1021', '微风', '7', '06:17', '17:41', '26', '11', '晴', '多云', 'images/qing.png', '10', '星期二', 'e3f1ac1d-f5b5-4e71-bbaf-5b935378b6cb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('aa6547c9-e6c4-4d21-9d61-c63ddc3fb5a6', '100', '100', '2015-10-20', '83', '无持续风向', '31', '0.0', '0', '1014', '微风', '11', '06:23', '17:32', '27', '15', '晴', '晴', 'images/qing.png', '10', '星期二', 'c0f4f0ee-dec1-4b38-81c6-09964fb630c5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('aa848724-d85c-4914-bcaa-bf90a060d3f5', '100', '101', '2015-10-13', '199', '西南风', '19', '0.0', '0', '1022', '3-4', '11', '06:17', '17:41', '25', '10', '晴', '多云', 'images/qing.png', '10', '星期二', '37b62595-c596-47d2-aa85-affcacde22b5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('ac9b549d-be9d-47db-9669-5a340b940247', '100', '100', '2015-10-17', '182', '无持续风向', '29', '0.0', '0', '1017', '微风', '9', '06:20', '17:36', '27', '12', '晴', '晴', 'images/qing.png', '10', '星期六', '7a856cb3-5b40-4a67-a5d0-a1230768bcfd');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('acd06533-ab80-425c-b039-a5ef0ad92e2e', '100', '101', '2015-10-20', '167', '无持续风向', '33', '0.0', '0', '1015', '微风', '4', '06:23', '17:32', '26', '15', '晴', '多云', 'images/qing.png', '10', '星期二', '20a31c2b-fc46-4ac0-bee1-2207e3b226f9');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('af056059-f3b8-415a-82c1-9e7c1d25ee9b', '100', '100', '2015-09-27', '131', '无持续风向', '18', '0.0', '1', '1014', '微风', '4', '06:04', '18:03', '28', '18', '晴', '晴', 'images/qing.png', '10', '星期日', 'dc0dbb0c-0f76-4544-bd61-1a65c3ee80e5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('af1ea662-b059-4170-bc1d-bb58db204820', '100', '101', '2015-10-15', '195', '无持续风向', '23', '0.0', '0', '1018', '微风', '7', '06:19', '17:38', '26', '12', '晴', '多云', 'images/qing.png', '10', '星期四', '4d4e5d4a-097c-4b93-bf9c-7792cc9f75a2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('b153be3a-c0fb-41bb-b257-c54fb731d0a7', '101', '101', '2015-10-18', '58', '无持续风向', '28', '0.0', '0', '1020', '微风', '11', '06:21', '17:34', '26', '15', '多云', '多云', 'images/duoyun.png', '10', '星期日', 'c50b611e-6dc2-47df-9f06-d7c1a64d2931');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('b25cb723-c44c-40fc-baa4-66389b293604', '100', '100', '2015-10-12', '330', '南风', '19', '0.0', '0', '1023', '3-4', '7', '06:16', '17:42', '24', '10', '晴', '晴', 'images/qing.png', '10', '星期一', 'e2caa130-4580-436c-b647-aa5a2667fbde');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('b289b1eb-f557-460d-a8f0-bfbc10fde710', '101', '100', '2015-10-17', '182', '无持续风向', '32', '0.0', '0', '1017', '微风', '8', '06:20', '17:36', '24', '13', '多云', '晴', 'images/duoyun.png', '10', '星期六', '26c065b2-ed18-429c-877e-7ab41586af9d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('b2c0d538-c440-4fa4-b427-a8462f71077d', '100', '100', '2015-10-12', '3', '西南风', '20', '0.0', '0', '1023', '3-4', '11', '06:16', '17:42', '24', '8', '晴', '晴', 'images/qing.png', '10', '星期一', '37b62595-c596-47d2-aa85-affcacde22b5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('b2ce1110-b328-48ad-9b63-20031450026d', '101', '300', '2015-09-22', '164', '南风', '44', '1.5', '70', '1011', '微风', '4', '06:00', '18:11', '28', '18', '多云', '阵雨', 'images/duoyun.png', '10', '星期二', '30382c9c-8088-447a-b604-4267fe87461d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('b3233648-c50a-4dae-99b1-4a8ecd2cf052', '101', '101', '2015-10-18', '92', '无持续风向', '27', '0.0', '0', '1019', '微风', '4', '06:21', '17:34', '25', '14', '多云', '多云', 'images/duoyun.png', '10', '星期日', '6b291c64-06d5-4965-b535-488f8d011865');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('b3770607-f364-4da9-8795-b8d14e9b126d', '100', '100', '2015-10-15', '204', '无持续风向', '24', '0.0', '0', '1016', '微风', '14', '06:19', '17:38', '26', '12', '晴', '晴', 'images/qing.png', '10', '星期四', 'e4551817-b109-4ad0-9525-ad004d91d100');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('b3db34bb-d769-4ce5-ac01-f0536b0374b2', '101', '101', '2015-10-14', '193', '南风', '20', '0.0', '0', '1017', '微风', '7', '06:18', '17:40', '25', '11', '多云', '多云', 'images/duoyun.png', '10', '星期三', 'e3f1ac1d-f5b5-4e71-bbaf-5b935378b6cb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('b42319c4-9155-4fd7-b135-d826108cf9ac', '100', '101', '2015-10-13', '199', '南风', '19', '0.0', '0', '1022', '微风', '4', '06:17', '17:41', '26', '11', '晴', '多云', 'images/qing.png', '10', '星期二', '97d962ec-aeec-45d4-b9ca-191d3b12d0f1');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('b4853754-e6d0-46e5-b9ac-19f2c7f728fb', '100', '100', '2015-10-13', '211', '无持续风向', '19', '0.0', '0', '1023', '微风', '14', '06:17', '17:41', '25', '13', '晴', '晴', 'images/qing.png', '10', '星期二', 'e4551817-b109-4ad0-9525-ad004d91d100');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('b4a6c496-8951-4f7a-ba20-c688878b918e', '100', '100', '2015-10-19', '147', '无持续风向', '25', '0.0', '0', '1016', '微风', '4', '06:22', '17:33', '26', '15', '晴', '晴', 'images/qing.png', '10', '星期一', '978b086a-5382-4552-b95b-c759bc7619dc');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('b4bb4c34-9668-45f0-a20c-516a353b9505', '100', '100', '2015-10-18', '16', '无持续风向', '31', '0.0', '0', '1019', '微风', '4', '06:21', '17:34', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期日', '0d1be87e-2d44-44b6-ba0d-e920994f72b2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('b5c85fda-cebe-4d1b-b263-73103e76a199', '100', '100', '2015-10-15', '68', '北风', '28', '0.0', '0', '1016', '微风', '4', '06:19', '17:38', '26', '8', '晴', '晴', 'images/qing.png', '10', '星期四', '0eb20599-8c3a-477b-bd2c-1cf53883f82a');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('b8716832-c4b3-4fdb-96f1-4bf289d04ae0', '101', '100', '2015-10-13', '202', '无持续风向', '18', '0.0', '0', '1021', '微风', '4', '06:17', '17:41', '25', '10', '多云', '晴', 'images/duoyun.png', '10', '星期二', 'b411b524-5a8d-4e25-ad6c-25aa9aa9bb04');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('b97fa700-cc29-4cfb-b592-7490338cf3cd', '100', '101', '2015-09-26', '89', '无持续风向', '32', '0.0', '0', '1012', '微风', '4', '06:03', '18:05', '29', '18', '晴', '多云', 'images/qing.png', '10', '星期六', 'dc0dbb0c-0f76-4544-bd61-1a65c3ee80e5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('b9a4fec7-7878-426c-9f90-11a0b685a837', '101', '101', '2015-10-17', '180', '无持续风向', '31', '0.0', '0', '1017', '微风', '4', '06:20', '17:36', '27', '15', '多云', '多云', 'images/duoyun.png', '10', '星期六', '2792d049-e8e0-49a1-a988-4efc974f61be');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('b9d628aa-5542-4465-974e-c4eef2212a58', '100', '101', '2015-10-18', '69', '无持续风向', '27', '0.0', '0', '1019', '微风', '7', '06:21', '17:34', '23', '15', '晴', '多云', 'images/qing.png', '10', '星期日', '9004e631-74f7-4ba0-aa4c-3ab0d43eb8db');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('b9e93aa5-545a-4247-af37-c8b47fbc3754', '101', '101', '2015-10-18', '142', '无持续风向', '27', '0.0', '0', '1019', '微风', '15', '06:21', '17:34', '26', '15', '多云', '多云', 'images/duoyun.png', '10', '星期日', 'd6bdba54-752b-4f62-beb1-f02622b11619');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('ba93d413-eb1b-4801-970b-3c1bfb9c5455', '101', '101', '2015-09-21', '165', '南风', '36', '0.0', '0', '1011', '微风', '4', '05:59', '18:12', '29', '17', '多云', '多云', 'images/duoyun.png', '10', '星期一', 'dc0dbb0c-0f76-4544-bd61-1a65c3ee80e5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('baffaa41-0cd8-443e-9c05-c6093176dbc3', '100', '100', '2015-10-15', '177', '东南风', '26', '0.0', '0', '1016', '微风', '7', '06:19', '17:38', '26', '11', '晴', '晴', 'images/qing.png', '10', '星期四', 'e3f1ac1d-f5b5-4e71-bbaf-5b935378b6cb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('bb8a0d47-dc6d-4a47-98d2-dc022f4937d2', '101', '101', '2015-10-14', '194', '南风', '20', '0.0', '0', '1017', '微风', '8', '06:18', '17:40', '25', '11', '多云', '多云', 'images/duoyun.png', '10', '星期三', '26c065b2-ed18-429c-877e-7ab41586af9d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('bc5f7fad-9cc7-4699-9850-7226000dc43d', '100', '100', '2015-10-15', '68', '北风', '28', '0.0', '0', '1016', '微风', '4', '06:19', '17:38', '26', '8', '晴', '晴', 'images/qing.png', '10', '星期四', '8801860c-99dc-43b9-bee2-66686e2f9ff2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('bc7ddaae-4312-480d-87f4-12100311dc0e', '100', '100', '2015-10-16', '148', '无持续风向', '22', '0.0', '0', '1018', '微风', '4', '06:19', '17:37', '27', '12', '晴', '晴', 'images/qing.png', '10', '星期五', '20a31c2b-fc46-4ac0-bee1-2207e3b226f9');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('bcb1ac1b-4f93-4772-b21d-aa8b14823422', '101', '101', '2015-10-14', '196', '东南风', '21', '0.0', '0', '1018', '微风', '4', '06:18', '17:40', '25', '12', '多云', '多云', 'images/duoyun.png', '10', '星期三', 'a7a5d3f9-bff1-4f11-b54c-e3c05ddd3b9e');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('be65c0dc-16ea-449d-8811-b6308e7a84e7', '101', '101', '2015-09-23', '51', '南风', '41', '0.0', '0', '1009', '微风', '4', '06:01', '18:09', '29', '19', '多云', '多云', 'images/duoyun.png', '10', '星期三', '3dbc7e93-e5ca-4606-a44d-42f43d499e2a');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('be7ad8e9-e039-43bc-8d19-8a110d94b324', '100', '100', '2015-10-15', '208', '无持续风向', '23', '0.0', '0', '1017', '微风', '7', '06:19', '17:38', '27', '13', '晴', '晴', 'images/qing.png', '10', '星期四', 'bdcdbd4b-097a-4112-8a70-33bf9be5b1c0');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('beb21488-94c5-45f7-9a53-958a7c1a7b53', '100', '100', '2015-10-15', '51', '无持续风向', '22', '0.0', '0', '1016', '微风', '4', '06:19', '17:38', '27', '13', '晴', '晴', 'images/qing.png', '10', '星期四', 'ba8c15ba-41f4-40e9-abfb-a81a2dcb0324');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('bfe27085-9254-4571-9955-40e24de47dc8', '101', '101', '2015-10-18', '92', '无持续风向', '27', '0.0', '0', '1019', '微风', '4', '06:21', '17:34', '25', '14', '多云', '多云', 'images/duoyun.png', '10', '星期日', '978b086a-5382-4552-b95b-c759bc7619dc');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('bfe79aff-1850-4512-91e6-5569275ae5a9', '101', '101', '2015-10-17', '183', '无持续风向', '26', '0.0', '0', '1016', '微风', '4', '06:20', '17:36', '28', '14', '多云', '多云', 'images/duoyun.png', '10', '星期六', '71147a0f-6fba-4e15-a6e7-f29099056b55');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c01876c4-0395-4ffd-8205-a9ad3d752494', '100', '100', '2015-10-16', '168', '无持续风向', '29', '0.0', '0', '1018', '微风', '7', '06:19', '17:37', '27', '15', '晴', '晴', 'images/qing.png', '10', '星期五', '9004e631-74f7-4ba0-aa4c-3ab0d43eb8db');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c0fba438-60f7-402c-b698-bc3035d05b69', '100', '100', '2015-10-17', '191', '无持续风向', '28', '0.0', '0', '1018', '微风', '11', '06:20', '17:36', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期六', 'b5992b35-8896-4d2e-96b0-deee3f288109');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c1114662-85ac-4f77-be14-1136fdc52557', '101', '100', '2015-09-25', '55', '无持续风向', '38', '0.0', '19', '1009', '微风', '4', '06:02', '18:06', '28', '18', '多云', '晴', 'images/duoyun.png', '10', '星期五', 'beeca77c-46dd-42d8-915a-a5a33863bb61');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c1c2adc1-9492-4db4-9525-7a0a7c64266d', '100', '100', '2015-10-12', '5', '南风', '23', '0.0', '0', '1023', '微风', '14', '06:16', '17:42', '23', '8', '晴', '晴', 'images/qing.png', '10', '星期一', 'e4551817-b109-4ad0-9525-ad004d91d100');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c207fa65-e341-4719-a046-b8af1c0ac6e3', '100', '100', '2015-09-27', '131', '无持续风向', '18', '0.0', '1', '1014', '微风', '4', '06:04', '18:03', '28', '18', '晴', '晴', 'images/qing.png', '10', '星期日', 'beeca77c-46dd-42d8-915a-a5a33863bb61');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c2b878de-1d73-4cd5-a79c-88cb6615c020', '101', '101', '2015-10-14', '213', '无持续风向', '21', '0.0', '0', '1019', '微风', '7', '06:18', '17:40', '25', '13', '多云', '多云', 'images/duoyun.png', '10', '星期三', 'e2caa130-4580-436c-b647-aa5a2667fbde');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c3d4ac99-170c-473f-874c-ab05a5855b2b', '101', '100', '2015-10-14', '197', '无持续风向', '22', '0.0', '0', '1016', '微风', '11', '06:18', '17:40', '27', '11', '多云', '晴', 'images/duoyun.png', '10', '星期三', 'b5992b35-8896-4d2e-96b0-deee3f288109');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c453fe79-73e8-456d-99e3-aba87056bac4', '101', '100', '2015-10-14', '197', '无持续风向', '20', '0.0', '0', '1017', '微风', '4', '06:18', '17:40', '26', '12', '多云', '晴', 'images/duoyun.png', '10', '星期三', '71147a0f-6fba-4e15-a6e7-f29099056b55');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c45a1e74-9cc0-4d1d-8e33-81fb272bc1ba', '305', '104', '2015-09-24', '115', '无持续风向', '42', '0.0', '39', '1009', '微风', '4', '06:02', '18:08', '25', '20', '小雨', '阴', 'images/xiaoyu.png', '10', '星期四', 'e1c44614-22bb-40d8-9db2-ce03556fc806');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c5479eca-a371-4979-b34f-96c08172693b', '100', '101', '2015-10-20', '150', '无持续风向', '30', '0.0', '0', '1015', '微风', '4', '06:23', '17:32', '26', '15', '晴', '多云', 'images/qing.png', '10', '星期二', 'ccfc76c5-6669-4372-8007-0705a8a014f3');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c5d5473d-5bfd-4b0d-8f15-9a7db972c971', '101', '101', '2015-09-21', '165', '南风', '36', '0.0', '0', '1011', '微风', '4', '05:59', '18:12', '29', '17', '多云', '多云', 'images/duoyun.png', '10', '星期一', 'beeca77c-46dd-42d8-915a-a5a33863bb61');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c7088199-944c-48df-9c31-d22b3dcebad0', '100', '101', '2015-10-18', '69', '无持续风向', '27', '0.0', '0', '1019', '微风', '7', '06:21', '17:34', '23', '15', '晴', '多云', 'images/qing.png', '10', '星期日', '6e2c44a5-9575-4379-bcea-1674f3175048');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c70bf6f8-b7da-443b-ac0d-1cea7023477d', '101', '101', '2015-10-18', '92', '无持续风向', '27', '0.0', '0', '1019', '微风', '4', '06:21', '17:34', '25', '14', '多云', '多云', 'images/duoyun.png', '10', '星期日', '2792d049-e8e0-49a1-a988-4efc974f61be');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c737f91a-d91b-4ab8-807e-2f881a54b143', '101', '100', '2015-10-15', '331', '无持续风向', '21', '0.0', '0', '1016', '微风', '15', '06:19', '17:38', '27', '11', '多云', '晴', 'images/duoyun.png', '10', '星期四', 'd6bdba54-752b-4f62-beb1-f02622b11619');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c76808fc-99c8-4d15-b7a1-7f2a2ab4844e', '100', '100', '2015-10-19', '170', '无持续风向', '22', '0.0', '0', '1016', '微风', '9', '06:22', '17:33', '26', '15', '晴', '晴', 'images/qing.png', '10', '星期一', 'd5eab9bd-91e4-40a9-a370-4beeaffeb3ae');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c8766d84-12dc-47b7-b091-0940ddbb9363', '101', '101', '2015-10-17', '183', '无持续风向', '26', '0.0', '0', '1016', '微风', '4', '06:20', '17:36', '28', '14', '多云', '多云', 'images/duoyun.png', '10', '星期六', '8977b5b4-7db1-4131-a180-29f07fba47ab');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c905dc6f-f3e5-43b1-8c4e-b345e24a6a6d', '101', '100', '2015-10-15', '343', '无持续风向', '25', '0.0', '0', '1016', '微风', '4', '06:19', '17:38', '26', '12', '多云', '晴', 'images/duoyun.png', '10', '星期四', '6b291c64-06d5-4965-b535-488f8d011865');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c99b7ada-4bc7-4462-94d3-81c6e496c469', '101', '100', '2015-10-14', '194', '无持续风向', '22', '0.0', '0', '1016', '微风', '15', '06:18', '17:40', '27', '11', '多云', '晴', 'images/duoyun.png', '10', '星期三', 'd6bdba54-752b-4f62-beb1-f02622b11619');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('c9ee61b2-8813-410b-89fb-8fa00b393694', '101', '101', '2015-10-14', '193', '南风', '20', '0.0', '0', '1017', '微风', '7', '06:18', '17:40', '25', '11', '多云', '多云', 'images/duoyun.png', '10', '星期三', '9004e631-74f7-4ba0-aa4c-3ab0d43eb8db');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('ca6216b9-3b1b-4400-b708-25d7435fe1ff', '101', '101', '2015-10-19', '192', '无持续风向', '35', '0.0', '0', '1016', '微风', '7', '06:22', '17:33', '26', '15', '多云', '多云', 'images/duoyun.png', '10', '星期一', '6e2c44a5-9575-4379-bcea-1674f3175048');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('cb8d6e6f-62e0-4430-8e55-6c52960232f2', '101', '101', '2015-10-17', '169', '无持续风向', '31', '0.0', '0', '1016', '微风', '7', '06:20', '17:36', '28', '14', '多云', '多云', 'images/duoyun.png', '10', '星期六', '6300d30d-7c05-4709-b0c9-b870d90e00c7');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('cb9e11a2-4f62-4fc0-8a80-ffa5301f51c5', '100', '100', '2015-10-15', '177', '东南风', '26', '0.0', '0', '1016', '微风', '7', '06:19', '17:38', '26', '11', '晴', '晴', 'images/qing.png', '10', '星期四', '6e2c44a5-9575-4379-bcea-1674f3175048');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('cc0124c1-fcc3-427e-9891-819fa91d24f6', '100', '101', '2015-10-13', '199', '西南风', '19', '0.0', '0', '1022', '3-4', '11', '06:17', '17:41', '25', '10', '晴', '多云', 'images/qing.png', '10', '星期二', '040a0a72-be00-4f7a-909d-43b07e68eccb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('cd481bd3-4550-40dd-aef5-1b9bb76d11d6', '101', '101', '2015-09-23', '51', '南风', '41', '0.0', '0', '1009', '微风', '4', '06:01', '18:09', '29', '19', '多云', '多云', 'images/duoyun.png', '10', '星期三', 'beeca77c-46dd-42d8-915a-a5a33863bb61');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('cea1f32c-47de-44bc-999d-527108b519e8', '100', '101', '2015-10-13', '199', '南风', '19', '0.0', '0', '1022', '微风', '4', '06:17', '17:41', '26', '11', '晴', '多云', 'images/qing.png', '10', '星期二', 'a7a5d3f9-bff1-4f11-b54c-e3c05ddd3b9e');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('cf3bc3a9-bf8c-4547-8ccb-8ec3847fe3c5', '100', '100', '2015-10-16', '175', '无持续风向', '24', '0.0', '0', '1018', '微风', '7', '06:19', '17:37', '28', '15', '晴', '晴', 'images/qing.png', '10', '星期五', 'bdcdbd4b-097a-4112-8a70-33bf9be5b1c0');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('d18b8c83-0db1-4be2-81ca-4eeb5d5862ef', '100', '101', '2015-10-14', '191', '南风', '20', '0.0', '0', '1017', '微风', '4', '06:18', '17:40', '28', '12', '晴', '多云', 'images/qing.png', '10', '星期三', '3a73898d-f5b1-4228-9e4a-9205e09c6afb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('d3b2a661-70ea-40b0-928c-06252eb7f9ee', '101', '101', '2015-10-18', '58', '无持续风向', '28', '0.0', '0', '1020', '微风', '11', '06:21', '17:34', '26', '15', '多云', '多云', 'images/duoyun.png', '10', '星期日', 'b5992b35-8896-4d2e-96b0-deee3f288109');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('d4b1e3e6-bf51-41a3-ad81-ab612a500793', '101', '300', '2015-09-22', '164', '南风', '44', '1.5', '70', '1011', '微风', '4', '06:00', '18:11', '28', '18', '多云', '阵雨', 'images/duoyun.png', '10', '星期二', '8d23507d-9c63-4bac-bbfc-59914cc07a81');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('d4c948a3-0dfc-4dbc-9641-0b49f2fa8003', '100', '100', '2015-10-16', '175', '无持续风向', '30', '0.0', '0', '1018', '微风', '7', '06:19', '17:37', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期五', '4d4e5d4a-097c-4b93-bf9c-7792cc9f75a2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('d529c1ac-aa1c-4383-8879-f9794c7738f7', '100', '100', '2015-10-19', '151', '无持续风向', '36', '0.0', '0', '1017', '微风', '4', '06:22', '17:33', '26', '15', '晴', '晴', 'images/qing.png', '10', '星期一', '37b7ea55-bc41-4c39-973e-0f0b137239f6');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('d559e8c6-4076-481c-9def-fa5f50c2dfd0', '101', '100', '2015-10-15', '343', '无持续风向', '25', '0.0', '0', '1016', '微风', '4', '06:19', '17:38', '26', '12', '多云', '晴', 'images/duoyun.png', '10', '星期四', 'ccfc76c5-6669-4372-8007-0705a8a014f3');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('d5a87e57-66b9-44d6-9d47-0b9a7fc8218a', '100', '101', '2015-10-14', '191', '南风', '20', '0.0', '0', '1017', '微风', '4', '06:18', '17:40', '28', '12', '晴', '多云', 'images/qing.png', '10', '星期三', '2792d049-e8e0-49a1-a988-4efc974f61be');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('d5fd31bd-e0a6-4c3a-8a9d-ced7230f7c52', '305', '104', '2015-09-24', '115', '无持续风向', '42', '0.0', '39', '1009', '微风', '4', '06:02', '18:08', '25', '20', '小雨', '阴', 'images/xiaoyu.png', '10', '星期四', '3dbc7e93-e5ca-4606-a44d-42f43d499e2a');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('d6956a1e-ace2-40a5-9df7-25ee842baed4', '100', '100', '2015-10-10', '332', '北风', '20', '0.0', '0', '1018', '3-4', '14', '06:14', '17:45', '20', '6', '晴', '晴', 'images/qing.png', '10', '星期六', 'e4551817-b109-4ad0-9525-ad004d91d100');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('d77459a1-0c4b-4aa3-8e27-ed1ba1519a9d', '100', '101', '2015-10-13', '199', '南风', '19', '0.0', '0', '1022', '微风', '4', '06:17', '17:41', '26', '11', '晴', '多云', 'images/qing.png', '10', '星期二', '0378b4ff-3867-40ba-8364-5f65c0543063');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('d7f5f337-57e5-4a39-a71c-6075001b2763', '100', '100', '2015-10-13', '216', '南风', '18', '0.0', '0', '1022', '微风', '7', '06:17', '17:41', '25', '8', '晴', '晴', 'images/qing.png', '10', '星期二', '1ebbf1f6-ff2f-454a-ab1d-c8735cf1cfdf');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('d887c4b7-2617-47ff-b9be-cd191fcd7fa9', '100', '101', '2015-10-15', '202', '无持续风向', '25', '0.0', '0', '1016', '微风', '11', '06:19', '17:38', '26', '12', '晴', '多云', 'images/qing.png', '10', '星期四', '43201297-f8ee-4025-add1-d9eab2862d49');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('d9278390-92ef-47b4-84a6-76e161d7b21a', '101', '101', '2015-10-17', '180', '无持续风向', '31', '0.0', '0', '1017', '微风', '4', '06:20', '17:36', '27', '15', '多云', '多云', 'images/duoyun.png', '10', '星期六', '6b291c64-06d5-4965-b535-488f8d011865');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('da58e2dd-e6db-4b8d-ab2e-af4af1fccf09', '100', '100', '2015-10-15', '177', '东南风', '26', '0.0', '0', '1016', '微风', '7', '06:19', '17:38', '26', '11', '晴', '晴', 'images/qing.png', '10', '星期四', '9004e631-74f7-4ba0-aa4c-3ab0d43eb8db');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('da60479c-1284-476b-855a-a310f56af337', '100', '101', '2015-10-18', '123', '无持续风向', '31', '0.0', '0', '1018', '微风', '4', '06:21', '17:34', '23', '15', '晴', '多云', 'images/qing.png', '10', '星期日', 'a7a5d3f9-bff1-4f11-b54c-e3c05ddd3b9e');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('da810e99-3002-491d-9d49-f0829113bfcb', '101', '100', '2015-10-17', '178', '无持续风向', '26', '0.0', '0', '1016', '微风', '4', '06:20', '17:36', '24', '13', '多云', '晴', 'images/duoyun.png', '10', '星期六', '0378b4ff-3867-40ba-8364-5f65c0543063');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('dc82dc77-da53-4f0a-a240-5f58a2631de2', '101', '101', '2015-10-18', '92', '无持续风向', '27', '0.0', '0', '1019', '微风', '4', '06:21', '17:34', '25', '14', '多云', '多云', 'images/duoyun.png', '10', '星期日', 'd6be07ca-b1e5-4509-b1d5-623717739ac9');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('dc89c13f-1b56-48b2-a388-cb9899ed284b', '101', '100', '2015-10-14', '203', '无持续风向', '18', '0.0', '0', '1019', '微风', '7', '06:18', '17:40', '26', '14', '多云', '晴', 'images/duoyun.png', '10', '星期三', '1ebbf1f6-ff2f-454a-ab1d-c8735cf1cfdf');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('dcfc8f17-f990-4d06-9564-e958dfaa41af', '100', '100', '2015-10-16', '173', '无持续风向', '23', '0.0', '0', '1017', '微风', '4', '06:19', '17:37', '28', '15', '晴', '晴', 'images/qing.png', '10', '星期五', '8977b5b4-7db1-4131-a180-29f07fba47ab');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('dd5a8d15-21eb-4336-96ab-25963c6452c6', '100', '100', '2015-10-19', '165', '无持续风向', '35', '0.0', '0', '1017', '微风', '11', '06:22', '17:33', '26', '15', '晴', '晴', 'images/qing.png', '10', '星期一', 'c0f4f0ee-dec1-4b38-81c6-09964fb630c5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('dd7330a9-2922-4ceb-ae27-57835bbc696b', '101', '100', '2015-10-15', '332', '无持续风向', '22', '0.0', '0', '1016', '微风', '11', '06:19', '17:38', '27', '11', '多云', '晴', 'images/duoyun.png', '10', '星期四', '4ccc3e88-7bd3-487b-9988-7a74fa271fb0');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('ddae4ea0-0c14-4ed7-8db4-cc35a8af2b40', '101', '100', '2015-10-15', '343', '无持续风向', '25', '0.0', '0', '1016', '微风', '4', '06:19', '17:38', '26', '12', '多云', '晴', 'images/duoyun.png', '10', '星期四', '3a73898d-f5b1-4228-9e4a-9205e09c6afb');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('de1aa6d0-c570-43e6-9530-3434cd65829b', '101', '100', '2015-10-14', '197', '无持续风向', '22', '0.0', '0', '1016', '微风', '11', '06:18', '17:40', '27', '11', '多云', '晴', 'images/duoyun.png', '10', '星期三', 'c0f4f0ee-dec1-4b38-81c6-09964fb630c5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('de93e824-c401-46ea-8f91-92a13dde53c7', '100', '100', '2015-10-15', '203', '无持续风向', '25', '0.0', '0', '1017', '微风', '4', '06:19', '17:38', '26', '13', '晴', '晴', 'images/qing.png', '10', '星期四', '97d962ec-aeec-45d4-b9ca-191d3b12d0f1');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('ded9e570-4b4f-430b-997f-8272e8cabb5e', '101', '100', '2015-10-12', '331', '无持续风向', '18', '0.0', '0', '1022', '微风', '4', '06:16', '17:42', '24', '7', '多云', '晴', 'images/duoyun.png', '10', '星期一', 'ba8c15ba-41f4-40e9-abfb-a81a2dcb0324');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('dfed000b-f0f7-4fe9-8fb4-cb26fbd19263', '100', '100', '2015-10-12', '355', '南风', '19', '0.0', '0', '1022', '微风', '7', '06:16', '17:42', '24', '6', '晴', '晴', 'images/qing.png', '10', '星期一', 'bdcdbd4b-097a-4112-8a70-33bf9be5b1c0');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('e117217e-c371-452f-ab9c-93c339f48598', '101', '300', '2015-09-22', '164', '南风', '44', '1.5', '70', '1011', '微风', '4', '06:00', '18:11', '28', '18', '多云', '阵雨', 'images/duoyun.png', '10', '星期二', '3dbc7e93-e5ca-4606-a44d-42f43d499e2a');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('e248e857-026a-459c-908c-7ec8edf6ad48', '101', '100', '2015-09-25', '55', '无持续风向', '38', '0.0', '19', '1009', '微风', '4', '06:02', '18:06', '28', '18', '多云', '晴', 'images/duoyun.png', '10', '星期五', 'e1c44614-22bb-40d8-9db2-ce03556fc806');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('e2bdfe10-7d83-4cdc-9b25-1dd2ed4a75f8', '101', '101', '2015-10-17', '180', '无持续风向', '31', '0.0', '0', '1017', '微风', '4', '06:20', '17:36', '27', '15', '多云', '多云', 'images/duoyun.png', '10', '星期六', 'd6be07ca-b1e5-4509-b1d5-623717739ac9');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('e423caf9-ff50-4341-96b9-c4316e058016', '305', '104', '2015-09-24', '115', '无持续风向', '42', '0.0', '39', '1009', '微风', '4', '06:02', '18:08', '25', '20', '小雨', '阴', 'images/xiaoyu.png', '10', '星期四', '05a3b4ac-1fd3-47e3-a9fa-9e4f66cbaf15');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('e4b010e9-6d56-494f-9b56-55561c9f01f8', '101', '100', '2015-10-14', '203', '无持续风向', '18', '0.0', '0', '1019', '微风', '7', '06:18', '17:40', '26', '14', '多云', '晴', 'images/duoyun.png', '10', '星期三', 'bdcdbd4b-097a-4112-8a70-33bf9be5b1c0');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('e568fcc0-3fbd-4e07-a852-d6d5e8cfc34e', '101', '101', '2015-09-21', '165', '南风', '36', '0.0', '0', '1011', '微风', '4', '05:59', '18:12', '29', '17', '多云', '多云', 'images/duoyun.png', '10', '星期一', '8d23507d-9c63-4bac-bbfc-59914cc07a81');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('e5819ff8-9bbc-4f34-bf42-4039e3c2e4a3', '101', '100', '2015-10-14', '197', '无持续风向', '22', '0.0', '0', '1016', '微风', '11', '06:18', '17:40', '27', '11', '多云', '晴', 'images/duoyun.png', '10', '星期三', '4ccc3e88-7bd3-487b-9988-7a74fa271fb0');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('e5e2652d-db56-4077-b1bd-13e8a71b5e18', '101', '101', '2015-09-21', '165', '南风', '36', '0.0', '0', '1011', '微风', '4', '05:59', '18:12', '29', '17', '多云', '多云', 'images/duoyun.png', '10', '星期一', '30382c9c-8088-447a-b604-4267fe87461d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('e63290d4-6e96-47d1-a968-fbe2551f9fad', '100', '100', '2015-10-16', '171', '无持续风向', '25', '0.0', '0', '1018', '微风', '4', '06:19', '17:37', '28', '14', '晴', '晴', 'images/qing.png', '10', '星期五', '8801860c-99dc-43b9-bee2-66686e2f9ff2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('e926f86c-73f0-45a7-8fbf-037992293180', '101', '100', '2015-10-15', '29', '北风', '28', '0.0', '0', '1016', '微风', '4', '06:19', '17:38', '26', '10', '多云', '晴', 'images/duoyun.png', '10', '星期四', '8dd107b6-6144-4450-b42f-2dbd104b433b');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('ea080dd4-583d-47c7-971b-c7959cef14f7', '101', '100', '2015-10-15', '29', '北风', '28', '0.0', '0', '1016', '微风', '4', '06:19', '17:38', '26', '10', '多云', '晴', 'images/duoyun.png', '10', '星期四', '20a31c2b-fc46-4ac0-bee1-2207e3b226f9');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('ea3087be-08a4-43f2-b802-31e4cb63b2da', '100', '101', '2015-10-20', '167', '无持续风向', '33', '0.0', '0', '1015', '微风', '4', '06:23', '17:32', '26', '15', '晴', '多云', 'images/qing.png', '10', '星期二', '37b7ea55-bc41-4c39-973e-0f0b137239f6');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('ebc2e54b-6a39-4569-bc94-9ce78375d1fc', '305', '104', '2015-09-24', '115', '无持续风向', '42', '0.0', '39', '1009', '微风', '4', '06:02', '18:08', '25', '20', '小雨', '阴', 'images/xiaoyu.png', '10', '星期四', '8d23507d-9c63-4bac-bbfc-59914cc07a81');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('ec9a3bf0-e638-4d06-a481-cb8e84f73425', '101', '100', '2015-09-25', '55', '无持续风向', '38', '0.0', '19', '1009', '微风', '4', '06:02', '18:06', '28', '18', '多云', '晴', 'images/duoyun.png', '10', '星期五', '3dbc7e93-e5ca-4606-a44d-42f43d499e2a');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('ed130935-cf70-47fe-8825-6582b3963e68', '100', '100', '2015-10-19', '282', '无持续风向', '31', '0.0', '0', '1017', '微风', '7', '06:22', '17:33', '25', '14', '晴', '晴', 'images/qing.png', '10', '星期一', '4a3b6c50-4402-43b6-b0e8-ab10128c275e');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('ed506464-619d-468e-949f-fbf88f289680', '100', '100', '2015-09-27', '131', '无持续风向', '18', '0.0', '1', '1014', '微风', '4', '06:04', '18:03', '28', '18', '晴', '晴', 'images/qing.png', '10', '星期日', '3dbc7e93-e5ca-4606-a44d-42f43d499e2a');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('edb36cb3-5249-45bf-b3fb-9bb416c571c0', '101', '101', '2015-10-18', '42', '无持续风向', '30', '0.0', '0', '1019', '微风', '4', '06:21', '17:34', '26', '15', '多云', '多云', 'images/duoyun.png', '10', '星期日', '8801860c-99dc-43b9-bee2-66686e2f9ff2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('edc8efe4-09bb-41a9-bc7e-3e428c7adb28', '100', '101', '2015-09-26', '89', '无持续风向', '32', '0.0', '0', '1012', '微风', '4', '06:03', '18:05', '29', '18', '晴', '多云', 'images/qing.png', '10', '星期六', '30382c9c-8088-447a-b604-4267fe87461d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('edccb61e-2a51-4004-b6cf-1f6fb13f4152', '100', '100', '2015-10-17', '181', '无持续风向', '23', '0.0', '0', '1016', '微风', '11', '06:20', '17:36', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期六', '69f9f736-24bf-499a-bafb-418e3a87a0ec');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('ee44412c-214e-4c1f-b1f8-15b563b97aba', '101', '101', '2015-10-14', '195', '南风', '20', '0.0', '0', '1017', '微风', '4', '06:18', '17:40', '27', '10', '多云', '多云', 'images/duoyun.png', '10', '星期三', '8801860c-99dc-43b9-bee2-66686e2f9ff2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('eefabe6f-69c3-43c9-9e9a-7e24628442b2', '100', '100', '2015-10-16', '192', '无持续风向', '24', '0.0', '0', '1017', '微风', '11', '06:19', '17:37', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期五', '69f9f736-24bf-499a-bafb-418e3a87a0ec');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('ef767982-3004-488e-8f0f-0ee20a6c27cc', '100', '100', '2015-10-19', '243', '无持续风向', '38', '0.0', '0', '1016', '微风', '4', '06:22', '17:33', '26', '15', '晴', '晴', 'images/qing.png', '10', '星期一', 'cc663d03-3cfd-439b-9203-bf0b99b43a85');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('f08df5a4-837e-4c9f-b830-773cdeb8a840', '101', '101', '2015-10-18', '42', '无持续风向', '30', '0.0', '0', '1019', '微风', '4', '06:21', '17:34', '26', '15', '多云', '多云', 'images/duoyun.png', '10', '星期日', 'cc663d03-3cfd-439b-9203-bf0b99b43a85');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('f15bf5b9-9112-4e1a-8caf-fa9aaa9f705f', '101', '100', '2015-10-21', '147', '无持续风向', '28', '0.0', '0', '1017', '微风', '7', '06:24', '17:31', '24', '15', '多云', '晴', 'images/duoyun.png', '10', '星期三', '4a3b6c50-4402-43b6-b0e8-ab10128c275e');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('f2398e15-019f-43c4-95ca-08ae017a3486', '100', '100', '2015-10-13', '200', '南风', '17', '0.0', '0', '1021', '微风', '4', '06:17', '17:41', '29', '8', '晴', '晴', 'images/qing.png', '10', '星期二', '0eb20599-8c3a-477b-bd2c-1cf53883f82a');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('f6054c96-32fc-4477-9b3e-ad68f85be3a8', '100', '100', '2015-10-20', '83', '无持续风向', '31', '0.0', '0', '1014', '微风', '11', '06:23', '17:32', '27', '15', '晴', '晴', 'images/qing.png', '10', '星期二', '4ccc3e88-7bd3-487b-9988-7a74fa271fb0');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('f734f0ab-70ae-4f20-997d-f3cca65e3eea', '100', '100', '2015-10-19', '165', '无持续风向', '35', '0.0', '0', '1017', '微风', '11', '06:22', '17:33', '26', '15', '晴', '晴', 'images/qing.png', '10', '星期一', '4ccc3e88-7bd3-487b-9988-7a74fa271fb0');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('f743732c-c438-4b54-b9da-ca9d1b54df52', '100', '100', '2015-10-16', '192', '无持续风向', '25', '0.0', '0', '1017', '微风', '4', '06:19', '17:37', '27', '15', '晴', '晴', 'images/qing.png', '10', '星期五', 'a7a5d3f9-bff1-4f11-b54c-e3c05ddd3b9e');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('f7529140-f5bb-4fce-b5cd-cd543eebea09', '100', '100', '2015-10-15', '203', '无持续风向', '25', '0.0', '0', '1017', '微风', '4', '06:19', '17:38', '26', '13', '晴', '晴', 'images/qing.png', '10', '星期四', 'a7a5d3f9-bff1-4f11-b54c-e3c05ddd3b9e');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('f8b2083f-6eff-4ebf-9dcb-2128e65e8a6c', '101', '100', '2015-10-12', '331', '无持续风向', '18', '0.0', '0', '1022', '微风', '4', '06:16', '17:42', '24', '7', '多云', '晴', 'images/duoyun.png', '10', '星期一', '8977b5b4-7db1-4131-a180-29f07fba47ab');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('f8e974c8-206d-4a2c-bd24-bb8934c26e49', '100', '100', '2015-10-16', '148', '无持续风向', '22', '0.0', '0', '1018', '微风', '4', '06:19', '17:37', '27', '12', '晴', '晴', 'images/qing.png', '10', '星期五', '37b7ea55-bc41-4c39-973e-0f0b137239f6');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('fa2b9da4-afcb-4b54-90e7-2c910a658934', '100', '100', '2015-10-16', '192', '无持续风向', '24', '0.0', '0', '1017', '微风', '11', '06:19', '17:37', '27', '14', '晴', '晴', 'images/qing.png', '10', '星期五', '43201297-f8ee-4025-add1-d9eab2862d49');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('fad6f006-597d-4f0c-82f2-906b2f144784', '100', '100', '2015-10-20', '95', '无持续风向', '35', '0.0', '0', '1016', '微风', '7', '06:23', '17:32', '25', '15', '晴', '晴', 'images/qing.png', '10', '星期二', 'f377f019-0348-4d84-a93a-0ed0b2706a2d');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('fade250f-9f20-4a3a-8430-c4c8414adfad', '101', '300', '2015-09-22', '164', '南风', '44', '1.5', '70', '1011', '微风', '4', '06:00', '18:11', '28', '18', '多云', '阵雨', 'images/duoyun.png', '10', '星期二', 'dc0dbb0c-0f76-4544-bd61-1a65c3ee80e5');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('fbd6873b-417a-4cc2-9dd1-6c56e3f389d9', '100', '100', '2015-10-17', '182', '无持续风向', '29', '0.0', '0', '1017', '微风', '4', '06:20', '17:36', '27', '12', '晴', '晴', 'images/qing.png', '10', '星期六', '37b7ea55-bc41-4c39-973e-0f0b137239f6');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('fd3b29ec-ad9b-4fe0-89ab-dee2eb8df4a0', '101', '100', '2015-10-13', '220', '无持续风向', '16', '0.0', '0', '1021', '微风', '7', '06:17', '17:41', '25', '11', '多云', '晴', 'images/duoyun.png', '10', '星期二', '4d4e5d4a-097c-4b93-bf9c-7792cc9f75a2');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('ff93d828-1634-400c-85e5-dedc4f5d0a7f', '100', '100', '2015-09-27', '131', '无持续风向', '18', '0.0', '1', '1014', '微风', '4', '06:04', '18:03', '28', '18', '晴', '晴', 'images/qing.png', '10', '星期日', 'c3278f94-6e0f-4ead-a27e-374c09465276');
INSERT INTO `we_sys_dayweatherinfo` VALUES ('fffb6106-d6ad-4302-b2f2-378cd7009cd7', '100', '100', '2015-10-12', '4', '南风', '19', '0.0', '0', '1022', '微风', '4', '06:16', '17:42', '25', '6', '晴', '晴', 'images/qing.png', '10', '星期一', '0378b4ff-3867-40ba-8364-5f65c0543063');

-- ----------------------------
-- Table structure for `we_sys_weather`
-- ----------------------------
DROP TABLE IF EXISTS `we_sys_weather`;
CREATE TABLE `we_sys_weather` (
  `ID` varchar(36) NOT NULL,
  `CITY` varchar(255) DEFAULT NULL,
  `CITYID` varchar(255) DEFAULT NULL,
  `CNTY` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `UPDATELOC` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of we_sys_weather
-- ----------------------------
INSERT INTO `we_sys_weather` VALUES ('0378b4ff-3867-40ba-8364-5f65c0543063', '兖州', 'CN101120705', '中国', 'ok', '2015-10-12 19:49');
INSERT INTO `we_sys_weather` VALUES ('040a0a72-be00-4f7a-909d-43b07e68eccb', '兖州', 'CN101120705', '中国', 'ok', '2015-10-11 11:49');
INSERT INTO `we_sys_weather` VALUES ('05a3b4ac-1fd3-47e3-a9fa-9e4f66cbaf15', '兖州', 'CN101120705', '中国', 'ok', '2015-09-21 10:49');
INSERT INTO `we_sys_weather` VALUES ('0d1be87e-2d44-44b6-ba0d-e920994f72b2', '兖州', 'CN101120705', '中国', 'ok', '2015-10-12 17:49');
INSERT INTO `we_sys_weather` VALUES ('0eb20599-8c3a-477b-bd2c-1cf53883f82a', '兖州', 'CN101120705', '中国', 'ok', '2015-10-13 21:50');
INSERT INTO `we_sys_weather` VALUES ('1ebbf1f6-ff2f-454a-ab1d-c8735cf1cfdf', '兖州', 'CN101120705', '中国', 'ok', '2015-10-11 23:50');
INSERT INTO `we_sys_weather` VALUES ('20a31c2b-fc46-4ac0-bee1-2207e3b226f9', '兖州', 'CN101120705', '中国', 'ok', '2015-10-15 17:57');
INSERT INTO `we_sys_weather` VALUES ('26c065b2-ed18-429c-877e-7ab41586af9d', '兖州', 'CN101120705', '中国', 'ok', '2015-10-13 09:49');
INSERT INTO `we_sys_weather` VALUES ('2792d049-e8e0-49a1-a988-4efc974f61be', '兖州', 'CN101120705', '中国', 'ok', '2015-10-14 21:53');
INSERT INTO `we_sys_weather` VALUES ('30382c9c-8088-447a-b604-4267fe87461d', '兖州', 'CN101120705', '中国', 'ok', '2015-09-21 10:49');
INSERT INTO `we_sys_weather` VALUES ('37b62595-c596-47d2-aa85-affcacde22b5', '兖州', 'CN101120705', '中国', 'ok', '2015-10-11 13:50');
INSERT INTO `we_sys_weather` VALUES ('37b7ea55-bc41-4c39-973e-0f0b137239f6', '兖州', 'CN101120705', '中国', 'ok', '2015-10-15 15:50');
INSERT INTO `we_sys_weather` VALUES ('3a73898d-f5b1-4228-9e4a-9205e09c6afb', '兖州', 'CN101120705', '中国', 'ok', '2015-10-15 05:49');
INSERT INTO `we_sys_weather` VALUES ('3dbc7e93-e5ca-4606-a44d-42f43d499e2a', '兖州', 'CN101120705', '中国', 'ok', '2015-09-21 10:49');
INSERT INTO `we_sys_weather` VALUES ('43201297-f8ee-4025-add1-d9eab2862d49', '兖州', 'CN101120705', '中国', 'ok', '2015-10-11 15:52');
INSERT INTO `we_sys_weather` VALUES ('4a3b6c50-4402-43b6-b0e8-ab10128c275e', '兖州', 'CN101120705', '中国', 'ok', '2015-10-15 21:54');
INSERT INTO `we_sys_weather` VALUES ('4ccc3e88-7bd3-487b-9988-7a74fa271fb0', '兖州', 'CN101120705', '中国', 'ok', '2015-10-14 15:49');
INSERT INTO `we_sys_weather` VALUES ('4d4e5d4a-097c-4b93-bf9c-7792cc9f75a2', '兖州', 'CN101120705', '中国', 'ok', '2015-10-10 21:49');
INSERT INTO `we_sys_weather` VALUES ('6300d30d-7c05-4709-b0c9-b870d90e00c7', '兖州', 'CN101120705', '中国', 'ok', '2015-10-11 20:49');
INSERT INTO `we_sys_weather` VALUES ('69f9f736-24bf-499a-bafb-418e3a87a0ec', '兖州', 'CN101120705', '中国', 'ok', '2015-10-11 17:49');
INSERT INTO `we_sys_weather` VALUES ('6b291c64-06d5-4965-b535-488f8d011865', '兖州', 'CN101120705', '中国', 'ok', '2015-10-14 19:53');
INSERT INTO `we_sys_weather` VALUES ('6e2c44a5-9575-4379-bcea-1674f3175048', '兖州', 'CN101120705', '中国', 'ok', '2015-10-13 15:50');
INSERT INTO `we_sys_weather` VALUES ('71147a0f-6fba-4e15-a6e7-f29099056b55', '兖州', 'CN101120705', '中国', 'ok', '2015-10-12 15:49');
INSERT INTO `we_sys_weather` VALUES ('7a856cb3-5b40-4a67-a5d0-a1230768bcfd', '兖州', 'CN101120705', '中国', 'ok', '2015-10-15 09:49');
INSERT INTO `we_sys_weather` VALUES ('8801860c-99dc-43b9-bee2-66686e2f9ff2', '兖州', 'CN101120705', '中国', 'ok', '2015-10-13 23:51');
INSERT INTO `we_sys_weather` VALUES ('8977b5b4-7db1-4131-a180-29f07fba47ab', '兖州', 'CN101120705', '中国', 'ok', '2015-10-12 13:49');
INSERT INTO `we_sys_weather` VALUES ('8d23507d-9c63-4bac-bbfc-59914cc07a81', '兖州', 'CN101120705', '中国', 'ok', '2015-09-21 10:49');
INSERT INTO `we_sys_weather` VALUES ('8dd107b6-6144-4450-b42f-2dbd104b433b', '兖州', 'CN101120705', '中国', 'ok', '2015-10-15 13:49');
INSERT INTO `we_sys_weather` VALUES ('9004e631-74f7-4ba0-aa4c-3ab0d43eb8db', '兖州', 'CN101120705', '中国', 'ok', '2015-10-13 11:49');
INSERT INTO `we_sys_weather` VALUES ('978b086a-5382-4552-b95b-c759bc7619dc', '兖州', 'CN101120705', '中国', 'ok', '2015-10-15 03:49');
INSERT INTO `we_sys_weather` VALUES ('97d962ec-aeec-45d4-b9ca-191d3b12d0f1', '兖州', 'CN101120705', '中国', 'ok', '2015-10-12 21:52');
INSERT INTO `we_sys_weather` VALUES ('a7a5d3f9-bff1-4f11-b54c-e3c05ddd3b9e', '兖州', 'CN101120705', '中国', 'ok', '2015-10-12 23:50');
INSERT INTO `we_sys_weather` VALUES ('b411b524-5a8d-4e25-ad6c-25aa9aa9bb04', '兖州', 'CN101120705', '中国', 'ok', '2015-10-12 09:49');
INSERT INTO `we_sys_weather` VALUES ('b5992b35-8896-4d2e-96b0-deee3f288109', '兖州', 'CN101120705', '中国', 'ok', '2015-10-14 13:49');
INSERT INTO `we_sys_weather` VALUES ('ba8c15ba-41f4-40e9-abfb-a81a2dcb0324', '兖州', 'CN101120705', '中国', 'ok', '2015-10-12 11:49');
INSERT INTO `we_sys_weather` VALUES ('bdcdbd4b-097a-4112-8a70-33bf9be5b1c0', '兖州', 'CN101120705', '中国', 'ok', '2015-10-11 19:49');
INSERT INTO `we_sys_weather` VALUES ('beeca77c-46dd-42d8-915a-a5a33863bb61', '兖州', 'CN101120705', '中国', 'ok', '2015-09-21 10:49');
INSERT INTO `we_sys_weather` VALUES ('c0f4f0ee-dec1-4b38-81c6-09964fb630c5', '兖州', 'CN101120705', '中国', 'ok', '2015-10-14 11:49');
INSERT INTO `we_sys_weather` VALUES ('c3278f94-6e0f-4ead-a27e-374c09465276', '兖州', 'CN101120705', '中国', 'ok', '2015-09-21 10:49');
INSERT INTO `we_sys_weather` VALUES ('c50b611e-6dc2-47df-9f06-d7c1a64d2931', '兖州', 'CN101120705', '中国', 'ok', '2015-10-14 17:50');
INSERT INTO `we_sys_weather` VALUES ('cc663d03-3cfd-439b-9203-bf0b99b43a85', '兖州', 'CN101120705', '中国', 'ok', '2015-10-13 19:51');
INSERT INTO `we_sys_weather` VALUES ('ccfc76c5-6669-4372-8007-0705a8a014f3', '兖州', 'CN101120705', '中国', 'ok', '2015-10-14 23:52');
INSERT INTO `we_sys_weather` VALUES ('d5eab9bd-91e4-40a9-a370-4beeaffeb3ae', '兖州', 'CN101120705', '中国', 'ok', '2015-10-15 10:49');
INSERT INTO `we_sys_weather` VALUES ('d6bdba54-752b-4f62-beb1-f02622b11619', '兖州', 'CN101120705', '中国', 'ok', '2015-10-14 09:50');
INSERT INTO `we_sys_weather` VALUES ('d6be07ca-b1e5-4509-b1d5-623717739ac9', '兖州', 'CN101120705', '中国', 'ok', '2015-10-15 07:49');
INSERT INTO `we_sys_weather` VALUES ('dc0dbb0c-0f76-4544-bd61-1a65c3ee80e5', '兖州', 'CN101120705', '中国', 'ok', '2015-09-21 10:49');
INSERT INTO `we_sys_weather` VALUES ('dfd10274-dc82-4f34-bbd7-8fd758890ae5', '兖州', 'CN101120705', '中国', 'ok', '2015-10-13 17:50');
INSERT INTO `we_sys_weather` VALUES ('e1c44614-22bb-40d8-9db2-ce03556fc806', '兖州', 'CN101120705', '中国', 'ok', '2015-09-21 10:49');
INSERT INTO `we_sys_weather` VALUES ('e2caa130-4580-436c-b647-aa5a2667fbde', '兖州', 'CN101120705', '中国', 'ok', '2015-10-10 23:49');
INSERT INTO `we_sys_weather` VALUES ('e3f1ac1d-f5b5-4e71-bbaf-5b935378b6cb', '兖州', 'CN101120705', '中国', 'ok', '2015-10-13 13:49');
INSERT INTO `we_sys_weather` VALUES ('e4551817-b109-4ad0-9525-ad004d91d100', '兖州', 'CN101120705', '中国', 'ok', '2015-10-10 15:49');
INSERT INTO `we_sys_weather` VALUES ('f377f019-0348-4d84-a93a-0ed0b2706a2d', '兖州', 'CN101120705', '中国', 'ok', '2015-10-15 19:51');
